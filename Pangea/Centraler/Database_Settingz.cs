﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Centraler
{
    public class Database_Settingz
    {

        //
        // DATABASE SETTINGS
        //

        public enum enum_Database_Running_Version
        {
            Staging,
            Production
        }



        public static enum_Database_Running_Version Current_Database_Running_Version = enum_Database_Running_Version.Staging;

        //
        // Kevin Local
        //
        //public static string svc_enroll_conn_LOCAL = "Data Source=DESKTOP-ATES7MG\\SQLEXPRESS;Initial Catalog={0};Integrated Security=True";
        //public static string svc_admin_conn_LOCAL = "Data Source=DESKTOP-ATES7MG\\SQLEXPRESS;Initial Catalog={0};Integrated Security=True";


        /*
        // 
        // Chris Local
        //
        
        public static string Data_Source = "Cerberus\\porto,1433";

        public static string svc_enroll_conn_LOCAL_user_name = "svc_pan_enroll_FA";

        public static string svc_enroll_conn_LOCAL_password = "goose22";

        public static string svc_admin_conn_LOCAL_user_name = "svc_pan_admin_FA";

        public static string svc_admin_conn_LOCAL_password = "gander33";

        public static string DB_Staging = "Pangea-FA-Raw-v-2020-10-06--TRUNCATED";

        public static string DB_Production = "Pangea-FA-Raw-v-2020-10-06--TRUNCATED";
        */


        //
        // FEED DB SETTINGS
        //      
        public static string Data_Source = "localhost";

        public static string svc_enroll_conn_LOCAL_user_name = "sa";

        public static string svc_enroll_conn_LOCAL_password = "root";

        public static string svc_admin_conn_LOCAL_user_name = "sa";
        
        public static string svc_admin_conn_LOCAL_password = "root";
        
        public static string DB_Staging = "PANGEA_STAGING";

        public static string DB_Production = "PANGEA_PROD"; 

        public static string svc_enroll_conn_LOCAL = "Data Source=" + Data_Source + ";Initial Catalog=PANGEA_STAGING;Integrated Security=True;";

        public static string svc_admin_conn_LOCAL = "Data Source=" + Data_Source + ";Initial Catalog=PANGEA_STAGING;Integrated Security=True;";



        //
        // APP BUILD SETTING
        // 
        public static bool Is_Test_Version =
            (Current_Database_Running_Version == enum_Database_Running_Version.Staging);


        public static string Currently_Used_DB
        {
            get
            {

                switch (Current_Database_Running_Version)
                {
                    case enum_Database_Running_Version.Production:

                        return DB_Production;

                    case enum_Database_Running_Version.Staging:
                    default:

                        return DB_Staging;
                }

            }
        }

        public static string Currently_Used_DB_Conn_String_svc_enroll_conn_LOCAL
        {
            get
            {
                string the_db = Currently_Used_DB;

                string stemp = string.Format(svc_enroll_conn_LOCAL, the_db);

                return stemp;
            }
        }


        public static string Currently_Used_DB_Conn_String_svc_admin_conn_LOCAL
        {
            get
            {
                string the_db = Currently_Used_DB;

                string stemp = string.Format(svc_admin_conn_LOCAL, the_db);

                return stemp;
            }
        }

        public static bool Is_Special_Handling_For_Bad_Data = false; // true;


    }
}
