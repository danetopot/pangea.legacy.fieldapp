﻿using System;
using System.Data.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;


using Pangea;
using Pangea.Data;
using Pangea.Data.DataObjects;
using static Pangea.Data.DataObjects.Aggregate_Child_Data_TablesDataContext;

namespace UT_SPROCs
{
    [TestClass]
    public class unitTests_SPROCs
    {

        [TestMethod]
        public void AddChild_INSERT()
        {

            ;
            
            Handle_Add_Or_Update_Child_Data(Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type.INSERT);

            ;

        }

        [TestMethod]
        public void Child_UPDATE()
        {

            ;

            Guid gd = Guid.NewGuid();

            Handle_Add_Or_Update_Child_Data(Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type.INSERT, gd);

            ;

        }



        private void Handle_Add_Or_Update_Child_Data
            (
            Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type e_Table_Action_Type,
            Guid? child_ID = null
            )
        {

            Aggregate_Child_Data_TablesDataContext _enrollmentDataContext = Data_Context_Given_Action_Type(e_Table_Action_Type);

            ;

            AddChildResult _addChildResults = _enrollmentDataContext.Add_Or_Update_Child_Data
                (
                e_Table_Action_Type,
                PangeaInfo.User.UserID,
                "vvv", // FirstName,
                "vvv", // LastName,
                child_ID, // null, // ChildID,
                null, // CHILD NUMBER
                "vvv", // MiddleName,
                new DateTime(2016, 2, 10, 0, 0, 0), //  DateOfBirth,
                44, // GradeLvl?.CodeID,
                3, // Health?.CodeID,
                2, // Lives_With?.CodeID,
                5, // FavLearning?.CodeID,
                null, // CHILD RECORD STATUS CODE ID
                null, // CHILD REMOVE REASON CODE ID
                2, // (byte?)Gender?.CodeID,
                0, // (byte?)NumberBrothers,
                0, // (byte?)NumberSisters,
                false, // HasDisability,
                4022, // ChildLocation?.CodeID,
                "Ray Ray" // NickName
                );

        }

        private static Aggregate_Child_Data_TablesDataContext Data_Context_Given_Action_Type
            (
            Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type e_Table_Action_Type
            )
        {

            CustomMappingSource cust_Map_Source = new CustomMappingSource(e_Table_Action_Type);

            Aggregate_Child_Data_TablesDataContext _enrollmentDataContext
                = new Aggregate_Child_Data_TablesDataContext
                (
                    PangeaInfo.DBCon,
                    e_Table_Action_Type,
                    cust_Map_Source
                    );

            return _enrollmentDataContext;

        }




        [TestMethod]
        public void Add_Child_Duplicates_For_Updates()
        {

            ;

            /*
             * 
            Get this error with below:

            System.Data.SqlClient.SqlException: 'Transaction count after EXECUTE indicates a mismatching number of BEGIN and COMMIT statements. Previous count = 0, current count = 1.'

             */

            Guid gd_User_ID = Guid.NewGuid();
            
            Guid gd_Child_1 = Guid.NewGuid();

            Guid gd_Child_2 = Guid.NewGuid();

            Aggregate_Child_Data_TablesDataContext _enrollmentDataContext = Data_Context_Given_Action_Type
                (
                 Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type.UPDATE
                );

            _enrollmentDataContext.Add_Child_Duplicates_for_Updates
                (
                gd_User_ID, 
                gd_Child_1,
                gd_Child_2
                );

            ;

        }


        [TestMethod]
        public void Add_Child_Favorite_Activity_INSERT()
        {

            ;

            Guid gd_User_ID = Guid.NewGuid();

            Guid gd_Child = Guid.NewGuid();

            DateTime dt_start = DateTime.Now.AddDays(-100);

            int favorite_activity_code = 11;

            DateTime dt_end = DateTime.Now;

            Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type e_Type
                = Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type.INSERT;
            
            Add_Child_Favorite_Activity_Handler
                (
                gd_User_ID, 
                gd_Child, 
                dt_start, 
                favorite_activity_code, 
                dt_end, 
                e_Type
                );

            ;

        }


        [TestMethod]
        public void Add_Child_Favorite_Activity_UPDATE()
        {

            ;

            Guid gd_User_ID = Guid.NewGuid();

            Guid gd_Child = Guid.NewGuid();

            DateTime dt_start = DateTime.Now.AddDays(-100);

            int favorite_activity_code = 11;

            DateTime dt_end = DateTime.Now;

            Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type e_Type
                = Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type.UPDATE;

            Add_Child_Favorite_Activity_Handler
                (
                gd_User_ID,
                gd_Child,
                dt_start,
                favorite_activity_code,
                dt_end,
                e_Type
                );

            ;

        }



        private static void Add_Child_Favorite_Activity_Handler
            (
            Guid gd_User_ID, 
            Guid gd_Child, 
            DateTime dt_start, 
            int favorite_activity_code, 
            DateTime dt_end, 
            Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type e_Type
            )
        {

            Aggregate_Child_Data_TablesDataContext _enrollmentDataContext = Data_Context_Given_Action_Type(e_Type);

            _enrollmentDataContext.Add_Child_Favorite_Activity
                (
                e_Type,
                gd_User_ID,
                gd_Child,
                dt_start,
                favorite_activity_code,
                dt_end
                );

        }


        [TestMethod]
        public void Add_Child_File_INSERT()
        {

            ;

            enum_Aggregate_Child_Data_TablesDataContext_Type e_Table_Action_Type = enum_Aggregate_Child_Data_TablesDataContext_Type.INSERT;
            
            Add_Child_File_Handler(e_Table_Action_Type);

            ;

        }


        [TestMethod]
        public void Add_Child_File_UPDATE()
        {

            ;

            enum_Aggregate_Child_Data_TablesDataContext_Type e_Table_Action_Type = enum_Aggregate_Child_Data_TablesDataContext_Type.UPDATE;

            Add_Child_File_Handler(e_Table_Action_Type);

            ;

        }

        private static void Add_Child_File_Handler(enum_Aggregate_Child_Data_TablesDataContext_Type e_Table_Action_Type)
        {

            Aggregate_Child_Data_TablesDataContext _enrollmentDataContext = Data_Context_Given_Action_Type(e_Table_Action_Type);

            Guid? user_ID = Guid.NewGuid();

            Guid? child_ID = Guid.NewGuid();

            byte[] file = { 1, 2, 3, 4 };

            string fileName = "file.txt";

            int? content_Type_ID = 4;

            int? file_Type_ID = 8;

            string make = "wwq";

            string model = "eer";

            string software = "swre";

            DateTime? dateTime = DateTime.Now;

            DateTime? dateTimeOriginal = DateTime.Now;

            DateTime? dateTimeDigitized = DateTime.Now;

            string gPSVersionID = "2q34e";

            string gPSLatitudeRef = "c";

            string gPSLatitude = "z";

            string gPSLongitudeRef = "aa";

            string gPSLongitude = "22";

            string gPSAltitudeRef = "98";

            string gPSAltitude = "223";

            string gPSTimeStamp = "we897";

            string gPSImgDirectionRef = "W";

            string gPSDateStamp = "1/1/2001";


            _enrollmentDataContext.Add_Child_File
                (
                e_Table_Action_Type,
                user_ID,
                child_ID,
                file,
                fileName,
                content_Type_ID,
                file_Type_ID,
                make,
                model,
                software,
                dateTime,
                dateTimeOriginal,
                dateTimeDigitized,
                gPSVersionID,
                gPSLatitudeRef,
                gPSLatitude,
                gPSLongitudeRef,
                gPSLongitude,
                gPSAltitudeRef,
                gPSAltitude,
                gPSTimeStamp,
                gPSImgDirectionRef,
                gPSDateStamp
                );
        }





        [TestMethod]
        public void Add_Child_Major_Life_Event_UPDATE()
        {

            ;

            enum_Aggregate_Child_Data_TablesDataContext_Type e_Table_Action_Type = enum_Aggregate_Child_Data_TablesDataContext_Type.UPDATE;

            ;

            Guid? user_ID = Guid.NewGuid();

            Guid? child_ID = Guid.NewGuid();
            
            DateTime? start_Date = DateTime.Now.AddDays(-100);

            int? language_Code_ID = 6;

            string major_Life_Description = "MLD";

            DateTime? end_Date = DateTime.Now;
            
            Aggregate_Child_Data_TablesDataContext _enrollmentDataContext = Data_Context_Given_Action_Type(e_Table_Action_Type);

            _enrollmentDataContext.Add_Child_Major_Life_Event
                (
                user_ID,
                child_ID,
                start_Date,
                language_Code_ID,
                major_Life_Description,
                end_Date,
                enum_Aggregate_Child_Data_TablesDataContext_Type.UPDATE
                );

            ;

        }



        [TestMethod]
        public void Add_Child_Non_Duplicates_for_Inserts()
        {

            ;

            enum_Aggregate_Child_Data_TablesDataContext_Type e_Table_Action_Type = enum_Aggregate_Child_Data_TablesDataContext_Type.INSERT;

            ;

            Guid gd_User_ID = Guid.NewGuid();

            Guid gd_Child_1 = Guid.NewGuid();

            Guid gd_Child_2 = Guid.NewGuid();

            Aggregate_Child_Data_TablesDataContext _enrollmentDataContext = Data_Context_Given_Action_Type(e_Table_Action_Type);

            _enrollmentDataContext.Add_Child_Non_Duplicates_for_Inserts
                (
                gd_User_ID,
                gd_Child_1,
                gd_Child_2
                );

            ;

        }


        
        [TestMethod]
        public void Add_Child_Personality_Type_INSERT()
        {

            ;
            
            Add_Child_Personality_Type_Handler(enum_Aggregate_Child_Data_TablesDataContext_Type.INSERT);

            ;

        }


        [TestMethod]
        public void Add_Child_Personality_Type_UPDATE()
        {

            ;

            Add_Child_Personality_Type_Handler(enum_Aggregate_Child_Data_TablesDataContext_Type.UPDATE);

            ;

        }



        private static void Add_Child_Personality_Type_Handler(enum_Aggregate_Child_Data_TablesDataContext_Type e_Table_Action_Type)
        {

            ;

            Guid? gd_User_ID = Guid.NewGuid();

            Guid? child_ID = Guid.NewGuid();

            DateTime? start_Date = DateTime.Now.AddDays(-100);

            int? personality_Type_Code_ID = 7;

            DateTime? end_Date = DateTime.Now;

            Aggregate_Child_Data_TablesDataContext _enrollmentDataContext = Data_Context_Given_Action_Type(e_Table_Action_Type);

            _enrollmentDataContext.Add_Child_Personality_Type
                (
                e_Table_Action_Type,
                gd_User_ID,
                child_ID,
                start_Date,
                personality_Type_Code_ID,
                end_Date
                );

        }



        [TestMethod]
        public void Add_Or_Update_Child_Data_INSERT()
        {

            /*
             * ON INSERT, SEEMS TO WORK GOOD AND RETURN GUID
             */

            ;

            enum_Aggregate_Child_Data_TablesDataContext_Type e_Table_Action_Type = enum_Aggregate_Child_Data_TablesDataContext_Type.INSERT;
                        
            Add_Or_Update_Child_Data_Handler(e_Table_Action_Type);

            ;

        }

        [TestMethod]
        public void Add_Or_Update_Child_Data_UPDATE()
        {

            /*
             * ON INSERT, SEEMS TO WORK GOOD AND RETURN GUID
             */

            ;

            enum_Aggregate_Child_Data_TablesDataContext_Type e_Table_Action_Type = enum_Aggregate_Child_Data_TablesDataContext_Type.UPDATE;

            Add_Or_Update_Child_Data_Handler(e_Table_Action_Type);

            ;

        }


        private static void Add_Or_Update_Child_Data_Handler(enum_Aggregate_Child_Data_TablesDataContext_Type e_Table_Action_Type)
        {

            Guid? user_ID = Guid.NewGuid();

            string first_Name = "Jo";

            string last_Name = "Smith";

            Guid? child_ID = Guid.NewGuid();

            long? child_Number = 2342;

            string middle_Name = "Dow";

            DateTime? date_of_Birth = new DateTime(2014, 1, 12);

            int? grade_Level_Code_ID = 3;

            int? health_Status_Code_ID = 4;

            int? lives_With_Code_ID = 2;

            int? favorite_Learning_Code_ID = 8;

            int? child_Record_Status_Code_ID = 3;

            int? child_Remove_Reason_Code_ID = 7;

            int? gender_Code_ID = 1;

            byte? number_Brothers = 2;

            byte? number_Sisters = 1;

            bool? disability_Status_Code_ID = false;

            int? location_Code_ID = 2376;

            string nickName = "Rikk";

            bool _readyToTransit = false;

            Aggregate_Child_Data_TablesDataContext _enrollmentDataContext = Data_Context_Given_Action_Type(e_Table_Action_Type);

            _enrollmentDataContext.Add_Or_Update_Child_Data
                (
                e_Table_Action_Type,
                user_ID,
                first_Name,
                last_Name,
                child_ID,
                child_Number,
                middle_Name,
                date_of_Birth,
                grade_Level_Code_ID,
                health_Status_Code_ID,
                lives_With_Code_ID,
                favorite_Learning_Code_ID,
                child_Record_Status_Code_ID,
                child_Remove_Reason_Code_ID,
                gender_Code_ID,
                number_Brothers,
                number_Sisters,
                disability_Status_Code_ID,
                location_Code_ID,
                nickName,
                _readyToTransit
                );
        }



        
        [TestMethod]
        public void Clear_Child_Codes_INSERT()
        {

            ;

            enum_Aggregate_Child_Data_TablesDataContext_Type e_Table_Action_Type = enum_Aggregate_Child_Data_TablesDataContext_Type.INSERT;
            
            Clear_Child_Codes_Handler(e_Table_Action_Type);

            ;

        }

        [TestMethod]
        public void Clear_Child_Codes_UPDATE()
        {

            ;

            enum_Aggregate_Child_Data_TablesDataContext_Type e_Table_Action_Type = enum_Aggregate_Child_Data_TablesDataContext_Type.UPDATE;

            Clear_Child_Codes_Handler(e_Table_Action_Type);

            ;

        }

        private static void Clear_Child_Codes_Handler(enum_Aggregate_Child_Data_TablesDataContext_Type e_Table_Action_Type)
        {
            ;

            Guid gd_User_ID = Guid.NewGuid();

            Guid gd_Child_1 = Guid.NewGuid();

            DateTime? end_Date = DateTime.Now;


            Aggregate_Child_Data_TablesDataContext _enrollmentDataContext = Data_Context_Given_Action_Type(e_Table_Action_Type);

            _enrollmentDataContext.Clear_Child_Codes
                (
                e_Table_Action_Type,
                gd_User_ID,
                gd_Child_1,
                end_Date
                );
        }
    }
}
