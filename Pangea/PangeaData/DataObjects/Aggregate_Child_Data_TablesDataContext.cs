﻿using PangeaData.Model.SPROCs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data.SqlClient;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Pangea.Data.DataObjects
{
    public class Aggregate_Child_Data_TablesDataContext : DataContext
    {

        public static CustomMappingSource mappingSource_Pending = new CustomMappingSource("Pending");

        public static CustomMappingSource mappingSource_Enrollment = new CustomMappingSource("Enrollment");

        public enum enum_Aggregate_Child_Data_TablesDataContext_Type { INSERT, UPDATE };

        public Aggregate_Child_Data_TablesDataContext
            (
            string connString,
            enum_Aggregate_Child_Data_TablesDataContext_Type _enum_Aggregate_Child_Data_TablesDataContext_Type,
            CustomMappingSource mappingSource
            ) : base(connString, mappingSource)
        {

        }



        /// <param name="_readyToTransit">Whether the record is to saved for Enrollment(true) or just to save progress(false)</param>
        public AddChildResult Add_Or_Update_Child_Data
            (
            enum_Aggregate_Child_Data_TablesDataContext_Type e_Table_Action_Type,
                Guid? user_ID,
                string first_Name,
                string last_Name,
                Guid? child_ID,              
                long? child_Number,
                string middle_Name,
                DateTime? date_of_Birth,
                int? grade_Level_Code_ID,
                int? health_Status_Code_ID,
                int? lives_With_Code_ID,
                int? favorite_Learning_Code_ID,
                int? child_Record_Status_Code_ID,
                int? child_Remove_Reason_Code_ID,
                int? gender_Code_ID,
                byte? number_Brothers,
                byte? number_Sisters,
                bool? disability_Status_Code_ID,
                int? location_Code_ID,
                string nickName,
                bool _readyToTransit = false
            )
        {

            AddChildResult the_data = null;

            string the_schema = Get_Schemas_Given_Data_Action_Type(e_Table_Action_Type);

            List<SqlParameter> l_SqlParameters = new List<SqlParameter>();

            l_SqlParameters.Add
                (
                new SqlParameter("@User_ID", user_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@First_Name", first_Name)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Last_Name", last_Name)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Child_ID", child_ID)
                );

            //
            //
            if (e_Table_Action_Type == enum_Aggregate_Child_Data_TablesDataContext_Type.UPDATE)
            {
                l_SqlParameters.Add
                (
                new SqlParameter("@Child_Number", child_Number)
                );
            }

            l_SqlParameters.Add
                (
                new SqlParameter("@Middle_Name", middle_Name)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Date_of_Birth", date_of_Birth)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Grade_Level_Code_ID", grade_Level_Code_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Health_Status_Code_ID", health_Status_Code_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Lives_With_Code_ID", lives_With_Code_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Favorite_Learning_Code_ID", favorite_Learning_Code_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Child_Record_Status_Code_ID", child_Record_Status_Code_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Child_Remove_Reason_Code_ID", child_Remove_Reason_Code_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Gender_Code_ID", gender_Code_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Number_Brothers", number_Brothers)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Number_Sisters", number_Sisters)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Disability_Status_Code_ID", disability_Status_Code_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Location_Code_ID", location_Code_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@NickName", nickName)
                );

            string s_SPROC_Suffix_After_Period = "usp_Add_Child";

            PangeaData.Model.DataTable_From_SPROC_Results SPROC_Results = PangeaData.Utils.DB_Utils.DataTable_From_SPROC(the_schema, l_SqlParameters, s_SPROC_Suffix_After_Period);

            the_data = new AddChildResult();

            switch (e_Table_Action_Type)
            {
                case enum_Aggregate_Child_Data_TablesDataContext_Type.INSERT:

                    the_data.Child_ID = (Guid)SPROC_Results.the_DataTable.Rows[0][0];

                    break;
                case enum_Aggregate_Child_Data_TablesDataContext_Type.UPDATE:

                    the_data.Child_ID = child_ID.Value;

                    break;
            }

            return the_data;

        }



        private static string Get_Schemas_Given_Data_Action_Type(enum_Aggregate_Child_Data_TablesDataContext_Type e_Table_Action_Type)
        {
            string the_schema = "";

            switch (e_Table_Action_Type)
            {
                case enum_Aggregate_Child_Data_TablesDataContext_Type.INSERT:

                    the_schema = "Enrollment";

                    break;
                case enum_Aggregate_Child_Data_TablesDataContext_Type.UPDATE:

                    the_schema = "Pending";

                    break;
            }

            return the_schema;
        }





        //[Function(Name = "Enrollment.usp_Clear_Child_Codes")]
        //public int Clear_Child_Codes(
        public int Clear_Child_Codes
            (
            enum_Aggregate_Child_Data_TablesDataContext_Type e_Table_Action_Type,
            Guid? user_ID,
            Guid? child_ID,
            DateTime? end_Date
            )
        {

            string the_schema = Get_Schemas_Given_Data_Action_Type(e_Table_Action_Type);

            string s_SPROC_Suffix_After_Period = "usp_Clear_Child_Codes";

            int i_result = -1;

            List<SqlParameter> l_SqlParameters = new List<SqlParameter>();

            l_SqlParameters.Add
                (
                new SqlParameter("@User_ID", user_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Child_ID", child_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@End_Date", end_Date)
                );

            try
            {      
                PangeaData.Model.DataTable_From_SPROC_Results dt_Results = 
                    PangeaData
                    .Utils
                    .DB_Utils
                    .DataTable_From_SPROC
                    (
                        the_schema, 
                        l_SqlParameters, 
                        s_SPROC_Suffix_After_Period
                        ); 
                
                i_result = 1;
            }
            catch (Exception ee) { }
            
            return i_result;
        }


        //[Function(Name = "Enrollment.usp_Add_Child_Chore")]
        //public int Add_Child_Chore(
        public int Add_Child_Chore
            (
            enum_Aggregate_Child_Data_TablesDataContext_Type e_Table_Action_Type,
            Guid? user_ID,
            Guid? child_ID,
            DateTime? start_Date,
            int? chore_Code_ID,
            DateTime? end_Date
            )
        {

            string the_schema = Get_Schemas_Given_Data_Action_Type(e_Table_Action_Type);

            string s_SPROC_Suffix_After_Period = "usp_Add_Child_Chore";

            int i_result = -1;

            List<SqlParameter> l_SqlParameters = new List<SqlParameter>();

            l_SqlParameters.Add
                (
                new SqlParameter("@User_ID", user_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Child_ID", child_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Start_Date", start_Date)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Chore_Code_ID", chore_Code_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@End_Date", end_Date)
                );

            try
            {
                PangeaData.Model.DataTable_From_SPROC_Results dt_Results = PangeaData.Utils.DB_Utils.DataTable_From_SPROC(the_schema, l_SqlParameters, s_SPROC_Suffix_After_Period);
                    
                i_result = 1;
            }
            catch (Exception ee) { }
      
            return i_result;

        }

              

        //[Function(Name = "Enrollment.usp_Add_Child_Favorite_Activity")]
        //public int Add_Child_Favorite_Activity(
        public int Add_Child_Favorite_Activity
            (
            enum_Aggregate_Child_Data_TablesDataContext_Type e_Table_Action_Type,
            Guid? user_ID,
            Guid? child_ID,
            DateTime? start_Date,
            int? favorite_Activity_Code_ID,
            DateTime? end_Date
            )
        {

            string the_schema = Get_Schemas_Given_Data_Action_Type(e_Table_Action_Type);

            string s_SPROC_Suffix_After_Period = "usp_Add_Child_Favorite_Activity";

            int i_result = -1;

            List<SqlParameter> l_SqlParameters = new List<SqlParameter>();

            l_SqlParameters.Add
                (
                new SqlParameter("@User_ID", user_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Child_ID", child_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Start_Date", start_Date)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Favorite_Activity_Code_ID", favorite_Activity_Code_ID)
                );
                        
                
            l_SqlParameters.Add
                (
                new SqlParameter("@End_Date", end_Date)
                );


            try
            {

                PangeaData.Model.DataTable_From_SPROC_Results dt_Results =
                    PangeaData
                    .Utils
                    .DB_Utils
                    .DataTable_From_SPROC
                    (
                        the_schema,
                        l_SqlParameters,
                        s_SPROC_Suffix_After_Period
                        );

                i_result = 1;                
            }
            catch (Exception ee) { }
       
            return i_result;
        }


        //[Function(Name = "Enrollment.usp_Add_Child_Personality_Type")]
        //public int Add_Child_Personality_Type(
        public int Add_Child_Personality_Type
            (
            enum_Aggregate_Child_Data_TablesDataContext_Type e_Table_Action_Type,
            Guid? user_ID,
            Guid? child_ID,
            DateTime? start_Date,
            int? personality_Type_Code_ID,
            DateTime? end_Date
            )
        {

            string the_schema = Get_Schemas_Given_Data_Action_Type(e_Table_Action_Type);

            string s_SPROC_Suffix_After_Period = "usp_Add_Child_Personality_Type";

            int i_result = -1;

            List<SqlParameter> l_SqlParameters = new List<SqlParameter>();

            l_SqlParameters.Add
                (
                new SqlParameter("@User_ID", user_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Child_ID", child_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Start_Date", start_Date)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Personality_Type_Code_ID", personality_Type_Code_ID)
                );

            l_SqlParameters.Add
            (
            new SqlParameter("@End_Date", end_Date)
            );

            PangeaData.Model.DataTable_From_SPROC_Results dt_Results = PangeaData.Utils.DB_Utils.DataTable_From_SPROC(the_schema, l_SqlParameters, s_SPROC_Suffix_After_Period);
                            
            i_result = 1;
                        
            return i_result;
        }



        //[Function(Name = "Pending.usp_Add_Child_File")]
        //public int Add_Child_File(
        public int Add_Child_File            
            (
            enum_Aggregate_Child_Data_TablesDataContext_Type e_Table_Action_Type,
            Guid? user_ID,
            Guid? child_ID,
            byte[] file,
            string fileName,
            int? content_Type_ID,
            int? file_Type_ID,
            string make,
            string model,
            string software,
            DateTime? dateTime,
            DateTime? dateTimeOriginal,
            DateTime? dateTimeDigitized,
            string gPSVersionID,
            string gPSLatitudeRef,
            string gPSLatitude,
            string gPSLongitudeRef,
            string gPSLongitude,
            string gPSAltitudeRef,
            string gPSAltitude,
            string gPSTimeStamp,
            string gPSImgDirectionRef,
            string gPSDateStamp
            )
        {

            string the_schema = Get_Schemas_Given_Data_Action_Type(e_Table_Action_Type);

            string s_SPROC_Suffix_After_Period = "usp_Add_Child_File";

            int i_result = -1;

            List<SqlParameter> l_SqlParameters = new List<SqlParameter>();

            l_SqlParameters.Add
                (
                new SqlParameter("@User_ID", user_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Child_ID", child_ID)
                );

            SqlParameter parameter = new SqlParameter("@File", System.Data.SqlDbType.VarBinary);

            parameter.Value = file;

            l_SqlParameters.Add
            (
            parameter
            );

            l_SqlParameters.Add
                (
                new SqlParameter("@FileName", fileName)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Content_Type_ID", content_Type_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@File_Type_ID", file_Type_ID)
                );

            l_SqlParameters.Add
            (
            new SqlParameter("@Make", make)
            );

            l_SqlParameters.Add
            (
            new SqlParameter("@Model", model)
            );

            l_SqlParameters.Add
            (
            new SqlParameter("@Software", software)
            );

            l_SqlParameters.Add
            (
            new SqlParameter("@DateTime", dateTime)
            );

            l_SqlParameters.Add
            (
            new SqlParameter("@DateTimeOriginal", dateTimeOriginal)
            );

            l_SqlParameters.Add
            (
            new SqlParameter("@DateTimeDigitized", dateTimeDigitized)
            );

            l_SqlParameters.Add
            (
            new SqlParameter("@GPSVersionID", gPSVersionID)
            );

            l_SqlParameters.Add
            (
            new SqlParameter("@GPSLatitudeRef", gPSLatitudeRef)
            );

            l_SqlParameters.Add
                (
            new SqlParameter("@GPSLatitude", gPSLatitude)
            );

            l_SqlParameters.Add
                (
                new SqlParameter("@GPSLongitudeRef", gPSLongitudeRef)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@GPSLongitude", gPSLongitude)
                );

            l_SqlParameters.Add
            (
            new SqlParameter("@GPSAltitudeRef", gPSAltitudeRef)
            );

            l_SqlParameters.Add
            (
            new SqlParameter("@GPSAltitude", gPSAltitude)
            );

            l_SqlParameters.Add
            (
            new SqlParameter("@GPSTimeStamp", gPSTimeStamp)
            );

            l_SqlParameters.Add
            (
            new SqlParameter("@GPSImgDirectionRef", gPSImgDirectionRef)
            );

            l_SqlParameters.Add
            (
            new SqlParameter("@GPSDateStamp", gPSDateStamp)
            );

            PangeaData.Model.DataTable_From_SPROC_Results dt_Results = PangeaData.Utils.DB_Utils.DataTable_From_SPROC
                (
                the_schema, 
                l_SqlParameters, 
                s_SPROC_Suffix_After_Period
                );

            string the_SPROC = the_schema.ToLower() + "." + s_SPROC_Suffix_After_Period.ToLower();
            
            if (the_SPROC == "enrollment.usp_add_child_file")
            {

                try
                {
                    Enrollment__usp_Add_Child_File the_Return_Data = new Enrollment__usp_Add_Child_File();
                    
                    DataRow dr_Data = dt_Results.the_DataTable.Rows[0];

                    the_Return_Data.ID = Convert.ToInt32(dr_Data[0]);

                    the_Return_Data.File_GUID = (Guid) dr_Data[1];

                    i_result = 1;
                }
                catch (Exception ee) { }
            }
            else
            {
                i_result = 1;
            }
            
            return i_result;

        }


        //[Function(Name = "Pending.usp_Add_Child_Duplicates")]
        //public int Add_Child_Duplicates([Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID, [Parameter(Name = "Child_ID_1", DbType = "UniqueIdentifier")] Guid? child_ID_1, [Parameter(Name = "Child_ID_2", DbType = "UniqueIdentifier")] Guid? child_ID_2)
        public int Add_Child_Duplicates_for_Updates
            (
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Child_ID_1", DbType = "UniqueIdentifier")] Guid? child_ID_1,
            [Parameter(Name = "Child_ID_2", DbType = "UniqueIdentifier")] Guid? child_ID_2
            )
        {

            string the_schema = "Pending";

            string s_SPROC_Suffix_After_Period = "usp_Add_Child_Duplicates";

            int i_result = -1;

            List<SqlParameter> l_SqlParameters = new List<SqlParameter>();

            l_SqlParameters.Add
                (
                new SqlParameter("@User_ID", user_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Child_ID_1", child_ID_1)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Child_ID_2", child_ID_2)
                );

            PangeaData.Model.DataTable_From_SPROC_Results dt_Results = PangeaData.Utils.DB_Utils.DataTable_From_SPROC(the_schema, l_SqlParameters, s_SPROC_Suffix_After_Period);

            i_result = (int)dt_Results.the_DataTable.Rows[0][0];

            return i_result;

        }



        //[Function(Name = "Enrollment.usp_Add_Child_Non_Duplicates")]
        //public int Add_Child_Non_Duplicates(
        public int Add_Child_Non_Duplicates_for_Inserts
            (
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Child_ID_1", DbType = "UniqueIdentifier")] Guid? child_ID_1,
            [Parameter(Name = "Child_ID_2", DbType = "UniqueIdentifier")] Guid? child_ID_2
            )
        {

            string the_schema = "Enrollment";

            string s_SPROC_Suffix_After_Period = "usp_Add_Child_Non_Duplicates";

            int i_result = -1;

            List<SqlParameter> l_SqlParameters = new List<SqlParameter>();

            l_SqlParameters.Add
                (
                new SqlParameter("@User_ID", user_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Child_ID_1", child_ID_1)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Child_ID_2", child_ID_2)
                );

            PangeaData.Model.DataTable_From_SPROC_Results dt_Results = PangeaData.Utils.DB_Utils.DataTable_From_SPROC(the_schema, l_SqlParameters, s_SPROC_Suffix_After_Period);
                            
            i_result = (int)dt_Results.the_DataTable.Rows[0][0];

            return i_result;
        }

        
        public class Child_Major_Life_Event_Markers
        {
            public int Major_Life_Event_ID { get; set; }

            public int LanguageCodeID { get; set; }
        }

        //[Function(Name = "Pending.usp_Add_Child_Major_Life_Event")]
        //public int Add_Child_Major_Life_Event(
        public Child_Major_Life_Event_Markers Add_Child_Major_Life_Event
            (
            Guid? user_ID,
            Guid? child_ID,
            DateTime? start_Date,
            int? language_Code_ID,
            string major_Life_Description,
            DateTime? end_Date,
            enum_Aggregate_Child_Data_TablesDataContext_Type e_Table_Action_Type
            )
        {

            string the_schema = Get_Schemas_Given_Data_Action_Type(e_Table_Action_Type);

            string s_SPROC_Suffix_After_Period = "usp_Add_Child_Major_Life_Event";

            int i_result = -1;

            List<SqlParameter> l_SqlParameters = new List<SqlParameter>();

            l_SqlParameters.Add
                (
                new SqlParameter("@User_ID", user_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Child_ID", child_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Start_Date", start_Date)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Language_Code_ID", language_Code_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Major_Life_Description", major_Life_Description)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@End_Date", end_Date)
                );

            PangeaData.Model.DataTable_From_SPROC_Results dt_Results = PangeaData.Utils.DB_Utils.DataTable_From_SPROC
                (
                the_schema, 
                l_SqlParameters, 
                s_SPROC_Suffix_After_Period
                );

            Child_Major_Life_Event_Markers the_data = new Child_Major_Life_Event_Markers();
                        
            the_data.Major_Life_Event_ID = (int)dt_Results.the_DataTable.Rows[0][0];
                
            the_data.LanguageCodeID = (int)dt_Results.the_DataTable.Rows[0][1];

            return the_data;
        }



       
        public int Enroll_Child
            (
            Guid? User_ID,
            Guid? Child_ID
            )
        {

            List<SqlParameter> l_SqlParameters = new List<SqlParameter>();

            l_SqlParameters.Add
                (
                new SqlParameter("@User_ID", User_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Child_ID", Child_ID)
                );

            string s_SPROC_Suffix_After_Period = "usp_Enroll_Child";

            int the_data = -1;

            try
            {
                PangeaData.Model.DataTable_From_SPROC_Results dt_Results = PangeaData.Utils.DB_Utils.DataTable_From_SPROC
                    (
                    "Enrollment", 
                    l_SqlParameters, 
                    s_SPROC_Suffix_After_Period
                    );

                the_data = 1;
            }
            catch(Exception ee)
            {            
            }

            return the_data;
        }



        public string Get_Enrollment_Major_Life_Event
            (
                Guid? User_ID,
                Guid? Child_ID
            )
        {

            List<SqlParameter> l_SqlParameters = new List<SqlParameter>();

            l_SqlParameters.Add
                (
                new SqlParameter("@User_ID", User_ID)
                );

            l_SqlParameters.Add
                (
                new SqlParameter("@Child_ID", Child_ID)
                );

            string s_SPROC_Suffix_After_Period = "usp_Get_Enrollment_Major_Life_Event";

            PangeaData.Model.DataTable_From_SPROC_Results dt_Results = PangeaData.Utils.DB_Utils.DataTable_From_SPROC
                (
                "Enrollment",
                l_SqlParameters,
                s_SPROC_Suffix_After_Period
                );

            string the_Major_Life_Event = "";

            try
            {

                the_Major_Life_Event = 
                    (dt_Results.the_DataTable.Rows[0][0] != null) 
                    ? 
                    Convert.ToString(dt_Results.the_DataTable.Rows[0][0]) 
                    : 
                    "";

            }
            catch (Exception ee) { }

            return the_Major_Life_Event;

        }

    }

}