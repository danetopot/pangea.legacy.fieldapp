﻿using System;
using System.Configuration;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Reflection;

namespace Pangea.Data.DataObjects
{
    // [Database(Name = "Pangea")]
    public partial class DBOTablesDataContext : DataContext
    {
        private static CustomMappingSource mappingSource = new CustomMappingSource("dbo");

        private static string conn_admin_raw = Centraler.Database_Settingz.svc_admin_conn_LOCAL;  // ConfigurationManager.AppSettings["svc_admin_conn"].ToString();

        #region Extensibility Method Definitions
        partial void OnCreated();
        #endregion

        public DBOTablesDataContext(string dbName) :
                base(String.Format(conn_admin_raw, dbName), mappingSource)
        {
            OnCreated();
        }

        //public DBOTablesDataContext() :
        //        base("Data Source=.\\SQLEXPRESS;Initial Catalog=Pangea;Integrated Security=False;uid=svc_pan_admin;pwd=8toDAF57%X0k", mappingSource)
        //{
        //    OnCreated();
        //}

        [Function(Name = "dbo.usp_Add_Action_Code")]
        public int Add_Action_Code(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID, 
            [Parameter(Name = "Action_Name", DbType = "NVarChar(100)")] string action_Name)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, action_Name);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Get_Saved_Childs")]
        public ISingleResult<ChildDO> Get_Saved_Childs(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID);
            return ((ISingleResult<ChildDO>)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Add_Action_Reason")]
        public int Add_Action_Reason(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID, 
            [Parameter(Name = "Action_Reason", DbType = "NVarChar(100)")] string action_Reason, 
            [Parameter(Name = "Description", DbType = "NVarChar(255)")] string description)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, action_Reason, description);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Add_Child")]
        public ISingleResult<AddChildResult> Add_Child(
                    [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
                    [Parameter(Name = "Child_Number", DbType = "BigInt")] long? child_Number,
                    [Parameter(Name = "First_Name", DbType = "NVarChar(50)")] string first_Name,
                    [Parameter(Name = "Last_Name", DbType = "NVarChar(50)")] string last_Name,
                    [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID,
                    [Parameter(Name = "Middle_Name", DbType = "NVarChar(50)")] string middle_Name,
                    [Parameter(Name = "Date_of_Birth", DbType = "DateTime")] DateTime? date_of_Birth,
                    [Parameter(Name = "Grade_Level_Code_ID", DbType = "Int")] int? grade_Level_Code_ID,
                    [Parameter(Name = "Health_Status_Code_ID", DbType = "Int")] int? health_Status_Code_ID,
                    [Parameter(Name = "Lives_With_Code_ID", DbType = "Int")] int? lives_With_Code_ID,
                    [Parameter(Name = "Favorite_Learning_Code_ID", DbType = "Int")] int? favorite_Learning_Code_ID,
                    [Parameter(Name = "Child_Record_Status_Code_ID", DbType = "Int")] int? child_Record_Status_Code_ID,
                    [Parameter(Name = "Child_Remove_Reason_Code_ID", DbType = "Int")] int? child_Remove_Reason_Code_ID,
                    [Parameter(Name = "Gender_Code_ID", DbType = "Int")] int? gender_Code_ID,
                    [Parameter(Name = "Number_Brothers", DbType = "TinyInt")] byte? number_Brothers,
                    [Parameter(Name = "Number_Sisters", DbType = "TinyInt")] byte? number_Sisters,
                    [Parameter(Name = "Disability_Status_Code_ID", DbType = "Bit")] bool? disability_Status_Code_ID,
                    [Parameter(Name = "Location_Code_ID", DbType = "Int")] int? location_Code_ID,
                    [Parameter(Name = "NickName", DbType = "NVarChar(50)")] string nickName)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, child_Number, first_Name, last_Name, child_ID, middle_Name, date_of_Birth, grade_Level_Code_ID, health_Status_Code_ID, lives_With_Code_ID, favorite_Learning_Code_ID, child_Record_Status_Code_ID, child_Remove_Reason_Code_ID, gender_Code_ID, number_Brothers, number_Sisters, disability_Status_Code_ID, location_Code_ID, nickName);
            return ((ISingleResult<AddChildResult>)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Add_Child_Chore")]
        public int Add_Child_Chore(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID, 
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID, 
            [Parameter(Name = "Start_Date", DbType = "DateTime")] DateTime? start_Date, 
            [Parameter(Name = "Chore_Code_ID", DbType = "Int")] int? chore_Code_ID, 
            [Parameter(Name = "End_Date", DbType = "DateTime")] DateTime? end_Date)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, child_ID, start_Date, chore_Code_ID, end_Date);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Add_Child_Duplicates")]
        public int Add_Child_Duplicates(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID, 
            [Parameter(Name = "Child_ID_1", DbType = "UniqueIdentifier")] Guid? child_ID_1, 
            [Parameter(Name = "Child_ID_2", DbType = "UniqueIdentifier")] Guid? child_ID_2)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, child_ID_1, child_ID_2);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Add_Child_Favorite_Activity")]
        public int Add_Child_Favorite_Activity(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID, 
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID, 
            [Parameter(Name = "Start_Date", DbType = "DateTime")] DateTime? start_Date, 
            [Parameter(Name = "Favorite_Activity_Code_ID", DbType = "Int")] int? favorite_Activity_Code_ID,
            [Parameter(Name = "End_Date", DbType = "DateTime")] DateTime? end_Date)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, child_ID, start_Date, favorite_Activity_Code_ID, end_Date);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Add_Child_File")]
        public int Add_Child_File(
                    [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
                    [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID,
                    [Parameter(Name = "File", DbType = "VarBinary(MAX)")] Binary file,
                    [Parameter(Name = "FileName", DbType = "NVarChar(512)")] string fileName,
                    [Parameter(Name = "Content_Type_ID", DbType = "Int")] int? content_Type_ID,
                    [Parameter(Name = "File_Type_ID", DbType = "Int")] int? file_Type_ID,
                    [Parameter(Name = "Make", DbType = "NVarChar(250)")] string make,
                    [Parameter(Name = "Model", DbType = "NVarChar(250)")] string model,
                    [Parameter(Name = "Software", DbType = "NVarChar(250)")] string software,
                    [Parameter(Name = "DateTime", DbType = "DateTime")] DateTime? dateTime,
                    [Parameter(Name = "DateTimeOriginal", DbType = "DateTime")] DateTime? dateTimeOriginal,
                    [Parameter(Name = "DateTimeDigitized", DbType = "DateTime")] DateTime? dateTimeDigitized,
                    [Parameter(Name = "GPSVersionID", DbType = "NVarChar(13)")] string gPSVersionID,
                    [Parameter(Name = "GPSLatitudeRef", DbType = "NVarChar(50)")] string gPSLatitudeRef,
                    [Parameter(Name = "GPSLatitude", DbType = "NVarChar(69)")] string gPSLatitude,
                    [Parameter(Name = "GPSLongitudeRef", DbType = "NVarChar(50)")] string gPSLongitudeRef,
                    [Parameter(Name = "GPSLongitude", DbType = "NVarChar(69)")] string gPSLongitude,
                    [Parameter(Name = "GPSAltitudeRef", DbType = "NVarChar(8)")] string gPSAltitudeRef,
                    [Parameter(Name = "GPSAltitude", DbType = "NVarChar(64)")] string gPSAltitude,
                    [Parameter(Name = "GPSTimeStamp", DbType = "NVarChar(69)")] string gPSTimeStamp,
                    [Parameter(Name = "GPSImgDirectionRef", DbType = "NVarChar(10)")] string gPSImgDirectionRef,
                    [Parameter(Name = "GPSDateStamp", DbType = "NVarChar(69)")] string gPSDateStamp)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, child_ID, file, fileName, content_Type_ID, file_Type_ID, make, model, software, dateTime, dateTimeOriginal, dateTimeDigitized, gPSVersionID, gPSLatitudeRef, gPSLatitude, gPSLongitudeRef, gPSLongitude, gPSAltitudeRef, gPSAltitude, gPSTimeStamp, gPSImgDirectionRef, gPSDateStamp);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Add_Child_Major_Life_Event")]
        public int Add_Child_Major_Life_Event(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID, 
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID, 
            [Parameter(Name = "Start_Date", DbType = "DateTime")] DateTime? start_Date, 
            [Parameter(Name = "Language_Code_ID", DbType = "Int")] int? language_Code_ID, 
            [Parameter(Name = "Major_Life_Description", DbType = "NVarChar(255)")] string major_Life_Description, 
            [Parameter(Name = "End_Date", DbType = "DateTime")] DateTime? end_Date)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, child_ID, start_Date, language_Code_ID, major_Life_Description, end_Date);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Add_Child_Major_Life_Event_Other_Lang")]
        public int Add_Child_Major_Life_Event_Other_Lang(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID, 
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID, 
            [Parameter(Name = "Start_Date", DbType = "DateTime")] DateTime? start_Date, 
            [Parameter(Name = "Language_Code_ID", DbType = "Int")] int? language_Code_ID, 
            [Parameter(Name = "Child_Major_Life_Event", DbType = "Int")] int? child_Major_Life_Event, 
            [Parameter(Name = "Major_Life_Description", DbType = "NVarChar(255)")] string major_Life_Description, 
            [Parameter(Name = "End_Date", DbType = "DateTime")] DateTime? end_Date)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, child_ID, start_Date, language_Code_ID, child_Major_Life_Event, major_Life_Description, end_Date);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Add_Child_Paragraph")]
        public int Add_Child_Paragraph(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID, 
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID,
            [Parameter(Name = "Start_Date", DbType = "DateTime")] DateTime? start_Date, 
            [Parameter(Name = "Language_Code_ID", DbType = "Int")] int? language_Code_ID,
            [Parameter(Name = "Paragraph_Description", DbType = "NVarChar(255)")] string paragraph_Description, 
            [Parameter(Name = "End_Date", DbType = "DateTime")] DateTime? end_Date)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, child_ID, start_Date, language_Code_ID, paragraph_Description, end_Date);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Add_Child_Paragraph_Other_Lang")]
        public int Add_Child_Paragraph_Other_Lang(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID, 
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID, 
            [Parameter(Name = "Start_Date", DbType = "DateTime")] DateTime? start_Date, 
            [Parameter(Name = "Language_Code_ID", DbType = "Int")] int? language_Code_ID, 
            [Parameter(Name = "Child_Paragraph", DbType = "Int")] int? child_Paragraph, 
            [Parameter(Name = "Paragraph_Description", DbType = "NVarChar(255)")] string paragraph_Description, 
            [Parameter(Name = "End_Date", DbType = "DateTime")] DateTime? end_Date)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, child_ID, start_Date, language_Code_ID, child_Paragraph, paragraph_Description, end_Date);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Add_Child_Personality_Type")]
        public int Add_Child_Personality_Type(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID, 
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID,
            [Parameter(Name = "Start_Date", DbType = "DateTime")] DateTime? start_Date, 
            [Parameter(Name = "Personality_Type_Code_ID", DbType = "Int")] int? personality_Type_Code_ID,
            [Parameter(Name = "End_Date", DbType = "DateTime")] DateTime? end_Date)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, child_ID, start_Date, personality_Type_Code_ID, end_Date);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Check_Child_Duplicates")]
        public void Check_Child_Duplicates([Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID);
        }

        [Function(Name = "dbo.usp_Clear_Child_Codes")]
        public int Clear_Child_Codes(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID,
            [Parameter(Name = "End_Date", DbType = "DateTime")] DateTime? end_Date)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, child_ID, end_Date);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Delete_Action_Code")]
        public int Delete_Action_Code(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID, 
            [Parameter(Name = "Action_Name", DbType = "NVarChar(100)")] string action_Name)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, action_Name);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Delete_Action_Reason")]
        public int Delete_Action_Reason(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID, 
            [Parameter(Name = "Action_Reason", DbType = "NVarChar(100)")] string action_Reason)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, action_Reason);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Delete_Child")]
        public int Delete_Child(
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID, 
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), child_ID, user_ID);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Get_A_Saved_Child")]
        public ISingleResult<GetASavedResult> Get_A_Saved_Child(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID, 
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, child_ID);
            return ((ISingleResult<GetASavedResult>)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Get_Chores")]
        public ISingleResult<GetEnrollmentChoresResult> Get_Chores(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID, 
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, child_ID);
            return ((ISingleResult<GetEnrollmentChoresResult>)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Get_Favorite_Activities")]
        public ISingleResult<GetEnrollmentFavoriteActivitiesResult> Get_Favorite_Activities(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID, 
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, child_ID);
            return ((ISingleResult<GetEnrollmentFavoriteActivitiesResult>)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Get_Personality_Type")]
        public ISingleResult<GetEnrollmentPersonalityTypeResult> Get_Personality_Type(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, child_ID);
            return ((ISingleResult<GetEnrollmentPersonalityTypeResult>)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Get_Previously_Transmitted")]
        public ISingleResult<ChildDO> Get_Previously_Transmitted([Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID);
            return ((ISingleResult<ChildDO>)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Get_Ready_To_Transmit")]
        public ISingleResult<ChildDO> Get_Ready_To_Transmit([Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID);
            return ((ISingleResult<ChildDO>)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Get_Major_Life_Event")]
        public ISingleResult<GetMajorLifeEventResult> Get_Major_Life_Event(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, child_ID);
            return ((ISingleResult<GetMajorLifeEventResult>)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Get_File")]
        public ISingleResult<GetFileResult> Get_File(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(DbType = "UniqueIdentifier")] Guid? stream_id, 
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID,
            [Parameter(Name = "File_Type_ID", DbType = "Int")] int? file_Type_ID,
            [Parameter(Name = "Content_Type_ID", DbType = "Int")] int? content_Type_ID,
            [Parameter(Name = "Current", DbType = "Bit")] bool? current)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, stream_id, child_ID, file_Type_ID, content_Type_ID, current);
            return ((ISingleResult<GetFileResult>)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Get_File_Data")]
        public ISingleResult<GetFileDataResult> Get_File_Data(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID, 
            [Parameter(DbType = "UniqueIdentifier")] Guid? stream_id, 
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID,
            [Parameter(Name = "File_Type_ID", DbType = "Int")] int? file_Type_ID, 
            [Parameter(Name = "Content_Type_ID", DbType = "Int")] int? content_Type_ID, 
            [Parameter(Name = "Current", DbType = "Bit")] bool? current)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, stream_id, child_ID, file_Type_ID, content_Type_ID, current);
            return ((ISingleResult<GetFileDataResult>)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Field_Update_Child")]
        public ISingleResult<FieldUpdateChildResult> Field_Update_Child(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Location_Code_ID", DbType = "Int")] int? location_Code_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, location_Code_ID);
            return ((ISingleResult<FieldUpdateChildResult>)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Add_DB_Version")]
        public int Add_DB_Version(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID, 
            [Parameter(Name = "Major", DbType = "Int")] int? major, 
            [Parameter(Name = "Minor", DbType = "Int")] int? minor, 
            [Parameter(Name = "Build", DbType = "Int")] int? build, 
            [Parameter(Name = "Revision", DbType = "Int")] int? revision, 
            [Parameter(Name = "Min_App_Major", DbType = "Int")] int? min_App_Major, 
            [Parameter(Name = "Min_App_Minor", DbType = "Int")] int? min_App_Minor, 
            [Parameter(Name = "Min_App_Build", DbType = "Int")] int? min_App_Build,
            [Parameter(Name = "Min_App_Revision", DbType = "Int")] int? min_App_Revision,
            [Parameter(Name = "Max_App_Major", DbType = "Int")] int? max_App_Major,
            [Parameter(Name = "Max_App_Minor", DbType = "Int")] int? max_App_Minor, 
            [Parameter(Name = "Max_App_Build", DbType = "Int")] int? max_App_Build, 
            [Parameter(Name = "Max_App_Revision", DbType = "Int")] int? max_App_Revision,
            [Parameter(Name = "SQLScriptText", DbType = "NVarChar(MAX)")] string sQLScriptText, 
            [Parameter(Name = "ReleaseNotesText", DbType = "NVarChar(MAX)")] string releaseNotesText)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, major, minor, build, revision, min_App_Major, min_App_Minor, min_App_Build, min_App_Revision, max_App_Major, max_App_Minor, max_App_Build, max_App_Revision, sQLScriptText, releaseNotesText);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Get_DB_Version")]
        public ISingleResult<GetDBVersionResult> Get_DB_Version(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID, 
            [Parameter(Name = "Major", DbType = "Int")] int? major, 
            [Parameter(Name = "Minor", DbType = "Int")] int? minor, 
            [Parameter(Name = "Build", DbType = "Int")] int? build, 
            [Parameter(Name = "Revision", DbType = "Int")] int? revision)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, major, minor, build, revision);
            return ((ISingleResult<GetDBVersionResult>)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Reset_DB")]
        public int Reset_DB()
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())));
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "dbo.usp_Set_Config")]
        public ISingleResult<SetConfigResult> Set_Config(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Name", DbType = "NVarChar(50)")] string name,
            [Parameter(Name = "Description", DbType = "NVarChar(255)")] string description,
            [Parameter(Name = "Bit_Value", DbType = "Bit")] bool? bit_Value,
            [Parameter(Name = "Int_Value", DbType = "BigInt")] long? int_Value,
            [Parameter(Name = "Text_Value", DbType = "NVarChar(255)")] string text_Value,
            [Parameter(Name = "Dec_Value", DbType = "Decimal(18,0)")] decimal? dec_Value,
            [Parameter(Name = "Datetime_Value", DbType = "DateTime")] DateTime? datetime_Value,
            [Parameter(Name = "Active", DbType = "Bit")] bool? active,
            [Parameter(Name = "Config_ID", DbType = "Int")] int? config_ID)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, name, description, bit_Value, int_Value, text_Value, dec_Value, datetime_Value, active, config_ID);
            return ((ISingleResult<SetConfigResult>)(result.ReturnValue));
        }
    }
}
