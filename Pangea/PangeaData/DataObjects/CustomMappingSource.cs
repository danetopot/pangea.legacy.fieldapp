﻿using Pangea.Data.DataObjects;
using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Reflection;

namespace Pangea.Data
{
    public class CustomMappingSource : MappingSource
    {
        private AttributeMappingSource mapping = new AttributeMappingSource();
        private String schema;

        public CustomMappingSource(String schema) : base()
        {
            this.schema = schema;
        }

        public CustomMappingSource(Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type e_Table_Action_Type) : base()
        {
            string schema = "";

            switch (e_Table_Action_Type)
            {
                case Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type.INSERT:

                    schema = "Enrollment";

                    break;
                case Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type.UPDATE:

                    schema = "Pending";

                    break;
            }

            this.schema = schema;
        }

        /*
         * 
            Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type 
                e_Table_Action_Type = Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type.INSERT;

         */

        protected override MetaModel CreateModel(Type dataContextType)
        {
            return new CustomMetaModel(mapping.GetModel(dataContextType), schema);
        }
    }

    public class CustomMetaModel : MetaModel
    {
        private static CustomMappingSource mapping;
        private MetaModel model;
        private String schema;

        public CustomMetaModel(MetaModel model, String schema)
        {
            this.model = model;
            this.schema = schema;

            mapping = new CustomMappingSource(schema);
        }

        public override Type ContextType
        {
            get
            {
                return model.ContextType;
            }
        }

        public override MappingSource MappingSource
        {
            get
            {
                return mapping;
            }
        }


        public override string DatabaseName
        {
            get
            {
                return model.DatabaseName;
            }
        }

        public override Type ProviderType
        {
            get
            {
                return model.ProviderType;
            }
        }

        public override MetaTable GetTable(Type rowType)
        {
            return new CustomMetaTable(model.GetTable(rowType), model, schema);   
        }

        public override IEnumerable<MetaTable> GetTables()
        {
            foreach (var table in model.GetTables())
                yield return new CustomMetaTable(table, model, schema);
        }

        public override MetaFunction GetFunction(MethodInfo method)
        {
            return model.GetFunction(method);
        }

        public override IEnumerable<MetaFunction> GetFunctions()
        {
            return model.GetFunctions();
        }

        public override MetaType GetMetaType(Type type)
        {
            return model.GetMetaType(type);
        }
    }

    public class CustomMetaTable : MetaTable
    {
        private MetaTable table;
        private MetaModel model;
        private String schema;

        public CustomMetaTable(MetaTable table, MetaModel model, String schema)
        {
            this.table = table;
            this.model = model;
            this.schema = schema;

            if (this.table != null)
            {
                var tableNameField = this.table.GetType().FindMembers(MemberTypes.Field, BindingFlags.NonPublic | BindingFlags.Instance, (member, criteria) => member.Name.ToLower() == "tablename", null).OfType<FieldInfo>().FirstOrDefault();
                if (tableNameField != null)
                    tableNameField.SetValue(this.table, TableName);
            }
        }

        public override MethodInfo DeleteMethod
        {
            get
            {
                return table.DeleteMethod;
            }
        }

        public override MethodInfo InsertMethod
        {
            get
            {
                return table.InsertMethod;
            }
        }

        public override MethodInfo UpdateMethod
        {
            get
            {
                return table.UpdateMethod;
            }
        }

        public override MetaModel Model
        {
            get
            {
                return model;
            }
        }

        public override string TableName
        {
            get
            {
                return table.TableName
                    .Replace("[Schema]", schema);
            }
        }

        public override MetaType RowType
        {
            get
            {
                return table.RowType;
            }
        }
    }
}
