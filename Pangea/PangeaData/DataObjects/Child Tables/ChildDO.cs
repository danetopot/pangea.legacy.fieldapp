﻿////////////////////////////////////////////////////////////////////////////////
/// This is the Object use for representing the Enrollment.Child Database
/// Table.  This was creating by Linq to SQL
////////////////////////////////////////////////////////////////////////////////

using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace Pangea.Data.DataObjects
{
    [Table(Name = "[Schema].Child")]
    public partial class ChildDO
    {
        private bool? _Active;

        private int? _Action_ID;

        private Guid _Action_User;

        private DateTime _Action_Date;

        private int? _Action_Reason;

        private Binary _TS;

        private long? _TSLONG;

        private long? _TSMAN;

        private DateTime? _BlackBaud_Uploaded;

        private long _ID;

        private Guid _Child_ID;

        private long? _Child_Number;

        private Guid? _BlackBaud_Guid;

        private string _First_Name;

        private string _Middle_Name;

        private string _Last_Name;

        private DateTime? _Date_of_Birth;

        private int? _Grade_Level_Code_ID;

        private int? _Health_Status_Code_ID;

        private int? _Lives_With_Code_ID;

        private int? _Favorite_Learning_Code_ID;

        private int? _Child_Record_Status_Code_ID;

        private int? _Child_Remove_Reason_Code_ID;

        private int? _Gender_Code_ID;

        private byte? _Number_Brothers;

        private byte? _Number_Sisters;

        private bool? _Disability_Status_Code_ID;

        private int? _Location_Code_ID;

        private long? _Profile_Photo_Image;

        private int? _Profile_Photo_Image_SeqNo;

        private long? _Action_Photo_Image;

        private int? _Action_Photo_Image_SeqNo;

        private bool? _Enrollment_Ready;

        private string _Nickname;

        #region Extensibility Method Definitions
        partial void OnLoaded();
        partial void OnValidate(ChangeAction action);
        partial void OnCreated();
        partial void OnActiveChanging(bool? value);
        partial void OnActiveChanged();
        partial void OnAction_IDChanging(int? value);
        partial void OnAction_IDChanged();
        partial void OnAction_UserChanging(Guid value);
        partial void OnAction_UserChanged();
        partial void OnAction_DateChanging(DateTime value);
        partial void OnAction_DateChanged();
        partial void OnAction_ReasonChanging(int? value);
        partial void OnAction_ReasonChanged();
        partial void OnTSChanging(Binary value);
        partial void OnTSChanged();
        partial void OnTSLONGChanging(long? value);
        partial void OnTSLONGChanged();
        partial void OnTSMANChanging(long? value);
        partial void OnTSMANChanged();
        partial void OnBlackBaud_UploadedChanging(DateTime? value);
        partial void OnBlackBaud_UploadedChanged();
        partial void OnIDChanging(long value);
        partial void OnIDChanged();
        partial void OnChild_IDChanging(Guid value);
        partial void OnChild_IDChanged();
        partial void OnChild_NumberChanging(long? value);
        partial void OnChild_NumberChanged();
        partial void OnBlackBaud_GuidChanging(Guid? value);
        partial void OnBlackBaud_GuidChanged();
        partial void OnFirst_NameChanging(string value);
        partial void OnFirst_NameChanged();
        partial void OnMiddle_NameChanging(string value);
        partial void OnMiddle_NameChanged();
        partial void OnLast_NameChanging(string value);
        partial void OnLast_NameChanged();
        partial void OnDate_of_BirthChanging(DateTime? value);
        partial void OnDate_of_BirthChanged();
        partial void OnGrade_Level_Code_IDChanging(int? value);
        partial void OnGrade_Level_Code_IDChanged();
        partial void OnHealth_Status_Code_IDChanging(int? value);
        partial void OnHealth_Status_Code_IDChanged();
        partial void OnLives_With_Code_IDChanging(int? value);
        partial void OnLives_With_Code_IDChanged();
        partial void OnFavorite_Learning_Code_IDChanging(int? value);
        partial void OnFavorite_Learning_Code_IDChanged();
        partial void OnChild_Record_Status_Code_IDChanging(int? value);
        partial void OnChild_Record_Status_Code_IDChanged();
        partial void OnChild_Remove_Reason_Code_IDChanging(int? value);
        partial void OnChild_Remove_Reason_Code_IDChanged();
        partial void OnGender_Code_IDChanging(int? value);
        partial void OnGender_Code_IDChanged();
        partial void OnNumber_BrothersChanging(byte? value);
        partial void OnNumber_BrothersChanged();
        partial void OnNumber_SistersChanging(byte? value);
        partial void OnNumber_SistersChanged();
        partial void OnDisability_Status_Code_IDChanging(bool? value);
        partial void OnDisability_Status_Code_IDChanged();
        partial void OnLocation_Code_IDChanging(int? value);
        partial void OnLocation_Code_IDChanged();
        partial void OnProfile_Photo_ImageChanging(long? value);
        partial void OnProfile_Photo_ImageChanged();
        partial void OnProfile_Photo_Image_SeqNoChanging(int? value);
        partial void OnProfile_Photo_Image_SeqNoChanged();
        partial void OnAction_Photo_ImageChanging(long? value);
        partial void OnAction_Photo_ImageChanged();
        partial void OnAction_Photo_Image_SeqNoChanging(int? value);
        partial void OnAction_Photo_Image_SeqNoChanged();
        partial void OnEnrollment_ReadyChanging(bool? value);
        partial void OnEnrollment_ReadyChanged();
        partial void OnNicknameChanging(string value);
        partial void OnNicknameChanged();
        #endregion

        public ChildDO()
        {
            OnCreated();
        }

        [Column(Storage = "_Active", DbType = "Bit", UpdateCheck = UpdateCheck.Never)]
        public bool? Active
        {
            get
            {
                return _Active;
            }
            set
            {
                if ((_Active != value))
                {
                    OnActiveChanging(value);
                    _Active = value;
                    OnActiveChanged();
                }
            }
        }

        [Column(Storage = "_Action_ID", DbType = "Int NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public int? Action_ID
        {
            get
            {
                return _Action_ID;
            }
            set
            {
                if ((_Action_ID != value))
                {
                    OnAction_IDChanging(value);
                    _Action_ID = value;
                    OnAction_IDChanged();
                }
            }
        }

        [Column(Storage = "_Action_User", DbType = "UniqueIdentifier NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public Guid Action_User
        {
            get
            {
                return _Action_User;
            }
            set
            {
                if ((_Action_User != value))
                {
                    OnAction_UserChanging(value);
                    _Action_User = value;
                    OnAction_UserChanged();
                }
            }
        }

        [Column(Storage = "_Action_Date", DbType = "DateTime NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public DateTime Action_Date
        {
            get
            {
                return _Action_Date;
            }
            set
            {
                if ((_Action_Date != value))
                {
                    OnAction_DateChanging(value);
                    _Action_Date = value;
                    OnAction_DateChanged();
                }
            }
        }

        [Column(Storage = "_Action_Reason", DbType = "Int", UpdateCheck = UpdateCheck.Never)]
        public int? Action_Reason
        {
            get
            {
                return _Action_Reason;
            }
            set
            {
                if ((_Action_Reason != value))
                {
                    OnAction_ReasonChanging(value);
                    _Action_Reason = value;
                    OnAction_ReasonChanged();
                }
            }
        }

        [Column(Storage = "_TS", AutoSync = AutoSync.Always, DbType = "rowversion NOT NULL", CanBeNull = false, IsDbGenerated = true, IsVersion = true, UpdateCheck = UpdateCheck.Never)]
        public Binary TS
        {
            get
            {
                return _TS;
            }
            set
            {
                if ((_TS != value))
                {
                    OnTSChanging(value);
                    _TS = value;
                    OnTSChanged();
                }
            }
        }

        [Column(Storage = "_TSLONG", AutoSync = AutoSync.Always, DbType = "BigInt", IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)]
        public long? TSLONG
        {
            get
            {
                return _TSLONG;
            }
            set
            {
                if ((_TSLONG != value))
                {
                    OnTSLONGChanging(value);
                    _TSLONG = value;
                    OnTSLONGChanged();
                }
            }
        }

        [Column(Storage = "_TSMAN", DbType = "BigInt", UpdateCheck = UpdateCheck.Never)]
        public long? TSMAN
        {
            get
            {
                return _TSMAN;
            }
            set
            {
                if ((_TSMAN != value))
                {
                    OnTSMANChanging(value);
                    _TSMAN = value;
                    OnTSMANChanged();
                }
            }
        }

        [Column(Storage = "_BlackBaud_Uploaded", DbType = "DateTime", UpdateCheck = UpdateCheck.Never)]
        public DateTime? BlackBaud_Uploaded
        {
            get
            {
                return _BlackBaud_Uploaded;
            }
            set
            {
                if ((_BlackBaud_Uploaded != value))
                {
                    OnBlackBaud_UploadedChanging(value);
                    _BlackBaud_Uploaded = value;
                    OnBlackBaud_UploadedChanged();
                }
            }
        }

        [Column(Storage = "_ID", AutoSync = AutoSync.Always, DbType = "BigInt NOT NULL IDENTITY", IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)]
        public long ID
        {
            get
            {
                return _ID;
            }
            set
            {
                if ((_ID != value))
                {
                    OnIDChanging(value);
                    _ID = value;
                    OnIDChanged();
                }
            }
        }

        [Column(Storage = "_Child_ID", DbType = "UniqueIdentifier NOT NULL", IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never)]
        public Guid Child_ID
        {
            get
            {
                return _Child_ID;
            }
            set
            {
                if ((_Child_ID != value))
                {
                    OnChild_IDChanging(value);
                    _Child_ID = value;
                    OnChild_IDChanged();
                }
            }
        }

        [Column(Storage = "_Child_Number", DbType = "BigInt", UpdateCheck = UpdateCheck.Never)]
        public long? Child_Number
        {
            get
            {
                return _Child_Number;
            }
            set
            {
                if ((_Child_Number != value))
                {
                    OnChild_NumberChanging(value);
                    _Child_Number = value;
                    OnChild_NumberChanged();
                }
            }
        }

        [Column(Storage = "_BlackBaud_Guid", DbType = "UniqueIdentifier", UpdateCheck = UpdateCheck.Never)]
        public Guid? BlackBaud_Guid
        {
            get
            {
                return _BlackBaud_Guid;
            }
            set
            {
                if ((_BlackBaud_Guid != value))
                {
                    OnBlackBaud_GuidChanging(value);
                    _BlackBaud_Guid = value;
                    OnBlackBaud_GuidChanged();
                }
            }
        }

        [Column(Storage = "_First_Name", DbType = "NVarChar(50)", UpdateCheck = UpdateCheck.Never)]
        public string First_Name
        {
            get
            {
                return _First_Name;
            }
            set
            {
                if ((_First_Name != value))
                {
                    OnFirst_NameChanging(value);
                    _First_Name = value;
                    OnFirst_NameChanged();
                }
            }
        }

        [Column(Storage = "_Middle_Name", DbType = "NVarChar(50)", UpdateCheck = UpdateCheck.Never)]
        public string Middle_Name
        {
            get
            {
                return _Middle_Name;
            }
            set
            {
                if ((_Middle_Name != value))
                {
                    OnMiddle_NameChanging(value);
                    _Middle_Name = value;
                    OnMiddle_NameChanged();
                }
            }
        }

        [Column(Storage = "_Last_Name", DbType = "NVarChar(50)", UpdateCheck = UpdateCheck.Never)]
        public string Last_Name
        {
            get
            {
                return _Last_Name;
            }
            set
            {
                if ((_Last_Name != value))
                {
                    OnLast_NameChanging(value);
                    _Last_Name = value;
                    OnLast_NameChanged();
                }
            }
        }

        [Column(Storage = "_Date_of_Birth", DbType = "DateTime", UpdateCheck = UpdateCheck.Never)]
        public DateTime? Date_of_Birth
        {
            get
            {
                return _Date_of_Birth;
            }
            set
            {
                if ((_Date_of_Birth != value))
                {
                    OnDate_of_BirthChanging(value);
                    _Date_of_Birth = value;
                    OnDate_of_BirthChanged();
                }
            }
        }

        [Column(Storage = "_Grade_Level_Code_ID", DbType = "Int", UpdateCheck = UpdateCheck.Never)]
        public int? Grade_Level_Code_ID
        {
            get
            {
                return _Grade_Level_Code_ID;
            }
            set
            {
                if ((_Grade_Level_Code_ID != value))
                {
                    OnGrade_Level_Code_IDChanging(value);
                    _Grade_Level_Code_ID = value;
                    OnGrade_Level_Code_IDChanged();
                }
            }
        }

        [Column(Storage = "_Health_Status_Code_ID", DbType = "Int", UpdateCheck = UpdateCheck.Never)]
        public int? Health_Status_Code_ID
        {
            get
            {
                return _Health_Status_Code_ID;
            }
            set
            {
                if ((_Health_Status_Code_ID != value))
                {
                    OnHealth_Status_Code_IDChanging(value);
                    _Health_Status_Code_ID = value;
                    OnHealth_Status_Code_IDChanged();
                }
            }
        }

        [Column(Storage = "_Lives_With_Code_ID", DbType = "Int", UpdateCheck = UpdateCheck.Never)]
        public int? Lives_With_Code_ID
        {
            get
            {
                return _Lives_With_Code_ID;
            }
            set
            {
                if ((_Lives_With_Code_ID != value))
                {
                    OnLives_With_Code_IDChanging(value);
                    _Lives_With_Code_ID = value;
                    OnLives_With_Code_IDChanged();
                }
            }
        }

        [Column(Storage = "_Favorite_Learning_Code_ID", DbType = "Int", UpdateCheck = UpdateCheck.Never)]
        public int? Favorite_Learning_Code_ID
        {
            get
            {
                return _Favorite_Learning_Code_ID;
            }
            set
            {
                if ((_Favorite_Learning_Code_ID != value))
                {
                    OnFavorite_Learning_Code_IDChanging(value);
                    _Favorite_Learning_Code_ID = value;
                    OnFavorite_Learning_Code_IDChanged();
                }
            }
        }

        [Column(Storage = "_Child_Record_Status_Code_ID", DbType = "Int", UpdateCheck = UpdateCheck.Never)]
        public int? Child_Record_Status_Code_ID
        {
            get
            {
                return _Child_Record_Status_Code_ID;
            }
            set
            {
                if ((_Child_Record_Status_Code_ID != value))
                {
                    OnChild_Record_Status_Code_IDChanging(value);
                    _Child_Record_Status_Code_ID = value;
                    OnChild_Record_Status_Code_IDChanged();
                }
            }
        }

        [Column(Storage = "_Child_Remove_Reason_Code_ID", DbType = "Int", UpdateCheck = UpdateCheck.Never)]
        public int? Child_Remove_Reason_Code_ID
        {
            get
            {
                return _Child_Remove_Reason_Code_ID;
            }
            set
            {
                if ((_Child_Remove_Reason_Code_ID != value))
                {
                    OnChild_Remove_Reason_Code_IDChanging(value);
                    _Child_Remove_Reason_Code_ID = value;
                    OnChild_Remove_Reason_Code_IDChanged();
                }
            }
        }

        [Column(Storage = "_Gender_Code_ID", DbType = "Int", UpdateCheck = UpdateCheck.Never)]
        public int? Gender_Code_ID
        {
            get
            {
                return _Gender_Code_ID;
            }
            set
            {
                if ((_Gender_Code_ID != value))
                {
                    OnGender_Code_IDChanging(value);
                    _Gender_Code_ID = value;
                    OnGender_Code_IDChanged();
                }
            }
        }

        [Column(Storage = "_Number_Brothers", DbType = "TinyInt", UpdateCheck = UpdateCheck.Never)]
        public byte? Number_Brothers
        {
            get
            {
                return _Number_Brothers;
            }
            set
            {
                if ((_Number_Brothers != value))
                {
                    OnNumber_BrothersChanging(value);
                    _Number_Brothers = value;
                    OnNumber_BrothersChanged();
                }
            }
        }

        [Column(Storage = "_Number_Sisters", DbType = "TinyInt", UpdateCheck = UpdateCheck.Never)]
        public byte? Number_Sisters
        {
            get
            {
                return _Number_Sisters;
            }
            set
            {
                if ((_Number_Sisters != value))
                {
                    OnNumber_SistersChanging(value);
                    _Number_Sisters = value;
                    OnNumber_SistersChanged();
                }
            }
        }

        [Column(Storage = "_Disability_Status_Code_ID", DbType = "Bit", UpdateCheck = UpdateCheck.Never)]
        public bool? Disability_Status_Code_ID
        {
            get
            {
                return _Disability_Status_Code_ID;
            }
            set
            {
                if ((_Disability_Status_Code_ID != value))
                {
                    OnDisability_Status_Code_IDChanging(value);
                    _Disability_Status_Code_ID = value;
                    OnDisability_Status_Code_IDChanged();
                }
            }
        }

        [Column(Storage = "_Location_Code_ID", DbType = "Int", UpdateCheck = UpdateCheck.Never)]
        public int? Location_Code_ID
        {
            get
            {
                return _Location_Code_ID;
            }
            set
            {
                if ((_Location_Code_ID != value))
                {
                    OnLocation_Code_IDChanging(value);
                    _Location_Code_ID = value;
                    OnLocation_Code_IDChanged();
                }
            }
        }

        [Column(Storage = "_Profile_Photo_Image", DbType = "BigInt", UpdateCheck = UpdateCheck.Never)]
        public long? Profile_Photo_Image
        {
            get
            {
                return _Profile_Photo_Image;
            }
            set
            {
                if ((_Profile_Photo_Image != value))
                {
                    OnProfile_Photo_ImageChanging(value);
                    _Profile_Photo_Image = value;
                    OnProfile_Photo_ImageChanged();
                }
            }
        }

        [Column(Storage = "_Profile_Photo_Image_SeqNo", DbType = "Int", UpdateCheck = UpdateCheck.Never)]
        public int? Profile_Photo_Image_SeqNo
        {
            get
            {
                return _Profile_Photo_Image_SeqNo;
            }
            set
            {
                if ((_Profile_Photo_Image_SeqNo != value))
                {
                    OnProfile_Photo_Image_SeqNoChanging(value);
                    _Profile_Photo_Image_SeqNo = value;
                    OnProfile_Photo_Image_SeqNoChanged();
                }
            }
        }

        [Column(Storage = "_Action_Photo_Image", DbType = "BigInt", UpdateCheck = UpdateCheck.Never)]
        public long? Action_Photo_Image
        {
            get
            {
                return _Action_Photo_Image;
            }
            set
            {
                if ((_Action_Photo_Image != value))
                {
                    OnAction_Photo_ImageChanging(value);
                    _Action_Photo_Image = value;
                    OnAction_Photo_ImageChanged();
                }
            }
        }

        [Column(Storage = "_Action_Photo_Image_SeqNo", DbType = "Int", UpdateCheck = UpdateCheck.Never)]
        public int? Action_Photo_Image_SeqNo
        {
            get
            {
                return _Action_Photo_Image_SeqNo;
            }
            set
            {
                if ((_Action_Photo_Image_SeqNo != value))
                {
                    OnAction_Photo_Image_SeqNoChanging(value);
                    _Action_Photo_Image_SeqNo = value;
                    OnAction_Photo_Image_SeqNoChanged();
                }
            }
        }

        [Column(Storage = "_Enrollment_Ready", DbType = "Bit", UpdateCheck = UpdateCheck.Never)]
        public bool? Enrollment_Ready
        {
            get
            {
                return _Enrollment_Ready;
            }
            set
            {
                if ((_Enrollment_Ready != value))
                {
                    OnEnrollment_ReadyChanging(value);
                    _Enrollment_Ready = value;
                    OnEnrollment_ReadyChanged();
                }
            }
        }

        [Column(Storage = "_Nickname", DbType = "NVarChar(50)", UpdateCheck = UpdateCheck.Never)]
        public string Nickname
        {
            get
            {
                return _Nickname;
            }
            set
            {
                if ((_Nickname != value))
                {
                    OnNicknameChanging(value);
                    _Nickname = value;
                    OnNicknameChanged();
                }
            }
        }
    }
}
