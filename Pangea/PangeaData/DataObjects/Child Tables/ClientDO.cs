﻿////////////////////////////////////////////////////////////////////////////////
/// This is the Object use for representing the Enrollment.Client
/// Database Table.  This was creating by Linq to SQL
////////////////////////////////////////////////////////////////////////////////

using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace Pangea.Data.DataObjects
{
    [Table(Name = "[Schema].Client")]
    public partial class ClientDO
    {
        private bool? _Active;

        private int _Action_ID;

        private Guid _Action_User;

        private DateTime _Action_Date;

        private int? _Action_Reason;

        private Binary _TS;

        private long? _TSLONG;

        private long? _TSMAN;

        private int _ID;

        private Guid _Client_ID;

        private int? _Client_Number;

        private string _MAC_Address;

        #region Extensibility Method Definitions
        partial void OnLoaded();
        partial void OnValidate(ChangeAction action);
        partial void OnCreated();
        partial void OnActiveChanging(bool? value);
        partial void OnActiveChanged();
        partial void OnAction_IDChanging(int value);
        partial void OnAction_IDChanged();
        partial void OnAction_UserChanging(Guid value);
        partial void OnAction_UserChanged();
        partial void OnAction_DateChanging(DateTime value);
        partial void OnAction_DateChanged();
        partial void OnAction_ReasonChanging(int? value);
        partial void OnAction_ReasonChanged();
        partial void OnTSChanging(Binary value);
        partial void OnTSChanged();
        partial void OnTSLONGChanging(long? value);
        partial void OnTSLONGChanged();
        partial void OnTSMANChanging(long? value);
        partial void OnTSMANChanged();
        partial void OnIDChanging(int value);
        partial void OnIDChanged();
        partial void OnClient_IDChanging(Guid value);
        partial void OnClient_IDChanged();
        partial void OnClient_NumberChanging(int? value);
        partial void OnClient_NumberChanged();
        partial void OnMAC_AddressChanging(string value);
        partial void OnMAC_AddressChanged();
        #endregion

        public ClientDO()
        {
            OnCreated();
        }

        [Column(Storage = "_Active", DbType = "Bit", UpdateCheck = UpdateCheck.Never)]
        public bool? Active
        {
            get
            {
                return _Active;
            }
            set
            {
                if ((_Active != value))
                {
                    OnActiveChanging(value);
                    _Active = value;
                    OnActiveChanged();
                }
            }
        }

        [Column(Storage = "_Action_ID", DbType = "Int NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public int Action_ID
        {
            get
            {
                return _Action_ID;
            }
            set
            {
                if ((_Action_ID != value))
                {
                    OnAction_IDChanging(value);
                    _Action_ID = value;
                    OnAction_IDChanged();
                }
            }
        }

        [Column(Storage = "_Action_User", DbType = "UniqueIdentifier NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public Guid Action_User
        {
            get
            {
                return _Action_User;
            }
            set
            {
                if ((_Action_User != value))
                {
                    OnAction_UserChanging(value);
                    _Action_User = value;
                    OnAction_UserChanged();
                }
            }
        }

        [Column(Storage = "_Action_Date", DbType = "DateTime NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public DateTime Action_Date
        {
            get
            {
                return _Action_Date;
            }
            set
            {
                if ((_Action_Date != value))
                {
                    OnAction_DateChanging(value);
                    _Action_Date = value;
                    OnAction_DateChanged();
                }
            }
        }

        [Column(Storage = "_Action_Reason", DbType = "Int", UpdateCheck = UpdateCheck.Never)]
        public int? Action_Reason
        {
            get
            {
                return _Action_Reason;
            }
            set
            {
                if ((_Action_Reason != value))
                {
                    OnAction_ReasonChanging(value);
                    _Action_Reason = value;
                    OnAction_ReasonChanged();
                }
            }
        }

        [Column(Storage = "_TS", AutoSync = AutoSync.Always, DbType = "rowversion NOT NULL", CanBeNull = false, IsDbGenerated = true, IsVersion = true, UpdateCheck = UpdateCheck.Never)]
        public Binary TS
        {
            get
            {
                return _TS;
            }
            set
            {
                if ((_TS != value))
                {
                    OnTSChanging(value);
                    _TS = value;
                    OnTSChanged();
                }
            }
        }

        [Column(Storage = "_TSLONG", AutoSync = AutoSync.Always, DbType = "BigInt", IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)]
        public long? TSLONG
        {
            get
            {
                return _TSLONG;
            }
            set
            {
                if ((_TSLONG != value))
                {
                    OnTSLONGChanging(value);
                    _TSLONG = value;
                    OnTSLONGChanged();
                }
            }
        }

        [Column(Storage = "_TSMAN", DbType = "BigInt", UpdateCheck = UpdateCheck.Never)]
        public long? TSMAN
        {
            get
            {
                return _TSMAN;
            }
            set
            {
                if ((_TSMAN != value))
                {
                    OnTSMANChanging(value);
                    _TSMAN = value;
                    OnTSMANChanged();
                }
            }
        }

        [Column(Storage = "_ID", AutoSync = AutoSync.Always, DbType = "Int NOT NULL IDENTITY", IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)]
        public int ID
        {
            get
            {
                return _ID;
            }
            set
            {
                if ((_ID != value))
                {
                    OnIDChanging(value);
                    _ID = value;
                    OnIDChanged();
                }
            }
        }

        [Column(Storage = "_Client_ID", DbType = "UniqueIdentifier NOT NULL", IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never)]
        public Guid Client_ID
        {
            get
            {
                return _Client_ID;
            }
            set
            {
                if ((_Client_ID != value))
                {
                    OnClient_IDChanging(value);
                    _Client_ID = value;
                    OnClient_IDChanged();
                }
            }
        }

        [Column(Storage = "_Client_Number", DbType = "Int", UpdateCheck = UpdateCheck.Never)]
        public int? Client_Number
        {
            get
            {
                return _Client_Number;
            }
            set
            {
                if ((_Client_Number != value))
                {
                    OnClient_NumberChanging(value);
                    _Client_Number = value;
                    OnClient_NumberChanged();
                }
            }
        }

        [Column(Storage = "_MAC_Address", DbType = "VarChar(36)", UpdateCheck = UpdateCheck.Never)]
        public string MAC_Address
        {
            get
            {
                return _MAC_Address;
            }
            set
            {
                if ((_MAC_Address != value))
                {
                    OnMAC_AddressChanging(value);
                    _MAC_Address = value;
                    OnMAC_AddressChanged();
                }
            }
        }
    }
}
