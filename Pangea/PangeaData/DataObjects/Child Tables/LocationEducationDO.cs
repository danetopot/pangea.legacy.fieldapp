﻿////////////////////////////////////////////////////////////////////////////////
/// This is the Object use for representing the Enrollment.Location_Education
/// Database Table.  This was creating by Linq to SQL
////////////////////////////////////////////////////////////////////////////////

using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace Pangea.Data.DataObjects
{
    [Table(Name = "[Schema].Location_Education")]
    public partial class LocationEducationDO
    {
        private bool? _Active;

        private int _Action_ID;

        private Guid _Action_User;

        private DateTime _Action_Date;

        private int? _Action_Reason;

        private Binary _TS;

        private long? _TSLONG;

        private long? _TSMAN;

        private DateTime? _BlackBaud_Uploaded;

        private int _Location_Education_ID;

        private int _Location_Code_ID;

        private string _Description;

        #region Extensibility Method Definitions
        partial void OnLoaded();
        partial void OnValidate(ChangeAction action);
        partial void OnCreated();
        partial void OnActiveChanging(bool? value);
        partial void OnActiveChanged();
        partial void OnAction_IDChanging(int value);
        partial void OnAction_IDChanged();
        partial void OnAction_UserChanging(Guid value);
        partial void OnAction_UserChanged();
        partial void OnAction_DateChanging(DateTime value);
        partial void OnAction_DateChanged();
        partial void OnAction_ReasonChanging(int? value);
        partial void OnAction_ReasonChanged();
        partial void OnTSChanging(Binary value);
        partial void OnTSChanged();
        partial void OnTSLONGChanging(long? value);
        partial void OnTSLONGChanged();
        partial void OnTSMANChanging(long? value);
        partial void OnTSMANChanged();
        partial void OnBlackBaud_UploadedChanging(DateTime? value);
        partial void OnBlackBaud_UploadedChanged();
        partial void OnLocation_Education_IDChanging(int value);
        partial void OnLocation_Education_IDChanged();
        partial void OnLocation_Code_IDChanging(int value);
        partial void OnLocation_Code_IDChanged();
        partial void OnDescriptionChanging(string value);
        partial void OnDescriptionChanged();
        #endregion

        public LocationEducationDO()
        {
            OnCreated();
        }

        [Column(Storage = "_Active", DbType = "Bit", UpdateCheck = UpdateCheck.Never)]
        public bool? Active
        {
            get
            {
                return _Active;
            }
            set
            {
                if ((_Active != value))
                {
                    OnActiveChanging(value);
                    _Active = value;
                    OnActiveChanged();
                }
            }
        }

        [Column(Storage = "_Action_ID", DbType = "Int NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public int Action_ID
        {
            get
            {
                return _Action_ID;
            }
            set
            {
                if ((_Action_ID != value))
                {
                    OnAction_IDChanging(value);
                    _Action_ID = value;
                    OnAction_IDChanged();
                }
            }
        }

        [Column(Storage = "_Action_User", DbType = "UniqueIdentifier NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public Guid Action_User
        {
            get
            {
                return _Action_User;
            }
            set
            {
                if ((_Action_User != value))
                {
                    OnAction_UserChanging(value);
                    _Action_User = value;
                    OnAction_UserChanged();
                }
            }
        }

        [Column(Storage = "_Action_Date", DbType = "DateTime NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public DateTime Action_Date
        {
            get
            {
                return _Action_Date;
            }
            set
            {
                if ((_Action_Date != value))
                {
                    OnAction_DateChanging(value);
                    _Action_Date = value;
                    OnAction_DateChanged();
                }
            }
        }

        [Column(Storage = "_Action_Reason", DbType = "Int", UpdateCheck = UpdateCheck.Never)]
        public int? Action_Reason
        {
            get
            {
                return _Action_Reason;
            }
            set
            {
                if ((_Action_Reason != value))
                {
                    OnAction_ReasonChanging(value);
                    _Action_Reason = value;
                    OnAction_ReasonChanged();
                }
            }
        }

        [Column(Storage = "_TS", AutoSync = AutoSync.Always, DbType = "rowversion NOT NULL", CanBeNull = false, IsDbGenerated = true, IsVersion = true, UpdateCheck = UpdateCheck.Never)]
        public Binary TS
        {
            get
            {
                return _TS;
            }
            set
            {
                if ((_TS != value))
                {
                    OnTSChanging(value);
                    _TS = value;
                    OnTSChanged();
                }
            }
        }

        [Column(Storage = "_TSLONG", AutoSync = AutoSync.Always, DbType = "BigInt", IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)]
        public long? TSLONG
        {
            get
            {
                return _TSLONG;
            }
            set
            {
                if ((_TSLONG != value))
                {
                    OnTSLONGChanging(value);
                    _TSLONG = value;
                    OnTSLONGChanged();
                }
            }
        }

        [Column(Storage = "_TSMAN", DbType = "BigInt", UpdateCheck = UpdateCheck.Never)]
        public long? TSMAN
        {
            get
            {
                return _TSMAN;
            }
            set
            {
                if ((_TSMAN != value))
                {
                    OnTSMANChanging(value);
                    _TSMAN = value;
                    OnTSMANChanged();
                }
            }
        }

        [Column(Storage = "_BlackBaud_Uploaded", DbType = "DateTime", UpdateCheck = UpdateCheck.Never)]
        public DateTime? BlackBaud_Uploaded
        {
            get
            {
                return _BlackBaud_Uploaded;
            }
            set
            {
                if ((_BlackBaud_Uploaded != value))
                {
                    OnBlackBaud_UploadedChanging(value);
                    _BlackBaud_Uploaded = value;
                    OnBlackBaud_UploadedChanged();
                }
            }
        }

        [Column(Storage = "_Location_Education_ID", AutoSync = AutoSync.OnInsert, DbType = "Int NOT NULL IDENTITY", IsPrimaryKey = true, IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)]
        public int Location_Education_ID
        {
            get
            {
                return _Location_Education_ID;
            }
            set
            {
                if ((_Location_Education_ID != value))
                {
                    OnLocation_Education_IDChanging(value);
                    _Location_Education_ID = value;
                    OnLocation_Education_IDChanged();
                }
            }
        }

        [Column(Storage = "_Location_Code_ID", DbType = "Int NOT NULL", IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never)]
        public int Location_Code_ID
        {
            get
            {
                return _Location_Code_ID;
            }
            set
            {
                if ((_Location_Code_ID != value))
                {
                    OnLocation_Code_IDChanging(value);
                    _Location_Code_ID = value;
                    OnLocation_Code_IDChanged();
                }
            }
        }

        [Column(Storage = "_Description", DbType = "NVarChar(255)", UpdateCheck = UpdateCheck.Never)]
        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                if ((_Description != value))
                {
                    OnDescriptionChanging(value);
                    _Description = value;
                    OnDescriptionChanged();
                }
            }
        }
    }
}
