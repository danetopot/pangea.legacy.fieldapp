﻿////////////////////////////////////////////////////////////////////////////////
/// This is the Object use for representing the Enrollment.File_Data
/// Database Table.  This was creating by Linq to SQL
////////////////////////////////////////////////////////////////////////////////

using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace Pangea.Data.DataObjects
{
    [Table(Name = "[Schema].File_Data")]
    public partial class FileDataDO
    {
        private bool? _Active;

        private int _Action_ID;

        private Guid _Action_User;

        private DateTime _Action_Date;

        private int? _Action_Reason;

        private Binary _TS;

        private long? _TSLONG;

        private long? _TSMAN;

        private DateTime? _BlackBaud_Uploaded;

        private int _File_Data_ID;

        private Guid _Child_ID;

        private int? _File_Type_ID;

        private Guid _stream_id;

        private int _Content_Type_ID;

        private string _URI;

        private Decimal? _Geo_Latitude;

        private Decimal? _Geo_Longitude;

        private string _Make;

        private string _Model;

        private string _Software;

        private DateTime? _DateTime;

        private DateTime? _DateTimeOriginal;

        private DateTime? _DateTimeDigitized;

        private string _GPSVersionID;

        private string _GPSLatitudeRef;

        private string _GPSLatitude;

        private string _GPSLongitudeRef;

        private string _GPSLongitude;

        private string _GPSAltitudeRef;

        private string _GPSAltitude;

        private string _GPSTimeStamp;

        private string _GPSImgDirectionRef;

        private Decimal? _GPSImgDirection;

        private string _GPSDateStamp;

        #region Extensibility Method Definitions
        partial void OnLoaded();
        partial void OnValidate(ChangeAction action);
        partial void OnCreated();
        partial void OnActiveChanging(bool? value);
        partial void OnActiveChanged();
        partial void OnAction_IDChanging(int value);
        partial void OnAction_IDChanged();
        partial void OnAction_UserChanging(Guid value);
        partial void OnAction_UserChanged();
        partial void OnAction_DateChanging(DateTime value);
        partial void OnAction_DateChanged();
        partial void OnAction_ReasonChanging(int? value);
        partial void OnAction_ReasonChanged();
        partial void OnTSChanging(Binary value);
        partial void OnTSChanged();
        partial void OnTSLONGChanging(long? value);
        partial void OnTSLONGChanged();
        partial void OnTSMANChanging(long? value);
        partial void OnTSMANChanged();
        partial void OnBlackBaud_UploadedChanging(DateTime? value);
        partial void OnBlackBaud_UploadedChanged();
        partial void OnFile_Data_IDChanging(int value);
        partial void OnFile_Data_IDChanged();
        partial void OnChild_IDChanging(Guid value);
        partial void OnChild_IDChanged();
        partial void OnFile_Type_IDChanging(int? value);
        partial void OnFile_Type_IDChanged();
        partial void Onstream_idChanging(Guid value);
        partial void Onstream_idChanged();
        partial void OnContent_Type_IDChanging(int value);
        partial void OnContent_Type_IDChanged();
        partial void OnURIChanging(string value);
        partial void OnURIChanged();
        partial void OnGeo_LatitudeChanging(Decimal? value);
        partial void OnGeo_LatitudeChanged();
        partial void OnGeo_LongitudeChanging(Decimal? value);
        partial void OnGeo_LongitudeChanged();
        partial void OnMakeChanging(string value);
        partial void OnMakeChanged();
        partial void OnModelChanging(string value);
        partial void OnModelChanged();
        partial void OnSoftwareChanging(string value);
        partial void OnSoftwareChanged();
        partial void OnDateTimeChanging(DateTime? value);
        partial void OnDateTimeChanged();
        partial void OnDateTimeOriginalChanging(DateTime? value);
        partial void OnDateTimeOriginalChanged();
        partial void OnDateTimeDigitizedChanging(DateTime? value);
        partial void OnDateTimeDigitizedChanged();
        partial void OnGPSVersionIDChanging(string value);
        partial void OnGPSVersionIDChanged();
        partial void OnGPSLatitudeRefChanging(string value);
        partial void OnGPSLatitudeRefChanged();
        partial void OnGPSLatitudeChanging(string value);
        partial void OnGPSLatitudeChanged();
        partial void OnGPSLongitudeRefChanging(string value);
        partial void OnGPSLongitudeRefChanged();
        partial void OnGPSLongitudeChanging(string value);
        partial void OnGPSLongitudeChanged();
        partial void OnGPSAltitudeRefChanging(string value);
        partial void OnGPSAltitudeRefChanged();
        partial void OnGPSAltitudeChanging(string value);
        partial void OnGPSAltitudeChanged();
        partial void OnGPSTimeStampChanging(string value);
        partial void OnGPSTimeStampChanged();
        partial void OnGPSImgDirectionRefChanging(string value);
        partial void OnGPSImgDirectionRefChanged();
        partial void OnGPSImgDirectionChanging(Decimal? value);
        partial void OnGPSImgDirectionChanged();
        partial void OnGPSDateStampChanging(string value);
        partial void OnGPSDateStampChanged();
        #endregion

        public FileDataDO()
        {
            OnCreated();
        }

        [Column(Storage = "_Active", DbType = "Bit", UpdateCheck = UpdateCheck.Never)]
        public bool? Active
        {
            get
            {
                return _Active;
            }
            set
            {
                if ((_Active != value))
                {
                    OnActiveChanging(value);
                    _Active = value;
                    OnActiveChanged();
                }
            }
        }

        [Column(Storage = "_Action_ID", DbType = "Int NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public int Action_ID
        {
            get
            {
                return _Action_ID;
            }
            set
            {
                if ((_Action_ID != value))
                {
                    OnAction_IDChanging(value);
                    _Action_ID = value;
                    OnAction_IDChanged();
                }
            }
        }

        [Column(Storage = "_Action_User", DbType = "UniqueIdentifier NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public Guid Action_User
        {
            get
            {
                return _Action_User;
            }
            set
            {
                if ((_Action_User != value))
                {
                    OnAction_UserChanging(value);
                    _Action_User = value;
                    OnAction_UserChanged();
                }
            }
        }

        [Column(Storage = "_Action_Date", DbType = "DateTime NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public DateTime Action_Date
        {
            get
            {
                return _Action_Date;
            }
            set
            {
                if ((_Action_Date != value))
                {
                    OnAction_DateChanging(value);
                    _Action_Date = value;
                    OnAction_DateChanged();
                }
            }
        }

        [Column(Storage = "_Action_Reason", DbType = "Int", UpdateCheck = UpdateCheck.Never)]
        public int? Action_Reason
        {
            get
            {
                return _Action_Reason;
            }
            set
            {
                if ((_Action_Reason != value))
                {
                    OnAction_ReasonChanging(value);
                    _Action_Reason = value;
                    OnAction_ReasonChanged();
                }
            }
        }

        [Column(Storage = "_TS", AutoSync = AutoSync.Always, DbType = "rowversion NOT NULL", CanBeNull = false, IsDbGenerated = true, IsVersion = true, UpdateCheck = UpdateCheck.Never)]
        public Binary TS
        {
            get
            {
                return _TS;
            }
            set
            {
                if ((_TS != value))
                {
                    OnTSChanging(value);
                    _TS = value;
                    OnTSChanged();
                }
            }
        }

        [Column(Storage = "_TSLONG", AutoSync = AutoSync.Always, DbType = "BigInt", IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)]
        public long? TSLONG
        {
            get
            {
                return _TSLONG;
            }
            set
            {
                if ((_TSLONG != value))
                {
                    OnTSLONGChanging(value);
                    _TSLONG = value;
                    OnTSLONGChanged();
                }
            }
        }

        [Column(Storage = "_TSMAN", DbType = "BigInt", UpdateCheck = UpdateCheck.Never)]
        public long? TSMAN
        {
            get
            {
                return _TSMAN;
            }
            set
            {
                if ((_TSMAN != value))
                {
                    OnTSMANChanging(value);
                    _TSMAN = value;
                    OnTSMANChanged();
                }
            }
        }

        [Column(Storage = "_BlackBaud_Uploaded", DbType = "DateTime", UpdateCheck = UpdateCheck.Never)]
        public DateTime? BlackBaud_Uploaded
        {
            get
            {
                return _BlackBaud_Uploaded;
            }
            set
            {
                if ((_BlackBaud_Uploaded != value))
                {
                    OnBlackBaud_UploadedChanging(value);
                    _BlackBaud_Uploaded = value;
                    OnBlackBaud_UploadedChanged();
                }
            }
        }

        [Column(Storage = "_File_Data_ID", AutoSync = AutoSync.OnInsert, DbType = "Int NOT NULL IDENTITY", IsPrimaryKey = true, IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)]
        public int File_Data_ID
        {
            get
            {
                return _File_Data_ID;
            }
            set
            {
                if ((_File_Data_ID != value))
                {
                    OnFile_Data_IDChanging(value);
                    _File_Data_ID = value;
                    OnFile_Data_IDChanged();
                }
            }
        }

        [Column(Storage = "_Child_ID", DbType = "UniqueIdentifier NOT NULL", IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never)]
        public Guid Child_ID
        {
            get
            {
                return _Child_ID;
            }
            set
            {
                if ((_Child_ID != value))
                {
                    OnChild_IDChanging(value);
                    _Child_ID = value;
                    OnChild_IDChanged();
                }
            }
        }

        [Column(Storage = "_File_Type_ID", DbType = "Int", UpdateCheck = UpdateCheck.Never)]
        public int? File_Type_ID
        {
            get
            {
                return _File_Type_ID;
            }
            set
            {
                if ((_File_Type_ID != value))
                {
                    OnFile_Type_IDChanging(value);
                    _File_Type_ID = value;
                    OnFile_Type_IDChanged();
                }
            }
        }

        [Column(Storage = "_stream_id", DbType = "UniqueIdentifier NOT NULL", IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never)]
        public Guid stream_id
        {
            get
            {
                return _stream_id;
            }
            set
            {
                if ((_stream_id != value))
                {
                    Onstream_idChanging(value);
                    _stream_id = value;
                    Onstream_idChanged();
                }
            }
        }

        [Column(Storage = "_Content_Type_ID", DbType = "Int NOT NULL", IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never)]
        public int Content_Type_ID
        {
            get
            {
                return _Content_Type_ID;
            }
            set
            {
                if ((_Content_Type_ID != value))
                {
                    OnContent_Type_IDChanging(value);
                    _Content_Type_ID = value;
                    OnContent_Type_IDChanged();
                }
            }
        }

        [Column(Storage = "_URI", DbType = "NVarChar(255)", UpdateCheck = UpdateCheck.Never)]
        public string URI
        {
            get
            {
                return _URI;
            }
            set
            {
                if ((_URI != value))
                {
                    OnURIChanging(value);
                    _URI = value;
                    OnURIChanged();
                }
            }
        }

        [Column(Storage = "_Geo_Latitude", DbType = "Decimal(7,4)", UpdateCheck = UpdateCheck.Never)]
        public Decimal? Geo_Latitude
        {
            get
            {
                return _Geo_Latitude;
            }
            set
            {
                if ((_Geo_Latitude != value))
                {
                    OnGeo_LatitudeChanging(value);
                    _Geo_Latitude = value;
                    OnGeo_LatitudeChanged();
                }
            }
        }

        [Column(Storage = "_Geo_Longitude", DbType = "Decimal(7,4)", UpdateCheck = UpdateCheck.Never)]
        public Decimal? Geo_Longitude
        {
            get
            {
                return _Geo_Longitude;
            }
            set
            {
                if ((_Geo_Longitude != value))
                {
                    OnGeo_LongitudeChanging(value);
                    _Geo_Longitude = value;
                    OnGeo_LongitudeChanged();
                }
            }
        }

        [Column(Storage = "_Make", DbType = "NVarChar(250)", UpdateCheck = UpdateCheck.Never)]
        public string Make
        {
            get
            {
                return _Make;
            }
            set
            {
                if ((_Make != value))
                {
                    OnMakeChanging(value);
                    _Make = value;
                    OnMakeChanged();
                }
            }
        }

        [Column(Storage = "_Model", DbType = "NVarChar(250)", UpdateCheck = UpdateCheck.Never)]
        public string Model
        {
            get
            {
                return _Model;
            }
            set
            {
                if ((_Model != value))
                {
                    OnModelChanging(value);
                    _Model = value;
                    OnModelChanged();
                }
            }
        }

        [Column(Storage = "_Software", DbType = "NVarChar(250)", UpdateCheck = UpdateCheck.Never)]
        public string Software
        {
            get
            {
                return _Software;
            }
            set
            {
                if ((_Software != value))
                {
                    OnSoftwareChanging(value);
                    _Software = value;
                    OnSoftwareChanged();
                }
            }
        }

        [Column(Storage = "_DateTime", DbType = "DateTime", UpdateCheck = UpdateCheck.Never)]
        public DateTime? DateTime
        {
            get
            {
                return _DateTime;
            }
            set
            {
                if ((_DateTime != value))
                {
                    OnDateTimeChanging(value);
                    _DateTime = value;
                    OnDateTimeChanged();
                }
            }
        }

        [Column(Storage = "_DateTimeOriginal", DbType = "DateTime", UpdateCheck = UpdateCheck.Never)]
        public DateTime? DateTimeOriginal
        {
            get
            {
                return _DateTimeOriginal;
            }
            set
            {
                if ((_DateTimeOriginal != value))
                {
                    OnDateTimeOriginalChanging(value);
                    _DateTimeOriginal = value;
                    OnDateTimeOriginalChanged();
                }
            }
        }

        [Column(Storage = "_DateTimeDigitized", DbType = "DateTime", UpdateCheck = UpdateCheck.Never)]
        public DateTime? DateTimeDigitized
        {
            get
            {
                return _DateTimeDigitized;
            }
            set
            {
                if ((_DateTimeDigitized != value))
                {
                    OnDateTimeDigitizedChanging(value);
                    _DateTimeDigitized = value;
                    OnDateTimeDigitizedChanged();
                }
            }
        }

        [Column(Storage = "_GPSVersionID", DbType = "NVarChar(13)", UpdateCheck = UpdateCheck.Never)]
        public string GPSVersionID
        {
            get
            {
                return _GPSVersionID;
            }
            set
            {
                if ((_GPSVersionID != value))
                {
                    OnGPSVersionIDChanging(value);
                    _GPSVersionID = value;
                    OnGPSVersionIDChanged();
                }
            }
        }

        [Column(Storage = "_GPSLatitudeRef", DbType = "NVarChar(50)", UpdateCheck = UpdateCheck.Never)]
        public string GPSLatitudeRef
        {
            get
            {
                return _GPSLatitudeRef;
            }
            set
            {
                if ((_GPSLatitudeRef != value))
                {
                    OnGPSLatitudeRefChanging(value);
                    _GPSLatitudeRef = value;
                    OnGPSLatitudeRefChanged();
                }
            }
        }

        [Column(Storage = "_GPSLatitude", DbType = "NVarChar(69)", UpdateCheck = UpdateCheck.Never)]
        public string GPSLatitude
        {
            get
            {
                return _GPSLatitude;
            }
            set
            {
                if ((_GPSLatitude != value))
                {
                    OnGPSLatitudeChanging(value);
                    _GPSLatitude = value;
                    OnGPSLatitudeChanged();
                }
            }
        }

        [Column(Storage = "_GPSLongitudeRef", DbType = "NVarChar(50)", UpdateCheck = UpdateCheck.Never)]
        public string GPSLongitudeRef
        {
            get
            {
                return _GPSLongitudeRef;
            }
            set
            {
                if ((_GPSLongitudeRef != value))
                {
                    OnGPSLongitudeRefChanging(value);
                    _GPSLongitudeRef = value;
                    OnGPSLongitudeRefChanged();
                }
            }
        }

        [Column(Storage = "_GPSLongitude", DbType = "NVarChar(69)", UpdateCheck = UpdateCheck.Never)]
        public string GPSLongitude
        {
            get
            {
                return _GPSLongitude;
            }
            set
            {
                if ((_GPSLongitude != value))
                {
                    OnGPSLongitudeChanging(value);
                    _GPSLongitude = value;
                    OnGPSLongitudeChanged();
                }
            }
        }

        [Column(Storage = "_GPSAltitudeRef", DbType = "NVarChar(8)", UpdateCheck = UpdateCheck.Never)]
        public string GPSAltitudeRef
        {
            get
            {
                return _GPSAltitudeRef;
            }
            set
            {
                if ((_GPSAltitudeRef != value))
                {
                    OnGPSAltitudeRefChanging(value);
                    _GPSAltitudeRef = value;
                    OnGPSAltitudeRefChanged();
                }
            }
        }

        [Column(Storage = "_GPSAltitude", DbType = "NVarChar(64)", UpdateCheck = UpdateCheck.Never)]
        public string GPSAltitude
        {
            get
            {
                return _GPSAltitude;
            }
            set
            {
                if ((_GPSAltitude != value))
                {
                    OnGPSAltitudeChanging(value);
                    _GPSAltitude = value;
                    OnGPSAltitudeChanged();
                }
            }
        }

        [Column(Storage = "_GPSTimeStamp", DbType = "NVarChar(69)", UpdateCheck = UpdateCheck.Never)]
        public string GPSTimeStamp
        {
            get
            {
                return _GPSTimeStamp;
            }
            set
            {
                if ((_GPSTimeStamp != value))
                {
                    OnGPSTimeStampChanging(value);
                    _GPSTimeStamp = value;
                    OnGPSTimeStampChanged();
                }
            }
        }

        [Column(Storage = "_GPSImgDirectionRef", DbType = "NVarChar(10)", UpdateCheck = UpdateCheck.Never)]
        public string GPSImgDirectionRef
        {
            get
            {
                return _GPSImgDirectionRef;
            }
            set
            {
                if ((_GPSImgDirectionRef != value))
                {
                    OnGPSImgDirectionRefChanging(value);
                    _GPSImgDirectionRef = value;
                    OnGPSImgDirectionRefChanged();
                }
            }
        }

        [Column(Storage = "_GPSImgDirection", DbType = "Decimal(5,2)", UpdateCheck = UpdateCheck.Never)]
        public Decimal? GPSImgDirection
        {
            get
            {
                return _GPSImgDirection;
            }
            set
            {
                if ((_GPSImgDirection != value))
                {
                    OnGPSImgDirectionChanging(value);
                    _GPSImgDirection = value;
                    OnGPSImgDirectionChanged();
                }
            }
        }

        [Column(Storage = "_GPSDateStamp", DbType = "NVarChar(69)", UpdateCheck = UpdateCheck.Never)]
        public string GPSDateStamp
        {
            get
            {
                return _GPSDateStamp;
            }
            set
            {
                if ((_GPSDateStamp != value))
                {
                    OnGPSDateStampChanging(value);
                    _GPSDateStamp = value;
                    OnGPSDateStampChanged();
                }
            }
        }
    }
}
