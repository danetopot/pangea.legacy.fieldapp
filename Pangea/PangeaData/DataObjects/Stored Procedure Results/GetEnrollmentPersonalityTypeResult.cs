﻿using System.Data.Linq.Mapping;

namespace Pangea.Data.DataObjects
{
    ////////////////////////////////////////////////////////////////////////////////
    /// This is the Child Personality Type for a saved enrollment child.
    ////////////////////////////////////////////////////////////////////////////////
    public partial class GetEnrollmentPersonalityTypeResult
    {
        private int _Personality_Type_Code_ID;

        public GetEnrollmentPersonalityTypeResult()
        {
        }

        [Column(Storage = "_Personality_Type_Code_ID", DbType = "Int NOT NULL")]
        public int Personality_Type_Code_ID
        {
            get { return _Personality_Type_Code_ID; }
            set
            {
                if ((_Personality_Type_Code_ID != value))
                {
                    _Personality_Type_Code_ID = value;
                }
            }
        }
    }
}
