﻿using System.Data.Linq.Mapping;

namespace Pangea.Data.DataObjects
{
    /// <summary>
    /// This is the Return information for the Getting
    /// The Major Life Event from the DB
    /// </summary>
    public partial class GetMajorLifeEventResult
    {
        private string _Description;

        public GetMajorLifeEventResult()
        {
        }

        [Column(Storage = "_Description", DbType = "NVarChar(255) NOT NULL", CanBeNull = false)]
        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                if ((_Description != value))
                {
                    _Description = value;
                }
            }
        }
    }
}
