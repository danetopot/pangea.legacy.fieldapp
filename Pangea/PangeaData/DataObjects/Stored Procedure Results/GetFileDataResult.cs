﻿using System;
using System.Data.Linq.Mapping;

namespace Pangea.Data.DataObjects
{
    public partial class GetFileDataResult
    {

        private int _File_Data_ID;

        private Guid _Child_ID;

        private int? _File_Type_ID;

        private int? _File_Type_Seq_No;

        private Guid _stream_id;

        private int _Content_Type_ID;

        private string _URI;

        private string _CDN_URL;

        private decimal? _Geo_Latitude;

        private decimal? _Geo_Longitude;

        private string _Make;

        private string _Model;

        private string _Software;

        private DateTime? _DateTime;

        private DateTime? _DateTimeOriginal;

        private DateTime? _DateTimeDigitized;

        private string _GPSVersionID;

        private string _GPSLatitudeRef;

        private string _GPSLatitude;

        private string _GPSLongitudeRef;

        private string _GPSLongitude;

        private string _GPSAltitudeRef;

        private string _GPSAltitude;

        private string _GPSTimeStamp;

        private string _GPSImgDirectionRef;

        private decimal? _GPSImgDirection;

        private string _GPSDateStamp;

        private int _Action_ID;

        public GetFileDataResult()
        {
        }

        [Column(Storage = "_File_Data_ID", DbType = "Int NOT NULL")]
        public int File_Data_ID
        {
            get
            {
                return _File_Data_ID;
            }
            set
            {
                if ((_File_Data_ID != value))
                {
                    _File_Data_ID = value;
                }
            }
        }

        [Column(Storage = "_Child_ID", DbType = "UniqueIdentifier NOT NULL")]
        public Guid Child_ID
        {
            get
            {
                return _Child_ID;
            }
            set
            {
                if ((_Child_ID != value))
                {
                    _Child_ID = value;
                }
            }
        }

        [Column(Storage = "_File_Type_ID", DbType = "Int")]
        public int? File_Type_ID
        {
            get
            {
                return _File_Type_ID;
            }
            set
            {
                if ((_File_Type_ID != value))
                {
                    _File_Type_ID = value;
                }
            }
        }

        [Column(Storage = "_File_Type_Seq_No", DbType = "Int")]
        public int? File_Type_Seq_No
        {
            get
            {
                return _File_Type_Seq_No;
            }
            set
            {
                if ((_File_Type_Seq_No != value))
                {
                    _File_Type_Seq_No = value;
                }
            }
        }

        [Column(Storage = "_stream_id", DbType = "UniqueIdentifier NOT NULL")]
        public Guid stream_id
        {
            get
            {
                return _stream_id;
            }
            set
            {
                if ((_stream_id != value))
                {
                    _stream_id = value;
                }
            }
        }

        [Column(Storage = "_Content_Type_ID", DbType = "Int NOT NULL")]
        public int Content_Type_ID
        {
            get
            {
                return _Content_Type_ID;
            }
            set
            {
                if ((_Content_Type_ID != value))
                {
                    _Content_Type_ID = value;
                }
            }
        }

        [Column(Storage = "_URI", DbType = "NVarChar(255)")]
        public string URI
        {
            get
            {
                return _URI;
            }
            set
            {
                if ((_URI != value))
                {
                    _URI = value;
                }
            }
        }

        [Column(Storage = "_CDN_URL", DbType = "NVarChar(255)")]
        public string CDN_URL
        {
            get
            {
                return _CDN_URL;
            }
            set
            {
                if ((_CDN_URL != value))
                {
                    _CDN_URL = value;
                }
            }
        }

        [Column(Storage = "_Geo_Latitude", DbType = "Decimal(7,4)")]
        public decimal? Geo_Latitude
        {
            get
            {
                return _Geo_Latitude;
            }
            set
            {
                if ((_Geo_Latitude != value))
                {
                    _Geo_Latitude = value;
                }
            }
        }

        [Column(Storage = "_Geo_Longitude", DbType = "Decimal(7,4)")]
        public decimal? Geo_Longitude
        {
            get
            {
                return _Geo_Longitude;
            }
            set
            {
                if ((_Geo_Longitude != value))
                {
                    _Geo_Longitude = value;
                }
            }
        }

        [Column(Storage = "_Make", DbType = "NVarChar(250)")]
        public string Make
        {
            get
            {
                return _Make;
            }
            set
            {
                if ((_Make != value))
                {
                    _Make = value;
                }
            }
        }

        [Column(Storage = "_Model", DbType = "NVarChar(250)")]
        public string Model
        {
            get
            {
                return _Model;
            }
            set
            {
                if ((_Model != value))
                {
                    _Model = value;
                }
            }
        }

        [Column(Storage = "_Software", DbType = "NVarChar(250)")]
        public string Software
        {
            get
            {
                return _Software;
            }
            set
            {
                if ((_Software != value))
                {
                    _Software = value;
                }
            }
        }

        [Column(Storage = "_DateTime", DbType = "DateTime")]
        public DateTime? DateTime
        {
            get
            {
                return _DateTime;
            }
            set
            {
                if ((_DateTime != value))
                {
                    _DateTime = value;
                }
            }
        }

        [Column(Storage = "_DateTimeOriginal", DbType = "DateTime")]
        public DateTime? DateTimeOriginal
        {
            get
            {
                return _DateTimeOriginal;
            }
            set
            {
                if ((_DateTimeOriginal != value))
                {
                    _DateTimeOriginal = value;
                }
            }
        }

        [Column(Storage = "_DateTimeDigitized", DbType = "DateTime")]
        public DateTime? DateTimeDigitized
        {
            get
            {
                return _DateTimeDigitized;
            }
            set
            {
                if ((_DateTimeDigitized != value))
                {
                    _DateTimeDigitized = value;
                }
            }
        }

        [Column(Storage = "_GPSVersionID", DbType = "NVarChar(13)")]
        public string GPSVersionID
        {
            get
            {
                return _GPSVersionID;
            }
            set
            {
                if ((_GPSVersionID != value))
                {
                    _GPSVersionID = value;
                }
            }
        }

        [Column(Storage = "_GPSLatitudeRef", DbType = "NVarChar(50)")]
        public string GPSLatitudeRef
        {
            get
            {
                return _GPSLatitudeRef;
            }
            set
            {
                if ((_GPSLatitudeRef != value))
                {
                    _GPSLatitudeRef = value;
                }
            }
        }

        [Column(Storage = "_GPSLatitude", DbType = "NVarChar(69)")]
        public string GPSLatitude
        {
            get
            {
                return _GPSLatitude;
            }
            set
            {
                if ((_GPSLatitude != value))
                {
                    _GPSLatitude = value;
                }
            }
        }

        [Column(Storage = "_GPSLongitudeRef", DbType = "NVarChar(50)")]
        public string GPSLongitudeRef
        {
            get
            {
                return _GPSLongitudeRef;
            }
            set
            {
                if ((_GPSLongitudeRef != value))
                {
                    _GPSLongitudeRef = value;
                }
            }
        }

        [Column(Storage = "_GPSLongitude", DbType = "NVarChar(69)")]
        public string GPSLongitude
        {
            get
            {
                return _GPSLongitude;
            }
            set
            {
                if ((_GPSLongitude != value))
                {
                    _GPSLongitude = value;
                }
            }
        }

        [Column(Storage = "_GPSAltitudeRef", DbType = "NVarChar(8)")]
        public string GPSAltitudeRef
        {
            get
            {
                return _GPSAltitudeRef;
            }
            set
            {
                if ((_GPSAltitudeRef != value))
                {
                    _GPSAltitudeRef = value;
                }
            }
        }

        [Column(Storage = "_GPSAltitude", DbType = "NVarChar(64)")]
        public string GPSAltitude
        {
            get
            {
                return _GPSAltitude;
            }
            set
            {
                if ((_GPSAltitude != value))
                {
                    _GPSAltitude = value;
                }
            }
        }

        [Column(Storage = "_GPSTimeStamp", DbType = "NVarChar(69)")]
        public string GPSTimeStamp
        {
            get
            {
                return _GPSTimeStamp;
            }
            set
            {
                if ((_GPSTimeStamp != value))
                {
                    _GPSTimeStamp = value;
                }
            }
        }

        [Column(Storage = "_GPSImgDirectionRef", DbType = "NVarChar(10)")]
        public string GPSImgDirectionRef
        {
            get
            {
                return _GPSImgDirectionRef;
            }
            set
            {
                if ((_GPSImgDirectionRef != value))
                {
                    _GPSImgDirectionRef = value;
                }
            }
        }

        [Column(Storage = "_GPSImgDirection", DbType = "Decimal(5,2)")]
        public decimal? GPSImgDirection
        {
            get
            {
                return _GPSImgDirection;
            }
            set
            {
                if ((_GPSImgDirection != value))
                {
                    _GPSImgDirection = value;
                }
            }
        }

        [Column(Storage = "_GPSDateStamp", DbType = "NVarChar(69)")]
        public string GPSDateStamp
        {
            get
            {
                return _GPSDateStamp;
            }
            set
            {
                if ((_GPSDateStamp != value))
                {
                    _GPSDateStamp = value;
                }
            }
        }

        [Column(Storage = "_Action_ID", DbType = "Int NOT NULL")]
        public int Action_ID
        {
            get
            {
                return _Action_ID;
            }
            set
            {
                if ((_Action_ID != value))
                {
                    _Action_ID = value;
                }
            }
        }
    }
}
