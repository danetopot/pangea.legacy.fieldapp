﻿using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace Pangea.Data.DataObjects
{
    public partial class GetFileResult
    {

        private Guid _stream_id;

        private Binary _file_stream;

        private string _name;

        private Guid? _primary_stream_id;

        public GetFileResult()
        {
        }

        [Column(Storage = "_stream_id", DbType = "UniqueIdentifier NOT NULL")]
        public Guid stream_id
        {
            get
            {
                return _stream_id;
            }
            set
            {
                if ((_stream_id != value))
                {
                    _stream_id = value;
                }
            }
        }

        [Column(Storage = "_file_stream", DbType = "VarBinary(MAX)")]
        public Binary file_stream
        {
            get
            {
                return _file_stream;
            }
            set
            {
                if ((_file_stream != value))
                {
                    _file_stream = value;
                }
            }
        }

        [Column(Storage = "_name", DbType = "NVarChar(255) NOT NULL", CanBeNull = false)]
        public string name
        {
            get
            {
                return _name;
            }
            set
            {
                if ((_name != value))
                {
                    _name = value;
                }
            }
        }

        [Column(Storage = "_primary_stream_id", DbType = "UniqueIdentifier")]
        public Guid? primary_stream_id
        {
            get
            {
                return _primary_stream_id;
            }
            set
            {
                if ((_primary_stream_id != value))
                {
                    _primary_stream_id = value;
                }
            }
        }
    }
}
