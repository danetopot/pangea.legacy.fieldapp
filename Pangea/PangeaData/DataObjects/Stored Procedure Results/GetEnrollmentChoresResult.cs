﻿using System.Data.Linq.Mapping;

namespace Pangea.Data.DataObjects
{
    ////////////////////////////////////////////////////////////////////////////////
    /// This is the Child Chore Information for a saved enrollment child.
    ////////////////////////////////////////////////////////////////////////////////
    public partial class GetEnrollmentChoresResult
    {
		private int _Chore_Code_ID;
		
		public GetEnrollmentChoresResult()
		{
		}
		
		[Column(Storage="_Chore_Code_ID", DbType="Int NOT NULL")]
		public int Chore_Code_ID
		{
			get { return _Chore_Code_ID; }
			set
			{
				if ((_Chore_Code_ID != value))
				{
					_Chore_Code_ID = value;
				}
			}
		}
	}
}
