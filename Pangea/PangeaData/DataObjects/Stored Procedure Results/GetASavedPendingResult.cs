﻿using System;
using System.Data.Linq.Mapping;

namespace Pangea.Data.DataObjects
{
    public partial class GetASavedPendingResult
    {

        private Guid _Child_ID;

        private long? _Child_Number;

        private string _First_Name;

        private string _Middle_Name;

        private string _Last_Name;

        private DateTime? _Date_of_Birth;

        private int? _Grade_Level_Code_ID;

        private int? _Health_Status_Code_ID;

        private int? _Lives_With_Code_ID;

        private int? _Favorite_Learning_Code_ID;

        private int? _Gender_Code_ID;

        private byte? _Number_Brothers;

        private byte? _Number_Sisters;

        private bool? _Disability_Status;

        private int? _Location_Code_ID;

        private string _Nickname;

        private int _Action_ID;

        private int? _Child_Remove_Reason_Code_ID;


        public GetASavedPendingResult()
        {
        }

        [Column(Storage = "_Child_ID", DbType = "UniqueIdentifier NOT NULL")]
        public Guid Child_ID
        {
            get
            {
                return _Child_ID;
            }
            set
            {
                if ((_Child_ID != value))
                {
                    _Child_ID = value;
                }
            }
        }

        [Column(Storage = "_Child_Number", DbType = "BigInt")]
        public long? Child_Number
        {
            get
            {
                return _Child_Number;
            }
            set
            {
                if ((_Child_Number != value))
                {
                    _Child_Number = value;
                }
            }
        }

        [Column(Storage = "_First_Name", DbType = "NVarChar(50)")]
        public string First_Name
        {
            get
            {
                return _First_Name;
            }
            set
            {
                if ((_First_Name != value))
                {
                    _First_Name = value;
                }
            }
        }

        [Column(Storage = "_Middle_Name", DbType = "NVarChar(50)")]
        public string Middle_Name
        {
            get
            {
                return _Middle_Name;
            }
            set
            {
                if ((_Middle_Name != value))
                {
                    _Middle_Name = value;
                }
            }
        }

        [Column(Storage = "_Last_Name", DbType = "NVarChar(50)")]
        public string Last_Name
        {
            get
            {
                return _Last_Name;
            }
            set
            {
                if ((_Last_Name != value))
                {
                    _Last_Name = value;
                }
            }
        }

        [Column(Storage = "_Date_of_Birth", DbType = "DateTime")]
        public DateTime? Date_of_Birth
        {
            get
            {
                return _Date_of_Birth;
            }
            set
            {
                if ((_Date_of_Birth != value))
                {
                    _Date_of_Birth = value;
                }
            }
        }

        [Column(Storage = "_Grade_Level_Code_ID", DbType = "Int")]
        public int? Grade_Level_Code_ID
        {
            get
            {
                return _Grade_Level_Code_ID;
            }
            set
            {
                if ((_Grade_Level_Code_ID != value))
                {
                    _Grade_Level_Code_ID = value;
                }
            }
        }

        [Column(Storage = "_Health_Status_Code_ID", DbType = "Int")]
        public int? Health_Status_Code_ID
        {
            get
            {
                return _Health_Status_Code_ID;
            }
            set
            {
                if ((_Health_Status_Code_ID != value))
                {
                    _Health_Status_Code_ID = value;
                }
            }
        }

        [Column(Storage = "_Lives_With_Code_ID", DbType = "Int")]
        public int? Lives_With_Code_ID
        {
            get
            {
                return _Lives_With_Code_ID;
            }
            set
            {
                if ((_Lives_With_Code_ID != value))
                {
                    _Lives_With_Code_ID = value;
                }
            }
        }

        [Column(Storage = "_Favorite_Learning_Code_ID", DbType = "Int")]
        public int? Favorite_Learning_Code_ID
        {
            get
            {
                return _Favorite_Learning_Code_ID;
            }
            set
            {
                if ((_Favorite_Learning_Code_ID != value))
                {
                    _Favorite_Learning_Code_ID = value;
                }
            }
        }

        [Column(Storage = "_Gender_Code_ID", DbType = "Int")]
        public int? Gender_Code_ID
        {
            get
            {
                return _Gender_Code_ID;
            }
            set
            {
                if ((_Gender_Code_ID != value))
                {
                    _Gender_Code_ID = value;
                }
            }
        }

        [Column(Storage = "_Number_Brothers", DbType = "TinyInt")]
        public byte? Number_Brothers
        {
            get
            {
                return _Number_Brothers;
            }
            set
            {
                if ((_Number_Brothers != value))
                {
                    _Number_Brothers = value;
                }
            }
        }

        [Column(Storage = "_Number_Sisters", DbType = "TinyInt")]
        public byte? Number_Sisters
        {
            get
            {
                return _Number_Sisters;
            }
            set
            {
                if ((_Number_Sisters != value))
                {
                    _Number_Sisters = value;
                }
            }
        }

        [Column(Storage = "_Disability_Status", DbType = "Bit")]
        public bool? Disability_Status
        {
            get
            {
                return _Disability_Status;
            }
            set
            {
                if ((_Disability_Status != value))
                {
                    _Disability_Status = value;
                }
            }
        }

        [Column(Storage = "_Location_Code_ID", DbType = "Int")]
        public int? Location_Code_ID
        {
            get
            {
                return _Location_Code_ID;
            }
            set
            {
                if ((_Location_Code_ID != value))
                {
                    _Location_Code_ID = value;
                }
            }
        }

        [Column(Storage = "_Nickname", DbType = "NVarChar(50)")]
        public string Nickname
        {
            get
            {
                return _Nickname;
            }
            set
            {
                if ((_Nickname != value))
                {
                    _Nickname = value;
                }
            }
        }

        [Column(Storage = "_Action_ID", DbType = "Int NOT NULL")]
        public int Action_ID
        {
            get
            {
                return _Action_ID;
            }
            set
            {
                if ((_Action_ID != value))
                {
                    _Action_ID = value;
                }
            }
        }


        [Column(Storage = "_Child_Remove_Reason_Code_ID", DbType = "Int")]
        public int? Child_Remove_Reason_Code_ID
        {
            get
            {
                return _Child_Remove_Reason_Code_ID;
            }
            set
            {
                if ((_Child_Remove_Reason_Code_ID != value))
                {
                    _Child_Remove_Reason_Code_ID = value;
                }
            }
        }

    }
}
