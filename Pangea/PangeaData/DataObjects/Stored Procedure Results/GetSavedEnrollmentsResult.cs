﻿using System;
using System.Data.Linq.Mapping;

namespace Pangea.Data.DataObjects
{
    ////////////////////////////////////////////////////////////////////////////////
    /// This is the Results we should get back for any Stored Procedure ready saved Records
    /// through the stored procedure.
    ////////////////////////////////////////////////////////////////////////////////
    public partial class GetSavedResults
    {
        private Guid _Child_ID;

        private string _First_Name;

        private string _Middle_Name;

        private string _Last_Name;

        private DateTime? _Date_of_Birth;

        private int? _Gender_Code_ID;

        private string _Nickname;

        private int? _Location_Code_ID;

        public GetSavedResults()
        {
        }

        [Column(Storage = "_Child_ID", DbType = "UniqueIdentifier NOT NULL")]
        public Guid Child_ID
        {
            get { return _Child_ID; }
            set
            {
                if ((_Child_ID != value))
                {
                    _Child_ID = value;
                }
            }
        }

        [Column(Storage = "_First_Name", DbType = "NVarChar(50)")]
        public string First_Name
        {
            get { return _First_Name; }
            set
            {
                if ((_First_Name != value))
                {
                    _First_Name = value;
                }
            }
        }

        [Column(Storage = "_Middle_Name", DbType = "NVarChar(50)")]
        public string Middle_Name
        {
            get { return _Middle_Name; }
            set
            {
                if ((_Middle_Name != value))
                {
                    _Middle_Name = value;
                }
            }
        }

        [Column(Storage = "_Last_Name", DbType = "NVarChar(50)")]
        public string Last_Name
        {
            get { return _Last_Name; }
            set
            {
                if ((_Last_Name != value))
                {
                    _Last_Name = value;
                }
            }
        }

        [Column(Storage = "_Date_of_Birth", DbType = "DateTime")]
        public DateTime? Date_of_Birth
        {
            get { return _Date_of_Birth; }
            set
            {
                if ((_Date_of_Birth != value))
                {
                    _Date_of_Birth = value;
                }
            }
        }

        [Column(Storage = "_Gender_Code_ID", DbType = "Int")]
        public int? Gender_Code_ID
        {
            get { return _Gender_Code_ID; }
            set
            {
                if ((_Gender_Code_ID != value))
                {
                    _Gender_Code_ID = value;
                }
            }
        }

        [Column(Storage = "_Nickname", DbType = "NVarChar(50)")]
        public string Nickname
        {
            get { return _Nickname; }
            set
            {
                if ((_Nickname != value))
                {
                    _Nickname = value;
                }
            }
        }

        [Column(Storage = "_Location_Code_ID", DbType = "Int")]
        public int? Location_Code_ID
        {
            get { return _Location_Code_ID; }
            set
            {
                if ((_Location_Code_ID != value))
                {
                    _Location_Code_ID = value;
                }
            }
        }
    }
}
