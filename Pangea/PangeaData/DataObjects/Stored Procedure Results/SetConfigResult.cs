﻿using System.Data.Linq.Mapping;

namespace Pangea.Data.DataObjects
{
    public class SetConfigResult
    {
        private int? _Column1;

        public SetConfigResult()
        {
        }

        [Column(Name = "", Storage = "_Column1", DbType = "Int")]
        public int? Column1
        {
            get
            {
                return _Column1;
            }
            set
            {
                if ((_Column1 != value))
                {
                    _Column1 = value;
                }
            }
        }
    }
}
