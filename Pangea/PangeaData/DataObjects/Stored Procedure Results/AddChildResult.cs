﻿using System;
using System.Data.Linq.Mapping;

namespace Pangea.Data.DataObjects
{
    ////////////////////////////////////////////////////////////////////////////////
    /// This is the Results that we should get back from a successful Add Child
    /// through the stored procedure.
    ////////////////////////////////////////////////////////////////////////////////
    public partial class AddChildResult
    {

        private Guid _Child_ID;

        public AddChildResult()
        {
        }

        [Column(Storage = "_Child_ID", DbType = "UniqueIdentifier NOT NULL")]
        public Guid Child_ID
        {
            get { return _Child_ID; }
            set
            {
                if ((_Child_ID != value))
                {
                    _Child_ID = value;
                }
            }
        }
    }
}
