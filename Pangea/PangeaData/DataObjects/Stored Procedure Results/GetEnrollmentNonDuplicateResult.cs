﻿using System;
using System.Data.Linq.Mapping;

namespace Pangea.Data.DataObjects
{
    ////////////////////////////////////////////////////////////////////////////////
    /// This is the Results that we should get back from a successful Get Non Duplicate
    /// stored procedure call.
    ////////////////////////////////////////////////////////////////////////////////
    public partial class GetEnrollmentNonDuplicateResult
    {

        private long _Child_Non_Duplicate_ID;

        private Guid _Child_ID_1;

        private Guid _Child_ID_2;

        private long? _TSLONG;

        private long? _TSMAN;

        public GetEnrollmentNonDuplicateResult()
        {
        }

        [Column(Storage = "_Child_Non_Duplicate_ID", DbType = "BigInt NOT NULL")]
        public long Child_Non_Duplicate_ID
        {
            get
            {
                return _Child_Non_Duplicate_ID;
            }
            set
            {
                if ((_Child_Non_Duplicate_ID != value))
                {
                    _Child_Non_Duplicate_ID = value;
                }
            }
        }

        [Column(Storage = "_Child_ID_1", DbType = "UniqueIdentifier NOT NULL")]
        public Guid Child_ID_1
        {
            get
            {
                return _Child_ID_1;
            }
            set
            {
                if ((_Child_ID_1 != value))
                {
                    _Child_ID_1 = value;
                }
            }
        }

        [Column(Storage = "_Child_ID_2", DbType = "UniqueIdentifier NOT NULL")]
        public Guid Child_ID_2
        {
            get
            {
                return _Child_ID_2;
            }
            set
            {
                if ((_Child_ID_2 != value))
                {
                    _Child_ID_2 = value;
                }
            }
        }

        [Column(Storage = "_TSLONG", DbType = "BigInt")]
        public long? TSLONG
        {
            get
            {
                return _TSLONG;
            }
            set
            {
                if ((_TSLONG != value))
                {
                    _TSLONG = value;
                }
            }
        }

        [Column(Storage = "_TSMAN", DbType = "BigInt")]
        public long? TSMAN
        {
            get
            {
                return _TSMAN;
            }
            set
            {
                if ((_TSMAN != value))
                {
                    _TSMAN = value;
                }
            }
        }
    }
}
