﻿using System;
using System.Data.Linq.Mapping;

namespace Pangea.Data.DataObjects
{
    ////////////////////////////////////////////////////////////////////////////////
    /// This is the Child ID for a Possible Duplicate Child Record
    ////////////////////////////////////////////////////////////////////////////////
    public partial class GetPossibleChildDuplicatesResult
    {
        private Guid _Child_ID;

        public GetPossibleChildDuplicatesResult()
        {
        }

        [Column(Storage = "_Child_ID", DbType = "UniqueIdentifier NOT NULL")]
        public Guid Child_ID
        {
            get
            {
                return _Child_ID;
            }
            set
            {
                if ((_Child_ID != value))
                {
                    _Child_ID = value;
                }
            }
        }
    }
}
