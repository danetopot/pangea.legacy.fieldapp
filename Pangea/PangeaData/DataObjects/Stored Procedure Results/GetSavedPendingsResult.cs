﻿using System;
using System.Data.Linq.Mapping;

namespace Pangea.Data.DataObjects
{
    public class GetSavedPendingsResult
    {

        private Guid _Child_ID;

        private long? _Child_Number;

        private string _First_Name;

        private string _Middle_Name;

        private string _Last_Name;

        private DateTime? _Date_of_Birth;

        private string _Gender;

        private string _Nickname;

        private string _Location;

        public GetSavedPendingsResult()
        {
        }

        [Column(Storage = "_Child_ID", DbType = "UniqueIdentifier NOT NULL")]
        public Guid Child_ID
        {
            get
            {
                return _Child_ID;
            }
            set
            {
                if ((_Child_ID != value))
                {
                    _Child_ID = value;
                }
            }
        }

        [Column(Storage = "_Child_Number", DbType = "BigInt")]
        public long? Child_Number
        {
            get
            {
                return _Child_Number;
            }
            set
            {
                if ((_Child_Number != value))
                {
                    _Child_Number = value;
                }
            }
        }

        [Column(Storage = "_First_Name", DbType = "NVarChar(50)")]
        public string First_Name
        {
            get
            {
                return _First_Name;
            }
            set
            {
                if ((_First_Name != value))
                {
                    _First_Name = value;
                }
            }
        }

        [Column(Storage = "_Middle_Name", DbType = "NVarChar(50)")]
        public string Middle_Name
        {
            get
            {
                return _Middle_Name;
            }
            set
            {
                if ((_Middle_Name != value))
                {
                    _Middle_Name = value;
                }
            }
        }

        [Column(Storage = "_Last_Name", DbType = "NVarChar(50)")]
        public string Last_Name
        {
            get
            {
                return _Last_Name;
            }
            set
            {
                if ((_Last_Name != value))
                {
                    _Last_Name = value;
                }
            }
        }

        [Column(Storage = "_Date_of_Birth", DbType = "DateTime")]
        public DateTime? Date_of_Birth
        {
            get
            {
                return _Date_of_Birth;
            }
            set
            {
                if ((_Date_of_Birth != value))
                {
                    _Date_of_Birth = value;
                }
            }
        }

        [Column(Storage = "_Gender", DbType = "NVarChar(50)")]
        public string Gender
        {
            get
            {
                return _Gender;
            }
            set
            {
                if ((_Gender != value))
                {
                    _Gender = value;
                }
            }
        }

        [Column(Storage = "_Nickname", DbType = "NVarChar(50)")]
        public string Nickname
        {
            get
            {
                return _Nickname;
            }
            set
            {
                if ((_Nickname != value))
                {
                    _Nickname = value;
                }
            }
        }

        [Column(Storage = "_Location", DbType = "NVarChar(255)")]
        public string Location
        {
            get
            {
                return _Location;
            }
            set
            {
                if ((_Location != value))
                {
                    _Location = value;
                }
            }
        }
    }
}
