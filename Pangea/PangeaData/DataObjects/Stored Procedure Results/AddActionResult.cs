﻿using System.Data.Linq.Mapping;

namespace Pangea.Data.DataObjects
{
    public partial class AddCodesResult
    {
        private int? _Column1;

        public AddCodesResult()
        {
        }

        [Column(Name = "", Storage = "_Column1", DbType = "Int")]
        public int? Column1
        {
            get
            {
                return _Column1;
            }
            set
            {
                if ((_Column1 != value))
                {
                    _Column1 = value;
                }
            }
        }
    }
}
