﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PangeaData.Model.SPROCs
{
    public class Enrollment__usp_Add_Child_File
    {

        public int ID { get; set; }

        public Guid File_GUID { get; set; }

    }
}
