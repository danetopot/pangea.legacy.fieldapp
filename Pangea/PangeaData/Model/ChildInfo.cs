﻿///////////////////////////////////////////////////////////////////////////////
/// This is the Child Information Class that will inherit all of the base child 
/// information for enrollments and updates. This class will then take all the child
/// information and add it to the correct database tables when the user either
/// enrolls a child or updates a childs information.
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.IO;
using System.Windows;

using Pangea.Data.DataObjects;
using Pangea.Data.Model.CodeTables;
using Pangea.Utilities.Tools;
using PangeaData.Model.Logics;
using static Pangea.Data.DataObjects.Aggregate_Child_Data_TablesDataContext;

namespace Pangea.Data.Model
{
    public partial class ChildInfo : Child
    {
        public ChildInfo()
            : base()
        {
            // Bug 317 - When a new Default Country/Location is selected it is not throwing the Child Country/Child Location property changed event.
            if (PangeaInfo.DefaultCountry != null)
                ChildCountry = PangeaInfo.DefaultCountry;

            if (PangeaInfo.DefaultLocation != null)
                ChildLocation = PangeaInfo.DefaultLocation;

            DateOfBirth = null;

            NumberBrothers = 0;

            NumberSisters = 0;

            Gender = null;

            HasDisability = null;

            GradeLvl = null;

            FavLearning = null;

            Personality_type = null;

            Lives_With = null;

            Health = null;

            HasTransmitted = false;

            PangeaInfo.SaveRequired = false;// Bug 334
        }

        public ChildInfo(ChildDO childDO)
            : base(childDO)
        { }

        public ChildInfo(GetSavedResults _child)
            : base(_child)
        { }

        public ChildInfo(Guid? _childID, GetASavedResult _child)
            : base(_childID, _child)
        { }

        public ChildInfo(GetASavedPendingResult _child)
            : base(_child)
        { }

        public ChildInfo(Child _child)
        {
            Copy(_child);
        }

        public bool HasTransmitted { get; set; }

        /// <summary>
        /// This Performs the Saving of the Child Information.
        /// It is used for both just saving the progress of the child record and for Enrolling the child record.
        /// This function will save the progress of the child record as long as the minimum items needed for the 
        /// the save progress are available.
        /// </summary>
        /// <param name="_readyToTransit">Whether the record is to saved for Enrollment(true) or just to save progress(false)</param>
        /// <returns>Whether the save/enroll succeeded.</returns>
        public bool SaveChild(bool _readyToTransit = false)
        {

            LogManager.DebugLogManager.MethodBegin("ChildInfo.SaveChildBeingEnrolled");

            PangeaInfo.Messages.RemoveAll(MessageTypes.SaveSucceeded);

            PangeaInfo.Messages.RemoveAll(MessageTypes.SaveFailed);

            bool saveSucceed = false;

            // Make sure we have all the Data we need.
            // Tuple values are as follows : 
            // Item1 - Bool RetValSave - If it is ok to Save the child information
            // Item2 - Bool RetValEnroll - If it is ok to Save the Child Information for Enrollment
            Tuple<bool, bool> _validReturn = ValidateChildInfo();

            if (_validReturn.Item1)
            {
                ;

                //saveSucceed = (PangeaInfo.EnrollingChild ? SubmitChildData() : SubmitChildUpdateData());

                saveSucceed = 
                    (
                    PangeaInfo.EnrollingChild ? 
                    SubmitChild_Add_Or_Update_Data(enum_DataOperation.Add) 
                    :
                    SubmitChild_Add_Or_Update_Data(enum_DataOperation.Update)
                    );

                if
                    (
                    PangeaInfo.EnrollingChild &&
                    _readyToTransit &&
                    _validReturn.Item2 &&
                    saveSucceed
                    )
                {

                    // We are setting the Child Ready for Enrollment.
                    EnrollmentTablesDataContext _enrollmentDataContext = new EnrollmentTablesDataContext(PangeaInfo.DBCon);

                    ;


                    CustomMappingSource cust_Map_Source = new CustomMappingSource
                        (
                        Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type.INSERT
                        );

                    Aggregate_Child_Data_TablesDataContext aggregate_Child_Data_DataContext
                        = new Aggregate_Child_Data_TablesDataContext
                        (
                            PangeaInfo.DBCon,
                            Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type.INSERT,  // e_Table_Action_Type,
                            cust_Map_Source
                            );


                    ;

                    // ISingleResult<AddChildResult> _enrollChildResults = _enrollmentDataContext.Enroll_Child(PangeaInfo.User.UserID, ChildID);
                    // AddChildResult _enrollChildResults = aggregate_Child_Data_DataContext.Enroll_Child(PangeaInfo.User.UserID, ChildID);

                    int the_Child_ID = aggregate_Child_Data_DataContext.Enroll_Child(PangeaInfo.User.UserID, ChildID);

                    //ISingleResult<AddChildResult> _enrollChildResults = _enrollmentDataContext.Enroll_Child(PangeaInfo.User.UserID, ChildID);

                    // ISingleResult<AddChildResult> the_enrollChildResults = _enrollmentDataContext.Enroll_Child(PangeaInfo.User.UserID, ChildID);

                    // int the_enrollChildResults_2 = _enrollmentDataContext.Enroll_Child2(PangeaInfo.User.UserID, ChildID);

                    // ISingleResult<AddChildResult> the_enrollChildResults = _enrollmentDataContext.Enroll_Child(PangeaInfo.User.UserID, ChildID);

                    // AddChildResult _enrollChildResults = null; //  the_enrollChildResults[0];  // _enrollmentDataContext.Enroll_Child(PangeaInfo.User.UserID, ChildID);

                    /*
                    foreach (AddChildResult ecr in the_enrollChildResults) // _enrollChildResults)
                    {
                        if (ecr.Child_ID != ChildID) 
                        { 
                            saveSucceed = false;

                            //_enrollChildResults = ecr;
                        }
                    }
                    */

                    /*
                    if (the_enrollChildResults.Child_ID != ChildID)
                    {
                        saveSucceed = false;
                    }
                    */

                    ;

                    if (the_Child_ID == 0)
                    {
                        saveSucceed = false;
                    }

                    /*
                    if (_enrollChildResults.Child_ID != ChildID)
                    {
                        saveSucceed = false;
                    }
                    */

                    ;

                    // Setting the Action ID as Ready to Transmit.

                    if (saveSucceed) 
                    {

                        //
                        // RETURN PASS - 1 .... FAIL - 0
                        //
                        // LAST RUN FAILED AS NO MATCHING CHILD_ID VALUE RECORD BETWEEN PENDING.CHILD AND ENROLLMENT.CHILD, 
                        // MISSING CORRESPONDING ENROLLMENT.CHILD RECORD
                        //

                        int the_resultt = 
                            _enrollmentDataContext
                            .Update_Child_Action
                            (
                                PangeaInfo.User.UserID,

                                ChildID,

                                PangeaCollections
                                .ActionCollection
                                .Find("Ready to Transmit")
                                .ID
                                );

                        ;
                
                    }                
                }
                else if 
                    (
                    PangeaInfo.UpdatingChild && 
                    saveSucceed
                    )
                {
                    // Setting the Child Action ID as Ready to Ransmit otherwise its set to Update so that we know that we have started the Field Update.
                    PendingTablesDataContext _pendingDataContext = new PendingTablesDataContext(PangeaInfo.DBCon);


                    int the_resultt =
                        _pendingDataContext.Update_Child_Action
                        (
                            
                            PangeaInfo.User.UserID, 
                            
                            ChildID, 

                            (
                            _readyToTransit && 
                            _validReturn.Item2
                            ) 
                            
                            ? 
                            
                            PangeaCollections
                            .ActionCollection
                            .Find("Ready to Transmit")
                            .ID 
                            
                            : 
                            
                            PangeaCollections
                            .ActionCollection
                            .Find("Update")
                            .ID
                            
                            );
                
                }

                // TODO : Add check to make sure we truly successfoully Saved the Child Information.
                if (saveSucceed)
                {
                    if (
                        _readyToTransit && 
                        _validReturn.Item2
                        )
                        PangeaInfo.Messages.Add
                            (
                            new MessagesStruct
                            (
                                PangeaInfo.EnrollingChild 
                                ? 
                                "Enrollment Child is Ready for Transmit." 
                                : 
                                "Update Child is Ready for Transmit.", 

                                MessageTypes.SaveSucceeded, 

                                PangeaInfo.EnrollingChild 
                                ? 
                                "ChildSavedEnrolled" 
                                : 
                                "ChildSavedUpdate"

                                )
                            );
                    else
                        PangeaInfo
                            .Messages
                            .Add
                            (
                            new MessagesStruct
                            (

                                PangeaInfo.EnrollingChild 
                                ?
                                Centraler.Settingz.Complete_Record_Saved_Message  // // "Child Enrollment Progress has been Saved." 
                                : 
                                "Child Update Progress has been Saved.", 
                                
                                MessageTypes.SaveSucceeded, 

                                PangeaInfo.EnrollingChild 
                                ? 
                                "ChildSavedEnrolled" 
                                : 
                                "ChildSavedUpdate"

                                )
                            );
                }
                else
                    PangeaInfo
                        .Messages
                        .Add
                        (
                        new MessagesStruct
                        (
                            Centraler.Settingz.Incomplete_Record_Saved_Message,  // "Failed to Save Child Information.", 
                            MessageTypes.SaveFailed, 
                            "FailedSave"
                            )
                        );
            }
            else
                PangeaInfo
                    .Messages
                    .Add
                    (
                    new MessagesStruct
                    (
                        Centraler.Settingz.Incomplete_Record_Saved_Message,  // "Failed to Save Child Information.", 
                        MessageTypes.SaveFailed, 
                        "FailedSave"
                        )
                    );

            LogManager.DebugLogManager.MethodEnd("ChildInfo.SaveChildBeingEnrolled");

            bool breturn = 
                _readyToTransit 
                
                ?
                
                saveSucceed && 
                _validReturn.Item2 
                
                : 

                saveSucceed && 
                _validReturn.Item1;

            return breturn;
        }

        /// <summary>
        /// This is used to Save the Remove Update Child Reason and set the Child Ready to be Transmitted
        /// </summary>
        public void RemoveUpdateChild()
        {
            LogManager.DebugLogManager.MethodBegin("ChildInfo.RemoveChild");

            SubmitChildUpdateData(true);

            // Set Ready to Transmit
            PendingTablesDataContext _pendingDataContext = new PendingTablesDataContext(PangeaInfo.DBCon);

            _pendingDataContext.Update_Child_Action
                (
                PangeaInfo.User.UserID, 
                ChildID, 
                PangeaCollections.ActionCollection.Find("Ready to Transmit").ID
                );

            LogManager.DebugLogManager.MethodEnd("ChildInfo.RemoveChild");
        }

        /// <summary>
        /// This Function submits the Child Data to the Local DB.
        /// </summary>
        /// <param name="_enroll">Whether to submit the child as ready to send to enroll(true) or to save progress(false)</param>
        private bool SubmitChildData()
        {
            LogManager.DebugLogManager.MethodBegin("ChildInfo.SubmitChildData");

            bool retVal = true;

            try
            {
                EnrollmentTablesDataContext _enrollmentDataContext = new EnrollmentTablesDataContext(PangeaInfo.DBCon);

                ISingleResult<AddChildResult> _addChildResults = _enrollmentDataContext.Add_Child
                    (
                    PangeaInfo.User.UserID, 
                    FirstName, 
                    LastName, 
                    ChildID, 
                    MiddleName, 
                    DateOfBirth,
                    GradeLvl?.CodeID, 
                    Health?.CodeID, 
                    Lives_With?.CodeID, 
                    FavLearning?.CodeID,
                    null, 
                    null, 
                    (byte?)Gender?.CodeID, 
                    (byte?)NumberBrothers, 
                    (byte?)NumberSisters,
                    HasDisability, 
                    ChildLocation?.CodeID, 
                    NickName
                    );

                if (ChildID == null)
                {
                    foreach (AddChildResult result in _addChildResults)
                    {
                        ChildID = result.Child_ID;
                    }
                }

                int results = -1;

                SubmitChildData_Data_Save_Results ChildData_Data_Save_Results = new SubmitChildData_Data_Save_Results();

                // List<SubmitChildData_Data_Save_Result> the_Results = new List<SubmitChildData_Data_Save_Result>();

                if (ChildID != null)
                {
                    // Bug 253 - Clear out all the Child Codes from the DB and then add the new ones in.
                    results = _enrollmentDataContext.Clear_Child_Codes(PangeaInfo.User.UserID, ChildID, null);

                    ChildData_Data_Save_Results.the_Results.Add
                        (
                        new SubmitChildData_Data_Save_Result() 
                        {
                            Data_Save_Part = "Clear Child Codes",
                            result = results
                        }
                        );

                    foreach (Chore _chore in Chores)
                    {
                        results = _enrollmentDataContext.Add_Child_Chore(PangeaInfo.User.UserID, ChildID, DateTime.Now, _chore.CodeID, null);

                        ;

                        ChildData_Data_Save_Results.the_Results.Add
                            (
                            new SubmitChildData_Data_Save_Result()
                            {
                                Data_Save_Part = "Chore: " + _chore.Name,
                                result = results
                            }
                            );

                    }

                    foreach (FavoriteActivity _favAct in FavActivity)
                    {
                        results = _enrollmentDataContext.Add_Child_Favorite_Activity(PangeaInfo.User.UserID, ChildID, DateTime.Now, _favAct.CodeID, null);

                        ChildData_Data_Save_Results.the_Results.Add
                        (
                        new SubmitChildData_Data_Save_Result()
                        {
                            Data_Save_Part = "Favorite Activity: " + _favAct.Name,
                            result = results
                        }
                        );
                    }

                    if (Personality_type != null)
                    {
                        results = _enrollmentDataContext.Add_Child_Personality_Type(PangeaInfo.User.UserID, ChildID, DateTime.Now, Personality_type.CodeID, null);

                        ChildData_Data_Save_Results.the_Results.Add
                        (
                        new SubmitChildData_Data_Save_Result()
                        {
                            Data_Save_Part = "Personality Type: " + Personality_type.Name,
                            result = results
                        }
                        );
                    }

                    String _saveLocation = String.Empty;

                    if (ChildID != null)
                    {
                        if (ChildNumber != null)
                        {
                            _saveLocation = String.Format("{0}{1}\\", PangeaInfo.CommonAppPangeaEnrollmentLoc, PangeaInfo.Child.ChildNumber);

                            ChildData_Data_Save_Results.ChildNumber = PangeaInfo.Child.ChildNumber;
                        }
                        else 
                        { 
                            _saveLocation = String.Format("{0}{1}\\", PangeaInfo.CommonAppPangeaEnrollmentLoc, PangeaInfo.Child.ChildID);

                            ChildData_Data_Save_Results.ChildID = PangeaInfo.Child.ChildID;
                        }
                    }
                    else // Should never go into here.
                        _saveLocation = String.Format("{0}temp\\", PangeaInfo.CommonAppPangeaEnrollmentLoc);

                    // Also need to remove any saved Files that have been removed from the list.
                    for (int i = 0; i < AdditionalResources.Count; i++)//foreach (RequiredFileStruct _reqDoc in AdditionalResources)
                    {
                        PangeaFileStruct _reqDoc = AdditionalResources[i];

                        // Make Sure the Directory Exists
                        if (!Directory.Exists(_saveLocation))
                            Directory.CreateDirectory(_saveLocation);

                        // Need to move all the files to the ChildNumber/ChildID folder if its not already there.
                        if (!File.Exists(String.Format("{0}{1}", _saveLocation, _reqDoc.ShortFileName)))
                        {
                            File.Copy(_reqDoc.FullFileLoc, String.Format("{0}{1}", _saveLocation, _reqDoc.ShortFileName));

                            _reqDoc.FullFileLoc = String.Format("{0}{1}", _saveLocation, _reqDoc.ShortFileName);
                        }

                        String fileLocation = _reqDoc.FullFileLoc;

                        FileStream _fStream = File.OpenRead(fileLocation);

                        using (BinaryReader reader = new BinaryReader(_fStream))
                        {
                            results = _enrollmentDataContext.Add_Child_File
                                (
                                PangeaInfo.User.UserID, 
                                ChildID, 
                                new Binary(reader.ReadBytes((int)_fStream.Length)), 
                                _reqDoc.ShortFileName, 
                                _reqDoc.ContentTypeInfo.CodeID,
                                _reqDoc.FileTypeInfo.CodeID, 
                                _reqDoc.FileDataInfo.Make, 
                                _reqDoc.FileDataInfo.Model,
                                _reqDoc.FileDataInfo.Software, 
                                _reqDoc.FileDataInfo.DateTime,
                                _reqDoc.FileDataInfo.DateTimeOriginal, 
                                _reqDoc.FileDataInfo.DateTimeDigitized,
                                _reqDoc.FileDataInfo.GPSVersionID, 
                                _reqDoc.FileDataInfo.GPSLatitudeRef,
                                _reqDoc.FileDataInfo.GPSLatitude, 
                                _reqDoc.FileDataInfo.GPSLongitudeRef,
                                _reqDoc.FileDataInfo.GPSLongitude, 
                                _reqDoc.FileDataInfo.GPSAltitudeRef,
                                _reqDoc.FileDataInfo.GPSAltitude, 
                                _reqDoc.FileDataInfo.GPSTimeStamp,
                                _reqDoc.FileDataInfo.GPSImgDirectionRef, 
                                _reqDoc.FileDataInfo.GPSDateStamp
                                );

                            ChildData_Data_Save_Results.the_Results.Add
                                (
                                new SubmitChildData_Data_Save_Result()
                                {
                                    Data_Save_Part = "_enrollmentDataContext.Add_Child_File: " + _reqDoc.ShortFileName,
                                    result = results
                                }
                                );

                        }

                        AdditionalResources[i] = _reqDoc;
                    }

                    foreach (ChildDuplicate _cDup in NonDuplicates)
                    {
                        results = _enrollmentDataContext.Add_Child_Non_Duplicates(PangeaInfo.User.UserID, ChildID, _cDup.Child2_ID);

                        ChildData_Data_Save_Results.the_Results.Add
                                (
                                new SubmitChildData_Data_Save_Result()
                                {
                                    Data_Save_Part = "Duplicates - ChildID: " + (ChildID.HasValue ? ChildID.Value.ToString() : "") + " - " + "Child2_ID: " + _cDup.Child2_ID,
                                    result = results
                                }
                                );

                    }

                    // 
                    foreach (ChildDuplicate _cDup in NonDuplicates)
                    {
                        results = _enrollmentDataContext.Add_Child_Non_Duplicates(PangeaInfo.User.UserID, ChildID, _cDup.Child2_ID);

                        ChildData_Data_Save_Results.the_Results.Add
                                (
                                new SubmitChildData_Data_Save_Result()
                                {
                                    Data_Save_Part = "Duplicates - ChildID: " + (ChildID.HasValue ? ChildID.Value.ToString() : "") + " - " + "Child2_ID: " + _cDup.Child2_ID,
                                    result = results
                                }
                                );
                    }

                }

                retVal = results == 1;
            }
            catch (Exception ex)
            {
                LogManager.ErrorLogManager.WriteError("ChildInfo.SubmitChildData", ex);

                // BUG 267 - Added Message to let users know there was a problem calling the DB.
                PangeaInfo.Messages.Add(new MessagesStruct("Error with Database Call while saving Child Record.", MessageTypes.CriticalError, "DBError"));

                retVal = false;
#if DEBUG
                MessageBox.Show(ex.Message, "ChildInfo.SubmitChildData");
#endif
            }

            LogManager.DebugLogManager.MethodEnd("ChildInfo.SubmitChildData");

            return retVal;
        }

        /// <summary>
        /// This Function submits the Child Data to the Local DB.
        /// </summary>
        /// <param name="_enroll">Whether to submit the child as ready to send to enroll(true) or to save progress(false)</param>
        private bool SubmitChildUpdateData(bool removechild = false)
        {
            LogManager.DebugLogManager.MethodBegin("ChildInfo.SubmitChildUpdateData");

            bool retVal = true;

            try
            {

                PendingTablesDataContext _pendingDataContext = new PendingTablesDataContext(PangeaInfo.DBCon);

                ISingleResult<AddChildResult> _addChildResults = _pendingDataContext.Add_Child
                    (
                    PangeaInfo.User.UserID, 
                    FirstName, 
                    LastName, 
                    ChildID, 
                    ChildNumber, 
                    MiddleName, 
                    DateOfBirth,
                    GradeLvl?.CodeID, 
                    Health?.CodeID, 
                    Lives_With?.CodeID, 
                    FavLearning?.CodeID,
                    null, 
                    Remove_Reason?.CodeID, 
                    (byte?)Gender?.CodeID, 
                    (byte?)NumberBrothers, 
                    (byte?)NumberSisters,
                    HasDisability, 
                    ChildLocation?.CodeID, 
                    NickName
                    );

                int results = -1;

                if (!removechild)
                {

                    // Bug 253 - Clear out all the Child Codes from the DB and then add the new ones in.
                    results = _pendingDataContext.Clear_Child_Codes(PangeaInfo.User.UserID, ChildID, null);

                    foreach (Chore _chore in Chores)
                    {
                        results = _pendingDataContext.Add_Child_Chore(PangeaInfo.User.UserID, ChildID, DateTime.Now, _chore.CodeID, null);
                    }

                    foreach (FavoriteActivity _favAct in FavActivity)
                    {
                        results = _pendingDataContext.Add_Child_Favorite_Activity(PangeaInfo.User.UserID, ChildID, DateTime.Now, _favAct.CodeID, null);
                    }

                    if (Personality_type != null)
                    {
                        results = _pendingDataContext.Add_Child_Personality_Type(PangeaInfo.User.UserID, ChildID, DateTime.Now, Personality_type.CodeID, null);
                    }

                    String _saveLocation = String.Empty;

                    if (ChildID != null)
                    {
                        if (ChildNumber != null)
                            _saveLocation = String.Format("{0}{1}\\", PangeaInfo.CommonAppPangeaUpdateLoc, PangeaInfo.Child.ChildNumber);
                        else
                            _saveLocation = String.Format("{0}{1}\\", PangeaInfo.CommonAppPangeaUpdateLoc, PangeaInfo.Child.ChildID);
                    }
                    else // Should never go into here.
                        _saveLocation = String.Format("{0}temp\\", PangeaInfo.CommonAppPangeaUpdateLoc);

                    // Also need to remove any saved Files that have been removed from the list.
                    for (int i = 0; i < AdditionalResources.Count; i++)//foreach (RequiredFileStruct _reqDoc in AdditionalResources)
                    {
                        PangeaFileStruct _reqDoc = AdditionalResources[i];

                        // Make Sure the Directory Exists
                        if (!Directory.Exists(_saveLocation))
                            Directory.CreateDirectory(_saveLocation);

                        // Need to move all the files to the ChildNumber/ChildID folder if its not already there.
                        if (!File.Exists(String.Format("{0}{1}", _saveLocation, _reqDoc.ShortFileName)))
                        {
                            File.Copy(_reqDoc.FullFileLoc, String.Format("{0}{1}", _saveLocation, _reqDoc.ShortFileName));

                            _reqDoc.FullFileLoc = String.Format("{0}{1}", _saveLocation, _reqDoc.ShortFileName);
                        }

                        String fileLocation = _reqDoc.FullFileLoc;

                        FileStream _fStream = File.OpenRead(fileLocation);
                        using (BinaryReader reader = new BinaryReader(_fStream))
                        {

                            byte[] ba_Child_File = reader.ReadBytes((int)_fStream.Length);

                            results = _pendingDataContext.Add_Child_File
                            (
                                PangeaInfo.User.UserID, 
                                ChildID,
                                ba_Child_File,  // new Binary(reader.ReadBytes((int)_fStream.Length)), 
                                _reqDoc.ShortFileName, 
                                _reqDoc.ContentTypeInfo.CodeID,
                                _reqDoc.FileTypeInfo.CodeID, 
                                _reqDoc.FileDataInfo.Make, 
                                _reqDoc.FileDataInfo.Model,
                                _reqDoc.FileDataInfo.Software, 
                                _reqDoc.FileDataInfo.DateTime,
                                _reqDoc.FileDataInfo.DateTimeOriginal, 
                                _reqDoc.FileDataInfo.DateTimeDigitized,
                                _reqDoc.FileDataInfo.GPSVersionID, 
                                _reqDoc.FileDataInfo.GPSLatitudeRef,
                                _reqDoc.FileDataInfo.GPSLatitude, 
                                _reqDoc.FileDataInfo.GPSLongitudeRef,
                                _reqDoc.FileDataInfo.GPSLongitude, 
                                _reqDoc.FileDataInfo.GPSAltitudeRef,
                                _reqDoc.FileDataInfo.GPSAltitude, 
                                _reqDoc.FileDataInfo.GPSTimeStamp,
                                _reqDoc.FileDataInfo.GPSImgDirectionRef, 
                                _reqDoc.FileDataInfo.GPSDateStamp
                                );
                        }

                        AdditionalResources[i] = _reqDoc;
                    }

                    // Delete the Tempory File Saving location since we copied everything over to the correct directory.
                    if (Directory.Exists(String.Format("{0}temp\\", PangeaInfo.CommonAppPangeaUpdateLoc)))
                        Directory.Delete(String.Format("{0}temp\\", PangeaInfo.CommonAppPangeaUpdateLoc), true);

                    foreach (ChildDuplicate _cDup in NonDuplicates)
                    {
                        results = _pendingDataContext.Add_Child_Duplicates(PangeaInfo.User.UserID, ChildID, _cDup.Child2_ID);
                    }

                    // Bug 311 - The check was trying to save a null or empty string MLE.  Changed check to fix this.
                    if (!String.IsNullOrEmpty(MajorLifeEvent))
                        results = _pendingDataContext.Add_Child_Major_Life_Event
                            (
                            PangeaInfo.User.UserID, 
                            ChildID, 
                            DateTime.Now.Date, 
                            PangeaCollections.LanguageCollection.Find(System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName).CodeID, 
                            MajorLifeEvent, 
                            null
                            );
                }

                retVal = !removechild ? results == 1 : true;
            }
            catch (Exception ex)
            {
                LogManager.ErrorLogManager.WriteError("ChildInfo.SubmitChildUpdateData", ex);

                // BUG 267 - Added Message to let users know there was a problem calling the DB.
                PangeaInfo.Messages.Add(new MessagesStruct("Error with Database Call while saving Child Record.", MessageTypes.CriticalError, "DBError"));

                retVal = false;
#if DEBUG
                MessageBox.Show(ex.Message, "ChildInfo.SubmitChildUpdateData");
#endif
            }

            LogManager.DebugLogManager.MethodEnd("ChildInfo.SubmitChildUpdateData");

            return retVal;
        }

        /// <summary>
        /// Copy the Provided Child information.
        /// </summary>
        /// <param name="_child">Child Informatio to be copied</param>
        public void Copy(ChildInfo _child)
        {
            LogManager.DebugLogManager.MethodBegin("ChildInfo.Copy(ChildInfo)");

            // BUG 261 - Forgot to replace the Child ID and Child Number when coping the Child
            ChildID = _child.ChildID;

            ChildNumber = _child.ChildNumber;

            // BUG 317 - Have to set the Country before the Location or it messes up the Populating of the Controls.
            ChildCountry = _child.ChildCountry;

            ChildLocation = _child.ChildLocation;

            FirstName = _child.FirstName;

            MiddleName = _child.MiddleName;

            LastName = _child.LastName;

            NickName = _child.NickName;

            DateOfBirth = _child.DateOfBirth;

            Gender = _child.Gender;

            HasDisability = _child.HasDisability;

            NumberBrothers = _child.NumberBrothers;

            NumberSisters = _child.NumberSisters;

            FavLearning = _child.FavLearning;

            GradeLvl = _child.GradeLvl;

            Personality_type = _child.Personality_type;

            Lives_With = _child.Lives_With;

            Health = _child.Health;

            Chores = _child.Chores;

            FavActivity = _child.FavActivity;

            // Bug 333 - Added the Action and Remove_Reason to the copy.  They were missed so the Child was losing this information
            // when loaded.
            Action = _child.Action;

            Remove_Reason = _child.Remove_Reason;

            MajorLifeEvent = _child.MajorLifeEvent;

            // This has to be the Main Property so that the Collection Changed property can be resetup correctly
            AdditionalResources = _child.AdditionalResources;

            PangeaInfo.SaveRequired = false;// Bug 334

            LogManager.DebugLogManager.MethodEnd("ChildInfo.Copy(ChildInfo)");
        }

        /// <summary>
        /// Copy the Provided Child information.
        /// </summary>
        /// <param name="_child">Child Informatio to be copied</param>
        public void Copy(Child _child)
        {
            LogManager.DebugLogManager.MethodBegin("ChildInfo.Copy(Child)");

            // BUG 261 - Forgot to replace the Child ID and Child Number when coping the Child
            ChildID = _child.ChildID;

            ChildNumber = _child.ChildNumber;

            ChildCountry = _child.ChildCountry;

            ChildLocation = _child.ChildLocation;

            FirstName = _child.FirstName;

            MiddleName = _child.MiddleName;

            LastName = _child.LastName;

            NickName = _child.NickName;

            DateOfBirth = _child.DateOfBirth;

            Gender = _child.Gender;

            HasDisability = _child.HasDisability;

            NumberBrothers = _child.NumberBrothers;

            NumberSisters = _child.NumberSisters;

            FavLearning = _child.FavLearning;

            GradeLvl = _child.GradeLvl;

            Personality_type = _child.Personality_type;

            Lives_With = _child.Lives_With;

            Health = _child.Health;

            Chores = _child.Chores;

            FavActivity = _child.FavActivity;

            // Bug 333 - Added the Action and Remove_Reason to the copy.  They were missed so the Child was losing this information
            // when loaded.
            Action = _child.Action;

            Remove_Reason = _child.Remove_Reason;

            MajorLifeEvent = _child.MajorLifeEvent;

            // This has to be the Main Property so that the Collection Changed property can be resetup correctly
            AdditionalResources = _child.AdditionalResources;

            PangeaInfo.SaveRequired = false;// Bug 334

            LogManager.DebugLogManager.MethodEnd("ChildInfo.Copy(Child)");
        }

        public void Copy(GetASavedResult _childSavedInfo)
        {
            LogManager.DebugLogManager.MethodBegin("ChildInfo.Copy(GetASavedEnrollmentResult)");

            // BUG 317 - Have to set the Country before the Location or it messes up the Populating of the Controls.
            Location _tempLoc = PangeaCollections.LocationCollection.Find(_childSavedInfo.Location_Code_ID);

            ChildCountry = PangeaCollections.CountryCollection.Find(_tempLoc.CountryCodeID);
            
            ChildLocation = _tempLoc;

            FirstName = _childSavedInfo.First_Name;

            MiddleName = _childSavedInfo.Middle_Name;

            LastName = _childSavedInfo.Last_Name;

            NickName = _childSavedInfo.Nickname;

            DateOfBirth = _childSavedInfo.Date_of_Birth;

            Gender = PangeaCollections.GenderCollection.Find(_childSavedInfo.Gender_Code_ID);

            HasDisability = _childSavedInfo.Disability_Status;

            NumberBrothers = _childSavedInfo.Number_Brothers ?? 0;

            NumberSisters = _childSavedInfo.Number_Sisters ?? 0;

            FavLearning = PangeaCollections.FavoriteLearningCollection.Find(_childSavedInfo.Favorite_Learning_Code_ID);

            GradeLvl = PangeaCollections.GradeLevelCollection.Find(_childSavedInfo.Grade_Level_Code_ID);

            Lives_With = PangeaCollections.LivesWithCollection.Find(_childSavedInfo.Lives_With_Code_ID);

            Health = PangeaCollections.HealthStatusCollection.Find(_childSavedInfo.Health_Status_Code_ID);

            Action = PangeaCollections.ActionCollection.Find(_childSavedInfo.Action_ID);

            // This has to be the Main Property so that the Collection Changed property can be resetup correctly
            //AdditionalResources = _childSavedInfo.AdditionalResources; // TODO : Need to get this a different way.

            PangeaInfo.SaveRequired = false;// Bug 334

            LogManager.DebugLogManager.MethodEnd("ChildInfo.Copy(GetASavedEnrollmentResult)");
        }

        public void Copy(GetSavedResults _childSavedInfo)
        {
            LogManager.DebugLogManager.MethodBegin("ChildInfo.Copy(GetSavedEnrollmentsResult)");

            // BUG 317 - Have to set the Country before the Location or it messes up the Populating of the Controls.
            Location _tempLoc = PangeaCollections.LocationCollection.Find(_childSavedInfo.Location_Code_ID);

            // BUG 261 - Forgot to replace the Child ID and Child Number when coping the Child
            ChildNumber = null;

            ChildCountry = PangeaCollections.CountryCollection.Find(_tempLoc.CountryCodeID);

            ChildLocation = _tempLoc;

            ChildID = _childSavedInfo.Child_ID;

            FirstName = _childSavedInfo.First_Name;

            MiddleName = _childSavedInfo.Middle_Name;

            LastName = _childSavedInfo.Last_Name;

            NickName = _childSavedInfo.Nickname;

            DateOfBirth = _childSavedInfo.Date_of_Birth;

            Gender = PangeaCollections.GenderCollection.Find(_childSavedInfo.Gender_Code_ID);

            // This has to be the Main Property so that the Collection Changed property can be resetup correctly
            //AdditionalResources = _childSavedInfo.AdditionalResources; // TODO : Need to get this a different way.

            PangeaInfo.SaveRequired = false;// Bug 334

            LogManager.DebugLogManager.MethodEnd("ChildInfo.Copy(GetSavedEnrollmentsResult)");
        }


        public enum enum_DataOperation { Add, Update };



        /// <summary>
        /// This Function submits the Child Data to the Local DB.
        /// </summary>
        /// <param name="_enroll">Whether to submit the child as ready to send to enroll(true) or to save progress(false)</param>
        private bool SubmitChild_Add_Or_Update_Data
            (
            enum_DataOperation e_DataOperation,
            bool removechild = false
            )
        {
            LogManager.DebugLogManager.MethodBegin("ChildInfo.SubmitChildData");

            bool retVal = true;

            string error_flag_middle_piece = "";

                Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type e_Table_Action_Type
                    = Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type.INSERT; //  { INSERT, UPDATE };

            switch (e_DataOperation)
            {
                case enum_DataOperation.Add:

                    e_Table_Action_Type = Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type.INSERT;

                    break;
                case enum_DataOperation.Update:

                    e_Table_Action_Type = Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type.UPDATE;

                    error_flag_middle_piece = "Update";

                    break;
            }

            try
            {

                CustomMappingSource cust_Map_Source = null;

                cust_Map_Source = new CustomMappingSource(e_Table_Action_Type);

                Aggregate_Child_Data_TablesDataContext _Aggregate_Child_Data_DataContext
                    = new Aggregate_Child_Data_TablesDataContext
                    (
                        PangeaInfo.DBCon,
                        e_Table_Action_Type,
                        cust_Map_Source
                        );

                AddChildResult _addChildResults = 
                    _Aggregate_Child_Data_DataContext
                    .Add_Or_Update_Child_Data
                    (
                        e_Table_Action_Type,
                        PangeaInfo.User.UserID,
                        FirstName,
                        LastName,
                        ChildID,
                        null,
                        MiddleName,
                        DateOfBirth,
                        GradeLvl?.CodeID,
                        Health?.CodeID,
                        Lives_With?.CodeID,
                        FavLearning?.CodeID,
                        null,
                        null,
                        (byte?)Gender?.CodeID,
                        (byte?)NumberBrothers,
                        (byte?)NumberSisters,
                        HasDisability,
                        ChildLocation?.CodeID,
                        NickName
                        );

                if (e_Table_Action_Type == Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type.INSERT)
                {

                    if (ChildID == null)
                    {
                        ChildID = _addChildResults.Child_ID;
                    }

                }

                int results = -1;

                SubmitChildData_Data_Save_Results SubmitChildData_Data_Save_Results = new SubmitChildData_Data_Save_Results();

                if (

                    (
                    e_Table_Action_Type == Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type.INSERT && 
                    ChildID != null
                    ) 

                    ||

                    (
                    e_Table_Action_Type == Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type.UPDATE && 
                    !removechild
                    )

                    )
                {

                    //// Bug 253 - Clear out all the Child Codes from the DB and then add the new ones in.
                    //results = _enrollmentDataContext.Clear_Child_Codes
                    //    (
                    //    PangeaInfo.User.UserID, 
                    //    ChildID, 
                    //    null
                    //    );

                    ;

                    results = 
                        _Aggregate_Child_Data_DataContext
                        .Clear_Child_Codes
                        (
                            e_Table_Action_Type,
                            PangeaInfo.User.UserID,
                            ChildID,
                            null
                            );

                    SubmitChildData_Data_Save_Results
                        .the_Results
                        .Add
                        (
                        new SubmitChildData_Data_Save_Result()
                        {
                            Data_Save_Part = "Clear Child Codes",
                            result = results
                        }
                        );




                    //foreach (Chore _chore in Chores)
                    foreach (Chore _chore in Chores)
                    //{
                    {
                        //    results = _enrollmentDataContext.Add_Child_Chore(PangeaInfo.User.UserID, ChildID, DateTime.Now, _chore.CodeID, null);
                        results = 
                            _Aggregate_Child_Data_DataContext
                            .Add_Child_Chore
                            (
                                e_Table_Action_Type,
                                PangeaInfo.User.UserID,
                                ChildID,
                                DateTime.Now,
                                _chore.CodeID,
                                null
                                );

                        SubmitChildData_Data_Save_Results
                            .the_Results
                            .Add
                            (
                            new SubmitChildData_Data_Save_Result()
                            {
                                Data_Save_Part = "Chore: " + _chore.Name,
                                result = results
                            }
                            );

                    }
                    //}




                    //foreach (FavoriteActivity _favAct in FavActivity)
                    foreach (FavoriteActivity _favAct in FavActivity)
                    //{
                    {
                        //    results = _pendingDataContext.Add_Child_Favorite_Activity(PangeaInfo.User.UserID, ChildID, DateTime.Now, _favAct.CodeID, null);
                        results = 
                            _Aggregate_Child_Data_DataContext
                            .Add_Child_Favorite_Activity
                            (
                                e_Table_Action_Type,
                                PangeaInfo.User.UserID,
                                ChildID,
                                DateTime.Now,
                                _favAct.CodeID,
                                null
                                );

                        SubmitChildData_Data_Save_Results
                            .the_Results
                            .Add
                            (
                            new SubmitChildData_Data_Save_Result()
                            {
                                Data_Save_Part = "Favorite Activity: " + _favAct.Name,
                                result = results
                            }
                            );

                        //}
                    }


                    //if (Personality_type != null)
                    if (Personality_type != null)
                    //{
                    {

                        //    results = _enrollmentDataContext.Add_Child_Personality_Type(PangeaInfo.User.UserID, ChildID, DateTime.Now, Personality_type.CodeID, null);
                        results = 
                            _Aggregate_Child_Data_DataContext
                            .Add_Child_Personality_Type
                            (
                            e_Table_Action_Type,
                            PangeaInfo.User.UserID,
                            ChildID,
                            DateTime.Now,
                            Personality_type.CodeID,
                            null
                            );

                        SubmitChildData_Data_Save_Results.the_Results.Add
                        (
                        new SubmitChildData_Data_Save_Result()
                        {
                            Data_Save_Part = "Personality Type: " + Personality_type.Name,
                            result = results
                        }
                        );

                        //}
                    }

                    ;

                    String _saveLocation = String.Empty;

                    if (ChildID != null)
                    {
                        if (ChildNumber != null)
                        {
                            _saveLocation = String.Format("{0}{1}\\", PangeaInfo.CommonAppPangeaUpdateLoc, PangeaInfo.Child.ChildNumber);

                            SubmitChildData_Data_Save_Results.ChildNumber = ChildNumber;
                        }
                        else
                        {
                            _saveLocation = String.Format("{0}{1}\\", PangeaInfo.CommonAppPangeaUpdateLoc, PangeaInfo.Child.ChildID);

                            SubmitChildData_Data_Save_Results.ChildID = PangeaInfo.Child.ChildID.Value;
                        }
                        
                    }
                    else // Should never go into here.
                        _saveLocation = String.Format("{0}temp\\", PangeaInfo.CommonAppPangeaUpdateLoc);

                    // Also need to remove any saved Files that have been removed from the list.
                    for (int i = 0; i < AdditionalResources.Count; i++)//foreach (RequiredFileStruct _reqDoc in AdditionalResources)
                    {
                        PangeaFileStruct _reqDoc = AdditionalResources[i];

                        // Make Sure the Directory Exists
                        if (!Directory.Exists(_saveLocation))
                            Directory.CreateDirectory(_saveLocation);

                        // Need to move all the files to the ChildNumber/ChildID folder if its not already there.
                        if (!File.Exists(String.Format("{0}{1}", _saveLocation, _reqDoc.ShortFileName)))
                        {
                            File.Copy(_reqDoc.FullFileLoc, String.Format("{0}{1}", _saveLocation, _reqDoc.ShortFileName));

                            _reqDoc.FullFileLoc = String.Format("{0}{1}", _saveLocation, _reqDoc.ShortFileName);
                        }

                        String fileLocation = _reqDoc.FullFileLoc;

                        FileStream _fStream = File.OpenRead(fileLocation);

                        using (BinaryReader reader = new BinaryReader(_fStream))
                        {

                            ;

                            byte[] ba_Child_File = reader.ReadBytes((int)_fStream.Length);

                            ;

                            results = _Aggregate_Child_Data_DataContext.Add_Child_File
                                (
                                e_Table_Action_Type,
                                PangeaInfo.User.UserID,
                                ChildID,
                                ba_Child_File, // new Binary(reader.ReadBytes((int)_fStream.Length)),
                                _reqDoc.ShortFileName,
                                _reqDoc.ContentTypeInfo.CodeID,
                                _reqDoc.FileTypeInfo.CodeID,
                                _reqDoc.FileDataInfo.Make,
                                _reqDoc.FileDataInfo.Model,
                                _reqDoc.FileDataInfo.Software,
                                _reqDoc.FileDataInfo.DateTime,
                                _reqDoc.FileDataInfo.DateTimeOriginal,
                                _reqDoc.FileDataInfo.DateTimeDigitized,
                                _reqDoc.FileDataInfo.GPSVersionID,
                                _reqDoc.FileDataInfo.GPSLatitudeRef,
                                _reqDoc.FileDataInfo.GPSLatitude,
                                _reqDoc.FileDataInfo.GPSLongitudeRef,
                                _reqDoc.FileDataInfo.GPSLongitude,
                                _reqDoc.FileDataInfo.GPSAltitudeRef,
                                _reqDoc.FileDataInfo.GPSAltitude,
                                _reqDoc.FileDataInfo.GPSTimeStamp,
                                _reqDoc.FileDataInfo.GPSImgDirectionRef,
                                _reqDoc.FileDataInfo.GPSDateStamp
                                );

                            ;

                            SubmitChildData_Data_Save_Results
                                .the_Results
                                .Add
                                (
                                new SubmitChildData_Data_Save_Result()
                                {
                                    Data_Save_Part = "Saved File: " + _reqDoc.FullFileLoc,
                                    result = results
                                }
                                );

                            ;

                        }

                        AdditionalResources[i] = _reqDoc;

                    }

                    /*
                    if (e_Table_Action_Type == Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type.UPDATE)
                    {
                    }
                    */

                    ;

                    switch (e_Table_Action_Type)
                    {

                        case Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type.UPDATE:
                                                        
                            // Delete the Tempory File Saving location since we copied everything over to the correct directory.
                            if (Directory.Exists(String.Format("{0}temp\\", PangeaInfo.CommonAppPangeaUpdateLoc)))
                                Directory.Delete(String.Format("{0}temp\\", PangeaInfo.CommonAppPangeaUpdateLoc), true);

                            foreach (ChildDuplicate _cDup in NonDuplicates)
                            {

                                // results = _pendingDataContext.Add_Child_Duplicates(PangeaInfo.User.UserID, ChildID, _cDup.Child2_ID);                                
                                results = _Aggregate_Child_Data_DataContext.Add_Child_Duplicates_for_Updates
                                    (
                                    PangeaInfo.User.UserID,
                                    ChildID,
                                    _cDup.Child2_ID
                                    );


                                SubmitChildData_Data_Save_Results.the_Results.Add
                                    (
                                    new SubmitChildData_Data_Save_Result()
                                    {
                                        Data_Save_Part = "UPDATE - Duplicates - ChildID: " + ChildID.ToString() + " - Child2_ID: " + _cDup.Child2_ID.ToString(),
                                        result = results
                                    }
                                    );


                            }


                            // Bug 311 - The check was trying to save a null or empty string MLE.  Changed check to fix this.
                            if (!String.IsNullOrEmpty(MajorLifeEvent)) 
                            {

                                Child_Major_Life_Event_Markers the_data = _Aggregate_Child_Data_DataContext.Add_Child_Major_Life_Event
                                    (
                                    PangeaInfo.User.UserID,
                                    ChildID,
                                    DateTime.Now.Date,
                                    PangeaCollections.LanguageCollection.Find(System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName).CodeID,
                                    MajorLifeEvent,
                                    null,
                                    enum_Aggregate_Child_Data_TablesDataContext_Type.UPDATE
                                    );

                                LanguageCodeID = PangeaCollections.LanguageCollection.Find(System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName).CodeID; // the_data.LanguageCodeID;

                                results  = (the_data.Major_Life_Event_ID > 0) ? 1 : -1;

                                SubmitChildData_Data_Save_Results.the_Results.Add
                                    (
                                    new SubmitChildData_Data_Save_Result()
                                    {
                                        Data_Save_Part = "UPDATE - MLE: " + MajorLifeEvent,
                                        result = results
                                    }
                                    );

                            }

                            break;
                        case Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type.INSERT:

                            foreach (ChildDuplicate _cDup in NonDuplicates)
                            {

                                // Add_Child_Non_Duplicates_for_Inserts
                                results = 
                                    _Aggregate_Child_Data_DataContext
                                    .Add_Child_Non_Duplicates_for_Inserts
                                    (
                                        PangeaInfo.User.UserID,
                                        ChildID,
                                        _cDup.Child2_ID
                                        );

                                SubmitChildData_Data_Save_Results
                                    .the_Results.Add
                                    (
                                    new SubmitChildData_Data_Save_Result()
                                    {
                                        Data_Save_Part = "INSERT - Duplicates: ChildID: " + ChildID.ToString() + " Child2_ID: " + _cDup.Child2_ID.ToString (),
                                        result = results
                                    }
                                    );

                            }

                            ;




                            break;                    
                    }

                    ;

                }
                
                ;

                retVal = !removechild ? results == 1 : true;

                ;

            }

            catch (Exception ex)
            {
                LogManager.ErrorLogManager.WriteError("ChildInfo.SubmitChild" + error_flag_middle_piece + "Data", ex);

                // BUG 267 - Added Message to let users know there was a problem calling the DB.
                PangeaInfo.Messages.Add(new MessagesStruct("Error with Database Call while saving Child Record.", MessageTypes.CriticalError, "DBError"));

                retVal = false;

#if DEBUG

                MessageBox.Show(ex.Message, "ChildInfo.SubmitChild" + error_flag_middle_piece + "Data");

#endif
            }

            ;
            
            LogManager.DebugLogManager.MethodEnd("ChildInfo.SubmitChild" + error_flag_middle_piece + "Data");

            return retVal;

        }

    }

}
