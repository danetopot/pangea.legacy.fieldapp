﻿////////////////////////////////////////////////////////////////////
/// This is the Lives With Buisness Object.  It has a reference to the
/// Data Object and only exposes certain properties.
////////////////////////////////////////////////////////////////////

using Centraler;
using System;
using System.Collections.ObjectModel;

namespace Pangea.Data.Model.CodeTables
{
    public class LivesWith
    {
        public LivesWith(ObservableCollection<Object> _livesWith)
        {
            Active = Convert.ToBoolean(_livesWith[0]);
            CodeID = Convert.ToInt32(_livesWith[1]);
            Name = Convert.ToString(_livesWith[2]);
            Description = !_livesWith[3].Equals(DBNull.Value) ? Convert.ToString(_livesWith[3]) : String.Empty;
            PublicDescription = !_livesWith[4].Equals(DBNull.Value) ? Convert.ToString(_livesWith[4]) : String.Empty;
            MinAge = !_livesWith[5].Equals(DBNull.Value) ? Convert.ToInt32(_livesWith[5]) : int.MinValue;
        }

        public LivesWith(CodeTables_StandardData std_data)
        {
            Active = std_data.Active;
            CodeID = std_data.CodeID;
            Name = std_data.Name;
            Description = std_data.Description;
            PublicDescription = std_data.PublicDescription;
            MinAge = std_data.MinAge;
        }

        public bool Active { get; set; }

        public int CodeID { get; set; }

        public String Name { get; set; }

        public String Description { get; set; }

        public String PublicDescription { get; set; }

        public int MinAge { get; set; }

        /// <summary>
        /// Used to Determine if the two Lives With are equal.
        /// </summary>
        /// <param name="obj">The Lives With obj that we are testing with</param>
        /// <returns>True if they are equal/False if they are not.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            LivesWith _obj = obj as LivesWith;

            return _obj.CodeID == CodeID;
        }

        /// <summary>
        /// Operator to determine if the two Lives With are Equal
        /// </summary>
        /// <param name="c1">Lives With</param>
        /// <param name="c2">Lives With</param>
        /// <returns>True if Equal/False if Not Equal</returns>
        public static bool operator ==(LivesWith c1, LivesWith c2)
        {
            if ((object)c1 == null)
                return ((object)c2 == null);

            return c1.Equals(c2);
        }

        /// <summary>
        /// Operator to determine if the two Lives With are not Equal
        /// </summary>
        /// <param name="c1">Lives With</param>
        /// <param name="c2">Lives With</param>
        /// <returns>False if Equal/True if Not Equal</returns>
        public static bool operator !=(LivesWith c1, LivesWith c2)
        {
            if ((object)c1 == null)
                return ((object)c2 != null);

            return !c1.Equals(c2);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
