﻿
using System;
using System.Collections.ObjectModel;

namespace Pangea.Data.Model.CodeTables
{
    public class FileType
    {
        public FileType(ObservableCollection<Object> _fileType)
        {
            Active = Convert.ToBoolean(_fileType[0]);
            CodeID = Convert.ToInt32(_fileType[1]);
            Name = Convert.ToString(_fileType[2]);
            Description = !_fileType[3].Equals(DBNull.Value) ? Convert.ToString(_fileType[3]) : String.Empty;
            Extension = !_fileType[4].Equals(DBNull.Value) ? Convert.ToString(_fileType[4]) : String.Empty;
        }

        public bool Active { get; set; }

        public int CodeID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Extension { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            FileType tempType = obj as FileType;

            return tempType.CodeID == CodeID;
        }

        public static bool operator ==(FileType c1, FileType c2)
        {
            if ((object)c1 == null)
                return ((object)c2 == null);

            return c1.Equals(c2);
        }

        public static bool operator !=(FileType c1, FileType c2)
        {
            if ((object)c1 == null)
                return ((object)c2 != null);

            return !c1.Equals(c2);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
