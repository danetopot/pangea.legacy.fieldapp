﻿////////////////////////////////////////////////////////////////////
/// This is the Users Buisness Object.  It has a reference to the
/// Data Object and only exposes certain properties.
////////////////////////////////////////////////////////////////////

using System;

using Pangea.Utilities.Base.Classes;


namespace Pangea.Data.Model.CodeTables
{
    public class UserInfo : PangeaBaseNotifyProperties
    {
        public UserInfo()
        {
            _loggedIn = false;
            _canEnroll = false;
            _canUpdate = false;

            _lastLogginDate = null;
        }

        public void Clear()
        {
            UserID = Guid.Empty;
            UserName = String.Empty;
            FullName = String.Empty;

            LoggedIn = false;
            CanEnroll = false;
            CanUpdate = false;
            LastLogginDate = null;
        }

        public void Update(UserInfo _nUser)
        {
            UserID = _nUser.UserID;
            UserName = _nUser.UserName;
            FullName = _nUser.FullName;
            CanEnroll = _nUser.CanEnroll;
            CanUpdate = _nUser.CanUpdate;
            LastLogginDate = _nUser.LastLogginDate;
        }

        /// <summary>
        /// Lets the System know the User has been logged in or the
        /// saved user is less than 30 days old
        /// </summary>
        private bool _loggedIn;
        public bool LoggedIn
        {
            get { return _loggedIn; }
            set
            {
                if (_loggedIn != value)
                {
                    SendPropertyChanging();
                    _loggedIn = value;
                    SendPropertyChanged("LoggedIn");
                }
            }
        }

        /// <summary>
        /// Determines if the user is valid or not.
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// User ID associated with this user.
        /// </summary>
        public Guid UserID { get; set; }

        /// <summary>
        /// Username of the Employee. 
        /// Used after login to determine what the employee can do.
        /// </summary>
        public String UserName { get; set; }

        /// <summary>
        /// User's First and Last name.
        /// </summary>
        public String FullName { get; set; }

        /// <summary>
        /// User Can Enroll Children
        /// </summary>
        private bool _canEnroll;
        public bool CanEnroll
        {
            get { return _canEnroll; }
            set
            {
                if (_canEnroll != value)
                {
                    SendPropertyChanging();
                    _canEnroll = value;
                    SendPropertyChanged("CanEnroll");
                }
            }
        }

        /// <summary>
        /// User Can Update Children
        /// </summary>
        private bool _canUpdate;
        public bool CanUpdate
        {
            get { return _canUpdate; }
            set
            {
                if (_canUpdate != value)
                {
                    SendPropertyChanging();
                    _canUpdate = value;
                    SendPropertyChanged("CanUpdate");
                }
            }
        }

        /// <summary>
        /// Last Loggin Date for User.
        /// </summary>
        private DateTime? _lastLogginDate;
        public DateTime? LastLogginDate
        {
            get { return _lastLogginDate; }
            set
            {
                if (_lastLogginDate != value)
                {
                    SendPropertyChanging();
                    _lastLogginDate = value;
                    SendPropertyChanged("LastLogginDate");
                }
            }
        }
    }
}
