﻿////////////////////////////////////////////////////////////////////
/// This is the Country Gender Object.  It has a reference to the
/// Data Object and only exposes certain properties.
////////////////////////////////////////////////////////////////////

using System;
using System.Collections.ObjectModel;

namespace Pangea.Data.Model.CodeTables
{
    public class Gender
    {
        public Gender(ObservableCollection<Object> _gender)
        {
            Active = Convert.ToBoolean(_gender[0]);
            CodeID = Convert.ToInt32(_gender[1]);
            Name = !_gender[2].Equals(DBNull.Value) ? Convert.ToString(_gender[2]) : String.Empty;
            Description = !_gender[3].Equals(DBNull.Value) ? Convert.ToString(_gender[3]) : String.Empty;
            PublicDescription = !_gender[4].Equals(DBNull.Value) ? Convert.ToString(_gender[4]) : String.Empty;
        }


        public Gender
            (
            bool _Active,
            Int32 _CodeID,
            string _Name,
            string _Description,
            string _PublicDescription
            )
        {
            Active = _Active; //  Convert.ToBoolean(_gender[0]);
            CodeID = _CodeID; //  Convert.ToInt32(_gender[1]);
            Name = _Name; // !_gender[2].Equals(DBNull.Value) ? Convert.ToString(_gender[2]) : String.Empty;
            Description = _Description; // !_gender[3].Equals(DBNull.Value) ? Convert.ToString(_gender[3]) : String.Empty;
            PublicDescription = _PublicDescription; // !_gender[4].Equals(DBNull.Value) ? Convert.ToString(_gender[4]) : String.Empty;
        }


        public bool Active { get; set; }

        /// <summary>
        /// This is the ID for the Gender. 
        /// </summary>
        public int CodeID { get; set; }

        public String Name { get; set; }

        /// <summary>
        /// The Description of the current Item.
        /// In this case it is either Male of Female.
        /// </summary>
        public String Description { get; set; }

        public String PublicDescription { get; set; }

        /// <summary>
        /// Used to Determine if the two Genders are equal.
        /// </summary>
        /// <param name="obj">The Gender obj that we are testing with</param>
        /// <returns>True if they are equal/False if they are not.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Gender _obj = obj as Gender;

            return _obj.CodeID == CodeID;
        }

        /// <summary>
        /// Operator to determine if the two Genders are Equal
        /// </summary>
        /// <param name="c1">Gender</param>
        /// <param name="c2">Gender</param>
        /// <returns>True if Equal/False if Not Equal</returns>
        public static bool operator ==(Gender c1, Gender c2)
        {
            if ((object)c1 == null)
                return ((object)c2 == null);

            return c1.Equals(c2);
        }

        /// <summary>
        /// Operator to determine if the two Genders are not Equal
        /// </summary>
        /// <param name="c1">Gender</param>
        /// <param name="c2">Gender</param>
        /// <returns>False if Equal/True if Not Equal</returns>
        public static bool operator !=(Gender c1, Gender c2)
        {
            if ((object)c1 == null)
                return ((object)c2 != null);

            return !c1.Equals(c2);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
