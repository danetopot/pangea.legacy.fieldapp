﻿
using System;
using System.Collections.ObjectModel;

namespace Pangea.Data.Model.CodeTables
{
    public class Action
    {
        /// <summary>
        /// The Constructor of the Action Class.
        /// </summary>
        /// <param name="_action">The Current Action Properties</param>
        public Action(ObservableCollection<Object> _action)
        {
            ID =    Convert.ToInt32(_action[0]);
            Name =  Convert.ToString(_action[1]);
        }

        public Action(Int32 _ID, string _Name)
        {
            ID = _ID;

            Name = _Name;
        }


        /// <summary>
        /// The Unique ID for the Action
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// The Text Definition of the Action
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Used to Determine if the two Action are equal.
        /// </summary>
        /// <param name="obj">The Action obj that we are testing with</param>
        /// <returns>True if they are equal/False if they are not.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Action _obj = obj as Action;
            
            return _obj.ID == ID;
        }

        /// <summary>
        /// Operator to determine if the two Actions are Equal
        /// </summary>
        /// <param name="c1">Action</param>
        /// <param name="c2">Action</param>
        /// <returns>True if Equal/False if Not Equal</returns>
        public static bool operator ==(Action c1, Action c2)
        {
            if ((object)c1 == null)
                return ((object)c2 == null);

            return c1.Equals(c2);
        }

        /// <summary>
        /// Operator to determine if the two Actions are not Equal
        /// </summary>
        /// <param name="c1">Action</param>
        /// <param name="c2">Action</param>
        /// <returns>False if Equal/True if Not Equal</returns>
        public static bool operator !=(Action c1, Action c2)
        {
            if ((object)c1 == null)
                return ((object)c2 != null);

            return !c1.Equals(c2);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
