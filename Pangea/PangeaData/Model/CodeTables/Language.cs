﻿
using System;
using System.Collections.ObjectModel;

namespace Pangea.Data.Model.CodeTables
{
    public class Language
    {
        public Language(ObservableCollection<Object> _language)
        {
            Active = Convert.ToBoolean(_language[0]);
            CodeID = Convert.ToInt32(_language[1]);
            Code = Convert.ToString(_language[2]);
            Description = !_language[3].Equals(DBNull.Value) ? Convert.ToString(_language[3]) : String.Empty;
        }

        public bool Active { get; set; }

        public int CodeID { get; set; }

        public String Code { get; set; }

        public String Description { get; set; }

        /// <summary>
        /// Used to Determine if the two Language are equal.
        /// </summary>
        /// <param name="obj">The Language obj that we are testing with</param>
        /// <returns>True if they are equal/False if they are not.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Language _obj = obj as Language;

            return _obj.CodeID == CodeID;
        }

        /// <summary>
        /// Operator to determine if the two Language are Equal
        /// </summary>
        /// <param name="c1">Language</param>
        /// <param name="c2">Language</param>
        /// <returns>True if Equal/False if Not Equal</returns>
        public static bool operator ==(Language c1, Language c2)
        {
            if ((object)c1 == null)
                return ((object)c2 == null);

            return c1.Equals(c2);
        }

        /// <summary>
        /// Operator to determine if the two Language are not Equal
        /// </summary>
        /// <param name="c1">Language</param>
        /// <param name="c2">Language</param>
        /// <returns>False if Equal/True if Not Equal</returns>
        public static bool operator !=(Language c1, Language c2)
        {
            if ((object)c1 == null)
                return ((object)c2 != null);

            return !c1.Equals(c2);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
