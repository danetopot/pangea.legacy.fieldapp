﻿////////////////////////////////////////////////////////////////////
/// This is the Country Buisness Object.  It has a reference to the
/// Data Object and only exposes certain properties.
////////////////////////////////////////////////////////////////////

using System;
using System.Collections.ObjectModel;

namespace Pangea.Data.Model.CodeTables
{
    public class Country
    {
        public Country(ObservableCollection<Object> _country)
        {
            // 0        1           2           3           4              5    6       7       8               9               10          11              12                  13
            //Active, Action_ID, Action_User, Action_Date, Action_Reason, TS, TSLONG, TSMAN, Country_Code_ID, Country_Code, Region_Code_ID, Country_Name, [Default_Language], Min_School_Age
            Active = Convert.ToBoolean(_country[0]);
            CodeID = Convert.ToInt32(_country[8]);
            Code = !_country[9].Equals(DBNull.Value) ? Convert.ToString(_country[9]) : String.Empty;
            RegionCodeID = !_country[10].Equals(DBNull.Value) ? Convert.ToInt32(_country[10]) : int.MinValue;
            Name = Convert.ToString(_country[11]);
            DefaultLanguage = !_country[12].Equals(DBNull.Value) ? Convert.ToInt32(_country[12]) : int.MinValue;
            MinSchoolAge = !_country[13].Equals(DBNull.Value) ? Convert.ToInt32(_country[13]) : int.MinValue;
        }


        public Country
            (
            bool _Active,
            int _CodeID,
            string _Code,
            string _Name,
            int _RegionCodeID = int.MinValue,
            int _MinSchoolAge = int.MinValue,
            int _DefaultLanguage = int.MinValue
            )
        {
            // 0        1           2           3           4              5    6       7       8               9               10          11              12                  13
            //Active, Action_ID, Action_User, Action_Date, Action_Reason, TS, TSLONG, TSMAN, Country_Code_ID, Country_Code, Region_Code_ID, Country_Name, [Default_Language], Min_School_Age
            
            Active = _Active; // Convert.ToBoolean(_country[0]);
            
            CodeID = _CodeID; // Convert.ToInt32(_country[8]);

            Code = _Code; // !_country[9].Equals(DBNull.Value) ? Convert.ToString(_country[9]) : String.Empty;

            RegionCodeID = _RegionCodeID; // !_country[10].Equals(DBNull.Value) ? Convert.ToInt32(_country[10]) : int.MinValue;

            Name = _Name;  // Convert.ToString(_country[11]);

            DefaultLanguage = _DefaultLanguage; // !_country[12].Equals(DBNull.Value) ? Convert.ToInt32(_country[12]) : int.MinValue;

            MinSchoolAge = _MinSchoolAge; // !_country[13].Equals(DBNull.Value) ? Convert.ToInt32(_country[13]) : int.MinValue;
        }

        /// <summary>
        /// Is an Active Country
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// The ID for the Current Country.
        /// </summary>
        public int CodeID { get; set; }

        /// <summary>
        /// Code Associated With the Current County. (2 Digit Code)
        /// What is used to associate anythig that needs the country infomation.
        /// </summary>
        public String Code { get; set; }

        /// <summary>
        /// The Region this Country is associated with.
        /// </summary>
        public int RegionCodeID { get; set; }

        /// <summary>
        /// Name of the Country
        /// </summary>
        public String Name { get; set; }
        
        /// <summary>
        /// Default language of the Country
        /// </summary>
        public int DefaultLanguage { get; set; }

        /// <summary>
        /// The Min Age for School for this country.
        /// </summary>
        public int MinSchoolAge { get; set; }

        /// <summary>
        /// Used to Determine if the two countries are equal.
        /// </summary>
        /// <param name="obj">The Country obj that we are testing with</param>
        /// <returns>True if they are equal/False if they are not.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Country _obj = obj as Country;
            
            return _obj.CodeID == CodeID;
        }

        /// <summary>
        /// Operator to determine if the two Countries are Equal
        /// </summary>
        /// <param name="c1">Country</param>
        /// <param name="c2">Country</param>
        /// <returns>True if Equal/False if Not Equal</returns>
        public static bool operator ==(Country c1, Country c2)
        {
            if ((object)c1 == null)
                return ((object)c2 == null);

            return c1.Equals(c2);
        }

        /// <summary>
        /// Operator to determine if the two Countries are not Equal
        /// </summary>
        /// <param name="c1">Country</param>
        /// <param name="c2">Country</param>
        /// <returns>False if Equal/True if Not Equal</returns>
        public static bool operator !=(Country c1, Country c2)
        {
            if ((object)c1 == null)
                return ((object)c2 != null);

            return !c1.Equals(c2);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
