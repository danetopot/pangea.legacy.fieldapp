﻿///////////////////////////////////////////////////////////////////////////////
/// This is the base Child Class that will be used to store all the child information
/// for enrollments and updates. 
/// ////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.ObjectModel;

using Pangea.Data.Collections;
using Pangea.Data.DataObjects;
using Pangea.Data.Model.CodeTables;
using Pangea.Utilities.Base.Classes;


namespace Pangea.Data.Model
{
    /// <summary>
    /// Class for storing all the Child information
    /// </summary>PangeaCollections.ColumnRulesCollection
    public class Child : PangeaBaseNotifyProperties
    {
        protected ChildDO _childDO;

        public Child()
        {
            _childDO = new ChildDO();
            
            // BUG 334 
            PangeaInfo.SaveRequired = false;
        }

        public Child(ChildDO _child)
        {
            _childDO = _child;
            
            // BUG 334 
            PangeaInfo.SaveRequired = false;
        }

        public Child(GetSavedResults _child)
        {
            _childDO = new ChildDO();

            // BUG 317 - Have to set the Country before the Location or it messes up the Populating of the Controls.
            Location _tempLoc = PangeaCollections.LocationCollection.Find(_child.Location_Code_ID);

            ChildCountry = PangeaCollections.CountryCollection.Find(_tempLoc.CountryCodeID);

            ChildLocation = _tempLoc;

            ChildID = _child.Child_ID;

            FirstName = _child.First_Name;

            MiddleName = _child.Middle_Name;

            LastName = _child.Last_Name;

            DateOfBirth = _child.Date_of_Birth;

            Gender = PangeaCollections.GenderCollection.Find(_child.Gender_Code_ID);

            NickName = _child.Nickname;
            
            // BUG 334 
            PangeaInfo.SaveRequired = false;
        }

        public Child(Guid? _childID, GetASavedResult _child)
        {
            _childDO = new ChildDO();

            // BUG 317 - Have to set the Country before the Location or it messes up the Populating of the Controls.
            Location _tempLoc = PangeaCollections.LocationCollection.Find(_child.Location_Code_ID);
            
            bool Is_Temp_Location_NOT_NULL = !(_tempLoc == null);

            bool Is_Country_Not_Null = false;

            if (Is_Temp_Location_NOT_NULL)
            {

                Is_Country_Not_Null = !(PangeaCollections.CountryCollection.Find(_tempLoc.CountryCodeID) == null);

            }

            if(
                Is_Temp_Location_NOT_NULL &&
                Is_Country_Not_Null
                )
            {

                try
                {
                    ChildCountry = PangeaCollections.CountryCollection.Find(_tempLoc.CountryCodeID);
                }
                catch (Exception ee)
                {

                    ;

                    if (Centraler.Database_Settingz.Is_Special_Handling_For_Bad_Data)
                    {
                        _tempLoc = Shim_Country_Value();

                    }
                    else
                    {
                        throw (ee);
                    }
                }

            }
            else
            {
                if (Centraler.Database_Settingz.Is_Special_Handling_For_Bad_Data)
                {
                    _tempLoc = Shim_Country_Value();
                }
            }


            ChildLocation = _tempLoc;

            ChildID = _childID;

            ChildNumber = _child.Child_Number;

            FirstName = _child.First_Name;

            MiddleName = _child.Middle_Name;

            LastName = _child.Last_Name;

            DateOfBirth = _child.Date_of_Birth;

            GradeLvl = PangeaCollections.GradeLevelCollection.Find(_child.Grade_Level_Code_ID);

            Health = PangeaCollections.HealthStatusCollection.Find(_child.Health_Status_Code_ID);

            Lives_With = PangeaCollections.LivesWithCollection.Find(_child.Lives_With_Code_ID);

            FavLearning = PangeaCollections.FavoriteLearningCollection.Find(_child.Favorite_Learning_Code_ID);

            Gender = PangeaCollections.GenderCollection.Find(_child.Gender_Code_ID);

            NumberBrothers = Convert.ToInt32(_child.Number_Brothers);

            NumberSisters = Convert.ToInt32(_child.Number_Sisters);

            HasDisability = _child.Disability_Status;

            NickName = _child.Nickname;

            Action = PangeaCollections.ActionCollection.Find(_child.Action_ID);

            // BUG 334 
            PangeaInfo.SaveRequired = false;


            Location Shim_Country_Value()
            {
                Location _tempLoc_2 = PangeaCollections.LocationCollection.Find(4077);

                ChildCountry = PangeaCollections.CountryCollection.Find(_tempLoc_2.CountryCodeID);

                return _tempLoc_2;
            }

        }

        public Child(GetSavedPendingsResult _child)
        {
            _childDO = new ChildDO();

            // BUG 317 - Have to set the Country before the Location or it messes up the Populating of the Controls.
            Location _tempLoc = PangeaCollections.LocationCollection.Find(_child.Location);

            ChildCountry = PangeaCollections.CountryCollection.Find(_tempLoc.CountryCodeID);

            ChildLocation = _tempLoc;

            ChildID = _child.Child_ID;

            ChildNumber = _child.Child_Number;

            FirstName = _child.First_Name;

            MiddleName = _child.Middle_Name;

            LastName = _child.Last_Name;

            DateOfBirth = _child.Date_of_Birth;

            Gender = PangeaCollections.GenderCollection.Find(_child.Gender);

            NickName = _child.Nickname;

            // BUG 334 
            PangeaInfo.SaveRequired = false;
        }

        public Child(GetASavedPendingResult _child)
        {
            _childDO = new ChildDO();

            // BUG 317 - Have to set the Country before the Location or it messes up the Populating of the Controls.
            Location _tempLoc = PangeaCollections.LocationCollection.Find(_child.Location_Code_ID);

            ChildCountry = PangeaCollections.CountryCollection.Find(_tempLoc.CountryCodeID);

            ChildLocation = _tempLoc;

            ChildID = _child.Child_ID;

            ChildNumber = _child.Child_Number;

            FirstName = _child.First_Name;

            MiddleName = _child.Middle_Name;

            LastName = _child.Last_Name;

            DateOfBirth = _child.Date_of_Birth;

            GradeLvl = PangeaCollections.GradeLevelCollection.Find(_child.Grade_Level_Code_ID);

            Health = PangeaCollections.HealthStatusCollection.Find(_child.Health_Status_Code_ID);

            Lives_With = PangeaCollections.LivesWithCollection.Find(_child.Lives_With_Code_ID);

            FavLearning = PangeaCollections.FavoriteLearningCollection.Find(_child.Favorite_Learning_Code_ID);

            Gender = PangeaCollections.GenderCollection.Find(_child.Gender_Code_ID);

            NumberBrothers = Convert.ToInt32(_child.Number_Brothers);

            NumberSisters = Convert.ToInt32(_child.Number_Sisters);

            HasDisability = _child.Disability_Status;

            NickName = _child.Nickname;

            Action = PangeaCollections.ActionCollection.Find(_child.Action_ID);

            // BUG 334 
            PangeaInfo.SaveRequired = false;
        }

        /// <summary>
        /// Storage for the Child Number that is returned after the child is sent for enrollment
        /// </summary>
        public long? ChildNumber
        {
            get { return _childDO.Child_Number; }
            set
            {
                if (_childDO.Child_Number != value)
                {
                    SendPropertyChanging();

                    _childDO.Child_Number = value;

                    PangeaInfo.SaveRequired = true;// BUG 334 

                    SendPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Storage for the Child ID that is returned after child is saved the first time.
        /// </summary>
        private Guid? _childID;
        public Guid? ChildID
        {
            get
            {
                if (_childID == null && !_childDO.Child_ID.Equals(Guid.Empty))
                    _childID = _childDO.Child_ID;

                return _childID;
            }
            set
            {
                if (_childID != value)
                {
                    SendPropertyChanging();

                    _childID = value;

                    if (_childID.HasValue)
                        _childDO.Child_ID = _childID.Value;
                    else
                        _childDO.Child_ID = Guid.Empty; // BUG 261 - Need to make sure i empty the Child ID if the Child ID is set to NULL

                    PangeaInfo.SaveRequired = true;// BUG 334 

                    SendPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Storage for the Country of the Child
        /// </summary>
        private Country _childCountry;
        public Country ChildCountry
        {
            get
            {
                if (
                    _childCountry == null && 
                    ChildLocation != null
                    )
                    _childCountry = PangeaCollections.CountryCollection.Find(ChildLocation.CountryCodeID);

                return _childCountry;
            }
            set
            {
                if (_childCountry != value)
                {
                    SendPropertyChanging();

                    _childCountry = value;

                    PangeaInfo.SaveRequired = true;// BUG 334 

                    SendPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Storage for the Location of the Child
        /// </summary>
        private Location _childLocation;
        public Location ChildLocation
        {
            get
            {
                if 
                    (
                    _childLocation == null && 
                    _childDO.Location_Code_ID != null
                    )
                    _childLocation = 
                        PangeaCollections
                        .LocationCollection
                        .Find(_childDO.Location_Code_ID);

                return _childLocation;
            }
            set
            {
                if (_childLocation != value)
                {
                    SendPropertyChanging();

                    _childLocation = value;

                    _childDO.Location_Code_ID = _childLocation?.CodeID;

                    PangeaInfo.SaveRequired = true;// BUG 334 

                    SendPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Storage for the First Name of the Child
        /// </summary>
        public String FirstName
        {
            get { return _childDO.First_Name; }
            set
            {
                if (_childDO.First_Name != value)
                {
                    SendPropertyChanging();

                    _childDO.First_Name = value;

                    PangeaInfo.SaveRequired = true;// BUG 334 

                    SendPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Storage for the Middle Name of the Child
        /// </summary>
        public String MiddleName
        {
            get { return _childDO.Middle_Name; }
            set
            {
                if (_childDO.Middle_Name != value)
                {
                    SendPropertyChanging();

                    _childDO.Middle_Name = value;

                    PangeaInfo.SaveRequired = true;// BUG 334 

                    SendPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Storage for the Last Name of the Child
        /// </summary>
        public String LastName
        {
            get { return _childDO.Last_Name; }
            set
            {
                if (_childDO.Last_Name != value)
                {
                    SendPropertyChanging();

                    _childDO.Last_Name = value;

                    PangeaInfo.SaveRequired = true;// BUG 334 

                    SendPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Storage for the Other Name Know by of the Child
        /// </summary>
        public String NickName
        {
            get 
            { 
                return _childDO.Nickname; 
            }
            set
            {
                ;

                if (_childDO.Nickname != value)
                {
                    SendPropertyChanging();

                    _childDO.Nickname = value;

                    PangeaInfo.SaveRequired = true;// BUG 334 

                    SendPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Storage for the DOB of the Child
        /// </summary>
        private DateTime? _dateOfBirth;
        public DateTime? DateOfBirth
        {
            get
            {
                if (_childDO.Date_of_Birth != null && _childDO.Date_of_Birth != _dateOfBirth)
                    _dateOfBirth = _childDO.Date_of_Birth.Value;

                return _dateOfBirth;
            }
            set
            {
                if (_dateOfBirth != value)
                {
                    SendPropertyChanging();

                    _dateOfBirth = value;

                    _childDO.Date_of_Birth = _dateOfBirth;

                    PangeaInfo.SaveRequired = true;// BUG 334 

                    SendPropertyChanged();
                }
            }
        }

        public String DOBStr
        {
            get
            {
                if (DateOfBirth != null)
                    return DateOfBirth.Value.Date.ToString("d");

                return String.Empty;
            }
        }

        /// <summary>
        /// The Current Numeric Age of the Child.
        /// </summary>
        public double ChildAge
        {
            get
            {
                if (DateOfBirth.HasValue)
                    return DateTime.Now.Date.Subtract(DateOfBirth.Value.Date).TotalDays;

                return 0;
            }
        }

        /// <summary>
        /// Storage for the Gender of the Child
        /// </summary>
        private Gender _gender;
        public Gender Gender
        {
            get
            {
                if (_gender == null && _childDO.Gender_Code_ID != null)
                    _gender = PangeaCollections.GenderCollection.Find(_childDO.Gender_Code_ID);

                return _gender;
            }
            set
            {
                if (_gender != value)
                {
                    SendPropertyChanging();

                    _gender = value;

                    _childDO.Gender_Code_ID = _gender?.CodeID;

                    PangeaInfo.SaveRequired = true;// BUG 334 

                    SendPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Storage for the Disability of the Child
        /// </summary>
        private bool? _hasDisability;
        public bool? HasDisability
        {
            get
            {
                if (_hasDisability == null && _childDO.Disability_Status_Code_ID != null)
                    _hasDisability = _childDO.Disability_Status_Code_ID;

                return _hasDisability;
            }
            set
            {
                if (_hasDisability != value)
                {
                    SendPropertyChanging();

                    _hasDisability = value;

                    _childDO.Disability_Status_Code_ID = _hasDisability;

                    PangeaInfo.SaveRequired = true;// BUG 334 

                    SendPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Storage for the Number of Brothers of the Child
        /// </summary>
        private int _numberBrothers;
        public int NumberBrothers
        {
            get { return _numberBrothers; }
            set
            {
                if (_numberBrothers != value)
                {
                    SendPropertyChanging();

                    _numberBrothers = value;

                    _childDO.Number_Brothers = Convert.ToByte(_numberBrothers);

                    PangeaInfo.SaveRequired = true;// BUG 334 

                    SendPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Storage for the Number of Sisters of the Child
        /// </summary>
        private int _numberSisters;
        public int NumberSisters
        {
            get { return _numberSisters; }
            set
            {
                if (_numberSisters != value)
                {
                    SendPropertyChanging();

                    _numberSisters = value;

                    _childDO.Number_Sisters = Convert.ToByte(_numberSisters);

                    PangeaInfo.SaveRequired = true;// BUG 334 

                    SendPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Storage for the Favorite Learning of the Child
        /// </summary>
        private FavoriteLearning _favLearning;

        public FavoriteLearning FavLearning
        {
            get
            {
                if (_favLearning == null && _childDO.Favorite_Learning_Code_ID != null)
                    _favLearning = PangeaCollections.FavoriteLearningCollection.Find(_childDO.Favorite_Learning_Code_ID);

                return _favLearning;
            }
            set
            {
                if (_favLearning != value)
                {
                    SendPropertyChanging();

                    _favLearning = value;

                    _childDO.Favorite_Learning_Code_ID = _favLearning?.CodeID;

                    PangeaInfo.SaveRequired = true;// BUG 334 

                    SendPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Storage for the Grade Level of the Child
        /// </summary>
        private GradeLevel _gradeLevel;
        public GradeLevel GradeLvl
        {
            get
            {
                if (_gradeLevel == null && _childDO.Grade_Level_Code_ID != null)
                    _gradeLevel = PangeaCollections.GradeLevelCollection.Find(_childDO.Grade_Level_Code_ID);

                return _gradeLevel;
            }
            set
            {
                if (_gradeLevel != value)
                {
                    SendPropertyChanging();

                    _gradeLevel = value;

                    _childDO.Grade_Level_Code_ID = _gradeLevel?.CodeID;

                    PangeaInfo.SaveRequired = true;// BUG 334 

                    SendPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Storage for the Personality Type of the Child
        /// </summary>
        private PersonalityType _personalityType;
        public PersonalityType Personality_type
        {
            get { return _personalityType; }
            set
            {
                if (_personalityType != value)
                {
                    SendPropertyChanging();

                    _personalityType = value;

                    PangeaInfo.SaveRequired = true;// BUG 334 

                    SendPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Storage for the Lives With of the Child
        /// </summary>
        private LivesWith _livesWith;
        public LivesWith Lives_With
        {
            get
            {
                if (_livesWith == null && _childDO.Lives_With_Code_ID != null)
                    _livesWith = PangeaCollections.LivesWithCollection.Find(_childDO.Lives_With_Code_ID);

                return _livesWith;
            }
            set
            {
                if (_livesWith != value)
                {
                    SendPropertyChanging();

                    _livesWith = value;

                    _childDO.Lives_With_Code_ID = _livesWith?.CodeID;

                    PangeaInfo.SaveRequired = true;// BUG 334 

                    SendPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Storage for the Health of the Child
        /// </summary>
        private HealthStatus _health;
        public HealthStatus Health
        {
            get
            {
                if (
                    _health == null && 
                    _childDO.Health_Status_Code_ID != null
                    )
                    _health = 
                        PangeaCollections
                        .HealthStatusCollection
                        .Find(_childDO.Health_Status_Code_ID);

                return _health;
            }
            set
            {
                if (_health != value)
                {
                    SendPropertyChanging();

                    _health = value;

                    _childDO.Health_Status_Code_ID = _health?.CodeID;

                    PangeaInfo.SaveRequired = true;// BUG 334 

                    SendPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Storage for the Collection of the Childs Chores
        /// </summary>
        private ObservableCollection<Chore> _chores;
        public ObservableCollection<Chore> Chores
        {
            get
            {
                if (_chores == null)
                    _chores = new ObservableCollection<Chore>();
                return _chores;
            }
            set
            {
                if (_chores != value)
                {
                    SendPropertyChanging();

                    _chores = value;

                    SendPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Storage for the Collection of the Childs Favorite Activity
        /// </summary>
        private ObservableCollection<FavoriteActivity> _favAct;
        public ObservableCollection<FavoriteActivity> FavActivity
        {
            get
            {
                if (_favAct == null)
                    _favAct = new ObservableCollection<FavoriteActivity>();
                return _favAct;
            }
            set
            {
                if (_favAct != value)
                {
                    SendPropertyChanging();

                    _favAct = value;

                    SendPropertyChanged();
                }
            }
        }

        

        /// <summary>
        /// Storage for the Collection of Additional Resources of the child.
        /// Additional Resources are Consent Form, Profile Picture, and Action Picture.
        /// In future there may be more Additional Resources.. videos and stuff.
        /// </summary>
        private FilesCollection _additionalResources;

        public FilesCollection AdditionalResources
        {
            get
            {
                if (_additionalResources == null)
                    _additionalResources = new FilesCollection();

                return _additionalResources;
            }
            set
            {
                if (_additionalResources != value)
                {
                    SendPropertyChanging();

                    _additionalResources = value;

                    PangeaInfo.SaveRequired = true;// BUG 334 

                    SendPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Storage for a list of possible duplicates that the user has
        /// said this child is not a duplicate of them.
        /// </summary>
        private ObservableCollection<ChildDuplicate> _nonDuplicates;

        public ObservableCollection<ChildDuplicate> NonDuplicates
        {
            get
            {
                if (_nonDuplicates == null)
                    _nonDuplicates = new ObservableCollection<ChildDuplicate>();

                return _nonDuplicates;
            }
            set
            {
                if (_nonDuplicates != value)
                {
                    SendPropertyChanging();

                    _nonDuplicates = value;

                    SendPropertyChanged();
                }
            }
        }

        /// <summary>
        /// This is the value used to determine if the child record
        /// is just saved or if its saved waiting to be enrolled.
        /// We do not want to let anything modify this.
        /// </summary>
        public bool? EnrollmentReady
        {
            get { return _childDO.Enrollment_Ready; }
        }

        /// <summary>
        /// This is where we are going to Save the Major Life Event
        /// Text until we save it to the DB with the Child Information.
        /// </summary>
        private String _majorLifeEvent;

        public String MajorLifeEvent
        {
            get 
            { 
                return _majorLifeEvent; 
            }
            set
            {
                ;

                if (_majorLifeEvent != value)
                {
                    SendPropertyChanging();

                    _majorLifeEvent = value;

                    PangeaInfo.SaveRequired = true;// BUG 334 

                    SendPropertyChanged();
                }
            }
        }


        private int _LanguageCodeID;

        public int LanguageCodeID
        {
            get
            {
                return _LanguageCodeID;
            }
            set
            {
                ;

                if (_LanguageCodeID != value)
                {
                    SendPropertyChanging();

                    _LanguageCodeID = value;

                    PangeaInfo.SaveRequired = true;// BUG 334 

                    SendPropertyChanged();
                }
            }
        }



        /// <summary>
        /// The Action Reason associated with this child.
        /// </summary>
        private CodeTables.Action _action;

        public CodeTables.Action Action
        {
            get
            {
                if (_action == null && _childDO.Action_ID != null)
                    _action = PangeaCollections.ActionCollection.Find(_childDO.Action_ID);

                return _action;
            }
            set
            {
                if (_action != value)
                {
                    SendPropertyChanging();

                    _action = value;

                    if (_action != null)
                        _childDO.Action_ID = _action.ID;

                    SendPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Reason why the child is being removed
        /// </summary>
        private ChildRemoveReason _remove_reason;

        public ChildRemoveReason Remove_Reason
        {
            get
            {
                if (
                    _remove_reason == null && 
                    _childDO.Child_Remove_Reason_Code_ID != null
                    )
                    _remove_reason = PangeaCollections.ChildRemoveReasonCollection.Find(_childDO.Child_Remove_Reason_Code_ID);

                return _remove_reason;
            }
            set
            {
                if (_remove_reason != value)
                {
                    SendPropertyChanging();

                    _remove_reason = value;

                    _childDO.Child_Remove_Reason_Code_ID = _remove_reason?.CodeID;

                    PangeaInfo.SaveRequired = true;// BUG 334 

                    SendPropertyChanged();
                }
            }
        }
    }
}
