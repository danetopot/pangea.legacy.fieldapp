﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PangeaData.Model
{
    public class DataTable_From_SPROC_Results
    {

        public DataTable the_DataTable { get; set; } = new DataTable();

        public bool Operation_Completed_With_No_Errors { get; set; } = false;

        public Exception the_Exception { get; set; } = null;

    }
}
