﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Pangea.Data.Collections
{
    /// <summary>
    /// This is the Message Collection Class.
    /// It will contain the Messages that need to be added to the Message Bar
    /// on the Main Application Screen.
    /// </summary>
    public class MessageCollection : ObservableCollection<MessagesStruct>
    {
        public MessageCollection()
        { }

        public MessageCollection(IEnumerable<MessagesStruct> _msgs)
        {
            Clear();
            Add(_msgs);
        }

        public void Add(IEnumerable<MessagesStruct> _msgs)
        {
            foreach (MessagesStruct ms in _msgs)
            {
                Add(ms);
            }
        }

        /// <summary>
        /// Remove all of the Messages that Contain this Message ID
        /// from the Message Collection
        /// </summary>
        /// <param name="_msg">Message ID we want to remove.</param>
        public void RemoveAll(String _msgID)
        {
            MessageCollection temp = Find(_msgID);

            foreach (MessagesStruct ms in temp)
            {
                Remove(ms);
            }
        }

        /// <summary>
        /// Remove all o fthe Messages that Contain this Message Type
        /// from the Message Collection
        /// </summary>
        /// <param name="_msgType">The Message Type we wish to remove.</param>
        public void RemoveAll(MessageTypes _msgType)
        {
            MessageCollection temp = Find(_msgType);

            foreach (MessagesStruct ms in temp)
            {
                Remove(ms);
            }
        }

        /// <summary>
        /// This Finds every Message in the Collection that has
        /// the Message ID Provided.
        /// </summary>
        /// <param name="_msg">Message ID we wish to find.</param>
        /// <returns>Message Collection of the Messages Found</returns>
        public MessageCollection Find(String _msgID)
        {
            return new MessageCollection(this.Where(ms => ms.MessageID.Contains(_msgID)));
        }

        /// <summary>
        /// This Finds every Message in the Collection that has
        /// the Message Type Provided.
        /// </summary>
        /// <param name="_msg">Message Type we wish to find.</param>
        /// <returns>Message Collection of the Messages Found</returns>
        public MessageCollection Find(MessageTypes _msgType)
        {
            return new MessageCollection(this.Where(ms => ms.MessageType == _msgType));
        }

        /// <summary>
        /// This returns whether a specific Message ID is in the collection
        /// </summary>
        /// <param name="_msgID">Message ID we want to find.</param>
        /// <returns>True if at least one is in the collection; False otherwise</returns>
        public bool Contains(String _msgID)
        {
            return this.Where(ms => ms.MessageID.Equals(_msgID)).Count() > 0;
        }

        /// <summary>
        /// This returns whether a specific Message Type is in the collection
        /// </summary>
        /// <param name="_msgID">Message Type we want to find.</param>
        /// <returns>True if at least one is in the collection; False otherwise</returns>
        public bool Contains(MessageTypes _msgType)
        {
            return this.Where(ms => ms.MessageType == _msgType).Count() > 0;
        }
    }
}
