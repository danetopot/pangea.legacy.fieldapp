﻿////////////////////////////////////////////////////////////////
/// Observable collection of the Health Status data.  Used to store all
/// of the active Health Status from the Health Status Codes Database.
////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

using Pangea.Data.Model.CodeTables;

namespace Pangea.Data.Collections
{
    public class HealthStatusCollection : ObservableCollection<HealthStatus>
    {
        public HealthStatusCollection(ObservableCollection<ObservableCollection<Object>> _items)
        {
            foreach (ObservableCollection<Object> _item in _items)
            {
                Add(new HealthStatus(_item));
            }
        }

        public HealthStatusCollection(IEnumerable<HealthStatus> _items)
        {
            Clear();
            Add(_items);
        }

        public void Add(IEnumerable<HealthStatus> _items)
        {
            foreach (HealthStatus item in _items)
            {
                Add(item);
            }
        }

        public HealthStatus Find(HealthStatus _item)
        {
            if (_item == null)
                return _item;

            return this.Where(c => c.CodeID == _item.CodeID).FirstOrDefault();
        }

        public HealthStatus Find(int? _codeID)
        {
            if (_codeID == null)
                return null;

            return this.Where(hs => hs.CodeID == _codeID.Value).FirstOrDefault();
        }
    }
}
