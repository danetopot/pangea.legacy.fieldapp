﻿////////////////////////////////////////////////////////////////
/// Observable collection of the Gender data.  Used to store all
/// of the active Gender from the Gender Codes Database.
////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

using Pangea.Data.Model.CodeTables;

namespace Pangea.Data.Collections
{
    public class GenderCollection : ObservableCollection<Gender>
    {
        public GenderCollection(ObservableCollection<ObservableCollection<Object>> _items)
        {
            foreach (ObservableCollection<Object> _item in _items)
            {
                Add(new Gender(_item));
            }
        }

        public GenderCollection(IEnumerable<Gender> _items)
        {
            Clear();
            Add(_items);
        }

        public void Add(IEnumerable<Gender> _items)
        {
            foreach (Gender item in _items)
            {
                Add(item);
            }
        }

        public Gender Find(Gender _item)
        {
            if (_item == null)
                return _item;

            return this.FirstOrDefault(c => c.CodeID == _item.CodeID);
        }

        public Gender Find(int? _genderCodeID)
        {
            if (_genderCodeID == null)
                return null;

            return this.FirstOrDefault(c => c.CodeID == _genderCodeID.Value);
        }

        public Gender Find(String _name)
        {
            if (String.IsNullOrEmpty(_name))
                return null;

            return this.FirstOrDefault(c => c.Name.ToLower().Equals(_name.ToLower()));
        }
    }
}
