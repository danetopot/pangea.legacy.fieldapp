﻿////////////////////////////////////////////////////////////////
/// Observable collection of the Personality Type data.  Used 
/// to store allof the active Personality Type from the Personality
/// Type Codes Database.
////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

using Pangea.Data.Model.CodeTables;

namespace Pangea.Data.Collections
{
    public class PersonalityTypeCollection : ObservableCollection<PersonalityType>
    {
        public PersonalityTypeCollection(ObservableCollection<ObservableCollection<Object>> _items)
        {
            foreach (ObservableCollection<Object> _item in _items)
            {
                Add(new PersonalityType(_item));
            }
        }

        public PersonalityTypeCollection(IEnumerable<PersonalityType> _items)
        {
            Clear();
            Add(_items);
        }

        public void Add(IEnumerable<PersonalityType> _items)
        {
            foreach (PersonalityType item in _items)
            {
                Add(item);
            }
        }

        public PersonalityType Find(PersonalityType _item)
        {
            if (_item == null)
                return _item;

            return this.Where(c => c.CodeID == _item.CodeID).FirstOrDefault();
        }

        public PersonalityType Find(int? _codeID)
        {
            if (_codeID == null)
                return null;

            return this.Where(pt => pt.CodeID == _codeID.Value).FirstOrDefault();
        }
    }
}
