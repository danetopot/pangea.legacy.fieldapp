﻿////////////////////////////////////////////////////////////////
/// Observable collection of Country data.  Used to store all
/// of the active Countries from the Country Codes Database.
////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

using Pangea.Data.Model.CodeTables;

namespace Pangea.Data.Collections
{
    public class CountryCollection : ObservableCollection<Country>
    {
        public CountryCollection(ObservableCollection<ObservableCollection<Object>> _items)
        {
            foreach (ObservableCollection<Object> _item in _items)
            {
                Add(new Country(_item));
            }
        }

        public CountryCollection(IEnumerable<Country> _items)
        {
            Clear();
            Add(_items);
        }

        public void Add(IEnumerable<Country> _items)
        {
            foreach (Country item in _items)
            {
                Add(item);
            }
        }

        public Country Find(Country _item)
        {
            if (_item == null)
                return _item;

            return this.Where(c => c.CodeID == _item.CodeID).FirstOrDefault();
        }

        public Country Find(int? _codeID)
        {
            if (_codeID == null)
                return null;

            return this.Where(c => c.CodeID == _codeID.Value).FirstOrDefault();
        }

        public bool Contains(int? _codeID)
        {
            if (_codeID == null)
                return false;

            return this.Where(c => c.CodeID == _codeID.Value).Count() > 0;
        }
    }
}
