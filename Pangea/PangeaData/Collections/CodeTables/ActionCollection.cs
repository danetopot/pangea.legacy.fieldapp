﻿using Pangea.Data.Model.CodeTables;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Pangea.Data.Collections
{
    public class ActionCollection : ObservableCollection<Action>
    {
        public ActionCollection(ObservableCollection<ObservableCollection<System.Object>> _actions)
        {
            foreach (ObservableCollection<System.Object> _action in _actions)
            {
                Add(new Action(_action));
            }
        }

        public void Add(IEnumerable<Action> _items)
        {
            foreach (Action item in _items)
            {
                Add(item);
            }
        }

        public Action Find(Action _item)
        {
            if (_item == null)
                return _item;

            return this.Where(c => c.ID == _item.ID).FirstOrDefault();
        }

        public Action Find(int? _codeID)
        {
            if (_codeID == null || this == null)
                return null;

            return this.Where(c => c.ID == _codeID.Value).FirstOrDefault();
        }

        public Action Find(string _actionName)
        {
            if (string.IsNullOrEmpty(_actionName) || string.IsNullOrWhiteSpace(_actionName))
                return null;

            return this.Where(c => c.Name.ToLower().Equals(_actionName.ToLower())).FirstOrDefault();
        }
    }
}
