﻿using Pangea.Data.Model.CodeTables;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Pangea.Data.Collections
{
    public class ChildRemoveReasonCollection : ObservableCollection<ChildRemoveReason>
    {
        public ChildRemoveReasonCollection(ObservableCollection<ObservableCollection<System.Object>> _items)
        {
            foreach (ObservableCollection<System.Object> _item in _items)
            {
                Add(new ChildRemoveReason(_item));
            }
        }

        public ChildRemoveReasonCollection(IEnumerable<ChildRemoveReason> _items)
        {
            Clear();
            Add(_items);
        }

        public void Add(IEnumerable<ChildRemoveReason> _items)
        {
            foreach (ChildRemoveReason item in _items)
            {
                Add(item);
            }
        }

        public ChildRemoveReason Find(ChildRemoveReason _item)
        {
            if (_item == null)
                return _item;

            return this.Where(c => c.CodeID == _item.CodeID).FirstOrDefault();
        }

        public ChildRemoveReason Find(int? _codeID)
        {
            if (_codeID == null)
                return null;

            return this.Where(c => c.CodeID == _codeID.Value).FirstOrDefault();
        }

        public ChildRemoveReason Find(string _removeReason)
        {
            if (string.IsNullOrEmpty(_removeReason) || string.IsNullOrWhiteSpace(_removeReason))
                return null;

            return this.Where(c => c.Name.ToLower().Equals(_removeReason.ToLower())).FirstOrDefault();
        }
    }
}
