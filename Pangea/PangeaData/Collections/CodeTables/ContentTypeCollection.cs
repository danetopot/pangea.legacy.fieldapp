﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

using Pangea.Data.Model.CodeTables;

namespace Pangea.Data.Collections
{
    public class ContentTypeCollection : ObservableCollection<ContentType>
    {
        public ContentTypeCollection(ObservableCollection<ObservableCollection<Object>> _items)
        {
            foreach (ObservableCollection<Object> _item in _items)
            {
                Add(new ContentType(_item));
            }
        }

        public ContentTypeCollection(IEnumerable<ContentType> _items)
        {
            Clear();
            foreach (ContentType item in _items)
            {
                Add(item);
            }
        }

        public void Add(IEnumerable<ContentType> _items)
        {
            foreach(ContentType item in _items)
            {
                Add(item);
            }
        }

        public ContentType Find(ContentType _item)
        {
            if (_item == null)
                return _item;

            return this.Where(ct => ct.CodeID == _item.CodeID).FirstOrDefault();
        }

        public ContentType Find(int? _contentTypeID)
        {
            if (_contentTypeID == null)
                return null;

            return this.Where(ct => ct.CodeID == _contentTypeID.Value).FirstOrDefault();
        }

        public ContentType Find(string _contentType)
        {
            if (string.IsNullOrEmpty(_contentType) || string.IsNullOrWhiteSpace(_contentType))
                return null;

            return this.Where(ct => ct.Name.ToLower().Equals(_contentType.ToLower())).FirstOrDefault();
        }
    }
}
