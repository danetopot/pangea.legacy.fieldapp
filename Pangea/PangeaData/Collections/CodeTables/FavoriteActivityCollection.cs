﻿////////////////////////////////////////////////////////////////
/// Observable collection of Favorite Activity data.  Used to 
/// store all of the active Favorite Activitys from the Favorite 
/// Activity Codes Database.
////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

using Pangea.Data.Model.CodeTables;

namespace Pangea.Data.Collections
{
    public class FavoriteActivityCollection : ObservableCollection<FavoriteActivity>
    {
        public FavoriteActivityCollection(ObservableCollection<ObservableCollection<Object>> _items)
        {
            foreach (ObservableCollection<Object> _item in _items)
            {
                Add(new FavoriteActivity(_item));
            }
        }

        public FavoriteActivityCollection(IEnumerable<FavoriteActivity> _items)
        {
            Clear();
            Add(_items);
        }

        public void Add(IEnumerable<FavoriteActivity> _items)
        {
            foreach (FavoriteActivity item in _items)
            {
                Add(item);
            }
        }

        public FavoriteActivityCollection Without(FavoriteActivity _item)
        {
            if (_item == null)
                return this;

            return new FavoriteActivityCollection(this.Where(c => c.CodeID != _item.CodeID));
        }

        public FavoriteActivityCollection Without(IQueryable<FavoriteActivity> _items)
        {
            if (_items == null)
                return this;

            FavoriteActivityCollection _col = new FavoriteActivityCollection(this);
            foreach (FavoriteActivity _c in _items)
            {
                _col = new FavoriteActivityCollection(_col.Where(c => c.CodeID != _c.CodeID));
            }

            return _col;
        }

        public FavoriteActivityCollection Without(IEnumerable<FavoriteActivity> _items)
        {
            if (_items == null)
                return this;

            FavoriteActivityCollection _col = new FavoriteActivityCollection(this);
            foreach (FavoriteActivity _c in _items)
            {
                _col = new FavoriteActivityCollection(_col.Where(c => c.CodeID != _c.CodeID));
            }

            return _col;
        }

        // Bug 266 - Added to Restrict the List to only the ones that meet the current Age Requirement.
        public FavoriteActivityCollection RestrictByAge(double _childAge)
        {
            return new FavoriteActivityCollection(this.Where(item => item.MinAge <= _childAge));
        }

        public FavoriteActivity Find(FavoriteActivity _item)
        {
            if (_item == null)
                return _item;

            return this.Where(c => c.CodeID == _item.CodeID).FirstOrDefault();
        }

        public FavoriteActivity Find(int? _codeID)
        {
            if (_codeID == null)
                return null;

            return this.Where(fa => fa.CodeID == _codeID.Value).FirstOrDefault();
        }
    }
}
