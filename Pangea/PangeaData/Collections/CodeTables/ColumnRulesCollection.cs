﻿using Pangea.Data.Model.CodeTables;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Pangea.Data.Collections
{
    public class ColumnRulesCollection : ObservableCollection<ColumnRule>
    {
        public ColumnRulesCollection(ObservableCollection<ObservableCollection<Object>> _items)
        {
            foreach(ObservableCollection<Object> _item in _items)
            {
                Add(new ColumnRule(_item));
            }
        }

        public ColumnRulesCollection(IEnumerable<ColumnRule> _items)
        {
            Clear();
            Add(_items);
        }

        public void Add(IEnumerable<ColumnRule> _items)
        {
            foreach (ColumnRule item in _items)
            {
                Add(item);
            }
        }

        public ColumnRulesCollection GetRules(String columnName)
        {
            if (String.IsNullOrEmpty(columnName))
                return null;

            ColumnRulesCollection the_rules = new ColumnRulesCollection(this.Where(cr => cr.ColumnName.ToLower().Contains(columnName.ToLower())));

            return the_rules;
        }

        public ColumnRulesCollection GetRulesByModule(bool _isEnrollment)
        {
            if (_isEnrollment)
                return new ColumnRulesCollection(this.Where(crc => crc.ModuleName.ToLower().Equals("enrollment")));

            ColumnRulesCollection the_rules = new ColumnRulesCollection(this.Where(crc => crc.ModuleName.ToLower().Equals("update")));

            return the_rules;
        }

        public bool HasDefaultValue(String columnName)
        {
            bool retVal = false;

            IEnumerable<ColumnRule> rules = this.Where(item => item.ColumnName.ToLower().Contains(columnName.ToLower()));
            foreach (ColumnRule rule in rules)
            {
                if (!String.IsNullOrEmpty(rule.DefaultStrValue) || (rule.DefaultIntValue != null && rule.DefaultIntValue != long.MinValue))
                    retVal = true;
            }

            return retVal;
        }

        public bool GetActive(String Column_Rules_Name)
        {
            bool retVal = false;

            IEnumerable<ColumnRule> rules = this.Where(item => item.Name.ToLower().Contains(Column_Rules_Name.ToLower()));
            foreach (ColumnRule rule in rules)
            {
                if (rule != null)
                    retVal = rule.Active;
            }

            return retVal;
        }

        public bool GetRequired(String Column_Rules_Name)
        {
            bool retVal = false;

            IEnumerable<ColumnRule> rules = this.Where(item => item.Name.ToLower().Contains(Column_Rules_Name.ToLower()));
            foreach (ColumnRule rule in rules)
            {
                if (rule != null)
                    retVal = rule.Required;
            }

            return retVal;
        }

        public long? GetMinAllowed(String Column_Rules_Name)
        {
            long? retVal = null;

            IEnumerable<ColumnRule> rules = this.Where(item => item.Name.ToLower().Contains(Column_Rules_Name.ToLower()));
            foreach (ColumnRule rule in rules)
            {
                if (rule != null)
                {
                    if (rule.MinAllowed != int.MinValue && rule.MinAllowed != int.MaxValue)
                        retVal = rule.MinAllowed;
                }
            }

            return retVal;
        }

        public long? GetMaxAllowed(String Column_Rules_Name)
        {
            long? retVal = null;

            IEnumerable<ColumnRule> rules = this.Where(item => item.Name.ToLower().Contains(Column_Rules_Name.ToLower()));
            foreach (ColumnRule rule in rules)
            {
                if (rule != null)
                {
                    if (
                        rule.MaxAllowed != int.MinValue && 
                        rule.MaxAllowed != int.MaxValue
                        )
                        retVal = rule.MaxAllowed;
                }
            }

            return retVal;
        }

        public String DefaultStrValue(String Column_Rules_Name)
        {
            String retVal = String.Empty;

            IEnumerable<ColumnRule> rules = this.Where(item => item.Name.ToLower().Contains(Column_Rules_Name.ToLower()));
            foreach (ColumnRule rule in rules)
            {
                if (rule != null)
                    retVal = rule.DefaultStrValue;
            }

            return retVal;
        }

        public long? DefaultIntValue(String Column_Rules_Name)
        {
            long? retVal = null;

            IEnumerable<ColumnRule> rules = this.Where(item => item.Name.ToLower().Contains(Column_Rules_Name.ToLower()));
            foreach (ColumnRule rule in rules)
            {
                if (rule != null)
                    retVal = rule.DefaultIntValue;
            }

            return retVal;
        }

        public void ClearChecks()
        {
            foreach (ColumnRule rule in this)
            {
                rule.Checked = false;
            }
        }
    }
}
