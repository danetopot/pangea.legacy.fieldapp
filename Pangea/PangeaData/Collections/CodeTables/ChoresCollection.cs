﻿////////////////////////////////////////////////////////////////
/// Observable collection of Chore data.  Used to store all
/// of the active Chores from the Chore Codes Database.
////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Pangea.Data.Model.CodeTables;

namespace Pangea.Data.Collections
{
    public class ChoresCollection : ObservableCollection<Chore>
    {
        public ChoresCollection(ObservableCollection<ObservableCollection<System.Object>> _items)
        {
            foreach (ObservableCollection<System.Object> _item in _items)
            {
                Add(new Chore(_item));
            }
        }

        public ChoresCollection(IEnumerable<Chore> _items)
        {
            Clear();
            Add(_items);
        }

        public void Add(IEnumerable<Chore> _items)
        {
            foreach (Chore item in _items)
            {
                Add(item);
            }
        }

        public ChoresCollection Without(Chore _item)
        {
            if (_item == null)
                return this;

            return new ChoresCollection(this.Where( c => c.CodeID != _item.CodeID));
        }

        public ChoresCollection Without(IEnumerable<Chore> _items)
        {
            if (_items == null)
                return this;

            ChoresCollection _col = new ChoresCollection(this);
            foreach(Chore _c in _items)
            {
                _col = new ChoresCollection(_col.Where(c => c.CodeID != _c.CodeID));
            }

            return _col;
        }

        public ChoresCollection RestrictByAge(double _childAge)
        {
            return new ChoresCollection(this.Where(item => item.MinAge.HasValue ? item.MinAge <= _childAge : true));
        }

        public Chore Find(Chore _item)
        {
            if (_item == null)
                return _item;

            return this.Where(c => c.CodeID == _item.CodeID).FirstOrDefault();
        }

        public Chore Find(int? _codeID)
        {
            if (_codeID == null)
                return null;

            return this.Where(c => c.CodeID == _codeID.Value).FirstOrDefault();
        }

        public Chore Find(string _choreType)
        {
            if (string.IsNullOrEmpty(_choreType) || string.IsNullOrWhiteSpace(_choreType))
                return null;

            return this.Where(c => c.Name.ToLower().Equals(_choreType.ToLower())).FirstOrDefault();
        }
    }
}
