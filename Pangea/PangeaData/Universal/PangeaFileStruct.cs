﻿using System;

using Pangea.Data.Model;
using Pangea.Data.Model.CodeTables;
using System.Data.Linq;

namespace Pangea
{
    /// <summary>
    /// The Stucture to save the different types of Files that can be
    /// added to the child through the Application.
    /// </summary>
    public struct PangeaFileStruct
    {
        public PangeaFileStruct
            (
            Guid _streamID, 
            Binary _binaryFile, 
            bool _optionalFile, 
            String _shortFileName = null, 
            String _fullFileLoc = null, 
            ContentType _contentType = null, 
            FileType _fileType = null, 
            FileData _fileData = null
            )
        {
            StreamID = _streamID;

            BinaryFile = _binaryFile;

            ShortFileName = _shortFileName;

            FullFileLoc = _fullFileLoc;

            ContentTypeInfo = _contentType;

            FileTypeInfo = _fileType;

            FileDataInfo = _fileData;

            OptionalFile = _optionalFile; // Bug 371 - Adding way to determine file is Optional File.

            HasTransmitted = false;
        }

        public Guid StreamID { get; set; }

        public String ShortFileName { get; set; }

        public String FullFileLoc { get; set; }

        public ContentType ContentTypeInfo { get; set; }

        public FileType FileTypeInfo { get; set; }

        public FileData FileDataInfo { get; set; }

        public Binary BinaryFile { get; set; }

        public bool HasTransmitted { get; set; }

        // Bug 371 - Adding way to determine file is Optional File.
        public bool OptionalFile { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            var _obj = (PangeaFileStruct)obj;
            
            return (_obj.FullFileLoc == FullFileLoc && _obj.ShortFileName == ShortFileName && _obj.ContentTypeInfo == ContentTypeInfo && _obj.FileTypeInfo == FileTypeInfo);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static bool operator ==(PangeaFileStruct c1, PangeaFileStruct c2)
        {
            if ((object)c1 == null)
                return ((object)c2 == null);

            return c1.Equals(c2);
        }

        public static bool operator !=(PangeaFileStruct c1, PangeaFileStruct c2)
        {
            if ((object)c1 == null)
                return ((object)c2 != null);

            return !c1.Equals(c2);
        }

        public bool IsEmpty()
        {
            return ((ShortFileName == null || ShortFileName == String.Empty) && (FullFileLoc == null || FullFileLoc == String.Empty));
        }
    };
}
