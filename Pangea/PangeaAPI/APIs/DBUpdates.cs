﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Linq;
using System.Diagnostics;
using System.Net;


using Pangea.API.FieldServices;

using Pangea.Data.DataObjects;
using Pangea.Utilities.Tools;

namespace Pangea.API
{
    public class DBUpdates
    {
        /// <summary>
        /// This gets the DB update Scripts and starts the DB update process.
        /// </summary>
        /// <returns></returns>
        public static void UpdateDB(string dbName)
        {
            try
            {
                Guid userID = new Guid("0DD3FED7-9451-4FC2-A528-7C1805D55363");
                BasicHttpsBinding_IFieldServices _fieldServices = new BasicHttpsBinding_IFieldServices();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                DBOTablesDataContext _dboDataContext;

                _dboDataContext = new DBOTablesDataContext(dbName);

                LogManager.EventLogManager.WriteEventLog("Connect to Computer DB");

                bool _verFound = false;

                DBVersion curDbVer = new DBVersion();
                curDbVer.UserID = userID.ToString();

                try
                {
                    ISingleResult<GetDBVersionResult> _curDBVer = _dboDataContext.Get_DB_Version(userID, 0, 1, 1, 1);

                    foreach (GetDBVersionResult _gDBVerRes in _curDBVer)
                    {
                        curDbVer.Major = _gDBVerRes.Major;
                        curDbVer.MajorSpecified = true;
                        curDbVer.Minor = _gDBVerRes.Minor;
                        curDbVer.MinorSpecified = true;
                        curDbVer.Revision = _gDBVerRes.Revision;
                        curDbVer.RevisionSpecified = true;
                        curDbVer.Build = _gDBVerRes.Build;
                        curDbVer.BuildSpecified = true;

                        _verFound = true;
                    }
                }
                catch (Exception ex)
                {
                    LogManager.EventLogManager.WriteEventLog(ex.Message, EventLogEntryType.Error);
                    _verFound = false;
                }

                if (!_verFound)
                {
                    // This means our DB is original and we need to start updating.
                    curDbVer.Major = 0;
                    curDbVer.MajorSpecified = true;
                    curDbVer.Minor = 1;
                    curDbVer.MinorSpecified = true;
                    curDbVer.Revision = 1;
                    curDbVer.RevisionSpecified = true;
                    curDbVer.Build = 1;
                    curDbVer.BuildSpecified = true;
                }

                LogManager.EventLogManager.WriteEventLog("Attempt to get DB Scripts from API");
                DataSet _dbUpdate = _fieldServices.GetDBversion(curDbVer);

                LogManager.EventLogManager.WriteEventLog("Got DB Scripts Attempting to run them.");
                DataTable _tableData = _dbUpdate.Tables[0];
                foreach (DataRow _rowData in _tableData.Rows)
                {
                    if (_rowData[12] != null && Convert.ToString(_rowData[12]) != String.Empty)
                    {
                        UpdateDB(Convert.ToString(_rowData[12]), dbName);
                        _dboDataContext.Add_DB_Version(userID, (int?)_rowData[0], (int?)_rowData[1], (int?)_rowData[2], (int?)_rowData[3], (int?)_rowData[4],
                                                           (int?)_rowData[5], (int?)_rowData[6], (int?)_rowData[7], (int?)_rowData[8], (int?)_rowData[9],
                                                           (int?)_rowData[10], (int?)_rowData[11], Convert.ToString(_rowData[12]), Convert.ToString(_rowData[13]));
                    }
                }

                if (!_verFound)
                    _dboDataContext.Reset_DB();

                UpdateDBConfig(dbName);
                UpdateCodeTables(dbName);

                LogManager.EventLogManager.WriteEventLog("Finished running the scripts.");
            }
            catch (Exception ex)
            {
                LogManager.EventLogManager.WriteEventLog(ex.Message, EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// This takes the different DB Update Scripts and parses the commands out
        /// and then executes the DB commands on the local DB.
        /// </summary>
        /// <param name="script"></param>
        private static void UpdateDB(string script, string dbName)
        {

            DBOTablesDataContext _dboDataContextLocal = new DBOTablesDataContext
                (
                PangeaInfo.TestVer 
                
                ? 
                Centraler.Database_Settingz.DB_Staging : 
                Centraler.Database_Settingz.DB_Production
                );

            String updateString = String.Empty;

            while (script.Length > 0)
            {
                int indexGO = script.IndexOf("\r\nGO");
                if (indexGO < 0)
                {
                    updateString = script;
                    script = String.Empty;
                }
                else
                {
                    updateString = script.Substring(0, (indexGO + 4));
                    script = script.Remove(0, (indexGO + 4));
                }
                updateString = updateString.Replace("\r\nGO", "");

                if (updateString.Length > 0)
                {
                    _dboDataContextLocal.ExecuteCommand(updateString);
                }
            }

            _dboDataContextLocal.SubmitChanges();
        }

        // TODO : This needs to be put into a Stored Procedure and this code needs to be removed and have the stored procedure called.
        private static void ClearCodeTables(string dbName)
        {

            DBOTablesDataContext _dboDataContextLocal = new DBOTablesDataContext(dbName);

            String updateString = String.Empty;

            try
            {
                var _tableNames = _dboDataContextLocal.ExecuteQuery<String>
                    (
                    "SELECT name FROM sys.tables WHERE schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'Code')"
                    );

                foreach (String tableName in _tableNames)
                {                    

                    // updateString = String.Format("Delete FROM [Code].[{0}]", tableName);
                    updateString = String.Format("TRUNCATE TABLE [Code].[{0}]", tableName);

                    _dboDataContextLocal.ExecuteCommand(updateString);
                }

                _dboDataContextLocal.SubmitChanges();

            }
            catch (Exception ex)
            {
                LogManager.EventLogManager.WriteEventLog(ex.Message);
            }

        }

        /// <summary>
        /// This Will Get the Latest DB Config Table Data then update the Local DB Config Table
        /// Will Make sure the History Keeping flag is turned off.
        /// </summary>
        /// <param name="dbName">Name of the DB</param>
        /// <returns>True if succeeds; False if Failed.</returns>
        public static bool UpdateDBConfig(string dbName)
        {

            bool retVal = true;

            BasicHttpsBinding_IFieldServices _fieldAppApiConnection = new BasicHttpsBinding_IFieldServices();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            DBOTablesDataContext _dboDataContextLocal = new DBOTablesDataContext(dbName);

            try
            {
                DBConfigObject[] _dbConfigRows = _fieldAppApiConnection.GetDBConfig(PangeaInfo.User.UserID.ToString());

                foreach (DBConfigObject _dbconfigrow in _dbConfigRows)
                {

                    _dboDataContextLocal.Set_Config
                        (
                        PangeaInfo.User.UserID, 
                        _dbconfigrow.Name, 
                        _dbconfigrow.Description, 

                        _dbconfigrow.Name == "Is_Server_DB_versus_Client_DB" 
                        ? 
                        false 
                        : 
                        _dbconfigrow.Bit_Value, 
                        
                        _dbconfigrow.Int_Value, 
                        _dbconfigrow.Text_Value, 
                        _dbconfigrow.Dec_Value, 
                        _dbconfigrow.Datetime_Value, 
                        _dbconfigrow.Active, 
                        _dbconfigrow.Config_ID
                        );
                }
            }
            catch (Exception ex)
            {
                LogManager.EventLogManager.WriteEventLog(ex.Message);
            }

            _fieldAppApiConnection.Dispose();

            _dboDataContextLocal.Dispose();

            return retVal;

        }

        /// <summary>
        /// This Gets and Updates all the Code Tables
        /// </summary>
        /// <returns>True if successful. False if Failed.</returns>
        public static bool UpdateCodeTables(string dbName)
        {
            bool retVal = true;

            BasicHttpsBinding_IFieldServices _fieldAppApiConnection = new BasicHttpsBinding_IFieldServices();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            try
            {

                ;

                // String connString = String.Format("Data Source=.\\SQLEXPRESS;Initial Catalog={0};Integrated Security=False;uid=svc_pan_admin;pwd=8toDAF57%X0k", dbName);

                string connString_raw = Centraler.Database_Settingz.svc_admin_conn_LOCAL;  // ConfigurationManager.AppSettings["svc_admin_conn"].ToString();

                // This is here so that it can connect as the admin user of the DB and add the code table information correctly.
                String connString = String.Format(connString_raw, dbName);

                Guid userID = PangeaInfo.User.UserID; //new Guid("0DD3FED7-9451-4FC2-A528-7C1805D55363"); // This is here so that the Installer can correctly update the code tables. // TODO : need to changed this so that the User Gets loaded correctly when the installer is running.

                DataSet _codeTables = _fieldAppApiConnection.GetCodeTables(userID.ToString());

                if (_codeTables != null)
                {
                    ClearCodeTables(dbName);

                    CodeTablesDataContext _codeTablesDC = new CodeTablesDataContext(connString);
                    
                    DataRowCollection _tableNames = _codeTables.Tables[0].Rows;

                    for (int tbl = 1; tbl < _codeTables.Tables.Count; tbl++)
                    {

                        int rowData = 0;

                        String TblName = _tableNames[tbl - 1][0].ToString();

                        DataRowCollection _tableData = _codeTables.Tables[tbl].Rows;

                        if (TblName.Equals("Action"))
                        {
                            for (rowData = 0; rowData < _tableData.Count; rowData++)
                            {
                                // 0 Action_ID, 1 Action_Name
                                try
                                {

                                    string action_name = Convert.ToString(_tableData[rowData][1]).Trim();

                                    int? action_id = (int?)_tableData[rowData][0];

                                    _codeTablesDC.Add_Action
                                        (
                                        userID, 
                                        Convert.ToString(_tableData[rowData][1]).Trim(), 
                                        (int?)_tableData[rowData][0]
                                        );

                                }
                                catch (Exception ex)
                                {
                                    LogManager.EventLogManager.WriteEventLog(String.Format("{0} - {1} : Value Being Inserted - {2}  {3}", TblName, ex.Message, _tableData[rowData][1], _tableData[rowData][2]));
#if (DEBUG)
                                    retVal = false;
#endif
                                }
                            }
                        }
                        else if (TblName.Equals("Action_Reason"))
                        {
                            for (rowData = 0; rowData < _tableData.Count; rowData++)
                            {
                                // 0 Action_Reason_ID, 1 Reason_Name, 2 Reason_Description
                                try
                                {
                                    if (Convert.ToInt32(_tableData[rowData][0]) == 1)
                                    {
                                        _codeTablesDC.Add_Action_Reason
                                            (
                                            userID, 
                                            Convert.ToString(_tableData[rowData][1]).Trim(), 
                                            (_tableData[rowData][2] == null ? null : Convert.ToString(_tableData[rowData][2])).Trim(), 
                                            (int?)_tableData[rowData][0]
                                            );
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogManager.EventLogManager.WriteEventLog(String.Format("{0} - {1} : Value Being Inserted - {2}  {3}", TblName, ex.Message, _tableData[rowData][1], _tableData[rowData][2]));
#if (DEBUG)
                                    retVal = false;
#endif
                                }
                            }
                        }
                        else if (TblName.Equals("Child_Record_Status"))
                        {
                            _codeTablesDC.Deactivate_Child_Record_Status(userID);

                            for (rowData = 0; rowData < _tableData.Count; rowData++)
                            {
                                try
                                {
                                    // 0 Active, 1 Child_Record_Status_Code_ID, 2 Child_Record_Status, 3 [Description], 4 Public_Description, 5 Min_Age
                                    if (Convert.ToInt32(_tableData[rowData][0]) == 1)
                                    {
                                        _codeTablesDC.Add_Child_Record_Status
                                            (
                                            userID, 
                                            Convert.ToString(_tableData[rowData][2]).Trim(), 
                                            Convert.ToString(_tableData[rowData][3]).Trim(), 
                                            Convert.ToString(_tableData[rowData][4]).Trim(),
                                            !_tableData[rowData][5].Equals(DBNull.Value) ? (int?)_tableData[rowData][5] : null, 
                                            (int?)_tableData[rowData][1]
                                            );
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogManager.EventLogManager.WriteEventLog(String.Format("{0} - {1} : Value Being Inserted - {2}  {3}", TblName, ex.Message, _tableData[rowData][1], _tableData[rowData][2]));
#if (DEBUG)
                                    retVal = false;
#endif
                                }
                            }
                        }
                        else if (TblName.Equals("Child_Remove_Reason"))
                        {
                            _codeTablesDC.Deactivate_Child_Remove_Reason(userID);

                            for (rowData = 0; rowData < _tableData.Count; rowData++)
                            {
                                try
                                {
                                    // 0 Active, 1 Child_Remove_Reason_Code_ID, 2 Child_Remove_Reason, 3 [Description], 4 Public_Description, 5 Min_Age
                                    if (Convert.ToInt32(_tableData[rowData][0]) == 1)
                                    {
                                        _codeTablesDC.Add_Child_Remove_Reason
                                            (
                                            userID, 
                                            Convert.ToString(_tableData[rowData][2]).Trim(), 
                                            Convert.ToString(_tableData[rowData][3]).Trim(), 
                                            Convert.ToString(_tableData[rowData][4]).Trim(),
                                            !_tableData[rowData][5].Equals(DBNull.Value) ? (int?)_tableData[rowData][5] : null, 
                                            (int?)_tableData[rowData][1]
                                            );
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogManager.EventLogManager.WriteEventLog(String.Format("{0} - {1} : Value Being Inserted - {2}  {3}", TblName, ex.Message, _tableData[rowData][1], _tableData[rowData][2]));
#if (DEBUG)
                                    retVal = false;
#endif
                                }
                            }
                        }
                        else if (TblName.Equals("Chore"))
                        {
                            _codeTablesDC.Deactivate_All_Chores(userID);

                            for (rowData = 0; rowData < _tableData.Count; rowData++)
                            {
                                try
                                {
                                    // 0 Active, 1 Chore_Code_ID, 2 Chore, 3 [Description], 4 Public_Description, 5 Min_Age
                                    if (Convert.ToInt32(_tableData[rowData][0]) == 1) // Check to see if it is active
                                    {
                                        _codeTablesDC.Add_Chore
                                            (
                                            userID, 
                                            Convert.ToString
                                            (
                                                _tableData[rowData][2]).Trim(), 
                                            Convert.ToString(_tableData[rowData][3]).Trim(), 
                                            Convert.ToString(_tableData[rowData][4]).Trim(), 
                                            !_tableData[rowData][5].Equals(DBNull.Value) ? (int?)_tableData[rowData][5] : null, 
                                            (int?)_tableData[rowData][1]
                                            );
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogManager.EventLogManager.WriteEventLog(String.Format("{0} - {1} : Value Being Inserted - {2}  {3}", TblName, ex.Message, _tableData[rowData][1], _tableData[rowData][2]));
#if (DEBUG)
                                    retVal = false;
#endif
                                }
                            }
                        }
                        else if (TblName.Equals("Column_Rules"))
                        {
                            _codeTablesDC.Deactivate_All_Column_Rules(userID);

                            for (rowData = 0; rowData < _tableData.Count; rowData++)
                            {
                                try
                                {
                                    // 0 Active, 1 Column_Rules_Code_ID, 2 Column_Rules_Name, 3 Table_Name, 4 Module_Name, 5 Column_Name, 6 Default_String_Value, 7 Default_int_Value, 8 Min_int_Allowed_Value, 9 Max_int_Allowed_Value, 10 [Required], 11 Validation_Type
                                    if (Convert.ToInt32(_tableData[rowData][0]) == 1)
                                    {
                                        _codeTablesDC.Add_Column_Rule
                                            (
                                            userID, 
                                            Convert.ToString(_tableData[rowData][2]).Trim(), 
                                            Convert.ToString(_tableData[rowData][3]).Trim(), 
                                            Convert.ToString(_tableData[rowData][4]).Trim(),
                                            Convert.ToString(_tableData[rowData][5]).Trim(), 
                                            !_tableData[rowData][6].Equals(DBNull.Value) ? Convert.ToString(_tableData[rowData][6]).Trim() : null,
                                            !_tableData[rowData][7].Equals(DBNull.Value) ? (long?)_tableData[rowData][7] : null, 
                                            !_tableData[rowData][8].Equals(DBNull.Value) ? (long?)_tableData[rowData][8] : null,
                                            !_tableData[rowData][9].Equals(DBNull.Value) ? (long?)_tableData[rowData][9] : null, 
                                            Convert.ToBoolean(_tableData[rowData][10]),
                                            (_tableData[rowData][11] == null ? null : Convert.ToString(_tableData[rowData][11])).Trim(), 
                                            (int?)_tableData[rowData][1]
                                            );
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogManager.EventLogManager.WriteEventLog(String.Format("{0} - {1} : Value Being Inserted - {2}  {3}", TblName, ex.Message, _tableData[rowData][1], _tableData[rowData][2]));
#if (DEBUG)
                                    retVal = false;
#endif
                                }
                            }
                        }
                        else if (TblName.Equals("Content_Type"))
                        {
                            _codeTablesDC.Deactivate_All_Content_Type(userID);

                            for (rowData = 0; rowData < _tableData.Count; rowData++)
                            {
                                try
                                {
                                    // 0 Active, 1 Content_Type_Code_ID, 2 Content_Type, 3 [Description], 4 Public_Description, 5 [Required]
                                    if (Convert.ToInt32(_tableData[rowData][0]) == 1)
                                    {
                                        _codeTablesDC.Add_Content_Type
                                            (
                                            userID, 
                                            Convert.ToString(_tableData[rowData][2]).Trim(), 
                                            Convert.ToString(_tableData[rowData][3]).Trim(), 
                                            Convert.ToString(_tableData[rowData][4]).Trim(),
                                            Convert.ToBoolean(_tableData[rowData][5]), 
                                            (int?)_tableData[rowData][1]
                                            );
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogManager.EventLogManager.WriteEventLog(String.Format("{0} - {1} : Value Being Inserted - {2}  {3}", TblName, ex.Message, _tableData[rowData][1], _tableData[rowData][2]));
#if (DEBUG)
                                    retVal = false;
#endif
                                }
                            }
                        }
                        else if (TblName.Equals("Country"))
                        {
                            _codeTablesDC.Deactivate_All_Country(userID);

                            for (rowData = 0; rowData < _tableData.Count; rowData++)
                            {
                                try
                                {
                                    // 0 Active, 1 Country_Code_ID, 2 Country_Code, 3 Region_Code_ID, 4 Country_Name, 5 [Default_Language], 6 Min_School_Age
                                    if (Convert.ToInt32(_tableData[rowData][0]) == 1)
                                    {
                                        _codeTablesDC.Add_Country
                                            (
                                            userID, 
                                            !_tableData[rowData][2].Equals(DBNull.Value) ? Convert.ToString(_tableData[rowData][2]).Trim() : null,
                                            !_tableData[rowData][3].Equals(DBNull.Value) ? (int?)_tableData[rowData][3] : null, 
                                            Convert.ToString(_tableData[rowData][4]).Trim(),
                                            !_tableData[rowData][5].Equals(DBNull.Value) ? (int?)_tableData[rowData][5] : null, 
                                            !_tableData[rowData][6].Equals(DBNull.Value) ? (int?)_tableData[rowData][6] : null,
                                            (int?)_tableData[rowData][1]
                                            );
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogManager.EventLogManager.WriteEventLog(String.Format("{0} - {1} : Value Being Inserted - {2}  {3}", TblName, ex.Message, _tableData[rowData][1], _tableData[rowData][4]));
#if (DEBUG)
                                    retVal = false;
#endif
                                }
                            }
                        }
                        else if (TblName.Equals("Favorite_Activity"))
                        {
                            _codeTablesDC.Deactivate_All_Favorite_Activity(userID);

                            for (rowData = 0; rowData < _tableData.Count; rowData++)
                            {
                                try
                                {
                                    // 0 Active, 1 Favorite_Activity_Code_ID, 2 Favorite_Activity, 3 [Description], 4 Public_Description, 5 Min_Age
                                    if (Convert.ToInt32(_tableData[rowData][0]) == 1)
                                    {
                                        _codeTablesDC.Add_Favorite_Activity
                                            (
                                            userID, 
                                            Convert.ToString(_tableData[rowData][2]).Trim(), 
                                            Convert.ToString(_tableData[rowData][3]).Trim(),
                                            Convert.ToString(_tableData[rowData][4]).Trim(), 
                                            !_tableData[rowData][5].Equals(DBNull.Value) ? (int?)_tableData[rowData][5] : null,
                                            (int?)_tableData[rowData][1]
                                            );
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogManager.EventLogManager.WriteEventLog(String.Format("{0} - {1} : Value Being Inserted - {2}  {3}", TblName, ex.Message, _tableData[rowData][1], _tableData[rowData][2]));
#if (DEBUG)
                                    retVal = false;
#endif
                                }
                            }
                        }
                        else if (TblName.Equals("Favorite_Learning"))
                        {
                            _codeTablesDC.Deactivate_All_Favorite_Learning(userID);

                            for (rowData = 0; rowData < _tableData.Count; rowData++)
                            {
                                try
                                {
                                    // 0 Active, 1 Favorite_Learning_Code_ID, 2 Favorite_Learning, 3 [Description], 4 Public_Description, 5 Min_Age
                                    if (Convert.ToInt32(_tableData[rowData][0]) == 1)
                                    {
                                        _codeTablesDC.Add_Favorite_Learning
                                            (
                                            userID, 
                                            Convert.ToString(_tableData[rowData][2]).Trim(), 
                                            Convert.ToString(_tableData[rowData][3]).Trim(),
                                            Convert.ToString(_tableData[rowData][4]).Trim(), 
                                            !_tableData[rowData][5].Equals(DBNull.Value) ? (int?)_tableData[rowData][5] : null,
                                            (int?)_tableData[rowData][1]
                                            );
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogManager.EventLogManager.WriteEventLog(String.Format("{0} - {1} : Value Being Inserted - {2}  {3}", TblName, ex.Message, _tableData[rowData][1], _tableData[rowData][2]));
#if (DEBUG)
                                    retVal = false;
#endif
                                }
                            }
                        }
                        else if (TblName.Equals("File_Type"))
                        {
                            _codeTablesDC.Deactivate_All_File_Type(userID);

                            for (rowData = 0; rowData < _tableData.Count; rowData++)
                            {
                                try
                                {
                                    // 0 Active, 1 File_Type_Code_ID, 2 File_Type, 3 File_Description, 4 File_Extension
                                    if (Convert.ToInt32(_tableData[rowData][0]) == 1)
                                    {
                                        _codeTablesDC.Add_File_Type
                                            (
                                            userID, 
                                            Convert.ToString(_tableData[rowData][2]).Trim(), 
                                            Convert.ToString(_tableData[rowData][3]).Trim(),
                                            Convert.ToString(_tableData[rowData][4]).Trim(), 
                                            (int?)_tableData[rowData][1]
                                            );
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogManager.EventLogManager.WriteEventLog(String.Format("{0} - {1} : Value Being Inserted - {2}  {3}", TblName, ex.Message, _tableData[rowData][1], _tableData[rowData][2]));
#if (DEBUG)
                                    retVal = false;
#endif
                                }
                            }
                        }
                        else if (TblName.Equals("Gender"))
                        {
                            _codeTablesDC.Deactivate_All_Gender(userID);

                            for (rowData = 0; rowData < _tableData.Count; rowData++)
                            {
                                try
                                {
                                    // 0 Active, 1 Gender_Code_ID, 2 Gender, 3 [Description], 4 Public_Description
                                    if (Convert.ToInt32(_tableData[rowData][0]) == 1)
                                    {
                                        _codeTablesDC.Add_Gender
                                            (
                                            userID, 
                                            Convert.ToString(_tableData[rowData][2]).Trim(), 
                                            Convert.ToString(_tableData[rowData][3]).Trim(),
                                            Convert.ToString(_tableData[rowData][4]).Trim(), 
                                            (int?)_tableData[rowData][1]
                                            );
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogManager.EventLogManager.WriteEventLog(String.Format("{0} - {1} : Value Being Inserted - {2}  {3}", TblName, ex.Message, _tableData[rowData][1], _tableData[rowData][2]));
#if (DEBUG)
                                    retVal = false;
#endif
                                }
                            }
                        }
                        else if (TblName.Equals("Grade_Level"))
                        {
                            _codeTablesDC.Deactivate_All_Grade_Level(userID);

                            for (rowData = 0; rowData < _tableData.Count; rowData++)
                            {
                                try
                                {
                                    // 0 Active, 1 Grade_Level_Code_ID, 2 Grade_Level, 3 [Description], 4 Public_Description, 5 Min_Age
                                    if (Convert.ToInt32(_tableData[rowData][0]) == 1)
                                    {
                                        _codeTablesDC.Add_Grade_Level
                                            (
                                            userID, 
                                            Convert.ToString(_tableData[rowData][2]).Trim(), 
                                            Convert.ToString(_tableData[rowData][3]).Trim(),
                                            Convert.ToString(_tableData[rowData][4]).Trim(), 
                                            !_tableData[rowData][5].Equals(DBNull.Value) ? (int?)_tableData[rowData][5] : null,
                                            (int?)_tableData[rowData][1]
                                            );
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogManager.EventLogManager.WriteEventLog(String.Format("{0} - {1} : Value Being Inserted - {2}  {3}", TblName, ex.Message, _tableData[rowData][1], _tableData[rowData][2]));
#if (DEBUG)
                                    retVal = false;
#endif
                                }
                            }
                        }
                        else if (TblName.Equals("Health_Status"))
                        {
                            _codeTablesDC.Deactivate_All_Health_Status(userID);

                            for (rowData = 0; rowData < _tableData.Count; rowData++)
                            {
                                try
                                {
                                    // 0 Active, 1 Health_Status_Code_ID, 2 Health_Status, 3 [Description], 4 Public_Description, 5 Min_Age
                                    if (Convert.ToInt32(_tableData[rowData][0]) == 1)
                                    {
                                        _codeTablesDC.Add_Health_Status
                                            (
                                            userID, 
                                            Convert.ToString(_tableData[rowData][2]).Trim(), 
                                            Convert.ToString(_tableData[rowData][3]).Trim(),
                                            Convert.ToString(_tableData[rowData][4]).Trim(), 
                                            !_tableData[rowData][5].Equals(DBNull.Value) ? (int?)_tableData[rowData][5] : null,
                                            (int?)_tableData[rowData][1]
                                            );
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogManager.EventLogManager.WriteEventLog(String.Format("{0} - {1} : Value Being Inserted - {2}  {3}", TblName, ex.Message, _tableData[rowData][1], _tableData[rowData][2]));
#if (DEBUG)
                                    retVal = false;
#endif
                                }
                            }
                        }
                        else if (TblName.Equals("Language"))
                        {
                            _codeTablesDC.Deactivate_All_Language(userID);

                            for (rowData = 0; rowData < _tableData.Count; rowData++)
                            {
                                try
                                {
                                    // 0 Active, 1 Language_Code_ID, 2 Language_Code, 3 Language_Desc
                                    if (Convert.ToInt32(_tableData[rowData][0]) == 1)
                                    {
                                        _codeTablesDC.Add_Language
                                            (
                                            userID, 
                                            Convert.ToString(_tableData[rowData][2]).Trim(), 
                                            Convert.ToString(_tableData[rowData][3]).Trim(), 
                                            (int?)_tableData[rowData][1]
                                            );
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogManager.EventLogManager.WriteEventLog(String.Format("{0} - {1} : Value Being Inserted - {2}  {3}", TblName, ex.Message, _tableData[rowData][1], _tableData[rowData][2]));
#if (DEBUG)
                                    retVal = false;
#endif
                                }
                            }
                        }
                        else if (TblName.Equals("Lives_With"))
                        {
                            _codeTablesDC.Deactivate_All_Lives_With(userID);

                            for (rowData = 0; rowData < _tableData.Count; rowData++)
                            {
                                try
                                {
                                    // 0 Active, 1 Lives_With_Code_ID, 2 Lives_With, 3 [Description], 4 Public_Description, 5 Min_Age
                                    if (Convert.ToInt32(_tableData[rowData][0]) == 1)
                                    {
                                        _codeTablesDC.Add_Lives_With
                                            (
                                            userID, 
                                            Convert.ToString(_tableData[rowData][2]).Trim(), 
                                            Convert.ToString(_tableData[rowData][3]).Trim(),
                                            Convert.ToString(_tableData[rowData][4]).Trim(), 
                                            !_tableData[rowData][5].Equals(DBNull.Value) ? (int?)_tableData[rowData][5] : null,
                                            (int?)_tableData[rowData][1]
                                            );
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogManager.EventLogManager.WriteEventLog(String.Format("{0} - {1} : Value Being Inserted - {2}  {3}", TblName, ex.Message, _tableData[rowData][1], _tableData[rowData][2]));
#if (DEBUG)
                                    retVal = false;
#endif
                                }
                            }
                        }
                        else if (TblName.Equals("Location"))
                        {
                            _codeTablesDC.Deactivate_All_Location(userID);

                            for (rowData = 0; rowData < _tableData.Count; rowData++)
                            {
                                try
                                {
                                    // 0 Active, 1 Location_Code_ID, 2 Location_Name, 3 Country_Code_ID, 4 [Default_Language], 5 Sponsorship_Site
                                    if (Convert.ToInt32(_tableData[rowData][0]) == 1)
                                    {
                                        _codeTablesDC.Add_Location
                                            (
                                            userID, 
                                            Convert.ToString(_tableData[rowData][2]).Trim(), 
                                            Convert.ToInt32(_tableData[rowData][3]),
                                            !_tableData[rowData][4].Equals(DBNull.Value) ? (int?)_tableData[rowData][4] : null, 
                                            Convert.ToBoolean(_tableData[rowData][5]), 
                                            (int?)_tableData[rowData][1]
                                            );
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogManager.EventLogManager.WriteEventLog(String.Format("{0} - {1} : Value Being Inserted - {2}  {3}", TblName, ex.Message, _tableData[rowData][1], _tableData[rowData][2]));
#if (DEBUG)
                                    retVal = false;
#endif
                                }
                            }
                        }
                        else if (TblName.Equals("Personality_Type"))
                        {
                            _codeTablesDC.Deactivate_Personality_Type(userID);

                            for (rowData = 0; rowData < _tableData.Count; rowData++)
                            {
                                try
                                {
                                    // 0 Active, 1 Personality_Type_Code_ID, 2 Personality_Type, 3 [Description], 4 Public_Description, 5 Min_Age
                                    if (Convert.ToInt32(_tableData[rowData][0]) == 1)
                                    {
                                        _codeTablesDC.Add_Personality_Type
                                            (
                                            userID, 
                                            Convert.ToString(_tableData[rowData][2]).Trim(), 
                                            Convert.ToString(_tableData[rowData][3]).Trim(),
                                            Convert.ToString(_tableData[rowData][4]).Trim(), 
                                            !_tableData[rowData][5].Equals(DBNull.Value) ? (int?)_tableData[rowData][5] : null,
                                            (int?)_tableData[rowData][1]
                                            );
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogManager.EventLogManager.WriteEventLog(String.Format("{0} - {1} : Value Being Inserted - {2}  {3}", TblName, ex.Message, _tableData[rowData][1], _tableData[rowData][2]));
#if (DEBUG)
                                    retVal = false;
#endif
                                }
                            }
                        }
                    }

                    _codeTablesDC.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogManager.EventLogManager.WriteEventLog(String.Format("UpdatCodeTables - {0}", ex.Message));
                retVal = false;
            }

            _fieldAppApiConnection.Dispose();

            return retVal;
        }
    }
}
