﻿using Pangea.API.FieldServices;
using Pangea.API.Models;
using Pangea.Data.DataObjects;
using Pangea.Data.Model;
using Pangea.Utilities.Tools;
using System;
using System.Data;
using System.Data.Linq;
using System.IO;
using System.Linq;
using System.Net;

namespace Pangea.API
{
    public class ChildAPI
    {
        /// <summary>
        /// Retrieves the Child Data that needs to be Updated from the Server.
        /// </summary>
        /// <param name="_locationID">Location ID for the Update Data</param>
        /// <returns>True if successfully gets Update Data. False if it Fails.</returns>
        public static bool RetrieveUpdateData(int _locationID)
        {
            LogManager.DebugLogManager.MethodBegin("PangeaAPI.GetUpdateData");

            bool retVal = true;

            bool retrieveFilesSuccess = false;

            BasicHttpsBinding_IFieldServices _fieldAppApiConnection = new BasicHttpsBinding_IFieldServices();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            try
            {
                DBOTablesDataContext _dboDataContext = new DBOTablesDataContext
                    (
                    PangeaInfo.TestVer ?
                    Centraler.Database_Settingz.DB_Staging
                    :
                    Centraler.Database_Settingz.DB_Production
                    );

                PendingTablesDataContext _pendingDataContext = new PendingTablesDataContext(PangeaInfo.DBCon);

                AdminChild[] _retrieveUpdae =
                    _fieldAppApiConnection
                    .GetFieldAppUpdateChildren
                    (
                        PangeaInfo.User.UserID.ToString(),
                        Convert.ToString(_locationID)
                        );

                if (_retrieveUpdae.Count() > 0)
                    _pendingDataContext.Deactivate_Pending(PangeaInfo.User.UserID);

                int count = 0;

                foreach (AdminChild adChild in _retrieveUpdae)
                {
                    int _actionID = PangeaCollections.ActionCollection.Find("Field Update").ID;

                    retrieveFilesSuccess = true;

                    PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.RetrieveData);

                    PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.RetrieveFailed);

                    PangeaInfo.TransmitMessages.Add
                        (
                        new MessagesStruct
                        (
                            String.Format("Retrieving Update Data {0} of {1}", ++count, _retrieveUpdae.Length),
                            MessageTypes.RetrieveData
                            )
                        );

                    if (adChild.FirstName.ToLower().Equals("error"))
                    {
                        retVal = false;

                        break;
                    }

                    _dboDataContext
                        .Add_Child
                        (
                        PangeaInfo.User.UserID,
                        Convert.ToInt64(adChild.ChildNum),
                        adChild.FirstName.Trim(),
                        adChild.LastName.Trim(),
                        new Guid(adChild.ChildID),
                        adChild.MiddleName.Trim(),
                        adChild.DateOfBirth,
                        adChild.GradeLevelCodeID,
                        adChild.HealthStatusCodeID,
                        adChild.LivesWithCodeID,
                        adChild.FavoriteLearningCodeID,
                        null,
                        null,
                        adChild.GenderCodeID,
                        Convert.ToByte(adChild.NumberOfBrothers),
                        Convert.ToByte(adChild.NumberOfSisters),
                        Convert.ToBoolean(adChild.DisabilityStatus),
                        adChild.LocationCodeID,
                        adChild.OtherNameGoesBy.Trim()
                        );

                    _dboDataContext
                        .Add_Child_Personality_Type
                        (
                        PangeaInfo
                        .User
                        .UserID,

                        new Guid(adChild.ChildID),

                        DateTime.Today.Date,
                        adChild.PersonalityTypeID,
                        null
                        );

                    foreach (int chore in adChild.ChoreIDs)
                    {
                        _dboDataContext
                            .Add_Child_Chore
                            (
                            PangeaInfo.User.UserID,
                            new Guid(adChild.ChildID),
                            DateTime.Today.Date,
                            chore,
                            null
                            );
                    }

                    foreach (int favAct in adChild.FavoriteActivitieIDs)
                    {
                        _dboDataContext
                            .Add_Child_Favorite_Activity
                            (
                            PangeaInfo.User.UserID,
                            new Guid(adChild.ChildID),
                            DateTime.Today.Date,
                            favAct,
                            null
                            );
                    }

                    foreach (string dups in adChild.ConfirmedNonDups)
                    {
                        _dboDataContext
                            .Add_Child_Duplicates
                            (
                            PangeaInfo.User.UserID,
                            new Guid(adChild.ChildID),
                            new Guid(dups)
                            );
                    }

                    // BUG 311 - Was not saving the Major Life Event when retrieving the Child Information.
                    if (adChild.MajorLifeEvent != null)
                        _dboDataContext
                            .Add_Child_Major_Life_Event
                            (
                            PangeaInfo.User.UserID,
                            new Guid(adChild.ChildID),
                            DateTime.Today,
                            adChild.LanguageCodeID,
                            adChild.MajorLifeEvent,
                            null
                            );

                    ChildProfilePhoto _childProfilePhoto = new ChildProfilePhoto();

                    _childProfilePhoto.ChildID = adChild.ChildID;

                    _childProfilePhoto.UserID = PangeaInfo.User.UserID.ToString();

                    _childProfilePhoto = _fieldAppApiConnection.GetChildProfilePhoto(_childProfilePhoto);

                    ;

                    if (
                        _childProfilePhoto.FileBytes != null &&
                        _childProfilePhoto.FileBytes.Count() > 0
                        )
                    {
                        _dboDataContext
                            .Add_Child_File
                            (
                            PangeaInfo.User.UserID,
                            new Guid(adChild.ChildID),
                            _childProfilePhoto.FileBytes,

                            String.Format
                            (
                                "{0}_{1}",
                                adChild.ChildID,
                                "Profile.jpg"
                                ),

                            _childProfilePhoto.ContentTypeID,
                            _childProfilePhoto.FileTypeID,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null
                            );

                    }

                    bool _insertPendingChild = true;

                    ISingleResult<GetASavedPendingResult> _recentSavedPending =
                        _pendingDataContext
                        .Get_A_Saved_Pending
                        (
                            PangeaInfo.User.UserID,
                            new Guid(adChild.ChildID)
                            );

                    foreach (GetASavedPendingResult _recentChild in _recentSavedPending)
                    {
                        _actionID = _recentChild.Action_ID;

                        _insertPendingChild = false;
                    }

                    if (_insertPendingChild)
                        _pendingDataContext
                            .Add_Child
                            (
                            PangeaInfo.User.UserID,
                            null,
                            null,
                            new Guid(adChild.ChildID),
                            Convert.ToInt64(adChild.ChildNum),
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            adChild.LocationCodeID,
                            null
                            );

                    _pendingDataContext
                        .Update_Child_Action
                        (
                        PangeaInfo.User.UserID,
                        new Guid(adChild.ChildID),
                        _actionID
                        );

                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorLogManager.WriteError("PangeaAPI.GetUpdateData", ex);

                retVal = false;
            }

            PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.RetrieveData);

            PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.RetrieveFailed);

            if (!retVal)
                PangeaInfo.TransmitMessages.Add
                    (
                    new MessagesStruct
                    (
                        "Retrieve Failed.",
                        MessageTypes.RetrieveFailed
                        )
                    );
            else
            {
                if (retrieveFilesSuccess)
                    PangeaInfo.TransmitMessages.Add
                        (
                        new MessagesStruct
                        (
                            "Retrieve Completed.",
                            MessageTypes.RetrieveComplete
                            )
                        );
                else
                    PangeaInfo.TransmitMessages.Add
                        (
                        new MessagesStruct
                        (
                            "Retrieve Completed. No Files Found.",
                            MessageTypes.RetrieveComplete
                            )
                        );
            }

            _fieldAppApiConnection.Dispose();

            LogManager.DebugLogManager.MethodEnd("PangeaAPI.GetUpdateData");

            return retVal;
        }

        /// <summary>
        /// This sends the Child Data to the Server for a new Enroll Child.
        /// It also sets the Child Number that is returned.
        /// </summary>
        /// <param name="_child">Child information to be sent to the Server</param>
        /// <returns>True if successfully gets Update Data. False if it Fails.</returns>
        public static bool SaveEnrollmentChild(ref ChildInfo _child)
        {

            LogManager.DebugLogManager.MethodBegin("PangeaAPI.SaveEnrollmentChild");

            bool retVal = true;

            if (_child.HasTransmitted)
                return retVal;

            BasicHttpsBinding_IFieldServices _fieldAppApiConnection = new BasicHttpsBinding_IFieldServices();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            EnrollChild the_Enrolling_Child = CreateAPIEnrollChild(_child);

            ReturnMessage[] _retMsg =
                _fieldAppApiConnection
                .SaveEnrollment
                (
                    the_Enrolling_Child, // CreateAPIEnrollChild(_child), 
                    PangeaInfo.User.UserID.ToString()
                    );

            foreach (ReturnMessage _msg in _retMsg)
            {
                if (_msg.Message == "ERROR")
                    retVal = false;
            }

            if (retVal)
                _child.HasTransmitted = true;

            _fieldAppApiConnection.Dispose();

            LogManager.DebugLogManager.MethodEnd("PangeaAPI.SaveEnrollmentChild");

            return retVal;

        }


        /// <summary>
        /// This passes the Child File Information being enrolled to the Server
        /// </summary>
        /// <param name="_child">Child information to be sent to the Server</param>
        /// <returns>True if successfully gets Update Data. False if it Fails.</returns>
        public static bool SaveEnrollmentFiles(ref ChildInfo _child)
        {
            LogManager.DebugLogManager.MethodBegin("PangeaAPI.SaveEnrollmentFiles");

            bool retVal = Send_To_WebAPI_Child_Content_Files(_child, Settingzenum_Operation_Type.Enrollment);

            LogManager.DebugLogManager.MethodEnd("PangeaAPI.SaveEnrollmentFiles");

            return retVal;
        }


        private static bool Send_To_WebAPI_Child_Content_Files
            (
            ChildInfo _child,
            Settingzenum_Operation_Type the_Operation_Type
            )
        {
            bool retVal = true;

            if(the_Operation_Type == Settingzenum_Operation_Type.Update)
            {

                if (_child.Remove_Reason != null)
                    return retVal;

            }

            BasicHttpsBinding_IFieldServices _fieldAppApiConnection = new BasicHttpsBinding_IFieldServices();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            for (int fileIndex = 0; fileIndex < _child.AdditionalResources.Count; fileIndex++)
            {
                PangeaFileStruct pfs = _child.AdditionalResources[fileIndex];

                if (pfs.HasTransmitted)
                    continue;

                ReturnMessage[] _fileRetMsg = new ReturnMessage[0];

                Settingzenum_Content_Binary_Type the_Content_Binary_Type = Settingzenum_Content_Binary_Type.Action;

                switch (pfs.ContentTypeInfo.Name)
                {
                    case "Profile":

                        the_Content_Binary_Type = Settingzenum_Content_Binary_Type.Profile_Image;

                        break;

                    case "Consent":

                        the_Content_Binary_Type = Settingzenum_Content_Binary_Type.Consent;

                        break;

                    case "Action":

                        the_Content_Binary_Type = Settingzenum_Content_Binary_Type.Action;

                        break;

                    case "Art":

                        the_Content_Binary_Type = Settingzenum_Content_Binary_Type.Art;

                        break;

                    case "Letter":

                        the_Content_Binary_Type = Settingzenum_Content_Binary_Type.Letter;

                        break;
                }

                UploadFile the_UploadFile =
                    CreateAPIFile
                    (
                        _child.ChildID,
                        pfs
                        );

                _fileRetMsg =
                    _fieldAppApiConnection
                    .Send_Content_File
                    (
                        PangeaInfo.User.UserID.ToString(),
                        the_UploadFile,
                        the_Operation_Type, 
                        true,
                        the_Content_Binary_Type,
                        true
                        );

                foreach (ReturnMessage _msg in _fileRetMsg)
                {
                    if (_msg.Message == "ERROR")
                        retVal = false;
                }

                if (!retVal)
                    break;
                else
                {
                    pfs.HasTransmitted = true;

                    _child.AdditionalResources[fileIndex] = pfs;
                }
            }

            _fieldAppApiConnection.Dispose();

            return retVal;
        }

        /// <summary>
        /// Sets the Enroll Flag on the Server for the Enrollment Child that was sent.
        /// </summary>
        /// <param name="_child">Child to be set to Enroll on the server.</param>
        /// <returns>True if successfully gets Update Data. False if it Fails.</returns>
        public static bool EnrollChild(ref ChildInfo _child)
        {
            LogManager.DebugLogManager.MethodBegin("PangeaAPI.EnrollChild");

            bool retVal = true;

            BasicHttpsBinding_IFieldServices _fieldAppApiConnection = new BasicHttpsBinding_IFieldServices();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            ReturnMessage[] _retMsgs =
                _fieldAppApiConnection
                .EnrollAChild
                (
                    _child.ChildID.ToString(),
                    PangeaInfo.User.UserID.ToString()
                    );

            foreach (ReturnMessage _msg in _retMsgs)
            {
                if (_msg.Message == "ERROR")
                    retVal = false;

                else if (_msg.Message == "Child_Number")
                    _child.ChildNumber = Convert.ToInt64(_msg.Code);
            }

            if (retVal)
            {
                EnrollmentTablesDataContext etdc = new EnrollmentTablesDataContext(PangeaInfo.DBCon);

                //etdc.Add_Child(PangeaInfo.User.UserID, _child.FirstName, _child.LastName, _child.ChildID, _child.ChildNumber, _child.MiddleName, _child.DateOfBirth,
                //    _child.GradeLvl.CodeID, _child.Health.CodeID, _child.Lives_With.CodeID, _child.FavLearning.CodeID, null, null, _child.Gender.CodeID,
                //    Convert.ToByte(_child.NumberBrothers), Convert.ToByte(_child.NumberSisters), _child.HasDisability, _child.ChildLocation.CodeID, _child.NickName);

                etdc.Update_Child_Action
                    (
                    PangeaInfo.User.UserID,
                    _child.ChildID,

                    PangeaCollections
                    .ActionCollection
                    .Find("Transfer Complete")
                    .ID
                    );

                _child.Action = PangeaCollections.ActionCollection.Find("Transfer Complete");

                foreach (PangeaFileStruct pfs in _child.AdditionalResources)
                {
                    etdc
                        .Update_Child_File_Action
                        (
                        PangeaInfo.User.UserID,
                        _child.ChildID,

                        pfs.StreamID,

                        PangeaCollections
                        .ActionCollection
                        .Find("Transfer Complete")
                        .ID
                        )
                        ;

                    pfs.FileDataInfo.Action_ID =
                        PangeaCollections
                        .ActionCollection
                        .Find("Transfer Complete")
                        .ID;

                }
            }

            _fieldAppApiConnection.Dispose();

            LogManager.DebugLogManager.MethodEnd("PangeaAPI.EnrollChild");

            return retVal;
        }

        /// <summary>
        /// Sends the Update Information Child Data to the Server.
        /// </summary>
        /// <param name="_child">Child Information to Send</param>
        /// <returns>True if successfully gets Update Data. False if it Fails.</returns>
        public static bool UpdateSaveChild(ref ChildInfo _child)
        {
            LogManager.DebugLogManager.MethodBegin("PangeaAPI.UpdateChild");

            bool retVal = true;

            if
                (
                _child.HasTransmitted ||
                _child.Remove_Reason != null
                )
                return retVal;

            BasicHttpsBinding_IFieldServices _fieldAppApiConnection = new BasicHttpsBinding_IFieldServices();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            ReturnMessage[] _retMsg =
                _fieldAppApiConnection
                .SaveUpdate
                (
                    CreateAPIUpdateChild(_child),
                    PangeaInfo.User.UserID.ToString()
                    );

            foreach (ReturnMessage _msg in _retMsg)
            {
                if (_msg.Message == "ERROR")
                    retVal = false;
            }

            if (retVal)
                _child.HasTransmitted = true;

            _fieldAppApiConnection.Dispose();

            LogManager.DebugLogManager.MethodEnd("PangeaAPI.UpdateChild");

            return retVal;
        }

        /// <summary>
        /// Sends the Update Information File Data to the Server.
        /// </summary>
        /// <param name="_child">Child Information to Send</param>
        /// <returns>True if successfully gets Update Data. False if it Fails.</returns>
        public static bool UpdateSendFiles(ref ChildInfo _child)
        {

            LogManager.DebugLogManager.MethodBegin("PangeaAPI.UpdateSendFiles");

            

            bool retVal = Send_To_WebAPI_Child_Content_Files(_child, Settingzenum_Operation_Type.Update);

            LogManager.DebugLogManager.MethodEnd("PangeaAPI.UpdateSendFiles");

            return retVal;
        }

        /// <summary>
        /// Sends to the Server that the Child Has been completly transfered for
        /// Update.
        /// </summary>
        /// <param name="_child">Child being updated</param>
        /// <returns>if it succeeded or not</returns>
        public static bool FinalizeChildUpdate(ref ChildInfo _child)
        {
            LogManager.DebugLogManager.MethodBegin("PangeaAPI.FinalizeChildUpdate");

            bool retVal = true;

            BasicHttpsBinding_IFieldServices _fieldAppApiConnection = new BasicHttpsBinding_IFieldServices();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            if (_child.Remove_Reason == null)
            {
                ReturnMessage[] _retMsgs =
                    _fieldAppApiConnection
                    .SubmitUpdate
                    (
                        _child.ChildID.ToString(),
                        PangeaInfo.User.UserID.ToString()
                        );

                foreach (ReturnMessage retMsg in _retMsgs)
                {
                    if (retMsg.Message == "ERROR")
                        retVal = false;
                }
            }
            else
            {

                ReturnMessage _retMsg =
                    _fieldAppApiConnection
                    .RemoveAChild
                    (
                        CreatAPIRemoveChild(_child),
                        PangeaInfo.User.UserID.ToString()
                        );

                if (_retMsg.Message == "ERROR")
                    retVal = false;
            }

            if (retVal)
            {
                PendingTablesDataContext ptdc = new PendingTablesDataContext(PangeaInfo.DBCon);

                ptdc.Update_Child_Action
                    (
                    PangeaInfo.User.UserID,
                    _child.ChildID,
                    PangeaCollections.ActionCollection.Find("Transfer Complete").ID
                    );

                _child.Action = PangeaCollections.ActionCollection.Find("Transfer Complete");

                foreach (PangeaFileStruct pfs in _child.AdditionalResources)
                {
                    ptdc.Update_Child_File_Action
                        (
                        PangeaInfo.User.UserID,
                        _child.ChildID,
                        pfs.StreamID,
                        PangeaCollections.ActionCollection.Find("Transfer Complete").ID
                        );

                    pfs.FileDataInfo.Action_ID = PangeaCollections.ActionCollection.Find("Transfer Complete").ID;
                }
            }

            _fieldAppApiConnection.Dispose();

            LogManager.DebugLogManager.MethodEnd("PangeaAPI.FinalizeChildUpdate");

            return retVal;
        }

        /// <summary>
        /// Copy the Pangea Child Class to the API EnrollChild Class used
        /// to Pass the Pangea Child to the Server DB.
        /// </summary>
        /// <param name="_child">Pangea Child Data</param>
        /// <returns>API Child Class</returns>
        private static EnrollChild CreateAPIEnrollChild(ChildInfo _child)
        {
            LogManager.DebugLogManager.MethodBegin("PangeaAPI.CreateAPIEnrollChild");

            EnrollChild ec = new EnrollChild();

            ec.ChildID = _child.ChildID.HasValue ? _child.ChildID.ToString() : string.Empty;

            ec.ChoreIDs = _child.Chores.Select(ch => ch.CodeID).ToArray();

            ec.ConfirmedNonDups = _child.NonDuplicates.Select(nd => nd.Child2_ID.ToString()).ToArray();

            if (_child.DateOfBirth.HasValue)
                ec.DateOfBirth = _child.DateOfBirth.Value.ToUniversalTime().Date;

            ec.DateOfBirthSpecified = _child.DateOfBirth.HasValue;

            ec.DisabilityStatus = _child.HasDisability.HasValue ? _child.HasDisability.Value : false;

            ec.DisabilityStatusSpecified = _child.HasDisability.HasValue;

            ec.FavoriteActivitieIDs = _child.FavActivity.Select(fa => fa.CodeID).ToArray();

            ec.FavoriteLearningCodeID = _child.FavLearning != null ? _child.FavLearning.CodeID : 0;

            ec.FavoriteLearningCodeIDSpecified = _child.FavLearning != null;

            ec.FirstName = _child.FirstName;

            ec.GenderCodeID = _child.Gender != null ? _child.Gender.CodeID : 0;

            ec.GenderCodeIDSpecified = _child.Gender != null;

            ec.GradeLevelCodeID = _child.GradeLvl != null ? _child.GradeLvl.CodeID : 0;

            ec.GradeLevelCodeIDSpecified = _child.GradeLvl != null;

            ec.HealthStatusCodeID = _child.Health != null ? _child.Health.CodeID : 0;

            ec.HealthStatusCodeIDSpecified = _child.Health != null;

            ec.LastName = _child.LastName;

            ec.LivesWithCodeID = _child.Lives_With != null ? _child.Lives_With.CodeID : 0;

            ec.LivesWithCodeIDSpecified = _child.Lives_With != null;

            ec.LocationCodeID = _child.ChildLocation != null ? _child.ChildLocation.CodeID : 0;

            ec.LocationCodeIDSpecified = _child.ChildLocation != null;

            ec.MiddleName = _child.MiddleName;

            ec.NumberOfBrothers = _child.NumberBrothers;

            ec.NumberOfBrothersSpecified = true;

            ec.NumberOfSisters = _child.NumberSisters;

            ec.NumberOfSistersSpecified = true;

            ec.OtherNameGoesBy = _child.NickName;

            ec.PersonalityTypeID = _child.Personality_type != null ? _child.Personality_type.CodeID : 0;

            ec.PersonalityTypeIDSpecified = _child.Personality_type != null;

            // ec.MajorLifeEvent = _child.MajorLifeEvent;

            LogManager.DebugLogManager.MethodEnd("PangeaAPI.CreateAPIEnrollChild");

            return ec;
        }

        /// <summary>
        /// Copy the Pangea Child Class to the API UpdateChild Class used
        /// to Pass the Pangea Child to the Server DB.
        /// </summary>
        /// <param name="_child">Pangea Child Data</param>
        /// <returns>API Child Class</returns>
        public static UpdateChild CreateAPIUpdateChild(ChildInfo _child)
        {
            LogManager.DebugLogManager.MethodBegin("PangeaAPI.CreateAPIUpdateChild");

            UpdateChild uc = new UpdateChild();

            uc.ChildID = _child.ChildID.ToString();

            uc.ChoreIDs = _child.Chores.Select(ch => ch.CodeID).ToArray();

            uc.ConfirmedNonDups = _child.NonDuplicates.Select(nd => nd.Child2_ID.ToString()).ToArray();

            uc.DisabilityStatus = _child.HasDisability.HasValue ? _child.HasDisability.Value : false;

            uc.DisabilityStatusSpecified = _child.HasDisability.HasValue;

            uc.FavoriteActivitieIDs = _child.FavActivity.Select(fa => fa.CodeID).ToArray();

            uc.FavoriteLearningCodeID = _child.FavLearning != null ? _child.FavLearning.CodeID : 0;

            uc.FavoriteLearningCodeIDSpecified = _child.FavLearning != null;

            uc.GradeLevelCodeID = _child.GradeLvl != null ? _child.GradeLvl.CodeID : 0;

            uc.GradeLevelCodeIDSpecified = _child.GradeLvl != null;

            uc.HealthStatusCodeID = _child.Health != null ? _child.Health.CodeID : 0;

            uc.HealthStatusCodeIDSpecified = _child.Health != null;

            // BUG 311 - Setting language code ID based on the Current Two Letter ISO Language Name
            uc.LanguageCodeID = PangeaCollections.LanguageCollection.Find
                (
                System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName
                )?.CodeID ?? -1;

            uc.LanguageCodeIDSpecified = uc.LivesWithCodeID != -1;

            uc.LivesWithCodeID = _child.Lives_With != null ? _child.Lives_With.CodeID : 0;

            uc.LivesWithCodeIDSpecified = _child.Lives_With != null;

            // BUG 311 - Adding the passing of the Major Life Event over.
            uc.MajorLifeEvent = _child.MajorLifeEvent;

            uc.NumberOfBrothers = _child.NumberBrothers;

            uc.NumberOfBrothersSpecified = true;

            uc.NumberOfSisters = _child.NumberSisters;

            uc.NumberOfSistersSpecified = true;

            uc.PersonalityTypeID = _child.Personality_type != null ? _child.Personality_type.CodeID : 0;

            uc.PersonalityTypeIDSpecified = _child.Personality_type != null;

            LogManager.DebugLogManager.MethodEnd("PangeaAPI.CreateAPIUpdateChild");

            return uc;
        }

        /// <summary>
        /// Copy the Pangea Child File Information to the API
        /// File Information
        /// </summary>
        /// <param name="childID">ID of the Child the Files are associated with.</param>
        /// <param name="pfs">The Pangea File Structure that contains the File information</param>
        /// <returns>API Upload File Class</returns>
        private static UploadFile CreateAPIFile(Guid? childID, PangeaFileStruct pfs)
        {
            LogManager.DebugLogManager.MethodBegin("PangeaAPI.CreateAPIFile");

            UploadFile uf = new UploadFile();

            uf.ChildID = childID.ToString();

            uf.ContentTypeID = pfs.ContentTypeInfo.CodeID;

            uf.ContentTypeIDSpecified = true;

            uf.FileBytes = File.ReadAllBytes(pfs.FullFileLoc);

            uf.FileName = pfs.ShortFileName;

            uf.FileTypeID = pfs.FileTypeInfo.CodeID;

            uf.FileTypeIDSpecified = true;

            uf.MetaData = CreateAPIFileMetaData(pfs.FileDataInfo);

            uf.UserID = PangeaInfo.User.UserID.ToString();

            LogManager.DebugLogManager.MethodEnd("PangeaAPI.CreateAPIFile");

            return uf;
        }

        /// <summary>
        /// Copy the Pangea Child File Data Information to the API
        /// File Meta Data
        /// </summary>
        /// <param name="fd">Pangea File Data</param>
        /// <returns>API File Meta Data</returns>
        private static FileMetaData CreateAPIFileMetaData(FileData fd)
        {
            LogManager.DebugLogManager.MethodBegin("PangeaAPI.CreateAPIFileMetaData");

            FileMetaData fmd = new FileMetaData();

            fmd.DateTaken = fd.DateTime.HasValue ? fd.DateTime.Value.Date.ToString() : null;

            fmd.Digitized = fd.DateTimeDigitized.HasValue ? fd.DateTimeDigitized.Value.Date.ToString() : null;

            fmd.GPSAltitude = fd.GPSAltitude;

            fmd.GPSAltitudeRef = fd.GPSAltitudeRef;

            fmd.GPSDateStamp = fd.GPSDateStamp;

            fmd.GPSImgDirectionRef = fd.GPSImgDirectionRef;

            fmd.GPSLatitude = fd.GPSLatitude;

            fmd.GPSLatitudeRef = fd.GPSLatitudeRef;

            fmd.GPSLongitude = fd.GPSLongitude;

            fmd.GPSLongitudeRef = fd.GPSLongitudeRef;

            fmd.GPSTimeStamp = fd.GPSTimeStamp;

            fmd.GPSVersionID = fd.GPSVersionID;

            fmd.Make = fd.Make;

            fmd.Model = fd.Model;

            fmd.Original = fd.DateTimeOriginal.HasValue ? fd.DateTimeOriginal.Value.Date.ToString() : null;

            fmd.Software = fd.Software;

            LogManager.DebugLogManager.MethodEnd("PangeaAPI.CreateAPIFileMetaData");

            return fmd;
        }

        /// <summary>
        /// Creates the Remove Child Cbject used to pass the Child ID and 
        /// Remove reason through the API.
        /// </summary>
        /// <param name="_child">Child to be removed</param>
        /// <returns>Remove Child Object</returns>
        public static RemoveChild CreatAPIRemoveChild(ChildInfo _child)
        {
            RemoveChild retVal = new RemoveChild();

            retVal.ChildID = _child.ChildID.ToString();

            retVal.RemovalReasonID = _child.Remove_Reason != null ? _child.Remove_Reason.CodeID : -1;

            retVal.RemovalReasonIDSpecified = _child.Remove_Reason != null;

            return retVal;
        }


        public static Models.Process_WorkFlows_And_Get_PendingChild_ID_Returned Process_WorkFlows_And_Get_PendingChild_ID_INSERT
            (string the_Child_ID)
        {

            Process_WorkFlows_And_Get_PendingChild_ID_Returned ret_Data;

            string method_name = "Process_WorkFlows_And_Get_PendingChild_ID_INSERT";

            BasicHttpsBinding_IFieldServices fieldApp_ApiConnection;

            Process_WorkFlows_And_Get_PendingChild_ID_Initial_Steps
                (
                the_Child_ID,
                out ret_Data,
                method_name,
                out fieldApp_ApiConnection
                );



            if (ret_Data.PendingChild_ID.HasValue)
            {
                WorkFlowStep[] _chWrkFlowSteps =
                    fieldApp_ApiConnection
                    .GetChildWorkFlow
                    (
                        ret_Data.PendingChild_ID.Value,
                        true,
                        1,
                        true,
                        PangeaInfo.User.UserID.ToString(),
                        false,
                        true
                        );

                foreach (WorkFlowStep _step in _chWrkFlowSteps)
                {
                    _step.QCApprover = false;

                    _step.Complete = true;

                    fieldApp_ApiConnection.SaveWorkFlow
                        (
                        _step,
                        PangeaInfo.User.UserID.ToString()
                        );

                    ret_Data.Step_2_Succeeded = true;
                }

            }

            return Process_WorkFlows_And_Get_PendingChild_ID_End_Processing
                (
                ret_Data, 
                method_name, 
                fieldApp_ApiConnection
                );

        }

        private static Process_WorkFlows_And_Get_PendingChild_ID_Returned Process_WorkFlows_And_Get_PendingChild_ID_End_Processing
            (
            Process_WorkFlows_And_Get_PendingChild_ID_Returned ret_Data, 
            string method_name, 
            BasicHttpsBinding_IFieldServices fieldApp_ApiConnection
            )
        {
            fieldApp_ApiConnection.Dispose();

            LogManager.DebugLogManager.MethodEnd(method_name);

            return ret_Data;
        }

        public static Models.Process_WorkFlows_And_Get_PendingChild_ID_Returned Process_WorkFlows_And_Get_PendingChild_ID_UPDATE
            (string the_Child_ID)
        {

            Process_WorkFlows_And_Get_PendingChild_ID_Returned ret_Data;

            string method_name = "Process_WorkFlows_And_Get_PendingChild_ID_UPDATE";

            BasicHttpsBinding_IFieldServices fieldApp_ApiConnection;

            Process_WorkFlows_And_Get_PendingChild_ID_Initial_Steps
                (
                the_Child_ID,
                out ret_Data,
                method_name,
                out fieldApp_ApiConnection
                );


            if (ret_Data.PendingChild_ID.HasValue)
            {

                WorkFlowStep[] _chWrkFlowSteps =
                    fieldApp_ApiConnection
                    .GetChildWorkFlow
                    (
                        ret_Data.PendingChild_ID.Value,
                        true,
                        2,
                        true,
                        PangeaInfo.User.UserID.ToString(),
                        false,
                        true
                        );


                foreach (WorkFlowStep _step in _chWrkFlowSteps)
                {

                    _step.QCApprover = false;

                    _step.Complete = true;

                    fieldApp_ApiConnection.SaveWorkFlow
                        (
                        _step,
                        PangeaInfo.User.UserID.ToString()
                        );

                    ret_Data.Step_2_Succeeded = true;

                }

            }

            return Process_WorkFlows_And_Get_PendingChild_ID_End_Processing
                (
                ret_Data,
                method_name,
                fieldApp_ApiConnection
                );

        }

        

        private static void Process_WorkFlows_And_Get_PendingChild_ID_Initial_Steps
            (
            string the_Child_ID, 
            out Process_WorkFlows_And_Get_PendingChild_ID_Returned ret_Data, 
            string method_name, 
            out BasicHttpsBinding_IFieldServices fieldApp_ApiConnection
            )
        {
            ret_Data = new Models.Process_WorkFlows_And_Get_PendingChild_ID_Returned()
            {
                Step_1_Succeeded = false,
                Step_2_Succeeded = false,
                PendingChild_ID = null
            };
            
            LogManager.DebugLogManager.MethodBegin(method_name);

            fieldApp_ApiConnection = new BasicHttpsBinding_IFieldServices();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            Process_WorkFlows_And_Get_PendingChild_ID__Process_Step_1
                (
                the_Child_ID,
                ret_Data,
                fieldApp_ApiConnection
                );
        }

        private static void Process_WorkFlows_And_Get_PendingChild_ID__Process_Step_1
            (
            string the_Child_ID, 
            Process_WorkFlows_And_Get_PendingChild_ID_Returned ret_Data, 
            BasicHttpsBinding_IFieldServices fieldApp_ApiConnection
            )
        {
            DataSet QC_List_Dataset =
                fieldApp_ApiConnection
                .GetWorkFlows
                (
                    0,
                    false,
                    0,
                    false,
                    0,
                    false,
                    PangeaInfo.User.UserID.ToString(),
                    ""
                    );

            foreach (DataRow row in QC_List_Dataset.Tables[0].Rows)
            {
                if (
                    the_Child_ID
                    .Equals
                    (Convert.ToString(row["Child_GUID"]))
                    )
                {
                    ret_Data.PendingChild_ID = Convert.ToInt32(row["Pending_Child_ID"]);

                    ret_Data.Step_1_Succeeded = true;

                    break;
                }
            }
        }

        public static BasicHttpsBinding_IFieldServices Get_FieldServices_API_Connection()
        {
            
            BasicHttpsBinding_IFieldServices FieldAppApiConnection = new BasicHttpsBinding_IFieldServices();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            return FieldAppApiConnection;

        }


    }
}
