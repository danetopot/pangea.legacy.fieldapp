﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;

namespace Automater
{
    [TestClass]
    public class PangeafieldApp_Tester
    {


        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {

            ;

            pangeaFieldAppSession.Setup(context);

        }


        [ClassCleanup]
        public static void ClassCleanup()
        {
            pangeaFieldAppSession.TearDown();
        }


        [TestMethod]
        public void Pangea_Field_App_Tester()
        {
            ;

            int i = 0;

            ;

            try
            {
                pangeaFieldAppSession.app_session.FindElementByName("Refresh Data").FindElementByName("No").Click();
            }
            catch { }

            Thread.Sleep(TimeSpan.FromMilliseconds(500)); 

            ;

            // pangeaFieldAppSession.app_session.FindElementByName("HomeTItem").Click();

            // pangeaFieldAppSession.app_session.FindElementByXPath($"//TabItem[starts-with(@Name, \"EnrollmentTabItem\")]").Click();
            pangeaFieldAppSession.app_session.FindElementByXPath($"//TabItem[starts-with(@Name, \"Enrollment\")]").Click();

            ;

            Thread.Sleep(TimeSpan.FromMilliseconds(250));

            ;

            // 

            ;

            // 

            // pangeaFieldAppSession.app_session.FindElementByName($"//Text Editor[starts-with(@Name, \"ChildInfoFNameTBox\")]").SendKeys("Jo Bob!");

            // pangeaFieldAppSession.app_session.FindElementByName($"//WatermarkTextBox[starts-with(@Name, \"ChildInfoFNameTBox\")]").SendKeys("Jo Bob!");

            ;


            // pangeaFieldAppSession.app_session.FindElementByName("Default Location Select").FindElementByName("CountryCBox").Click();

            // pangeaFieldAppSession.app_session.FindElementByName("LocSelectWindow").FindElementByName("CountryCBox").Click();

            // pangeaFieldAppSession.app_session.FindElementByXPath($"//xctk:WatermarkComboBox[starts-with(@Name, \"CountryCBox\")]").Click();
            // pangeaFieldAppSession.app_session.FindElementByXPath($"//WatermarkComboBox[starts-with(@Name, \"CountryCBox\")]").Click();

            // pangeaFieldAppSession.app_session.FindElementByName("LocSelectWindow").FindElementByName("CountryCBox").Click();
            // pangeaFieldAppSession.app_session.FindElementByName("LocSelect").FindElementByName("Country").Click();

            // pangeaFieldAppSession.app_session.FindElementByWindowsUIAutomation("LocSelectWindow").FindElementByWindowsUIAutomation("CountryCBox").Click();

            // pangeaFieldAppSession.app_session.FindElementByAccessibilityId("LocSelectWindow").FindElementByAccessibilityId("CountryCBox").Click();

            // pangeaFieldAppSession.app_session.f("LocSelectWindow").FindElementByAccessibilityId("CountryCBox").Click();

            // pangeaFieldAppSession.app_session.FindElementByAccessibilityId("LocSelectWindow").Click();

            // bool is_displayed = pangeaFieldAppSession.app_session.FindElementByName("Default Location Select").Displayed;

            // bool is_displayed = pangeaFieldAppSession.app_session.FindElementByXPath($"//Window[starts-with(@Name, \"Default Location Select\")]").Displayed;

            // pangeaFieldAppSession.app_session.FindElementByClassName("TextBlock").FindElementByName("El Salvador").Click();

            /*
            string xpath_LeftClickTextElSalvador_810_545 = "/Pane[@ClassName=\"#32769\"][@Name=\"Desktop 1\"]/Window[@Name=\"Default Location Select\"][@AutomationId=\"LocSelectWindow\"]/ComboBox[@AutomationId=\"CountryCBox\"]/ListItem[@ClassName=\"ListBoxItem\"][@Name=\"Pangea.Data.Model.CodeTables.Country\"]/Text[@ClassName=\"TextBlock\"][@Name=\"El Salvador\"]";

            var winElem_LeftClickTextElSalvador_810_545 = pangeaFieldAppSession.app_session.FindElementByXPath(xpath_LeftClickTextElSalvador_810_545);
            */

            // pangeaFieldAppSession.app_session.FindElementByClassName("Text Block").FindElementByName("First Name").SendKeys("Jo Bob!");

            // ChildInfoFNameTBox\"]"

            // pangeaFieldAppSession.app_session.FindElementById("WpfTextView").SendKeys("Jo Bob!");
            // pangeaFieldAppSession.app_session.FindElementByName("yyz").SendKeys("Jo Bob!");
            // pangeaFieldAppSession.app_session.FindElementByAccessibilityId("yyz").SendKeys("Jo Bob!");

            // 
            // pangeaFieldAppSession.app_session.FindElementByAccessibilityId("yyyz").SendKeys("Jo Bob!");
            // pangeaFieldAppSession.app_session.FindElementByWindowsUIAutomation("yyyz").SendKeys("Jo Bob!");

            // pangeaFieldAppSession.app_session.FindElementByWindowsUIAutomation("yyyz").SendKeys("Jo Bob!");

            // ChildInfoFNameTBox

            //pangeaFieldAppSession.app_session.FindElementByAccessibilityId("CountryCBox").Click();

            // 
            // AutomationProperties.AutomationId = "zzx"

            ;

            pangeaFieldAppSession.app_session.FindElementByName("El Salvador").Click();

            ;

            ;

            pangeaFieldAppSession.app_session.FindElementByAccessibilityId("ChildInfoFNameTBox").SendKeys("Jo Bob!");
            
            pangeaFieldAppSession.app_session.FindElementByAccessibilityId("zzx").SendKeys("Jo Bob!");

            ;

            Thread.Sleep(TimeSpan.FromMilliseconds(150));

            ;

            // 

            ;

            Thread.Sleep(TimeSpan.FromMilliseconds(150));

            ;


            // pangeaFieldAppSession.app_session.FindElementByName("File").Click();

            pangeaFieldAppSession.app_session.FindElementByXPath($"//MenuItem[starts-with(@Name, \"File\")]").Click();

            Thread.Sleep(TimeSpan.FromMilliseconds(150)); 

            pangeaFieldAppSession.app_session.FindElementByXPath($"//MenuItem[starts-with(@Name, \"Close\")]").Click();

            ;

            Thread.Sleep(TimeSpan.FromSeconds(1)); // Wait for 1 second until the save dialog appears

            ;

            // pangeaFieldAppSession.app_session.FindElementByName("CloseMenuItem").Click();

            i++;

            Assert.IsTrue(i == 1);

        }




    }
}
