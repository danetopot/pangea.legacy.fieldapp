﻿using System.Windows;

using Pangea.FieldApplication.ViewModels;
using System.Windows.Input;

namespace Pangea.FieldApplication.Views
{
    /// <summary>
    /// Interaction logic for ResourceTypeSelectionWindow.xaml
    /// </summary>
    public partial class ResourceTypeSelectionWindow : Window
    {
        private ResourceTypeSelectionViewModel _resourceTypeSelVM;

        private bool _shiftKeyDown;

        public ResourceTypeSelectionWindow(bool otherFiles)
        {

            InitializeComponent();

            _resourceTypeSelVM = new ResourceTypeSelectionViewModel(otherFiles);

            DataContext = _resourceTypeSelVM;

            FileLoadBtn.Click += _resourceTypeSelVM.BrowseBtn_Click;

            _shiftKeyDown = false;

            KeyDown += ResourceTypeSel_KeyDown;

            KeyUp += ResourceTypeSel_KeyUp;

            _resourceTypeSelVM.Show_Web_Browser_Content_Item += _resourceTypeSelVM_Show_Web_Browser_Content_Item;

            _resourceTypeSelVM.Show_Image_Content_Item += _resourceTypeSelVM_Show_Image_Content_Item;

            Show_Image_Hide_Web_Browser_Display();

        }
        

        /// <summary>
        /// This is to prevent the Mouse Cursor from getting stuck as oe of the different arrow
        /// types that can occur while using the Cropping Tool.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResourceTypeSel_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            Point relativePoint = ImageDisplayImg.TransformToAncestor(this).Transform(new Point(0, 0));

            Point mousePos = e.GetPosition(this);

            if ((mousePos.X < relativePoint.X || mousePos.X > (relativePoint.X + ImageDisplayImg.ActualWidth)) ||
                (mousePos.Y < relativePoint.Y || mousePos.Y > (relativePoint.Y + ImageDisplayImg.ActualHeight)))
            {
                if (Mouse.OverrideCursor != Cursors.Arrow)
                    Mouse.OverrideCursor = Cursors.Arrow;
            }
        }

        /// <summary>
        /// Receives the KeyDown Message for the whole Resource Type Selection Window and
        /// does different commands as needed.
        /// </summary>
        /// <param name="sender">The Window</param>
        /// <param name="e">The keyevents that are happening.</param>
        private void ResourceTypeSel_KeyDown(object sender, KeyEventArgs e)
        {
            if (!FileTypeCBox.IsFocused)
            {
                if (e.Key != Key.Tab)
                    e.Handled = true;

                if (e.Key == Key.LeftShift || e.Key == Key.RightShift)
                    _shiftKeyDown = true;

                if (!Keyboard.IsKeyDown(Key.LeftShift) && !Keyboard.IsKeyDown(Key.RightShift))
                    _shiftKeyDown = false;

                if (e.Key == Key.Left || Keyboard.IsKeyDown(Key.Left))
                {
                    if (_shiftKeyDown)
                        // If shift it selected then we are shrinking the Cropped Boarder Window.
                        ImageDisplayImg.RightBorderMove(-1.0);
                    else
                    {
                        ImageDisplayImg.DetermineCroppedBoarderOrigOffsets();

                        ImageDisplayImg.MoveCroppedBorder(-1.0, 0.0);
                    }
                }

                if (e.Key == Key.Right || Keyboard.IsKeyDown(Key.Right))
                {
                    if (_shiftKeyDown)
                        // If shift it selected then we are shrinking the Cropped Boarder Window.
                        ImageDisplayImg.RightBorderMove(1.0);
                    else
                    {
                        ImageDisplayImg.DetermineCroppedBoarderOrigOffsets();

                        ImageDisplayImg.MoveCroppedBorder(1.0, 0.0);
                    }
                }

                if (e.Key == Key.Up || Keyboard.IsKeyDown(Key.Up))
                {
                    if (_shiftKeyDown)
                        // If shift it selected then we are shrinking the Cropped Boarder Window.
                        ImageDisplayImg.BottomBorderMove(-1.0);
                    else
                    {
                        ImageDisplayImg.DetermineCroppedBoarderOrigOffsets();

                        ImageDisplayImg.MoveCroppedBorder(0.0, -1.0);
                    }
                }


                if (e.Key == Key.Down || Keyboard.IsKeyDown(Key.Down))
                {
                    if (_shiftKeyDown)
                        // If shift it selected then we are shrinking the Cropped Boarder Window.
                        ImageDisplayImg.BottomBorderMove(1.0);
                    else
                    {
                        ImageDisplayImg.DetermineCroppedBoarderOrigOffsets();

                        ImageDisplayImg.MoveCroppedBorder(0.0, 1.0);
                    }
                }
            }
        }

        /// <summary>
        /// Receives the KeyUp Message for the whole Resource Type Selection Window and
        /// does different commands as needed.
        /// </summary>
        /// <param name="sender">The Window</param>
        /// <param name="e">The Key Event that is happening.</param>
        private void ResourceTypeSel_KeyUp(object sender, KeyEventArgs e)
        {
            if (!FileTypeCBox.IsFocused)
            {
                if (e.Key == Key.LeftShift || e.Key == Key.RightShift)
                    _shiftKeyDown = false;

                if (!Keyboard.IsKeyDown(Key.LeftShift) && !Keyboard.IsKeyDown(Key.RightShift))
                    _shiftKeyDown = false;
            }
        }


        private void _resourceTypeSelVM_Show_Web_Browser_Content_Item(string file_location)
        {

            Set_Browser_with_Given_Content_Path(file_location);

        }


        private void Set_Browser_with_Given_Content_Path(string file_location)
        {

            Show_Browser_Hide_Image_Display();

            web_browser.Navigate(file_location);

        }


        private void Show_Browser_Hide_Image_Display()
        {
            web_browser.Width = Centraler.Settingz.Web_Browser_Width;

            web_browser.Height = Centraler.Settingz.ImageDisplay_Img_Height;

            web_browser.VerticalAlignment = VerticalAlignment.Top;

            ImageDisplayImg.Visibility = Visibility.Collapsed;

            CroppenImageImg.Visibility = Visibility.Collapsed;

            brdr_Web_Browser.Visibility = Visibility.Visible;
        }

        private void Show_Image_Hide_Web_Browser_Display()
        {

            brdr_Web_Browser.Visibility = Visibility.Collapsed;

            ImageDisplayImg.Visibility = Visibility.Visible;

            CroppenImageImg.Visibility = Visibility.Visible;


            /*
            web_browser.Width = Centraler.Settingz.Web_Browser_Width;

            web_browser.Height = Centraler.Settingz.ImageDisplay_Img_Height;

            web_browser.VerticalAlignment = VerticalAlignment.Top;

            ImageDisplayImg.Visibility = Visibility.Collapsed;

            CroppenImageImg.Visibility = Visibility.Collapsed;
            
            web_browser.Navigate(file_location);
            */

        }


        private void _resourceTypeSelVM_Show_Image_Content_Item()
        {

            Show_Image_Hide_Web_Browser_Display();

        }


    }
}
