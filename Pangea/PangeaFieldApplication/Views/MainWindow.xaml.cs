﻿//////////////////////////////////////////////////////////////////
/// Main View for the Pangea Field Application.
/////////////////////////////////////////////////////////////////

using System;
using System.Windows;

using Pangea.FieldApplication.ViewModels;
using Pangea.Utilities.Tools;
using System.Windows.Controls;
using Pangea.Data.DataObjects;

namespace Pangea.FieldApplication.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowViewModel _mainWinVM;

        private HomeScreenControl _homeScreenControl;

        private ChildInformationControl _enrollment_childInfoControl;

        private EnrollmentInformationControl _enrollment_enrollControl;

        private ChildInformationControl _update_childInfoControl;

        private EnrollmentInformationControl _update_enrollControl;

        private ResourceAttachmentControl _resourceAttachmentControl;

        private ActionBarControl _actionBarControl;

        private ChildDataListControl _childDataListControl;

        private bool _enrollTabSelected;

        private bool _updateInfoTabSelected;

        private bool _showSelectDefaultLocationInfo;

        public MainWindow()
        {
            LogManager.DebugLogManager.MethodBegin("MainWindow");

            try
            {
                InitializeComponent();

                _mainWinVM = new MainWindowViewModel();

                DataContext = _mainWinVM;

                Loaded += MainWindow_Loaded;

                SizeChanged += MainWindow_SizeChanged;

                Closing += MainWindow_Closing;

                GISaveProgressBtn.Click += _mainWinVM.SaveProgressBtn_Click;

                ENSaveProgressBtn.Click += _mainWinVM.SaveProgressBtn_Click;

                GINextBtn.Click += NextBtn_Click;

                ENNextBtn.Click += NextBtn_Click;

                UpdateENSaveProgressBtn.Click += _mainWinVM.SaveProgressBtn_Click;

                UpdateGINextBtn.Click += NextBtn_Click;

                UpdateENNextBtn.Click += NextBtn_Click;

                MessageBarControlItem.MessageBarVM.PropertyChanged += _mainWinVM.MainWindow_PropertyChanged;

                _showSelectDefaultLocationInfo = true;

                _enrollTabSelected = false;

                _updateInfoTabSelected = false;

                _mainWinVM.PropertyChanged += _mainWinVM_PropertyChanged;

                InternetConnectionCtrl.MouseLeftButtonUp += _mainWinVM.InternetConnectionCtrl_MouseLeftButtonUp;

                LoginButton.Click += _mainWinVM.LoginButton_Click;
            }
            catch (Exception ex)
            {
                LogManager.ErrorLogManager.WriteError("MainWindow", ex);
#if DEBUG
                MessageBox.Show(ex.Message, "MainWindow");
#endif
            }

            LogManager.DebugLogManager.MethodEnd("MainWindow");
        }

        /// <summary>
        /// Is Called When the window starts to close.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _mainWinVM.Closing();
        }

        /// <summary>
        /// Is Called when the Main Window Size Changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("MainWindow.MainWindow_SizeChanged");

            InternetConnectionMenuItem.Margin = new Thickness((FieldMapMenu.ActualWidth - InternetConnectionMenuItem.ActualWidth), (0 - (InternetConnectionCtrl.ActualHeight / 2)), 0, 0);

            if (ChildInfoTabItem.IsSelected)
            {
                double topSpacing = (ChildInfoBtnSP.ActualHeight - (GINextBtn.Height)) / 2.0;

                double sideSpacing = (ChildInfoBtnSP.ActualWidth - (GISaveProgressBtn.Width + GINextBtn.Width + 50)) / 2.0;

                GISaveProgressBtn.Margin = new Thickness(sideSpacing, topSpacing, 0, 0);

                GINextBtn.Margin = new Thickness(50, topSpacing, 0, 0);
            }

            if (EnrollInfoTItem.IsSelected)
            {
                double hspace = ((EnrollItemBtnSP.ActualHeight - ENSaveProgressBtn.Height) / 2.0);

                double sideSpacing = (EnrollItemBtnSP.ActualWidth - (ENNextBtn.Width + ENSaveProgressBtn.Width + 50)) / 2.0;

                ENSaveProgressBtn.Margin = new Thickness(sideSpacing, hspace, 0, 0);

                ENNextBtn.Margin = new Thickness(50, hspace, 0, 0);
            }

            if (UpdateChildInfoTabItem.IsSelected)
            {
                double topSpacing = (UpdateChildInfoBtnSP.ActualHeight - (UpdateGINextBtn.Height)) / 2.0;

                double sideSpacing = (UpdateChildInfoBtnSP.ActualWidth - UpdateGINextBtn.Width) / 2.0;

                UpdateGINextBtn.Margin = new Thickness(sideSpacing, topSpacing, 0, 0);
            }

            if (UpdateInfoTItem.IsSelected)
            {
                double hspace = ((UpdateEnrollItemBtnSP.ActualHeight - UpdateENSaveProgressBtn.Height) / 2.0);

                double sideSpacing = (UpdateEnrollItemBtnSP.ActualWidth - (UpdateENNextBtn.Width + UpdateENSaveProgressBtn.Width + 50)) / 2.0;

                UpdateENSaveProgressBtn.Margin = new Thickness(sideSpacing, hspace, 0, 0);

                UpdateENNextBtn.Margin = new Thickness(50, hspace, 0, 0);
            }

            LogManager.DebugLogManager.MethodEnd("MainWindow.MainWindow_SizeChanged");
        }

        /// <summary>
        /// Is Called when the Main Window has finished Loading
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("MainWindow.MainWindow_Loaded");

            if (_homeScreenControl == null)
                _homeScreenControl = new HomeScreenControl();

            HomeDockPanel.Children.Add(_homeScreenControl);

            if (_resourceAttachmentControl == null)
                _resourceAttachmentControl = new ResourceAttachmentControl();

            _resourceAttachmentControl.Visibility = Visibility.Hidden;

            ResourceAttachmentGrid.Children.Add(_resourceAttachmentControl);

            if (_actionBarControl == null)
            {
                _actionBarControl = new ActionBarControl();

                ((ActionBarViewModel)_actionBarControl.DataContext).PropertyChanged += ActionBarControl_PropertyChanged;

                ActionBarStackPanel.Children.Add(_actionBarControl);
            }

            _mainWinVM.Loaded();

            LogManager.DebugLogManager.MethodEnd("MainWindow.MainWindow_Loaded");
        }

        /// <summary>
        /// This is fired when the Child Information Tab of the Enrollment Tab Controls is Selected
        /// </summary>
        /// <param name="sender">The Tab Selected</param>
        /// <param name="e">Selection Changed Event Arguments</param>
        private void ChildInfoTItem_Selected(object sender, RoutedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("MainWindow.ChildInfoTItem_Selected");

            if (_enrollment_childInfoControl == null)
            {
                _enrollment_childInfoControl = new ChildInformationControl();

                ChildInfoGrid.Children.Add(_enrollment_childInfoControl);
            }

            if (_enrollTabSelected && PangeaInfo.SaveRequired)// Bug 334
                PangeaInfo.SaveProgress();

            _enrollTabSelected = false;

            _updateInfoTabSelected = false;

            if (EnrollInfoTItem != null)
                EnrollInfoTItem.IsSelected = false;

            if (UpdateChildInfoTabItem != null)
                UpdateChildInfoTabItem.IsSelected = false;

            if (UpdateInfoTItem != null)
                UpdateInfoTItem.IsSelected = false;

            LogManager.DebugLogManager.MethodEnd("MainWindow.ChildInfoTItem_Selected");
        }

        private void UpdateChildInfoTabItem_Selected(object sender, RoutedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("MainWindow.UpdateChildInfoTabItem_Selected");

            if (_update_childInfoControl == null)
            {
                _update_childInfoControl = new ChildInformationControl();

                UpdateChildInfoGrid.Children.Add(_update_childInfoControl);
            }

            _enrollTabSelected = false;

            _updateInfoTabSelected = false;

            if (ChildInfoTabItem != null)
                ChildInfoTabItem.IsSelected = false;

            if (EnrollInfoTItem != null)
                EnrollInfoTItem.IsSelected = false;

            if (UpdateInfoTItem != null)
                UpdateInfoTItem.IsSelected = false;

            LogManager.DebugLogManager.MethodEnd("MainWindow.UpdateChildInfoTabItem_Selected");
        }

        /// <summary>
        /// This is fired when the Child Information Tab of the Enrollment Tab Controls is Selected
        /// </summary>
        /// <param name="sender">The Tab Selected</param>
        /// <param name="e">Selection Changed Event Arguments</param>
        private void EnrollInfoTItem_Selected(object sender, RoutedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("MainWindow.EnrollmentTCtrl_SelectionChanged");

            if (_enrollment_enrollControl == null)
            {
                _enrollment_enrollControl = new EnrollmentInformationControl();

                EnrollInfoGrid.Children.Add(_enrollment_enrollControl);
            }

            if (PangeaInfo.ValidateChildInfo())
            {
                if (PangeaInfo.ChildHasPossibleDuplicates())
                {
                    PossibleDuplicateWindow _possDuplicateWin = new PossibleDuplicateWindow();

                    _possDuplicateWin.ShowDialog();

                    if (PangeaInfo.PossibleDuplicate == PossibleDuplicates.Yes)
                    {
                        // Need to delete the child and start a new Enrollment
                        PangeaInfo.StartChildEnrollment();

                        LogManager.DebugLogManager.MethodEnd("MainWindow.EnrollmentTCtrl_SelectionChanged");

                        return;
                    }
                }

                PangeaInfo.SaveProgress();

                _updateInfoTabSelected = false;

                _enrollTabSelected = true;
            }

            if (ChildInfoTabItem != null)
                ChildInfoTabItem.IsSelected = false;

            if (UpdateChildInfoTabItem != null)
                UpdateChildInfoTabItem.IsSelected = false;

            if (UpdateInfoTItem != null)
                UpdateInfoTItem.IsSelected = false;

            LogManager.DebugLogManager.MethodEnd("MainWindow.EnrollmentTCtrl_SelectionChanged");
        }

        private void UpdateInfoTItem_Selected(object sender, RoutedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("MainWindow.UpdateInfoTItem_Selected");

            if (_update_enrollControl == null)
            {
                _update_enrollControl = new EnrollmentInformationControl();
                updateInfoGrid.Children.Add(_update_enrollControl);
            }

            _enrollTabSelected = false;

            _updateInfoTabSelected = true;

            if (ChildInfoTabItem != null)
                ChildInfoTabItem.IsSelected = false;

            if (EnrollInfoTItem != null)
                EnrollInfoTItem.IsSelected = false;

            if (UpdateChildInfoTabItem != null)
                UpdateChildInfoTabItem.IsSelected = false;

            LogManager.DebugLogManager.MethodEnd("MainWindow.UpdateInfoTItem_Selected");
        }

        /// <summary>
        /// The Home Tab has been selected on the Main Form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HomeTItem_Selected(object sender, RoutedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("MainWindow.MainTabControl_SelectionChanged");

            ClearTranmitMessages();

            if (_enrollment_childInfoControl != null)
            {
                ChildInfoGrid.Children.Remove(_enrollment_childInfoControl);

                _enrollment_childInfoControl = null;
            }

            if (_update_childInfoControl != null)
            {
                UpdateChildInfoGrid.Children.Remove(_update_childInfoControl);
                _update_childInfoControl = null;
            }

            if (_enrollment_enrollControl != null)
            {
                EnrollInfoGrid.Children.Remove(_enrollment_enrollControl);
                _enrollment_enrollControl = null;
            }

            if (_update_enrollControl != null)
            {
                updateInfoGrid.Children.Remove(_update_enrollControl);
                _update_enrollControl = null;
            }

            if (_mainWinVM.ShowDataListTab == Visibility.Visible)
            {
                ClearDataListDockPanel();
            }

            if (_mainWinVM.ShowUpdateTab == Visibility.Visible)
                _mainWinVM.ShowUpdateTab = Visibility.Collapsed;

            if (_resourceAttachmentControl != null)
                _resourceAttachmentControl.Visibility = Visibility.Hidden;

            if (_actionBarControl != null)
            {
                _actionBarControl.ActionBarVM.UpdateSelected = false;

                _actionBarControl.ActionBarVM.EnrollmentSelected = false;

                _actionBarControl.ActionBarVM.HomeSelected = true;
            }

            if (
                (
                PangeaInfo.EnrollingChild 
                || 
                PangeaInfo.UpdatingChild
                ) 

                && 

                PangeaInfo.SaveRequired
                )// Bug 334
            {
                // Ask if they want to save the Child Information.
                if (MessageBox.Show("Do you wish to Save the Progress of the Current Child?", "Save Child", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    PangeaInfo.SaveProgress(false);
                }
            }

            PangeaInfo.Reset();

            if (_childDataListControl != null)
            {
                _childDataListControl.ChildDataListVM.PropertyChanged -= _actionBarControl.ActionBarVM.ChildDataListControl_PropertyChanged;

                _childDataListControl.ChildDataListVM.PropertyChanged -= _mainWinVM.DataList_PropertyChagned;

                _mainWinVM.PropertyChanged -= _childDataListControl.ChildDataListVM.MainWinVM_PropertyChanged;

                HomeDockPanel.Children.Remove(_childDataListControl);

                _childDataListControl = null;
            }

            if (_homeScreenControl != null)
                _homeScreenControl.Refresh();

            _showSelectDefaultLocationInfo = true;

            _enrollTabSelected = false;

            _updateInfoTabSelected = false;

            if (ChildInfoTabItem != null)
                ChildInfoTabItem.IsSelected = false;

            if (EnrollInfoTItem != null)
                EnrollInfoTItem.IsSelected = false;

            if (UpdateChildInfoTabItem != null)
                UpdateChildInfoTabItem.IsSelected = false;

            if (UpdateInfoTItem != null)
                UpdateInfoTItem.IsSelected = false;

            LogManager.DebugLogManager.MethodEnd("MainWindow.MainTabControl_SelectionChanged");
        }



        /// <summary>
        /// The Enrollment Tab has been selected on the Main Form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EnrollmentTabItem_Selected(object sender, RoutedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("MainWindow.EnrollmentTabItem_Selected");

            ClearTranmitMessages();

            if (_mainWinVM.ShowDataListTab == Visibility.Visible)
            {
                ClearDataListDockPanel();
            }

            if (_mainWinVM.ShowUpdateTab == Visibility.Visible)
                _mainWinVM.ShowUpdateTab = Visibility.Collapsed;

            if (_actionBarControl != null)
            {
                _actionBarControl.ActionBarVM.HomeSelected = false;

                _actionBarControl.ActionBarVM.UpdateSelected = false;

                _actionBarControl.ActionBarVM.EnrollmentSelected = true;
            }

            if (_showSelectDefaultLocationInfo)
            {
                _showSelectDefaultLocationInfo = false;

                try
                {
                    LocationSelectWindow _locationSelectWindow = new LocationSelectWindow();

                    _locationSelectWindow.ShowDialog();
                }
                catch (Exception ex)
                {
                    LogManager.ErrorLogManager.WriteError("MainWindow.EnrollmentTabItem_Selected", ex);
#if DEBUG
                    MessageBox.Show(ex.Message, "MainWindow.EnrollmentTabItem_Selected");
#endif
                }
            }

            if (!PangeaInfo.EnrollingChild)
            {
                PangeaInfo.StartChildEnrollment();
            }

            if (_resourceAttachmentControl != null)
                _resourceAttachmentControl.Visibility = Visibility.Visible;

            if (!ChildInfoTabItem.IsSelected)
                ChildInfoTabItem.IsSelected = true;

            LogManager.DebugLogManager.MethodEnd("MainWindow.EnrollmentTabItem_Selected");
        }

        private void UpdateTabItem_Selected(object sender, RoutedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("MainWindow.UpdateTabItem_Selected");

            ClearTranmitMessages();

            if (_mainWinVM.ShowDataListTab == Visibility.Visible)
            {
                ClearDataListDockPanel();
            }

            if (_actionBarControl != null)
            {
                _actionBarControl.ActionBarVM.HomeSelected = false;

                _actionBarControl.ActionBarVM.EnrollmentSelected = false;

                _actionBarControl.ActionBarVM.UpdateSelected = true;
            }

            if (_resourceAttachmentControl != null)
                _resourceAttachmentControl.Visibility = Visibility.Visible;

            if (!UpdateChildInfoTabItem.IsSelected)
                UpdateChildInfoTabItem.IsSelected = true;

            LogManager.DebugLogManager.MethodEnd("MainWindow.UpdateTabItem_Selected");
        }

        /// <summary>
        /// This is called when the Enrollment Information Button Is Pressed 
        /// or the Enroll Button is Pressed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NextBtn_Click(object sender, RoutedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("MainWindow.GINextBtn_Click");

            // BUG 323 - Fixed the Update File being saved when switching to the Update Information Screen From
            // the Child Information Screen.
            bool _validChildInfo = PangeaInfo.ValidateChildInfo();

            if (
                _validChildInfo && 
                PangeaInfo.EnrollingChild
                )
            {
                // Check to See if the Child Has any Duplicates.
                // TODO : Add Back for the Pangea Possible Duplicates
                if (PangeaInfo.ChildHasPossibleDuplicates())
                {
                    PossibleDuplicateWindow _possibleDuplicateWindow = new PossibleDuplicateWindow();

                    _possibleDuplicateWindow.ShowDialog();

                    if (PangeaInfo.PossibleDuplicate == PossibleDuplicates.Yes)
                    {
                        // Need to Delete tHe Child and Start a new Enrollment.
                        PangeaInfo.StartChildEnrollment();

                        LogManager.DebugLogManager.MethodEnd("MainWindow.GINextBtn_Click");

                        return;
                    }
                }

                PangeaInfo.SaveProgress();
            }

            if 
                (
                ChildInfoTabItem.IsSelected && 
                _validChildInfo
                )
                EnrollInfoTItem.IsSelected = true;
            else if 
                (
                EnrollInfoTItem.IsSelected && 
                _enrollTabSelected && 
                _validChildInfo
                )
            {
                bool bcheck = _mainWinVM.EnrollAChild();

                if (bcheck)
                {
                    PangeaInfo.StartChildEnrollment();

                    _enrollTabSelected = false;

                    ChildInfoTabItem.IsSelected = true;
                }
            }

            else if (UpdateChildInfoTabItem.IsSelected)
                UpdateInfoTItem.IsSelected = true;

            else if 
                (
                UpdateInfoTItem.IsSelected && 
                _updateInfoTabSelected && 
                _validChildInfo
                )
            {

                if (_mainWinVM.UpdateAChild())
                {
                    PangeaInfo.Reset();

                    _updateInfoTabSelected = false;

                    HomeTItem.IsSelected = true;
                }
            }

            LogManager.DebugLogManager.MethodEnd("MainWindow.GINextBtn_Click");
        }

        /// <summary>
        /// This is watching the Action Bar Properties Changed Event Arguments.
        /// Then we do stuff depending on what event is thrown.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ActionBarControl_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("MainWindow.ActionBarControl_PropertyChanged");

            switch (e.PropertyName)
            {
                case "StartNewEnrollment":

                    _enrollTabSelected = false;

                    _updateInfoTabSelected = false;

                    if (!EnrollmentTabItem.IsSelected)
                        EnrollmentTabItem.IsSelected = true;

                    if (!ChildInfoTabItem.IsSelected)
                        ChildInfoTabItem.IsSelected = true;

                    ClearDataListDockPanel();

                    break;
                case "UpdateDataList":

                    if (PangeaInfo.UpdatingChild && PangeaInfo.SaveRequired)
                    {
                        // Check to see if they want to save the child or not.
                        if (MessageBox.Show("Do you wish to Save Current Child?", "Save Child", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                        {
                            if (!PangeaInfo.SaveProgress())
                                return; // Something happen unable to save.
                            else
                                PangeaInfo.Reset();
                        }
                    }

                    ClearDataListDockPanel();

                    AddDataListToDataListDockPanel(false);

                    break;
                case "EnrollmentDataList":
                    if (PangeaInfo.EnrollingChild && PangeaInfo.SaveRequired)
                    {
                        // Check to see if they want to save the child or not.
                        if (MessageBox.Show("Do you wish to Save Current Child?", "Save Child", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                        {
                            if (!PangeaInfo.SaveProgress())
                                return; // Something happen unable to save.
                            else
                                PangeaInfo.Reset();
                        }
                    }

                    ClearDataListDockPanel();

                    AddDataListToDataListDockPanel(true);

                    break;
                case "EditEnrollment":

                    ClearDataListDockPanel();

                    _enrollTabSelected = false;

                    _updateInfoTabSelected = false;

                    _showSelectDefaultLocationInfo = false;

                    if (!EnrollmentTabItem.IsSelected)
                        EnrollmentTabItem.IsSelected = true;

                    if (!ChildInfoTabItem.IsSelected)
                        ChildInfoTabItem.IsSelected = true;

                    break;
                case "EditUpdate":

                    ClearDataListDockPanel();

                    _mainWinVM.ShowUpdateTab = Visibility.Visible;

                    _enrollTabSelected = false;

                    _showSelectDefaultLocationInfo = true;

                    _updateInfoTabSelected = false;


                    if (!UpdateTabItem.IsSelected)
                        UpdateTabItem.IsSelected = true;

                    if (!UpdateChildInfoTabItem.IsSelected)
                        UpdateChildInfoTabItem.IsSelected = true;

                    break;
                case "DeleteEnrollment":

                    _childDataListControl.ChildDataListVM.UpdateDataListAfterDelete();

                    break;
                case "RemoveUpdateChild":

                    if (PangeaInfo.UpdatingChild)
                    {
                        PangeaInfo.Reset();

                        _updateInfoTabSelected = false;

                        HomeTItem.IsSelected = true;
                    }
                    break;
                case "UpdateCodeTables":

                    _mainWinVM.StartUpdateDBData();

                    break;

                default:

                    break;

            }

            LogManager.DebugLogManager.MethodEnd("MainWindow.ActionBarControl_PropertyChanged");
        }

        private void _mainWinVM_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("MainWindow._mainWinVM_PropertyChanged");

            switch (e.PropertyName)
            {
                case "RetrieveComplete":

                    _homeScreenControl.Refresh();

                    break;

                case "ConnSpeed":

                    InternetConnectionCtrl.DownloadSpeed = _mainWinVM.ConnSpeed;

                    break;
            }

            LogManager.DebugLogManager.MethodEnd("MainWindow._mainWinVM_PropertyChanged");
        }

        /// <summary>
        /// This is called when the Selected Tab Item changes.
        /// I am using this to make sure we don't allow the user to switch
        /// to enrollment information without the required fields filled.
        /// </summary>
        /// <param name="sender">The Enrollment Tab Control</param>
        /// <param name="e">The Events that have happen on the tab control.</param>
        private void EnrollmentTabCtrl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("MainWindow.EnrollmentTabCtrl_SelectionChanged");

            if (
                EnrollInfoTItem.IsSelected && 
                !_enrollTabSelected
                )
            {
                ChildInfoTabItem.IsSelected = true;
            }

            LogManager.DebugLogManager.MethodEnd("MainWindow.EnrollmentTabCtrl_SelectionChanged");
        }

        private void UpdateTabCtrl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("MainWindow.UpdateTabCtrl_SelectionChanged");

            if (
                UpdateInfoTItem.IsSelected && 
                !_updateInfoTabSelected
                )
                UpdateChildInfoTabItem.IsSelected = true;

            LogManager.DebugLogManager.MethodEnd("MainWindow.UpdateTabCtrl_SelectionChanged");
        }

        /// <summary>
        /// Clears the Child Data Grid control from the Home Screen
        /// area because it is no longer needed.
        /// </summary>
        private void ClearDataListDockPanel()
        {
            LogManager.DebugLogManager.MethodBegin("MainWindow.ClearHomeDockPanel");

            if (_childDataListControl != null)
            {
                _childDataListControl.ChildDataListVM.PropertyChanged -= _actionBarControl.ActionBarVM.ChildDataListControl_PropertyChanged;

                _childDataListControl.ChildDataListVM.PropertyChanged -= _mainWinVM.DataList_PropertyChagned;

                _mainWinVM.PropertyChanged -= _childDataListControl.ChildDataListVM.MainWinVM_PropertyChanged;

                DataListDockPanel.Children.Remove(_childDataListControl);
            }

            _mainWinVM.ShowDataListTab = Visibility.Collapsed;

            if (!TabContolMainGrid.RowDefinitions.Contains(PangeaDocGrid))
            {
                TabContolMainGrid.RowDefinitions.Add(PangeaDocGrid);

                TabContolMainGrid.Children.Add(ResourceAttachmentGrid);

                ResourceAttachmentGrid.Children.Add(_resourceAttachmentControl);
            }

            LogManager.DebugLogManager.MethodEnd("MainWindow.ClearHomeDockPanel");
        }

        /// <summary>
        /// Adds the Child Data Grid control from the Home Screen
        /// area because it is no longer needed.
        /// </summary>
        /// <param name="enrollment">Update Data list(true) or Enrollment Data List(false)</param>
        private void AddDataListToDataListDockPanel(bool enrollment)
        {
            LogManager.DebugLogManager.MethodBegin("MainWindow.AddDataListToHomeDockPanel");

            if (TabContolMainGrid.RowDefinitions.Contains(PangeaDocGrid))
            {
                ResourceAttachmentGrid.Children.Remove(_resourceAttachmentControl);

                TabContolMainGrid.Children.Remove(ResourceAttachmentGrid);

                TabContolMainGrid.RowDefinitions.Remove(PangeaDocGrid);
            }

            _childDataListControl = new ChildDataListControl(enrollment);

            DataListDockPanel.Children.Add(_childDataListControl);

            _childDataListControl.ChildDataListVM.PropertyChanged += _actionBarControl.ActionBarVM.ChildDataListControl_PropertyChanged;

            _childDataListControl.ChildDataListVM.PropertyChanged += _mainWinVM.DataList_PropertyChagned;

            _mainWinVM.PropertyChanged += _childDataListControl.ChildDataListVM.MainWinVM_PropertyChanged;

            _mainWinVM.ShowDataListTab = Visibility.Visible;

            DataListTabItem.IsSelected = true;

            DataListTabItem.Header = enrollment ? PangeaInfo.UILanguageDefMapping.GetTextValue("EnrollmentDataListLbl") : PangeaInfo.UILanguageDefMapping.GetTextValue("UpdateDataListLbl");

            LogManager.DebugLogManager.MethodEnd("MainWindow.AddDataListToHomeDockPanel");
        }

        /// <summary>
        /// Clears all the transmit complete and failed messages.
        /// </summary>
        private void ClearTranmitMessages()
        {
            PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.RetrieveComplete);

            PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.RetrieveFailed);

            PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.TransferComplete);

            PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.TransferFailed);
        }


        private void btn_Test_Web_Control_Click(object sender, RoutedEventArgs e)
        {

            Views.ResourceTypeSelectionWindow rtsw = new ResourceTypeSelectionWindow(true);

            rtsw.ShowDialog();

            int yy = 0;

            ;

        }

    }
}
