﻿/////////////////////////////////////////////////////////////////////////
/// Control for Collecting the Enrollment Information of the Child when 
/// Enrolling the Child.
////////////////////////////////////////////////////////////////////////

using System;
using System.Windows.Controls;

using Pangea.FieldApplication.ViewModels;
using Pangea.Utilities.Tools;

namespace Pangea.FieldApplication.Views
{
    /// <summary>
    /// Interaction logic for EnrollmentInformationControl.xaml
    /// </summary>
    public partial class EnrollmentInformationControl : UserControl
    {
        private EnrollmentInformationViewModel _enrollmentInfoVM;

        public EnrollmentInformationControl()
        {
            LogManager.DebugLogManager.MethodBegin("EnrollmentInformationControl");

            InitializeComponent();

            _enrollmentInfoVM = new EnrollmentInformationViewModel();

            ;

            /*
            if (PangeaInfo.EnrollingChild) 
            {
                MLE_StackPanel.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                MLE_StackPanel.Visibility = System.Windows.Visibility.Visible;
            }
            */

            DataContext = _enrollmentInfoVM;

            SizeChanged += EnrollmentInformationControl_SizeChanged;

            ChoresCBox.SelectionChanged += _enrollmentInfoVM.ChoresCBox_SelectionChanged;

            FavActivCBox.SelectionChanged += _enrollmentInfoVM.FavActivCBox_SelectionChanged;

            MajorLifeBtn.Click += _enrollmentInfoVM.MajorLifeBtn_Click;

            DisabilityQuestionLbl.MouseLeftButtonDown += _enrollmentInfoVM.DisabilityQuestionLbl_MouseLeftButtonDown;

            DisabilityQuestionLbl.MouseLeftButtonUp += _enrollmentInfoVM.DisabilityQuestionLbl_MouseLeftButtonUp;

            DisabilityQuestionLbl.MouseEnter += _enrollmentInfoVM.MouseEnter;

            DisabilityQuestionLbl.MouseLeave += _enrollmentInfoVM.MouseLeave;

            DisabilityHistoryLbl.MouseLeftButtonDown += _enrollmentInfoVM.DisabilityHistoryLbl_MouseLeftButtonDown;

            DisabilityHistoryLbl.MouseLeftButtonUp += _enrollmentInfoVM.DisabilityHistoryLbl_MouseLeftButtonUp;

            DisabilityHistoryLbl.MouseEnter += _enrollmentInfoVM.MouseEnter;

            DisabilityHistoryLbl.MouseLeave += _enrollmentInfoVM.MouseLeave;

            GradeQuestionLbl.MouseLeftButtonDown += _enrollmentInfoVM.GradeQuestionLbl_MouseLeftButtonDown;

            GradeQuestionLbl.MouseLeftButtonUp += _enrollmentInfoVM.GradeQuestionLbl_MouseLeftButtonUp;

            GradeQuestionLbl.MouseEnter += _enrollmentInfoVM.MouseEnter;

            GradeQuestionLbl.MouseLeave += _enrollmentInfoVM.MouseLeave;

            GradeHistoryLbl.MouseLeftButtonDown += _enrollmentInfoVM.GradeHistoryLbl_MouseLeftButtonDown;

            GradeHistoryLbl.MouseLeftButtonUp += _enrollmentInfoVM.GradeHistoryLbl_MouseLeftButtonUp;

            GradeHistoryLbl.MouseEnter += _enrollmentInfoVM.MouseEnter;

            GradeHistoryLbl.MouseLeave += _enrollmentInfoVM.MouseLeave;

            ChoresQuestionLbl.MouseLeftButtonDown += _enrollmentInfoVM.ChoresQuestionLbl_MouseLeftButtonDown;

            ChoresQuestionLbl.MouseLeftButtonUp += _enrollmentInfoVM.ChoresQuestionLbl_MouseLeftButtonUp;

            ChoresQuestionLbl.MouseEnter += _enrollmentInfoVM.MouseEnter;

            ChoresQuestionLbl.MouseLeave += _enrollmentInfoVM.MouseLeave;

            ChoresHistoryLbl.MouseLeftButtonDown += _enrollmentInfoVM.ChoresHistoryLbl_MouseLeftButtonDown;

            ChoresHistoryLbl.MouseLeftButtonUp += _enrollmentInfoVM.ChoresHistoryLbl_MouseLeftButtonUp;

            ChoresHistoryLbl.MouseEnter += _enrollmentInfoVM.MouseEnter;

            ChoresHistoryLbl.MouseLeave += _enrollmentInfoVM.MouseLeave;

            NumSiblingsQuestionLbl.MouseLeftButtonDown += _enrollmentInfoVM.NumSiblingsQuestionLbl_MouseLeftButtonDown;

            NumSiblingsQuestionLbl.MouseLeftButtonUp += _enrollmentInfoVM.NumSiblingsQuestionLbl_MouseLeftButtonUp;

            NumSiblingsQuestionLbl.MouseEnter += _enrollmentInfoVM.MouseEnter;

            NumSiblingsQuestionLbl.MouseLeave += _enrollmentInfoVM.MouseLeave;

            NumSiblingsHistoryLbl.MouseLeftButtonDown += _enrollmentInfoVM.NumSiblingsHistoryLbl_MouseLeftButtonDown;

            NumSiblingsHistoryLbl.MouseLeftButtonUp += _enrollmentInfoVM.NumSiblingsHistoryLbl_MouseLeftButtonUp;

            NumSiblingsHistoryLbl.MouseEnter += _enrollmentInfoVM.MouseEnter;

            NumSiblingsHistoryLbl.MouseLeave += _enrollmentInfoVM.MouseLeave;

            PersonalityQuestionLbl.MouseLeftButtonDown += _enrollmentInfoVM.PersonalityQuestionLbl_MouseLeftButtonDown;

            PersonalityQuestionLbl.MouseLeftButtonUp += _enrollmentInfoVM.PersonalityQuestionLbl_MouseLeftButtonUp;

            PersonalityQuestionLbl.MouseEnter += _enrollmentInfoVM.MouseEnter;

            PersonalityQuestionLbl.MouseLeave += _enrollmentInfoVM.MouseLeave;

            PersonalityHistoryLbl.MouseLeftButtonDown += _enrollmentInfoVM.PersonalityHistoryLbl_MouseLeftButtonDown;

            PersonalityHistoryLbl.MouseLeftButtonUp += _enrollmentInfoVM.PersonalityHistoryLbl_MouseLeftButtonUp;

            PersonalityHistoryLbl.MouseEnter += _enrollmentInfoVM.MouseEnter;

            PersonalityHistoryLbl.MouseLeave += _enrollmentInfoVM.MouseLeave;

            HealthQuestionLbl.MouseLeftButtonDown += _enrollmentInfoVM.HealthQuestionLbl_MouseLeftButtonDown;

            HealthQuestionLbl.MouseLeftButtonUp += _enrollmentInfoVM.HealthQuestionLbl_MouseLeftButtonUp;

            HealthQuestionLbl.MouseEnter += _enrollmentInfoVM.MouseEnter;

            HealthQuestionLbl.MouseLeave += _enrollmentInfoVM.MouseLeave;

            HealthHistoryLbl.MouseLeftButtonDown += _enrollmentInfoVM.HealthHistoryLbl_MouseLeftButtonDown;

            HealthHistoryLbl.MouseLeftButtonUp += _enrollmentInfoVM.HealthHistoryLbl_MouseLeftButtonUp;

            HealthHistoryLbl.MouseEnter += _enrollmentInfoVM.MouseEnter;

            HealthHistoryLbl.MouseLeave += _enrollmentInfoVM.MouseLeave;

            FavLearnQuestionLbl.MouseLeftButtonDown += _enrollmentInfoVM.FavLearnQuestionLbl_MouseLeftButtonDown;

            FavLearnQuestionLbl.MouseLeftButtonUp += _enrollmentInfoVM.FavLearnQuestionLbl_MouseLeftButtonUp;

            FavLearnQuestionLbl.MouseEnter += _enrollmentInfoVM.MouseEnter;

            FavLearnQuestionLbl.MouseLeave += _enrollmentInfoVM.MouseLeave;

            FavLearnHistoryLbl.MouseLeftButtonDown += _enrollmentInfoVM.FavLearnHistoryLbl_MouseLeftButtonDown;

            FavLearnHistoryLbl.MouseLeftButtonUp += _enrollmentInfoVM.FavLearnHistoryLbl_MouseLeftButtonUp;

            FavLearnHistoryLbl.MouseEnter += _enrollmentInfoVM.MouseEnter;

            FavLearnHistoryLbl.MouseLeave += _enrollmentInfoVM.MouseLeave;

            LivesWithQuestionLbl.MouseLeftButtonDown += _enrollmentInfoVM.LivesWithQuestionLbl_MouseLeftButtonDown;

            LivesWithQuestionLbl.MouseLeftButtonUp += _enrollmentInfoVM.LivesWithQuestionLbl_MouseLeftButtonUp;

            LivesWithQuestionLbl.MouseEnter += _enrollmentInfoVM.MouseEnter;

            LivesWithQuestionLbl.MouseLeave += _enrollmentInfoVM.MouseLeave;

            LivesWithHistoryLbl.MouseLeftButtonDown += _enrollmentInfoVM.LivesWithHistoryLbl_MouseLeftButtonDown;

            LivesWithHistoryLbl.MouseLeftButtonUp += _enrollmentInfoVM.LivesWithHistoryLbl_MouseLeftButtonUp;

            LivesWithHistoryLbl.MouseEnter += _enrollmentInfoVM.MouseEnter;

            LivesWithHistoryLbl.MouseLeave += _enrollmentInfoVM.MouseLeave;

            FavActivityQuestionLbl.MouseLeftButtonDown += _enrollmentInfoVM.FavActivityQuestionLbl_MouseLeftButtonDown;

            FavActivityQuestionLbl.MouseLeftButtonUp += _enrollmentInfoVM.FavActivityQuestionLbl_MouseLeftButtonUp;

            FavActivityQuestionLbl.MouseEnter += _enrollmentInfoVM.MouseEnter;

            FavActivityQuestionLbl.MouseLeave += _enrollmentInfoVM.MouseLeave;

            FavActivityHistoryLbl.MouseLeftButtonDown += _enrollmentInfoVM.FavActivityHistoryLbl_MouseLeftButtonDown;

            FavActivityHistoryLbl.MouseLeftButtonUp += _enrollmentInfoVM.FavActivityHistoryLbl_MouseLeftButtonUp;

            FavActivityHistoryLbl.MouseEnter += _enrollmentInfoVM.MouseEnter;

            FavActivityHistoryLbl.MouseLeave += _enrollmentInfoVM.MouseLeave;

            LogManager.DebugLogManager.MethodEnd("EnrollmentInformationControl");
        }

        private void EnrollmentInformationControl_SizeChanged(object sender, System.Windows.SizeChangedEventArgs e)
        {
            double _horizSpacing = (e.NewSize.Width - EnrollInfoLbl.ActualWidth - FNameLbl.ActualWidth - LNameLbl.ActualWidth) / 6.5;

            FNameLbl.Margin = new System.Windows.Thickness(_horizSpacing, 0, 0, 0);

            LNameLbl.Margin = new System.Windows.Thickness(_horizSpacing, 0, 0, 0);

            ChildNumberLbl.Margin = new System.Windows.Thickness(_horizSpacing, 0, 0, 0);

            _horizSpacing = (e.NewSize.Width - EnrollInfoLbl.ActualWidth - ChildNumberLbl.ActualWidth - CreatedByUserLbl.ActualWidth - CreatedByDateLbl.ActualWidth) / 6;

            CreatedByUserLbl.Margin = new System.Windows.Thickness(_horizSpacing, 0, 0, 0);

            CreatedByDateLbl.Margin = new System.Windows.Thickness(_horizSpacing, 0, 0, 0);

            double _verticalSpacing = (EnrollInfoRow.ActualHeight - DisabilityLbl.ActualHeight - DisabilityCBox.ActualHeight - GradeLbl.ActualHeight - SchoolGradeCBox.ActualHeight - ChoresLbl.ActualHeight - ChoresCBox.ActualHeight - ChoresListView.ActualHeight) / 3.5;
            
            double _controlWidth = ColumnOne.ActualWidth - 15;

            DisabilityLbl.Width = _controlWidth -
                (_enrollmentInfoVM.DisabilityAstrikVis == System.Windows.Visibility.Visible ? DisabilityAstrik.ActualWidth : 0.0) -
                (_enrollmentInfoVM.DisabilityQuestionVis == System.Windows.Visibility.Visible ? DisabilityQuestionLbl.ActualWidth : 0.0) -
                (_enrollmentInfoVM.DisabilityHistoryVis == System.Windows.Visibility.Visible ? DisabilityHistoryLbl.Width : 0.0);
            DisabilityCBox.Width = _controlWidth;

            GradeLbl.Width = _controlWidth -
                (_enrollmentInfoVM.SchoolGradeAstrikVis == System.Windows.Visibility.Visible ? GradeAstrik.ActualWidth : 0.0) -
                (_enrollmentInfoVM.SchoolGradeQuestionVis == System.Windows.Visibility.Visible ? GradeQuestionLbl.ActualWidth : 0.0) -
                (_enrollmentInfoVM.SchoolGradeHistoryVis == System.Windows.Visibility.Visible ? GradeHistoryLbl.Width : 0.0);
            SchoolGradeCBox.Width = _controlWidth;

            ChoresLbl.Width = _controlWidth -
                (_enrollmentInfoVM.ChoresAstrikVis == System.Windows.Visibility.Visible ? ChoresAstrik.ActualWidth : 0.0) -
                (_enrollmentInfoVM.ChoresQuestionVis == System.Windows.Visibility.Visible ? ChoresQuestionLbl.ActualWidth : 0.0) -
                (_enrollmentInfoVM.ChoresHistoryVis == System.Windows.Visibility.Visible ? ChoresHistoryLbl.Width : 0.0 );
            ChoresCBox.Width = _controlWidth;
            ChoresListView.Width = _controlWidth;

            DisabilityStackPanel.Margin = new System.Windows.Thickness(5, _verticalSpacing, 0, 0);

            GradeStackPanel.Margin = new System.Windows.Thickness(5, _verticalSpacing, 0, 0);

            ChoresStackPanel.Margin = new System.Windows.Thickness(5, _verticalSpacing, 0, 0);

            _controlWidth = ColumnTwo.ActualWidth - 15;

            NumSiblingsLbl.Width = _controlWidth -
                (_enrollmentInfoVM.NumberSiblingsAstrikVis == System.Windows.Visibility.Visible ? NumSiblingsAstrik.ActualWidth : 0.0) -
                (_enrollmentInfoVM.NumberSiblingsQuestionVis == System.Windows.Visibility.Visible ? NumSiblingsQuestionLbl.ActualWidth : 0.0) -
                (_enrollmentInfoVM.NumberSiblingsHistoryVis == System.Windows.Visibility.Visible ? NumSiblingsHistoryLbl.Width : 0.0);

            NumBroLbl.Width = (_controlWidth / 2.0) - NumBroNUD.ActualWidth;

            NumSisLbl.Width = (_controlWidth / 2.0) - NumSisNUD.ActualWidth;

            PersonalityLbl.Width = _controlWidth -
                (_enrollmentInfoVM.PersonalityAstrikVis == System.Windows.Visibility.Visible ? PersonalityAstrik.ActualWidth : 0.0) -
                (_enrollmentInfoVM.PersonalityQuestionVis == System.Windows.Visibility.Visible ? PersonalityQuestionLbl.ActualWidth : 0.0) -
                (_enrollmentInfoVM.PersonalityHistoryVis == System.Windows.Visibility.Visible ? PersonalityHistoryLbl.Width : 0.0);

            PersonalityCBox.Width = _controlWidth;

            FavActivityLbl.Width = _controlWidth -
                (_enrollmentInfoVM.FavActivAstrikVis == System.Windows.Visibility.Visible ? FavActivityAstrik.ActualWidth : 0.0) -
                (_enrollmentInfoVM.FavActivQuestionVis == System.Windows.Visibility.Visible ? FavActivityQuestionLbl.ActualWidth : 0.0) -
                (_enrollmentInfoVM.FavActivHistoryVis == System.Windows.Visibility.Visible ? FavActivityHistoryLbl.Width : 0.0);

            FavActivCBox.Width = _controlWidth;

            FavActivListView.Width = _controlWidth;

            NumSiblingsStackPanel.Margin = new System.Windows.Thickness(5, _verticalSpacing, 0, 0);

            PeronalityStackPanel.Margin = new System.Windows.Thickness(5, _verticalSpacing, 0, 0);

            FavActivityStackPanel.Margin = new System.Windows.Thickness(5, _verticalSpacing, 0, 0);

            _controlWidth = ColumnThree.ActualWidth - 15;

            FavLearnLbl.Width = _controlWidth -
                (_enrollmentInfoVM.FavLearnAstrikVis == System.Windows.Visibility.Visible ? FavLearnAstrik.ActualWidth : 0.0) -
                (_enrollmentInfoVM.FavLearnQuestionVis == System.Windows.Visibility.Visible ? FavLearnQuestionLbl.ActualWidth : 0.0) -
                (_enrollmentInfoVM.FavLearnHistoryVis == System.Windows.Visibility.Visible ? FavLearnHistoryLbl.Width : 0.0);
            FavLearnCBox.Width = _controlWidth;

            LivesWithLbl.Width = _controlWidth -
                (_enrollmentInfoVM.LivesWithAstrikVis == System.Windows.Visibility.Visible ? LivesWithAstrik.ActualWidth : 0.0) -
                (_enrollmentInfoVM.LivesWithQuestionVis == System.Windows.Visibility.Visible ? LivesWithQuestionLbl.ActualWidth : 0.0) -
                (_enrollmentInfoVM.LivesWithHistoryVis == System.Windows.Visibility.Visible ? LivesWithQuestionLbl.Width : 0.0);
            LivesWithCBox.Width = _controlWidth;

            ;

            /*
            lbl_Major_Life_Event.Width = _controlWidth -
                (_enrollmentInfoVM.LivesWithAstrikVis == System.Windows.Visibility.Visible ? LivesWithAstrik.ActualWidth : 0.0) -
                (_enrollmentInfoVM.LivesWithQuestionVis == System.Windows.Visibility.Visible ? LivesWithQuestionLbl.ActualWidth : 0.0) -
                (_enrollmentInfoVM.LivesWithHistoryVis == System.Windows.Visibility.Visible ? LivesWithQuestionLbl.Width : 0.0);

            lbl_Major_Life_Event.Width = _controlWidth;
            */

            ;

            /*
            HealthLbl.Width = _controlWidth -
                (_enrollmentInfoVM.HealthAstrikVis == System.Windows.Visibility.Visible ? HealthAstrik.ActualWidth : 0.0) -
                (_enrollmentInfoVM.HealthQuestionkVis == System.Windows.Visibility.Visible ? HealthQuestionLbl.ActualWidth : 0.0) -
                (_enrollmentInfoVM.HealthHistoryVis == System.Windows.Visibility.Visible ? HealthHistoryLbl.Width : 0.0);
            HealthCBox.Width = _controlWidth;
            */

            MajorLifeLbl.Width = _controlWidth -
                (_enrollmentInfoVM.MajorLifeAstrikVis == System.Windows.Visibility.Visible ? MajorLifeAstrik.ActualWidth : 0.0) -
                (_enrollmentInfoVM.MajorLifeQuestionkVis == System.Windows.Visibility.Visible ? MajorLifeQuestionLbl.ActualWidth : 0.0);
            
            MajorLifeBtn.Width = _controlWidth;

            FavLearningStackPanel.Margin = new System.Windows.Thickness(5, _verticalSpacing, 0, 0);

            LivesWithStackPanel.Margin = new System.Windows.Thickness(5, _verticalSpacing, 0, 0);

            HealthStackPanel.Margin = new System.Windows.Thickness(5, _verticalSpacing, 0, 0);

            // MajorLifeEventStackPanel.Margin = new System.Windows.Thickness(5, _verticalSpacing, 0, 0);
            
            // MLE_StackPanel.Margin = new System.Windows.Thickness(5, _verticalSpacing, 0, 0);


        }

        public EnrollmentInformationViewModel ViewModel
        {
            get 
            { 
                return _enrollmentInfoVM; 
            }
        }
    }
}
