﻿//////////////////////////////////////////////////////////////////////
/// This is the Window for selecting the Default Country and Location
/////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using Pangea.FieldApplication.ViewModels;
using Pangea.Utilities.Tools;
using System.Linq;
using System.IO;
using Newtonsoft.Json;
using Pangea.Data.Collections;

namespace Pangea.FieldApplication.Views
{
    /// <summary>
    /// Interaction logic for LocationSelect.xaml
    /// </summary>
    public partial class LocationSelectWindow : Window
    {
        private LocationSelectViewModel _locSelVM;

        public LocationSelectWindow()
        {

            LogManager.DebugLogManager.MethodBegin("LocationSelectWindow");

            InitializeComponent();


            List<int> the_Approved_Country_IDs = new List<int>();

            string Pangea_User_ID = PangeaInfo.User.UserID.ToString();

            the_Approved_Country_IDs = PangeaFieldApplication.Utils.Utilities.Get_Approved_Country_IDs_For_Current_User(Pangea_User_ID);

            _locSelVM = new LocationSelectViewModel(the_Approved_Country_IDs);

            Handle_User_Defaults();

            DataContext = _locSelVM;

            Loaded += _locSelVM.LocSelectWindow_Loaded;

            LogManager.DebugLogManager.MethodEnd("LocationSelectWindow");

        }


        private void Handle_User_Defaults()
        {
            string DefaultLocationData_file_path = Environment.CurrentDirectory + "\\" + Centraler.Settingz.Default_Location_JSON_FileName;

            PangeaData.Model.DefaultLocationChoice the_Data_Export = new PangeaData.Model.DefaultLocationChoice();

            bool File_Exists = File.Exists(DefaultLocationData_file_path);

            if (File_Exists)
            {

                using (StreamReader file = File.OpenText(DefaultLocationData_file_path))
                {

                    string stemp = file.ReadToEnd();

                    the_Data_Export = Newtonsoft.Json.JsonConvert.DeserializeObject<PangeaData.Model.DefaultLocationChoice>(stemp);

                }

            }

            if (the_Data_Export.Country_ID > 0)
            {
                PangeaInfo.DefaultCountry = _locSelVM.Countries.Where(xx => xx.CodeID == the_Data_Export.Country_ID).FirstOrDefault();
            }

            if (the_Data_Export.Location_ID > 0)
            {
                PangeaInfo.DefaultLocation = PangeaCollections.LocationCollection.Where(loc => loc.CodeID == the_Data_Export.Location_ID).FirstOrDefault();
            }
        }

        private void OKBtn_Click(object sender, RoutedEventArgs e)
        {
            Set_User_Default_Details();
        }


        private void Set_User_Default_Details()
        {

            var the_country = CountryCBox.SelectedValue;

            var the_Location_Name = LocationNameCBox.SelectedValue;

            PangeaData.Model.DefaultLocationChoice the_Data_Export = new PangeaData.Model.DefaultLocationChoice();

            if (the_country != null)
            {
                the_Data_Export.Country_ID = ((Pangea.Data.Model.CodeTables.Country)the_country).CodeID;

                PangeaInfo.DefaultCountry = (Pangea.Data.Model.CodeTables.Country)the_country;
            }
            else
            {
                the_Data_Export.Country_ID = -1;
            }

            if (the_Location_Name != null)
            {
                the_Data_Export.Location_ID = ((Pangea.Data.Model.CodeTables.Location)the_Location_Name).CodeID;

                PangeaInfo.DefaultLocation = (Pangea.Data.Model.CodeTables.Location)the_Location_Name;
            }
            else
            {
                the_Data_Export.Location_ID = -1;
            }

            string DefaultLocationData_file_path = Environment.CurrentDirectory + "\\" + Centraler.Settingz.Default_Location_JSON_FileName;

            using (StreamWriter file = File.CreateText(DefaultLocationData_file_path))
            {
                JsonSerializer serializer = new JsonSerializer();

                serializer.Serialize(file, the_Data_Export);
            }

        }

    }


}
