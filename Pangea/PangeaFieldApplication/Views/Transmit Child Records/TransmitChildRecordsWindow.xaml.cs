﻿using Pangea.FieldApplication.ViewModels;
using System.Windows;

namespace Pangea.FieldApplication.Views
{
    /// <summary>
    /// Interaction logic for TransmitChildRecordsWindow.xaml
    /// </summary>
    public partial class TransmitChildRecordsWindow : Window
    {
        private TransmitChildRecordsViewModel _transmitChildRecordsVM;

        public TransmitChildRecordsWindow(bool? enrollment = null)
        {
            InitializeComponent();

            _transmitChildRecordsVM = new TransmitChildRecordsViewModel(enrollment);

            DataContext = _transmitChildRecordsVM;

            ChildDataGrid.SelectionChanged += _transmitChildRecordsVM.ChildDataGrid_SelectionChanged;

            _transmitChildRecordsVM.PropertyChanged += _transmitChildRecordsVM_PropertyChanged;
        }

        private void _transmitChildRecordsVM_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "ResetSelected":

                    ChildDataGrid.SelectedItems.Clear();

                    ChildDataGrid.SelectedItem = null;

                    ChildDataGrid.SelectedIndex = -1;

                    break;

                default:
                    break;
            }
        }

        private void TransmitButton_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
