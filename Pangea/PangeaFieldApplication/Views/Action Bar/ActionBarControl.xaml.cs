﻿using Pangea.FieldApplication.ViewModels;
using System.Windows;
using System.Windows.Controls;

namespace Pangea.FieldApplication.Views
{
    /// <summary>
    /// Interaction logic for ActionBarControl.xaml
    /// </summary>
    public partial class ActionBarControl : UserControl
    {
        private ActionBarViewModel _actionBarVM;


        public ActionBarControl()
        {
            InitializeComponent();

            _actionBarVM = new ActionBarViewModel();

            DataContext = _actionBarVM;

            NewEnrollmentTextBlock.MouseLeftButtonDown += _actionBarVM.NewEnrollmentTextBlock_MouseLeftButtonDown;

            NewEnrollmentTextBlock.MouseLeftButtonUp += _actionBarVM.NewEnrollmentTextBlock_MouseLeftButtonUp;

            EditEnrollmentTextBlock.MouseLeftButtonDown += _actionBarVM.EditEnrollmentTextBlock_MouseLeftButtonDown;

            EditEnrollmentTextBlock.MouseLeftButtonUp += _actionBarVM.EditEnrollmentTextBlock_MouseLeftButtonUp;

            DeleteEnrollmentTextBlock.MouseLeftButtonDown += _actionBarVM.DeleteEnrollmentTextBlock_MouseLeftButtonDown;

            DeleteEnrollmentTextBlock.MouseLeftButtonUp += _actionBarVM.DeleteEnrollmentTextBlock_MouseLeftButtonUp;

            EnrollmentDataListTextBlock.MouseLeftButtonDown += _actionBarVM.EnrollmentDataListTextBlock_MouseLeftButtonDown;

            EnrollmentDataListTextBlock.MouseLeftButtonUp += _actionBarVM.EnrollmentDataListTextBlock_MouseLeftButtonUp;

            UpdateDataListTextBlock.MouseLeftButtonDown += _actionBarVM.UpdateDataListTextBlock_MouseLeftButtonDown;

            UpdateDataListTextBlock.MouseLeftButtonUp += _actionBarVM.UpdateDataListTextBlock_MouseLeftButtonUp;

            RemoveUpdateChildTextBlock.MouseLeftButtonDown += _actionBarVM.RemoveUpdateChildTextBlock_MouseLeftButtonDown;

            RemoveUpdateChildTextBlock.MouseLeftButtonUp += _actionBarVM.RemoveUpdateChildTextBlock_MouseLeftButtonUp;

            UpdateCodeTablesTextBlock.MouseLeftButtonDown += _actionBarVM.UpdateCodeTablesTextBlock_MouseLeftButtonDown;

            UpdateCodeTablesTextBlock.MouseLeftButtonUp += _actionBarVM.UpdateCodeTablesTextBlock_MouseLeftButtonUp;
        }


        public ActionBarViewModel ActionBarVM
        {
            get 
            { 
                return _actionBarVM; 
            }
        }

    }
}
