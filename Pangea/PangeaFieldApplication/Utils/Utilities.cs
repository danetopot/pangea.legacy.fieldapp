﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Newtonsoft.Json;
using PangeaData.Model.DataExport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PangeaFieldApplication.Utils
{


    public class UserDetails
    {

        //ID="1001", Name="ABCD", City ="City1", Country="USA"}

        public string ID { get; set; }

        public string Name { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

    }


    public static class Utilities
    {

        public static void Export_Spreadsheet(GridData_Details spreadsheet_Data)
        {

            string the_file_name;

            SaveFileDialog saveFileDialog = new SaveFileDialog();

            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            saveFileDialog.Filter = "Excel file (*.xlsx)|*.xlsx";

            DialogResult dr = saveFileDialog.ShowDialog(); 

            ;

            the_file_name = saveFileDialog.FileName;  

            if(the_file_name.Length == 0)
            {
                return;
            }

            ;

            using
                (
                SpreadsheetDocument document = SpreadsheetDocument.Create
                (
                    the_file_name,
                    SpreadsheetDocumentType.Workbook
                    )
                )
            {
                WorkbookPart workbookPart = document.AddWorkbookPart();

                workbookPart.Workbook = new Workbook();

                WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();

                var sheetData = new SheetData();

                worksheetPart.Worksheet = new Worksheet(sheetData);

                Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());

                Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Sheet1" };

                sheets.Append(sheet);

                Row headerRow = new Row();

                List<String> columns = new List<string>();

                foreach (string ColumnName in spreadsheet_Data.Grid_Export_Column_Headers)
                {
                    columns.Add(ColumnName);

                    Cell cell = new Cell();

                    cell.DataType = CellValues.String;

                    cell.CellValue = new CellValue(ColumnName);

                    headerRow.AppendChild(cell);
                }

                sheetData.AppendChild(headerRow);

                foreach (gridData dsrow in spreadsheet_Data.GridData)
                {
                    Row newRow = new Row();
                                        
                    Add_Cell_Within_Row(dsrow.Location_Name, newRow);

                    Add_Cell_Within_Row(dsrow.Location_Code, newRow);

                    Add_Cell_Within_Row(dsrow.Child_Number, newRow);

                    Add_Cell_Within_Row(dsrow.First_Name, newRow);

                    Add_Cell_Within_Row(dsrow.Middle_Name, newRow);

                    Add_Cell_Within_Row(dsrow.Last_Name, newRow);

                    Add_Cell_Within_Row(dsrow.Other_Name, newRow);

                    Add_Cell_Within_Row(dsrow.Date_Of_Birth, newRow);

                    Add_Cell_Within_Row(dsrow.Sex, newRow);

                    sheetData.AppendChild(newRow);

                }

                workbookPart.Workbook.Save();

                ;

            }

            Row Add_Cell_Within_Row
                (
                string the_CellValue, 
                Row theRow
                )
            {

                Cell cell = new Cell();

                cell.DataType = CellValues.String;

                cell.CellValue = new CellValue(the_CellValue);

                theRow.AppendChild(cell);

                return theRow;

            }
        }


        public static List<int> Get_Approved_Country_IDs_For_Current_User(string Pangea_User_ID)
        {

            Pangea.API.FieldServices.BasicHttpsBinding_IFieldServices _fieldServices = new Pangea.API.FieldServices.BasicHttpsBinding_IFieldServices();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var Country_Records = _fieldServices.GetCountryRecords(Pangea_User_ID);

            List<int> the_Approved_Country_IDs = Country_Records
                .ToList()
                .Select(xx => xx.Country_ID)
                .Distinct()
                .ToList();

            return the_Approved_Country_IDs;
        }


    }

}
