﻿using Pangea.Data.Collections;
using Pangea.Data.Model.CodeTables;
using Pangea.Utilities.Base.Commands;
using System;
using System.Windows;
using System.Windows.Input;

namespace Pangea.FieldApplication.ViewModels
{
    public class RemoveUpdateChildViewModel : PangeaBaseViewModel
    {
        public RemoveUpdateChildViewModel()
        { }

        public ChildRemoveReasonCollection RemoveReasonCol
        {
            get 
            { 
                return PangeaCollections.ChildRemoveReasonCollection; 
            }
        }

        private ChildRemoveReason _selRemoveReason;
        public ChildRemoveReason SelRemoveReason
        {
            get
            {
                if (
                    PangeaInfo.Child.Remove_Reason == null && 
                    PangeaInfo.Child.Remove_Reason != _selRemoveReason
                    )
                    return _selRemoveReason;

                return PangeaInfo.Child.Remove_Reason;
            }
            set
            {
                if (_selRemoveReason != value)
                {
                    SendPropertyChanging();

                    _selRemoveReason = value;

                    SendPropertyChanged("SelRemoveReason");
                }
            }
        }

        public String ChildName
        {
            get 
            { 
                return String.Format
                    (
                    "{0} {1} {2}({3})", 
                    PangeaInfo.Child.FirstName, 
                    PangeaInfo.Child.MiddleName, 
                    PangeaInfo.Child.LastName, 
                    PangeaInfo.Child.NickName
                    ); 
            }

        }

        public String ChildNumber
        {
            get 
            { 
                return PangeaInfo.Child.ChildNumber.ToString(); 
            }
        }

        /// <summary>
        /// Command used to Close the Possible Duplicate Windows when the Click the Yes Button.
        /// </summary>
        private ICommand _closeWindowYesCommand;
        public ICommand CloseWindowYesCommand
        {
            get 
            { 
                return _closeWindowYesCommand ?? (_closeWindowYesCommand = new RelayCommand(CloseWindowYes)); 
            }
        }

        /// <summary>
        /// Called when the Yes Button is clicked, will perform everything that needs to happen when 
        /// the Yes button is clicked.
        /// </summary>
        /// <param name="_window">The Possible Duplicates Window</param>
        private void CloseWindowYes(object _window)
        {
            Window curWindow = _window as Window;

            if 
                (
                curWindow != null && 
                SelRemoveReason != null
                )
            {

                PangeaInfo.Child.Remove_Reason = SelRemoveReason;

                curWindow.DialogResult = true;

                curWindow.Close();
            }
        }

        /// <summary>
        /// Command used to Close the Possible Duplicate Windows when the Click the No Button.
        /// </summary>
        private ICommand _closeWindowNoCommand;
        public ICommand CloseWindowNoCommand
        {
            get 
            { 
                return _closeWindowNoCommand 
                    ?? 
                    (_closeWindowNoCommand = new RelayCommand(CloseWindowNo)); 
            }
        }

        /// <summary>
        /// Called when the No Button is clicked, will perform everything that needs to happen when 
        /// the No button is clicked.
        /// </summary>
        /// <param name="_window">The Possible Duplicates Window</param>
        private void CloseWindowNo(object _window)
        {
            Window curWindow = _window as Window;

            if (curWindow != null)
            {
                curWindow.DialogResult = false;

                curWindow.Close();
            }
        }
    }
}
