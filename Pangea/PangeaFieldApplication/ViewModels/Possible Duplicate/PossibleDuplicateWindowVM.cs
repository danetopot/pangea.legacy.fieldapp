﻿using Pangea.Data.Model;
using Pangea.Utilities.Base.Commands;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace Pangea.FieldApplication.ViewModels
{
    public class PossibleDuplicateWindowVM
    {
        public PossibleDuplicateWindowVM()
        {
        }

        /// <summary>
        /// Collection of Possible Duplicates
        /// </summary>
        private ObservableCollection<Child> _possibleDuplicatesList;

        public ObservableCollection<Child> PossibleDuplicatesList
        {
            get
            {
                if (_possibleDuplicatesList == null)
                    _possibleDuplicatesList = PangeaInfo.Child.PossibleDuplicates;

                return _possibleDuplicatesList;
            }
        }

        /// <summary>
        /// Command used to Close the Possible Duplicate Windows when the Click the Yes Button.
        /// </summary>
        private ICommand _closeWindowYesCommand;

        public ICommand CloseWindowYesCommand
        {
            get 
            {
                ICommand ireturn = _closeWindowYesCommand ?? (_closeWindowYesCommand = new RelayCommand(CloseWindowYes));

                return ireturn;
            }
        }

        /// <summary>
        /// Called when the Yes Button is clicked, will perform everything that needs to happen when 
        /// the Yes button is clicked.
        /// </summary>
        /// <param name="_window">The Possible Duplicates Window</param>
        private void CloseWindowYes(object _window)
        {
            // This child is a duplicate, delete it from db if it has been added and start a new child enrollment.
            // TODO : Make sure we give this a warning that the current child information is going to be deleted.  So we want to make sure they want to do this.
            if (MessageBox.Show("This will Delete the current Child.  Do you wish to continue?", "Duplicate Child", MessageBoxButton.YesNo) == MessageBoxResult.No)
                return;

            Window curWindow = _window as Window;

            if (curWindow != null)
            {
                PangeaInfo.PossibleDuplicate = PossibleDuplicates.Yes;

                curWindow.DialogResult = true;

                curWindow.Close();
            }
        }

        /// <summary>
        /// Command used to Close the Possible Duplicate Windows when the Click the No Button.
        /// </summary>
        private ICommand _closeWindowNoCommand;

        public ICommand CloseWindowNoCommand
        {
            get 
            { 
                ICommand icmd = _closeWindowNoCommand ?? (_closeWindowNoCommand = new RelayCommand(CloseWindowNo));

                return icmd;
            }
        }

        /// <summary>
        /// Called when the No Button is clicked, will perform everything that needs to happen when 
        /// the No button is clicked.
        /// </summary>
        /// <param name="_window">The Possible Duplicates Window</param>
        private void CloseWindowNo(object _window)
        {
            // It is not a Duplicate and we want to associate the possible duplicates with this child.
            Window curWindow = _window as Window;

            foreach(Child _child in PangeaInfo.Child.PossibleDuplicates)
            {
                PangeaInfo.Child.NonDuplicates.Add(new ChildDuplicate(_child));
            }

            if (curWindow != null)
            {
                PangeaInfo.PossibleDuplicate = PossibleDuplicates.No;

                curWindow.DialogResult = true;

                curWindow.Close();
            }
        }

        /// <summary>
        /// Command used to Close the Possible Duplicate Windows when the Click the Continue Button.
        /// </summary>
        private ICommand _closeWindowContinueCommand;

        public ICommand CloseWindowContinueCommand
        {
            get 
            {
                ICommand ireturn = _closeWindowContinueCommand ?? (_closeWindowContinueCommand = new RelayCommand(CloseWindowContinue)); 
                
                ;

                return ireturn;
            }
        }

        /// <summary>
        /// Called when the Continue Button is clicked, will perform everything that needs to happen when 
        /// the Continue button is clicked.
        /// </summary>
        /// <param name="_window">The Possible Duplicates Window</param>
        private void CloseWindowContinue(object _window)
        {
            // Not sure if duplicate, just continue to pop up duplicate boxes as it is saved.
            Window curWindow = _window as Window;

            if (curWindow != null)
            {
                PangeaInfo.PossibleDuplicate = PossibleDuplicates.Continue;

                curWindow.DialogResult = true;

                curWindow.Close();
            }
    
        }

    }

}
