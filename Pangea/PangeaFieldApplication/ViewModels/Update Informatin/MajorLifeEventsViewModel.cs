﻿using Pangea.Utilities.Base.Commands;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Pangea.FieldApplication.ViewModels
{
    public class MajorLifeEventsViewModel : PangeaBaseViewModel
    {
        public MajorLifeEventsViewModel()
        {
            _majorLifeEvent = PangeaInfo.Child.MajorLifeEvent;
        }

        private String _majorLifeEvent;
        public String MajorLifeEvent
        {
            get { return _majorLifeEvent; }
            set
            {
                if (_majorLifeEvent != value)
                {
                    SendPropertyChanging();
                    _majorLifeEvent = value;
                    SendPropertyChanged("MajorLifeEvent");
                }
            }
        }

        public String PreviousMajorLifeEvent
        {
            get { return PangeaInfo.ChildHistory.MajorLifeEvent; }
        }

        /// <summary>
        /// Command used to Close the Possible Duplicate Windows when the Click the Yes Button.
        /// </summary>
        private ICommand _closeWindowYesCommand;
        public ICommand CloseWindowYesCommand
        {
            get { return _closeWindowYesCommand ?? (_closeWindowYesCommand = new RelayCommand(CloseWindowYes)); }
        }

        /// <summary>
        /// Called when the Yes Button is clicked, will perform everything that needs to happen when 
        /// the Yes button is clicked.
        /// </summary>
        /// <param name="_window">The Possible Duplicates Window</param>
        private void CloseWindowYes(object _window)
        {
            Window curWindow = _window as Window;

            if (curWindow != null)
            {
                PangeaInfo.Child.MajorLifeEvent = MajorLifeEvent;
                curWindow.DialogResult = true;
                curWindow.Close();
            }
        }

        /// <summary>
        /// Command used to Close the Possible Duplicate Windows when the Click the No Button.
        /// </summary>
        private ICommand _closeWindowCancelCommand;
        public ICommand CloseWindowNoCommand
        {
            get { return _closeWindowCancelCommand ?? (_closeWindowCancelCommand = new RelayCommand(CloseWindowCancel)); }
        }

        /// <summary>
        /// Called when the No Button is clicked, will perform everything that needs to happen when 
        /// the No button is clicked.
        /// </summary>
        /// <param name="_window">The Possible Duplicates Window</param>
        private void CloseWindowCancel(object _window)
        {
            // It is not a Duplicate and we want to associate the possible duplicates with this child.
            Window curWindow = _window as Window;
            if (curWindow != null)
            {
                curWindow.DialogResult = true;
                curWindow.Close();
            }
        }

        // BUG 311 - Added the Code to display the Majore Life Event Rule when you click on the (?)
        private ToolTip _toolTip; // Tooltip that shows up when you click on the rule.
        private bool _tooltipViewable = false; // Do we want to allow the tooltip to show.

        /// <summary>
        /// Action called when the Left Mouse Button is released on the (?) textblock
        /// </summary>
        /// <param name="sender">Mouse</param>
        /// <param name="e">The Mouse Button Event</param>
        public void MajorLifeEventQuestionTextBlock_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            _toolTip.IsOpen = false;
        }

        /// <summary>
        /// Action called when the Left Mouse Button is Pressed on the (?) textblock
        /// </summary>
        /// <param name="sender">Mouse</param>
        /// <param name="e">The Mouse Button Event</param>
        public void MajorLifeEventQuestionTextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Major Life Event"))
                {
                    _toolTip = new ToolTip 
                    { 
                        Content = String.Format
                        (
                            "Majore Life Event is an {0} Field.", 
                            PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Major Life Event") 
                            ? 
                            "Required" 
                            : 
                            "Optional"
                            ) 
                    };
                    _toolTip.IsOpen = true;
                }
            }
        }

        /// <summary>
        /// Action Called when the Mouse Leaves the Text Block for the (?)
        /// </summary>
        /// <param name="sender">Mouse</param>
        /// <param name="e">The Mouse Button Event</param>
        public void MajorLifeEventQuestionTextBlock_MouseLeave(object sender, MouseEventArgs e)
        {
            _tooltipViewable = false;
            if (_toolTip != null)
            {
                if (_toolTip.IsOpen)
                    _toolTip.IsOpen = false;
            }
        }

        /// <summary>
        /// Action Called when the Mouse Enters the Text Block for the (?)
        /// </summary>
        /// <param name="sender">Mouse</param>
        /// <param name="e">The Mouse Button Event</param>
        public void MajorLifeEventQuestionTextBlock_MouseEnter(object sender, MouseEventArgs e)
        {
            _tooltipViewable = true;
        }

        // END BUG 311
    }
}
