﻿/////////////////////////////////////////////////////////////////////////
/// View Model for the Main Window.
/// This controls the language changing functionality.
////////////////////////////////////////////////////////////////////////

using Microsoft.Identity.Client;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

using Pangea.API;
using Pangea.Data.Model;
using Pangea.Data.Model.CodeTables;
using Pangea.FieldApplication.Views;
using Pangea.FieldApplication.ViewModels;
using Pangea.Utilities.Base.Commands;
using Pangea.Utilities.CustomControls.LanguageControl;
using Pangea.Utilities.CustomControls.LanguageControl.Entities;
using Pangea.Utilities.Tools;
using Pangea.Data.DataObjects;
using Pangea.Data;
using PangeaFieldApplication.ViewModels.Authentication;
using Newtonsoft.Json;

namespace Pangea.FieldApplication.ViewModels
{
    public partial class MainWindowViewModel : PangeaBaseViewModel
    {
        private Thread _transferChildrenThread;

        private Thread _retrieveUpdateThread;

        private Thread _updateDBData;

        private ObservableCollection<Child> _transferChildren;

        private String _transferType;

        private int _retrieveLocCode;

        public MainWindowViewModel()
        {
            LogManager.DebugLogManager.MethodBegin("MainWindowViewModel");

            _connSpeed = 0.0;

            LoadSuppotedLanguages();

            System.Globalization.CultureInfo cCulture = System.Globalization.CultureInfo.CurrentCulture;

            if (UILanguageDefn.AllSupportedLanguageCodes.Contains(cCulture.TwoLetterISOLanguageName))
                _uiLanguageCode = cCulture.TwoLetterISOLanguageName;
            else
                _uiLanguageCode = UILanguageDefn.AllSupportedLanguageCodes.First();

            UpdateLanguageData();

            PangeaLanguageControl.Current.LanguageDefn = CurrentLanguage;

            PangeaInfo.Reset();

            PangeaInfo.TransferingData = false;

            _transferChildren = null;

            _transferType = String.Empty;

            _showDataListTab = Visibility.Collapsed;

            _showUpdateTab = Visibility.Collapsed;

            PangeaInfo.User.PropertyChanged += User_PropertyChanged;

            LogManager.DebugLogManager.MethodEnd("MainWindowViewModel");
        }

        /// <summary>
        /// This is used to Display the Data List Tab or Not.
        /// </summary>
        private Visibility _showDataListTab;

        public Visibility ShowDataListTab
        {
            get 
            { 
                return _showDataListTab; 
            }

            set
            {
                if (_showDataListTab != value)
                {
                    SendPropertyChanging();

                    _showDataListTab = value;

                    SendPropertyChanged("ShowDataListTab");
                }
            }
        }

        /// <summary>
        /// This is Used to Display the Update Tab or Not
        /// </summary>
        private Visibility _showUpdateTab;

        public Visibility ShowUpdateTab
        {
            get 
            { 
                return _showUpdateTab; 
            }
            set
            {
                if (_showUpdateTab != value)
                {
                    SendPropertyChanging();

                    _showUpdateTab = value;

                    SendPropertyChanged("ShowUpdateTab");
                }
            }
        }

        /// <summary>
        /// This is used to Display the Enrollment Tab or Not
        /// </summary>
        public Visibility ShowEnrollmentTab
        {
            get
            {
                if (PangeaInfo.User.CanEnroll)
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Is the Login Btn available to be clicked on.
        /// </summary>
        public bool LoginBtnEnabled
        {
            get 
            { 
                return ConnSpeed > 0.0; 
            }
        }

        /// <summary>
        /// The Text Displayed by the Loggin Button.
        /// </summary>
        public String LoginBtnText
        {
            get
            {
                if (PangeaInfo.User.LoggedIn)
                    return String.Format("Welcome, {0}", PangeaInfo.User.UserName);

                return "Sign In";
            }
        }

        /// <summary>
        /// Calculated Connection Speed to the network
        /// </summary>
        private double _connSpeed;
        public double ConnSpeed
        {
            get { return _connSpeed; }
            set
            {
                if (_connSpeed != value)
                {
                    SendPropertyChanging();

                    _connSpeed = value;

                    SendPropertyChanged("ConnSpeed");

                    SendPropertyChanged("LoginBtnEnabled");
                }
            }
        }

        /// <summary>
        /// This is Called after the Main Window is Loaded.
        /// </summary>
        public void Loaded()
        {
            LogManager.DebugLogManager.MethodBegin("MainWindowViewModel.Loaded");

            // Loads the Last Directories used for Images and documents from a config file.
            LoadLastDirectoryLocations();

            // This goes through the Documents Folder and Delets anything over the
            // value for DaysToKeepPangeaDocuments.
            PurgeDocumentsFolders();

            CheckConnectionAndLogin();

            LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.Loaded");
        }

        private string _graphAPIEndpoint = "https://graph.microsoft.com/v1.0/me";

        private string[] _scopes = new string[] { "user.read" };

        private AuthenticationResult _authResult;

        private async Task GetAuthenticationToken()
        {
            LogManager.DebugLogManager.MethodBegin("MainWindowViewModel.GetAuthenticationToken");

            var app = App.PublicClientApp;
            var accounts = await app.GetAccountsAsync();

            try
            {
                _authResult = await app.AcquireTokenSilentAsync(_scopes, accounts.FirstOrDefault());
            }
            catch (MsalUiRequiredException ex)
            {
                // A MsalUiRequiredException happened on AcquireTokenSilentAsync. This indicates you need to call AcquireTokenAsync to acquire a token
                LogManager.ErrorLogManager.WriteError("MainWindowViewModel.GetAuthenticationToken", ex);

                try
                {
                    _authResult = await App.PublicClientApp.AcquireTokenAsync(_scopes);
                }
                catch (MsalException msalex)
                {
                    LogManager.ErrorLogManager.WriteError("MainWindowViewModel.GetAuthenticationToken", msalex);
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorLogManager.WriteError("MainWindowViewModel.GetAuthenticationToken", ex);

                return;
            }

            if (_authResult != null)
            {

                if (Centraler.Settingz.Use_Microsoft_Account_Login_And_Not_Mock_Data)
                {
                    //PangeaInfo.User.FullName = _authResult.Account.u;

                    PangeaInfo.User.UserName = _authResult.Account.Username;

                    PangeaInfo.User.UserID = new Guid(_authResult.UniqueId);

                    PangeaInfo.User.LastLogginDate = DateTime.Now;

                    PangeaInfo.User.LoggedIn = true;

                
                    string tmp = await GetHttpContentWithToken(_graphAPIEndpoint, _authResult.AccessToken);

                }
                else
                {

                    //PangeaInfo.User.FullName = _authResult.Account.u;
                    PangeaInfo.User.UserName = "Jim Bobb";

                    PangeaInfo.User.UserID = Guid.NewGuid(); //  new Guid();

                    PangeaInfo.User.LastLogginDate = DateTime.Now;

                    PangeaInfo.User.LoggedIn = true;

                }

                // Now we have to try and get there Feed The Children Pangea Permissions.
                var userPermissions = Users.GetUserRules();
                WriteLastUser();


                var azureADUser = new AzureADUserVm {
                    Id = 1,
                    AccountId = _authResult.Account.HomeAccountId.Identifier,
                    UserName = _authResult.Account.Username,
                    UniqueId = _authResult.UniqueId,
                    TenantId = _authResult.TenantId,
                    AccessToken = _authResult.AccessToken,
                    IdToken = _authResult.IdToken,
                    FirstLoginOn = DateTime.Now,
                    LastLoginOn = DateTime.Now,
                    ExtendedExpiresOn = _authResult.ExtendedExpiresOn,
                    ExpiresOn = _authResult.ExpiresOn,
                    ExpiresOnDate = DateTime.Now.AddDays(30),
                    DaysToExpiry = 30,
                    Environment = _authResult.Account.Environment,
                    CanEnroll = PangeaInfo.User.CanEnroll,
                    CanUpdate = PangeaInfo.User.CanUpdate,
                    Scopes = _authResult.Scopes.ToList(),
                    LoggedIn = true
                };
                WriteLastUser(azureADUser);
            }

            LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.GetAuthenticationToken");
        }

        /// <summary>
        /// Perform an HTTP GET request to a URL using an HTTP Authorization header
        /// </summary>
        /// <param name="url">The URL</param>
        /// <param name="token">The token</param>
        /// <returns>String containing the results of the GET operation</returns>
        public async Task<string> GetHttpContentWithToken(string url, string token)
        {
            LogManager.DebugLogManager.MethodBegin("MainWindowViewModel.GetHttpContentWithToken");

            var httpClient = new System.Net.Http.HttpClient();

            System.Net.Http.HttpResponseMessage response;

            try
            {
                var request = new System.Net.Http.HttpRequestMessage(System.Net.Http.HttpMethod.Get, url);

                //Add the token in Authorization header
                request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

                response = await httpClient.SendAsync(request);

                var content = await response.Content.ReadAsStringAsync();

                LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.GetHttpContentWithToken");

                return content;
            }
            catch (Exception ex)
            {
                LogManager.ErrorLogManager.WriteError("MainWindowViewModel.GetHttpContentWithToken", ex);

                LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.GetHttpContentWithToken");

                return ex.ToString();
            }
        }

        /// <summary>
        /// This is Called when the Main window starts the Closing Procedure.
        /// </summary>
        public void Closing()
        {
            // Bug 370 - Moved the aborthing of threads to the Closing Method so it gets called when closed from the red X.
            if (_retrieveUpdateThread != null)
            {
                if (_retrieveUpdateThread.IsAlive)
                    _retrieveUpdateThread.Abort();
            }

            if (_transferChildrenThread != null)
            {
                if (_transferChildrenThread.IsAlive)
                    _transferChildrenThread.Abort();
            }

            if (_updateDBData != null)
            {
                if (_updateDBData.IsAlive)
                    _updateDBData.Abort();
            }

            WriteLastDirectoryLocations();

            WriteLastUser();
        }

        /// <summary>
        /// This is called when the Save Progress button is Pressed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void SaveProgressBtn_Click(object sender, RoutedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("MainWindowViewModel.GISaveProgressBtn_Click");

            if (PangeaInfo.ValidateChildInfo())
            {
                // Check to See if the Child Has any Duplicates.
                // TODO : Add Back for the Pangea Possible Duplicates
                if (PangeaInfo.ChildHasPossibleDuplicates())
                {
                    PossibleDuplicateWindow _possibleDuplicateWindow = new PossibleDuplicateWindow();

                    _possibleDuplicateWindow.ShowDialog();

                    if (PangeaInfo.PossibleDuplicate == PossibleDuplicates.Yes)
                    {
                        // Need to Delete tHe Child and Start a new Enrollment.
                        PangeaInfo.StartChildEnrollment();

                        LogManager.DebugLogManager.MethodEnd("MainWindow.GINextBtn_Click");

                        return;
                    }
                    else
                        PangeaInfo.SaveProgress();
                }
                else
                    PangeaInfo.SaveProgress();
            }

            LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.GISaveProgressBtn_Click");
        }

        /// <summary>
        /// Enrolls the Child, This is to the Local DB.
        /// </summary>
        /// <returns>True if succeded; False if failed or if validation failed.</returns>
        public bool EnrollAChild()
        {
            LogManager.DebugLogManager.MethodBegin("MainWindowViewModel.EnrollAChild");

            bool retVal = false;
                        
            bool b_ValidateChildInfo = PangeaInfo.ValidateChildInfo();
            
            if (b_ValidateChildInfo)
            {
                // Check to See if the Child Has any Duplicates.
                // TODO : Add Back for the Pangea Possible Duplicates

                bool b_ChildHasPossibleDuplicates = PangeaInfo.ChildHasPossibleDuplicates();

                if (b_ChildHasPossibleDuplicates)
                {
                    PossibleDuplicateWindow _possibleDuplicateWindow = new PossibleDuplicateWindow();

                    _possibleDuplicateWindow.ShowDialog();

                    if (PangeaInfo.PossibleDuplicate == PossibleDuplicates.Yes)
                    {
                        // Need to Delete tHe Child and Start a new Enrollment.
                        PangeaInfo.StartChildEnrollment();

                        LogManager.DebugLogManager.MethodEnd("MainWindow.GINextBtn_Click");

                        return retVal;
                    }
                    else
                        retVal = PangeaInfo.SaveProgress(true);
                }
                else
                    retVal = PangeaInfo.SaveProgress(true);
            }

            LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.EnrollAChild");

            return retVal;
        }

        /// <summary>
        /// Update the Child, This is to the Local DB.
        /// </summary>
        /// <returns>True if succeded; False if failed or if validation failed.</returns>
        public bool UpdateAChild()
        {
            LogManager.DebugLogManager.MethodBegin("MainWindowViewModel.UpdateAChild");

            bool retVal = false;

            if (PangeaInfo.ValidateChildInfo())
                retVal = PangeaInfo.SaveProgress(true);

            LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.UpdateAChild");

            return retVal;
        }

        /// <summary>
        /// Command used to Open the View Child Table Window
        /// </summary>
        private ICommand _viewTablesCommand;

        public ICommand ViewTablesCommand
        {
            get
            {
                if (_viewTablesCommand == null)
                    _viewTablesCommand = new RelayCommand(param => ViewChildTables());

                return _viewTablesCommand;
            }
        }

        /// <summary>
        /// Used to open the transfer Dialog for Enrollment Children.
        /// </summary>
        private ICommand _transmitChildrenCommand;
        public ICommand TransmitChildrenCommand
        {
            get 
            {
                return _transmitChildrenCommand ?? (_transmitChildrenCommand = new RelayCommand(param => SelectTransmitChildrenAmount())); 
            }
        }

        /// <summary>
        /// Starts the Update Data Retrieval Process
        /// </summary>
        private ICommand _retrieveUpdatesCommand;
        public ICommand RetrieveUpdatesCommand
        {
            get 
            { 
                return 
                    _retrieveUpdatesCommand 
                    
                    ?? 
                    
                    (
                    _retrieveUpdatesCommand = new RelayCommand(param => StartUpdateDataRetrieve())
                    ); 
            }

        }

        /// <summary>
        /// Used by the Menu item to close the App if the user selects File->close
        /// </summary>
        private ICommand _closeAppCommand;
        public ICommand CloseAppCommand
        {
            get 
            { 
                return _closeAppCommand ?? (_closeAppCommand = new RelayCommand<Window>(CloseApp)); 
            }
        }

        /// <summary>
        /// Opens the View Child Table Window
        /// </summary>
        private void ViewChildTables()
        {
            LogManager.DebugLogManager.MethodBegin("MainWindowViewModel.CheckInternet");

            ViewChildTableWindow _vctw = new ViewChildTableWindow();

            _vctw.Show();

            LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.CheckInternet");
        }

        public void StartUpdateDBData()
        {
            LogManager.DebugLogManager.MethodBegin("MainWindowViewModel.StartUpdateDBData");

            _updateDBData = new Thread(UpdateCodeTables);

            _updateDBData.Start();

            LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.StartUpdateDBData");
        }

        /// <summary>
        /// Updates the Code Tables in the Local DB to the latest information
        /// </summary>
        private void UpdateCodeTables()
        {
            LogManager.DebugLogManager.MethodBegin("UpdateCodeTables");

            PangeaInfo
                .Messages
                .RemoveAll("RetrieveCodeData");

            PangeaInfo
                .Messages
                .Add
                (
                new MessagesStruct
                (
                    "Refreshing data from Server.", 
                    MessageTypes.Information, 
                    "RetrieveCodeData"
                    )
                );

            bool ret = DBUpdates.UpdateDBConfig
                (
                PangeaInfo.TestVer ? 
                Centraler.Database_Settingz.DB_Staging 
                : 
                Centraler.Database_Settingz.DB_Production
                );

            ret = DBUpdates.UpdateCodeTables
                (
                PangeaInfo.TestVer 
                ? 
                Centraler.Database_Settingz.DB_Staging 
                : 
                Centraler.Database_Settingz.DB_Production
                );

            PangeaCollections.ResetCollections();

            PangeaInfo
                .Messages
                .RemoveAll("RetrieveCodeData");

            if (ret)
                PangeaInfo
                    .Messages
                    .Add
                    (
                    new MessagesStruct
                    (
                        "Data Updated.", 
                        MessageTypes.Information, 
                        "RetrieveCodeData"
                        )
                    );
            else
                PangeaInfo
                    .Messages
                    .Add
                    (
                    new MessagesStruct
                    (
                        "Data Update Failed.", 
                        MessageTypes.Error, 
                        "RetrieveCodeData"
                        )
                    );

            LogManager.DebugLogManager.MethodEnd("UpdateCodeTables");
        }

        /// <summary>
        /// Used to open the transfer Dialog
        /// </summary>
        private void SelectTransmitChildrenAmount(bool? enrollment = null)
        {
            LogManager.DebugLogManager.MethodBegin("MainWindowViewModel.TransmitEnrollmentChildren");

            if (PangeaInfo.RetrievegData)
            {
                MessageBox.Show("In the Process of Retrieving Data. Please wait till the Retrieval of data is complete.", "Transfer Failed", MessageBoxButton.OK);

                LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.SelectTransmitChildrenAmount");

                return;
            }

            if (PangeaInfo.TransferingData)
            {
                if (_transferChildrenThread.IsAlive)
                {
                    if (MessageBox.Show("Transfer in Progress. Do you wish to abort current transfer?", "Transfer in Progress", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    {
                        _transferChildrenThread.Abort();

                        PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.TransferInfo);

                        PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.TransferFailed);

                        PangeaInfo.TransmitMessages.Add(new MessagesStruct("Transfer Aborted.", MessageTypes.TransferFailed));
                    }
                }
            }
            else
            {
                TransmitChildRecordsWindow _transmitChildRecords = 
                    new TransmitChildRecordsWindow
                    (
                        enrollment
                    );

                if (_transmitChildRecords.ShowDialog() == true)
                {
                    SendPropertyChanged("TransferDataText");

                    _transferChildren = ((TransmitChildRecordsViewModel)_transmitChildRecords.DataContext).SelectedChildsForTransmit;

                    _transferType = ((TransmitChildRecordsViewModel)_transmitChildRecords.DataContext).TransmitType;

                    _transferChildrenThread = new Thread(TransmitChildren);

                    _transferChildrenThread.Start();
                }
            }

            LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.TransmitEnrollmentChildren");
        }

        /// <summary>
        /// Performs the Transfering of the data.
        /// </summary>
        private void TransmitChildren()
        {
            LogManager.DebugLogManager.MethodBegin("MainWindowViewModel.TransmitChildren");

            PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.TransferComplete);

            PangeaInfo.TransferingData = true;

            bool transmitedChildSuccess = false;

            bool transmitedChildAttachments = false;

            Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type the_action_Type = 
                Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type.INSERT;

            if (_transferType == "Enrollment")
            {
                the_action_Type = 
                    Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type.INSERT;
            }
            else
            {
                the_action_Type =
                       Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type.UPDATE;
            }

                
            try
            {
                if (_transferChildren.Count > 0)
                {
                    for (int curChild = 0; curChild < _transferChildren.Count; curChild++)
                    {
                        ChildInfo ch = new ChildInfo(_transferChildren[curChild]);

                        if (ch.Action == PangeaCollections.ActionCollection.Find("Transfer Complete"))
                            continue;

                        ;

                        CustomMappingSource cust_Map_Source = new CustomMappingSource(the_action_Type);

                        Aggregate_Child_Data_TablesDataContext aggregate_Child_Data_DataContext
                            = new Aggregate_Child_Data_TablesDataContext
                            (
                                PangeaInfo.DBCon,
                                the_action_Type,  
                                cust_Map_Source
                                );


                        ch.LanguageCodeID =
                            PangeaCollections
                            .LanguageCollection
                            .Find(System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName)
                            .CodeID;


                        transmitedChildSuccess = false;

                        transmitedChildAttachments = false;

                        String strStr = String.Format
                            (
                            "Transfering child {0} of {1}...", 
                            (curChild + 1), 
                            _transferChildren.Count
                            );

                        PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.TransferInfo);

                        PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.TransferFailed);

                        PangeaInfo.TransmitMessages.Add
                            (
                            new MessagesStruct(strStr, MessageTypes.TransferInfo)
                            );

                        ;

                        if (_transferType == "Enrollment")
                            transmitedChildSuccess = ChildAPI.SaveEnrollmentChild(ref ch);
                        else
                            transmitedChildSuccess = ChildAPI.UpdateSaveChild(ref ch);

                        if (transmitedChildSuccess)
                        {
                            // Send the Images.
                            if (_transferType == "Enrollment")
                                transmitedChildAttachments = ChildAPI.SaveEnrollmentFiles(ref ch);
                            else
                                transmitedChildAttachments = ChildAPI.UpdateSendFiles(ref ch);
                        }

                        if 
                            (
                            transmitedChildSuccess && 
                            transmitedChildAttachments
                            )
                        {
                            if (_transferType == "Enrollment")
                                transmitedChildSuccess = ChildAPI.EnrollChild(ref ch);
                            else
                                transmitedChildSuccess = ChildAPI.FinalizeChildUpdate(ref ch);
                        }


                        ;

                        Pangea.API.Models.Process_WorkFlows_And_Get_PendingChild_ID_Returned the_ret_data = null;

                        switch (the_action_Type)
                        {

                            case Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type.INSERT:

                                the_ret_data = 
                                    ChildAPI
                                    .Process_WorkFlows_And_Get_PendingChild_ID_INSERT
                                    (
                                        ch.ChildID.Value.ToString()
                                        );
                                
                                break;

                            case Aggregate_Child_Data_TablesDataContext.enum_Aggregate_Child_Data_TablesDataContext_Type.UPDATE:

                                the_ret_data = 
                                    ChildAPI
                                    .Process_WorkFlows_And_Get_PendingChild_ID_UPDATE
                                    (
                                        ch.ChildID.Value.ToString()
                                        );
                                
                                break;

                        }

                        //transmitedChildSuccess = the_ret_data.Both_Steps_Succeeded;


                        if (
                            !transmitedChildSuccess || 
                            !transmitedChildAttachments
                            )
                            break;
                    }


                    PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.TransferInfo);

                    PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.TransferFailed);

                    if (
                        (
                        !transmitedChildSuccess || 
                        !transmitedChildAttachments
                        ) && 
                        _transferChildren.Count > 0
                        )
                        PangeaInfo.TransmitMessages.Add
                            (
                            new MessagesStruct
                            (
                                "Transfer Failed.", 
                                MessageTypes.TransferFailed
                                )
                            );
                    else
                        PangeaInfo.TransmitMessages.Add
                            (
                            new MessagesStruct
                            (
                                "Transfer Completed.", 
                                MessageTypes.TransferComplete
                                )
                            );
                }
                else
                    PangeaInfo.TransmitMessages.Add
                        (
                        new MessagesStruct
                        (
                            "No Children Selected for Transfer.", 
                            MessageTypes.TransferInfo
                            )
                        );
            }
            catch (Exception ex)
            {
                PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.TransferInfo);

                PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.TransferFailed);

                PangeaInfo.TransmitMessages.Add
                    (
                    new MessagesStruct
                    (
                        "Transfer failed.", 
                        MessageTypes.TransferFailed
                        )
                    );

                LogManager.ErrorLogManager.WriteError("MainWindowViewModel.TransmitChildren", ex);

                transmitedChildSuccess = false;

                transmitedChildAttachments = false;

                #if DEBUG
                    MessageBox.Show(ex.Message, "MainWindowViewModel.TransmitChildren");
                #endif
            }

            PangeaInfo.TransferingData = false;

            if (_transferChildren.Count > 0)
                PangeaInfo.TransferFailed = !transmitedChildSuccess || !transmitedChildAttachments;
            else
                PangeaInfo.TransferFailed = false;

            SendPropertyChanged("TransmitComplete");

            LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.TransmitChildren");
        }

        /// <summary>
        /// Starts the Upate Data Retrieve Process.
        /// </summary>
        private void StartUpdateDataRetrieve()
        {
            LogManager.DebugLogManager.MethodBegin("MainWindowViewModel.StartUpdateDataRetrieve");

            if (PangeaInfo.TransferingData)
            {
                MessageBox.Show
                    (
                    "In the Process of Transfering Data. Please wait till the Transfering of data is done.", 
                    "Retrieval Failed", 
                    MessageBoxButton.OK
                    );

                LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.StartUpdateDataRetrieve");

                return;
            }

            if (PangeaInfo.RetrievegData)
            {
                if (_retrieveUpdateThread.IsAlive)
                {
                    if (MessageBox.Show("Retrieval in Progress. Do you wish to abort the current Retrieval?", "Retrieve in Progress", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    {
                        _retrieveUpdateThread.Abort();

                        PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.RetrieveData);

                        PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.RetrieveFailed);

                        PangeaInfo.TransmitMessages.Add(new MessagesStruct("Download Updates Aborted.", MessageTypes.RetrieveFailed));
                    }
                }
            }
            else
            {
                RetrieveChildRecordsWindow _retrieveChildRecords = new RetrieveChildRecordsWindow();

                if (_retrieveChildRecords.ShowDialog() == true)
                {
                    _retrieveLocCode = ((RetrieveChildRecordsViewModel)_retrieveChildRecords.DataContext).SelectedLocation.CodeID;

                    _retrieveUpdateThread = new Thread(RetrieveUpdateData);

                    _retrieveUpdateThread.Start();
                }
            }

            LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.StartUpdateDataRetrieve");
        }

        /// <summary>
        /// Retrieves the Update data from the Server
        /// </summary>
        private void RetrieveUpdateData()
        {
            LogManager.DebugLogManager.MethodBegin("MainWindowViewModel.RetrieveUpdateData");

            PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.RetrieveComplete);

            PangeaInfo.RetrievegData = true;

            bool retrieveSuccess = false;

            try
            {
                PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.RetrieveData);

                PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.RetrieveFailed);

                PangeaInfo.TransmitMessages.Add
                    (
                    new MessagesStruct("Retrieving Update Data", MessageTypes.RetrieveData)
                    );

                retrieveSuccess = ChildAPI.RetrieveUpdateData(_retrieveLocCode);
            }
            catch (Exception ex)
            {
                PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.RetrieveData);

                PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.RetrieveFailed);

                PangeaInfo.TransmitMessages.Add
                    (
                    new MessagesStruct("Retrieving Update Data Failed.", MessageTypes.RetrieveFailed)
                    );

                LogManager.ErrorLogManager.WriteError("MainWindowViewModel.RetrieveUpdateData", ex);

                retrieveSuccess = false;

#if DEBUG
                MessageBox.Show(ex.Message, "MainWindowViewModel.RetrieveUpdateData");
#endif
            }

            PangeaInfo.RetrievegData = false;

            PangeaInfo.RetrieveFailed = !retrieveSuccess;

            SendPropertyChanged("RetrieveComplete");

            LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.RetrieveUpdateData");
        }

        /// <summary>
        /// Used to see if the Transmit Button has been clicked on the Data List Screens.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void DataList_PropertyChagned(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch(e.PropertyName)
            {
                case "TransmitButtonClicked_Enrollment":

                    SelectTransmitChildrenAmount(true);

                    break;
                case "TransmitButtonClicked_Update":

                    SelectTransmitChildrenAmount(false);

                    break;
            }
        }

        /// <summary>
        /// Used to Check for the Stop Transfer button has been clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void MainWindow_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "StopTansfer":
                    if (_transferChildrenThread.IsAlive)
                    {
                        if (MessageBox.Show("Transfer in Progress. Do you wish to abort current transfer?", "Transfer in Progress", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                        {
                            _transferChildrenThread.Abort();

                            PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.TransferInfo);

                            PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.TransferFailed);

                            PangeaInfo.TransmitMessages.Add(new MessagesStruct("Transfer Aborted.", MessageTypes.TransferFailed));
                        }
                    }
                    break;
                case "RetryTransfer":
                    if (PangeaInfo.TransferFailed)
                    {
                        if (!_transferChildrenThread.IsAlive)
                        {
                            _transferChildrenThread = new Thread(TransmitChildren);

                            _transferChildrenThread.Start();
                        }
                    }
                    break;

                case "StopRetrieve":

                    if (_retrieveUpdateThread.IsAlive)
                    {
                        if ( MessageBox.Show("Retrieve in Progress. Do you wish to abort current Data Retrieve?", "Retrieve in Progress", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                        {
                            // DanetOpot 22.10.2021 JIRA Issue #PS-3
                            _retrieveUpdateThread.Abort();

                            PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.RetrieveData);

                            PangeaInfo.TransmitMessages.RemoveAll(MessageTypes.RetrieveFailed);

                            PangeaInfo.TransmitMessages.Add(new MessagesStruct("Transfer Aborted.", MessageTypes.RetrieveFailed));
                        }
                    }
                    break;

                case "RetryRetrieve":

                    if (PangeaInfo.RetrieveFailed)
                    {
                        if (!_retrieveUpdateThread.IsAlive)
                        {
                            _retrieveUpdateThread = new Thread(RetrieveUpdateData);

                            _retrieveUpdateThread.Start();
                        }
                    }
                    break;

                default:

                    break;
            }
        }

        private void User_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "LoggedIn":

                    SendPropertyChanged("LoginBtnText");

                    break;

                case "CanEnroll":

                    SendPropertyChanged("ShowEnrollmentTab");

                    break;
            }
        }

        /// <summary>
        /// Used by the Menu item to close the App if the user selects File->close
        /// </summary>
        /// <param name="_window">Main App Window</param>
        private void CloseApp(object _window)
        {
            LogManager.DebugLogManager.MethodBegin("MainWindowViewModel.TransmitEnrollmentChildren");

            // Bug 370 - Moved the aborthing of threads to the Closing Method so it gets called when closed from the red X.
            Window closeWin = _window as Window;

            if (closeWin != null)
                closeWin.Close();

            LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.TransmitEnrollmentChildren");
        }


        private readonly String PasswordHash = "P@ng3@";

        private readonly String SaltKey = "F13ld&App";

        private readonly String VIKey = "#1C3b3D4f983P1H876@";


        private UserInfo LoadLastUser()
        {
            LogManager.DebugLogManager.MethodBegin("MainWindowViewModel.LoadLastUser");

            UserInfo _retUserVal = new UserInfo();

            if (!Directory.Exists(PangeaInfo.CommonAppPangeaCurrentLoc))
                Directory.CreateDirectory(PangeaInfo.CommonAppPangeaCurrentLoc);

            String tempFile = String.Format("{0}l.cfg", PangeaInfo.CommonAppPangeaCurrentLoc);

            if (File.Exists(tempFile))
            {
                StreamReader sr = new StreamReader(tempFile);

                List<String> _fileLines = new List<String>();

                while (!sr.EndOfStream)
                {
                    _fileLines.Add(sr.ReadLine());
                }

                if (_fileLines.Count == 13)
                {
                    try
                    {
                        DateTime _lastLoggin = Convert.ToDateTime(Connection.Decrypt(_fileLines[9], PasswordHash, SaltKey, VIKey));

                        if ((DateTime.Now - _lastLoggin).TotalDays <= 30)
                        {
                            _retUserVal.UserName = Connection.Decrypt(_fileLines[1], PasswordHash, SaltKey, VIKey);

                            _retUserVal.UserID = new Guid(Connection.Decrypt(_fileLines[3], PasswordHash, SaltKey, VIKey));

                            //_retUserVal.FullName = Connection.Decrypt(_fileLines[5], PasswordHash, SaltKey, VIKey);

                            _retUserVal.CanEnroll = Convert.ToBoolean(Connection.Decrypt(_fileLines[5], PasswordHash, SaltKey, VIKey));

                            _retUserVal.CanUpdate = Convert.ToBoolean(Connection.Decrypt(_fileLines[7], PasswordHash, SaltKey, VIKey));

                            _retUserVal.LastLogginDate = _lastLoggin;
                        }
                    }
                    catch (Exception ex)
                    {
                        LogManager.ErrorLogManager.WriteError("MainWindowViewModel.LoadLastUser", ex);

                        _retUserVal = null;
                    }
                }
            }

            LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.LoadLastUser");

            return _retUserVal;
        }

        private UserInfo LoadLastUser(AzureADUserVm vm)
        {
            LogManager.DebugLogManager.MethodBegin("MainWindowViewModel.LoadLastUserJson");

            UserInfo userInfo = new UserInfo();
            //userInfo.UserID = new Guid(Connection.Decrypt(vm.UniqueId, PasswordHash, SaltKey, VIKey));
            userInfo.UserName = Connection.Decrypt(vm.UserName, PasswordHash, SaltKey, VIKey);
            userInfo.CanEnroll = vm.CanEnroll;
            userInfo.CanUpdate = vm.CanUpdate;
            userInfo.LastLogginDate = vm.LastLoginOn;

            LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.LoadLastUserJson");
            return userInfo;
        }

        private void WriteLastUser()
        {
            LogManager.DebugLogManager.MethodBegin("MainWindowViewModel.WriteLastUser");

            String tempFile = String.Format("{0}l.cfg", PangeaInfo.CommonAppPangeaCurrentLoc);

            if (PangeaInfo.User.LoggedIn)
            {
                String userName = Connection.Encrypt(PangeaInfo.User.UserName, PasswordHash, SaltKey, VIKey);

                String userID = Connection.Encrypt(PangeaInfo.User.UserID.ToString(), PasswordHash, SaltKey, VIKey);

                String fullName = Connection.Encrypt(PangeaInfo.User.FullName, PasswordHash, SaltKey, VIKey);

                String canEnroll = Connection.Encrypt(PangeaInfo.User.CanEnroll.ToString(), PasswordHash, SaltKey, VIKey);

                String canUpdate = Connection.Encrypt(PangeaInfo.User.CanUpdate.ToString(), PasswordHash, SaltKey, VIKey);

                String lastLogginDate = Connection.Encrypt(PangeaInfo.User.LastLogginDate?.ToString("d"), PasswordHash, SaltKey, VIKey);

                List<String> _randomStrings = new List<String>();

                var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+-=`~;':\"[]{}?/><,.";

                var random = new Random();

                for (int j = 0; j < 6; j++)
                {
                    int numChars = random.Next(20);

                    String stringChars = String.Empty;

                    for (int i = 0; i < numChars; i++)
                    {
                        stringChars += chars[random.Next(chars.Length)];
                    }

                    _randomStrings.Add(stringChars);
                }

                if (!Directory.Exists(PangeaInfo.CommonAppPangeaCurrentLoc))
                    Directory.CreateDirectory(PangeaInfo.CommonAppPangeaCurrentLoc);

                StreamWriter sw = new StreamWriter(tempFile, false);

                sw.WriteLine(_randomStrings[0]);

                sw.WriteLine(userName);

                sw.WriteLine(_randomStrings[1]);

                sw.WriteLine(userID);

                sw.WriteLine(_randomStrings[2]);

                //sw.WriteLine(fullName);
                //sw.WriteLine(_randomStrings[3]);

                sw.WriteLine(canEnroll);

                sw.WriteLine(_randomStrings[3]);

                sw.WriteLine(canUpdate);

                sw.WriteLine(_randomStrings[4]);

                sw.WriteLine(lastLogginDate);

                sw.WriteLine(_randomStrings[5]);

                sw.Close();
            }
            else
            {
                if (!Directory.Exists(PangeaInfo.CommonAppPangeaCurrentLoc))
                    Directory.CreateDirectory(PangeaInfo.CommonAppPangeaCurrentLoc);

                if (File.Exists(tempFile))
                    File.Delete(tempFile);
            }

            LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.WriteLastUser");
        }

        private void WriteLastUser(AzureADUserVm vm)
        {
            LogManager.DebugLogManager.MethodBegin("MainWindowViewModel.WriteLastUserJson");

            try 
            {
                if (PangeaInfo.User.LoggedIn)
                {
                    string azureADAuthFile = String.Format("{0}PangeaAzureADUserSettings.json", PangeaInfo.CommonAppPangeaCurrentLoc);
                    if (File.Exists(azureADAuthFile))
                        File.Delete(azureADAuthFile);


                    vm.AccountId = Connection.Encrypt(vm.AccountId, PasswordHash, SaltKey, VIKey);
                    vm.UserName = Connection.Encrypt(vm.UserName, PasswordHash, SaltKey, VIKey);
                    vm.UniqueId = Connection.Encrypt(vm.UniqueId, PasswordHash, SaltKey, VIKey);
                    vm.TenantId = Connection.Encrypt(vm.TenantId, PasswordHash, SaltKey, VIKey);
                    vm.IsExtendedLifeTimeToken = vm.IsExtendedLifeTimeToken;
                    vm.AccessToken = Connection.Encrypt(vm.AccessToken, PasswordHash, SaltKey, VIKey);
                    vm.IdToken = Connection.Encrypt(vm.IdToken, PasswordHash, SaltKey, VIKey);
                    vm.ExtendedExpiresOn = vm.ExtendedExpiresOn;
                    vm.Environment = Connection.Encrypt(vm.Environment, PasswordHash, SaltKey, VIKey);
                    List<string> userScopes = new List<string>();
                    foreach (var scope in vm.Scopes)
                    {
                        var encryptedScope = Connection.Encrypt(scope, PasswordHash, SaltKey, VIKey);
                        userScopes.Add(encryptedScope);
                    }
                    vm.Scopes = userScopes;
                    var AzureADUser = $"{JsonConvert.SerializeObject(vm)}";
                    File.WriteAllText($"{azureADAuthFile}", AzureADUser);
                }
            } 
            catch(Exception ex)
            {
                throw ex;
            }
            LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.WriteLastUserJson");
        }

        /// <summary>
        /// This will read in the Saved Last Directory Locations for Documents, Images, and Optional Files.
        /// </summary>
        private void LoadLastDirectoryLocations()
        {
            LogManager.DebugLogManager.MethodBegin("MainWindowViewModel.LoadLastDirectoryLocations");

            if (!Directory.Exists(PangeaInfo.CommonAppPangeaCurrentLoc))
                Directory.CreateDirectory(PangeaInfo.CommonAppPangeaCurrentLoc);

            String tempFile = String.Format("{0}LastDir.cfg", PangeaInfo.CommonAppPangeaCurrentLoc);
            if (File.Exists(tempFile))
            {
                String line = String.Empty;

                StreamReader sr = new StreamReader(tempFile);

                while ((line = sr.ReadLine()) != null)
                {
                    if (line.Equals("Documents"))
                    {
                        line = sr.ReadLine();

                        PangeaInfo.LastDirectoryDocuments = String.IsNullOrEmpty(line) ? String.Empty : line;
                    }
                    else if (line.Equals("Images"))
                    {
                        line = sr.ReadLine();

                        PangeaInfo.LastDirectoryImages = String.IsNullOrEmpty(line) ? String.Empty : line;
                    }
                    else if (line.Equals("Optional"))
                    {
                        line = sr.ReadLine();

                        PangeaInfo.LastDirectoryOptionalFiles = String.IsNullOrEmpty(line) ? String.Empty : line;
                    }
                }
            }

            LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.LoadLastDirectoryLocations");
        }

        /// <summary>
        /// This will write the Saved Last Directory Locations for Documents, Images, and Optional Files.
        /// </summary>
        private void WriteLastDirectoryLocations()
        {
            LogManager.DebugLogManager.MethodBegin("MainWindowViewModel.WriteLastDirectoryLocations");

            if (!Directory.Exists(PangeaInfo.CommonAppPangeaCurrentLoc))
                Directory.CreateDirectory(PangeaInfo.CommonAppPangeaCurrentLoc);

            String tempFile = String.Format("{0}LastDir.cfg", PangeaInfo.CommonAppPangeaCurrentLoc);

            StreamWriter sw = new StreamWriter(tempFile, false);

            sw.WriteLine("Documents");

            sw.WriteLine(String.IsNullOrEmpty(PangeaInfo.LastDirectoryDocuments) ? String.Empty : PangeaInfo.LastDirectoryDocuments);

            sw.WriteLine("Images");

            sw.WriteLine(String.IsNullOrEmpty(PangeaInfo.LastDirectoryImages) ? String.Empty : PangeaInfo.LastDirectoryImages);

            sw.WriteLine("Optional");

            sw.WriteLine(String.IsNullOrEmpty(PangeaInfo.LastDirectoryOptionalFiles) ? String.Empty : PangeaInfo.LastDirectoryOptionalFiles);

            sw.Close();

            LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.WriteLastDirectoryLocations");
        }

        /// <summary>
        /// This goes through the Documents Folder and Delets anything over the
        /// value for DaysToKeepPangeaDocuments.
        /// </summary>
        private void PurgeDocumentsFolders()
        {
            LogManager.DebugLogManager.MethodBegin("MainWindowViewModel.PurgeDocumentsFolders");

            // Start with the Current Folder
            if (Directory.Exists(PangeaInfo.CommonAppPangeaCurrentLoc))
                CleanOutDirectories(PangeaInfo.CommonAppPangeaCurrentLoc);

            if (Directory.Exists(PangeaInfo.CommonAppPangeaEnrollmentLoc))
                CleanOutDirectories(PangeaInfo.CommonAppPangeaEnrollmentLoc);

            if (Directory.Exists(PangeaInfo.CommonAppPangeaUpdateLoc))
                CleanOutDirectories(PangeaInfo.CommonAppPangeaUpdateLoc);

            LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.PurgeDocumentsFolders");
        }

        /// <summary>
        /// This loops through all of the Directories and files in a directory and starts to 
        /// clean out files that are too old.
        /// </summary>
        /// <param name="directory"></param>
        private void CleanOutDirectories(String directory)
        {
            LogManager.DebugLogManager.MethodBegin("MainWindowViewModel.CleanOutDirectories");

            int daysToKeepPangeaDocuments = Convert.ToInt32(ConfigurationManager.AppSettings["DaysToKeepPangeaDocuments"]);

            String[] _directories = Directory.GetDirectories(directory);

            foreach (String _dir in _directories)
            {
                CleanOutDirectories(_dir);
            }

            String[] _files = Directory.GetFiles(directory);

            foreach (String _file in _files)
            {
                if (_file.Contains(".cfg"))
                    continue; 

                DateTime fAccessTime = File.GetLastAccessTime(_file);

                int days = new DateTime(DateTime.Now.Date.Subtract(fAccessTime.Date).Ticks).Day;

                if (days >= daysToKeepPangeaDocuments)
                    File.Delete(_file);
            }

            if (Directory.GetDirectories(directory).Count() <= 0 && Directory.GetFiles(directory).Count() <= 0)
                Directory.Delete(directory);

            LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.CleanOutDirectories");
        }

        /// <summary>
        /// Checks to see if there is an internet Connection and then tries to Loggin or read the last logged user.
        /// </summary>
        private async void CheckConnectionAndLogin()
        { 
            LogManager.DebugLogManager.MethodBegin("MainWindowViewModel.CheckConnectionAndLogin");

            // Test Internet Connection
            ConnSpeed = Connection.TestConnection();


            // Check if User Azure AD File Is Valid
            bool tokenIsValid = true;
            int daysToExpiry = 0;
            List<string> azureScopes = new List<string>();

            var app = App.PublicClientApp;
            var account = (await app.GetAccountsAsync()).FirstOrDefault();
            string azureADAuthFile = String.Format("{0}PangeaAzureADUserSettings.json", PangeaInfo.CommonAppPangeaCurrentLoc);
            var azureADAuthModel = new AzureADUserVm();
            if (File.Exists(azureADAuthFile))
            {
                
                var azureADJsonFile = File.ReadAllText($"{azureADAuthFile}");
                azureADAuthModel = JsonConvert.DeserializeObject<AzureADUserVm>(azureADJsonFile);
                DateTime tokenExpiryDate = azureADAuthModel.ExpiresOnDate.Date;
                DateTime todayDate = DateTime.Now.Date;
                if (tokenExpiryDate < todayDate)
                {
                    tokenIsValid = false;
                }
                daysToExpiry = (tokenExpiryDate - todayDate).Days >= 0? (tokenExpiryDate - todayDate).Days : 0;
            }

            if (azureADAuthModel.AccessToken != null && tokenIsValid)
            {
                azureADAuthModel.DaysToExpiry = daysToExpiry;
                azureADAuthModel.AccountId = Connection.Decrypt(azureADAuthModel.AccountId, PasswordHash, SaltKey, VIKey);
                azureADAuthModel.UniqueId = Connection.Decrypt(azureADAuthModel.UniqueId, PasswordHash, SaltKey, VIKey);
                azureADAuthModel.UserName = Connection.Decrypt(azureADAuthModel.UserName, PasswordHash, SaltKey, VIKey);
                azureADAuthModel.TenantId = Connection.Decrypt(azureADAuthModel.TenantId, PasswordHash, SaltKey, VIKey);
                foreach (var scope in azureADAuthModel.Scopes)
                {
                    var encryptedScope = Connection.Decrypt(scope, PasswordHash, SaltKey, VIKey);
                    azureScopes.Add(encryptedScope);
                }
                azureADAuthModel.Scopes = azureScopes;
                azureADAuthModel.AccessToken = Connection.Decrypt(azureADAuthModel.AccessToken, PasswordHash, SaltKey, VIKey);
                azureADAuthModel.IdToken = Connection.Decrypt(azureADAuthModel.IdToken, PasswordHash, SaltKey, VIKey);
                azureADAuthModel.Environment = Connection.Decrypt(azureADAuthModel.Environment, PasswordHash, SaltKey, VIKey);

                PangeaInfo.User.UserName = azureADAuthModel.UserName;
                PangeaInfo.User.UserID = new Guid(azureADAuthModel.UniqueId);
                PangeaInfo.User.LastLogginDate = DateTime.Now;
                PangeaInfo.User.LoggedIn = true;
                PangeaInfo.User.CanEnroll = azureADAuthModel.CanEnroll;
                PangeaInfo.User.CanUpdate = azureADAuthModel.CanUpdate;

                WriteLastUser(azureADAuthModel);

                // Prompt for Data Referesh
                var dialog_result = MessageBox.Show
                        (
                            "It is recommended that you Run Data Refresh.  Would you like to do that now?",
                            "Refresh Data",
                            MessageBoxButton.YesNo
                            );

                if (dialog_result == MessageBoxResult.Yes)
                {
                    StartUpdateDBData();
                }
            }
            else
            {
                ConnSpeed = Connection.TestConnection();
                if (ConnSpeed > Centraler.Settingz.minimum_Internet_Connection_Speed)
                {
                    AzureADLogin();
                }
            }

            LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.CheckConnectionAndLogin");

        }

        /// <summary>
        /// Mouse Click Event for Checking if there is an internet connection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void InternetConnectionCtrl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("MainWindowViewModel.InternetConnectionCtrl_MouseLeftButtonUp");

            CheckConnectionAndLogin();

            LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.InternetConnectionCtrl_MouseLeftButtonUp");
        }

        /// <summary>
        /// Logs the User In and Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public async void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("MainWindowViewModel.LoginButton_Click");

            if (PangeaInfo.User.LoggedIn)
            {
                var accounts = await App.PublicClientApp.GetAccountsAsync();
                try
                {
                    await App.PublicClientApp.RemoveAsync(accounts.FirstOrDefault());
                    PangeaInfo.User.Clear();
                }
                catch (Exception ex)
                {
                    LogManager.ErrorLogManager.WriteError("MainWindowViewModel.LoginButton_Click", ex);
                }
            }
            else
            {
                if (ConnSpeed > 0.0)
                {
                    await GetAuthenticationToken();
                }
            }

            LogManager.DebugLogManager.MethodEnd("MainWindowViewModel.LoginButton_Click");
        }

        public async void AzureADLogin()
        {
            await GetAuthenticationToken();

            if (PangeaInfo.User.LoggedIn)
            {
                var dialog_result = MessageBox.Show
                        (
                            "It is recommended that you Run Data Refresh.  Would you like to do that now?",
                            "Refresh Data",
                            MessageBoxButton.YesNo
                            );

                if (dialog_result == MessageBoxResult.Yes)
                {
                    StartUpdateDBData();
                }
            }

        }
    }
}
