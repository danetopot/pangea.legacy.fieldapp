﻿
using System;
using System.Collections.ObjectModel;
using System.Data.Linq;
using System.Windows;
using System.Windows.Input;

using Pangea.Data.DataObjects;
using Pangea.Utilities.Base.Commands;

namespace Pangea.FieldApplication.ViewModels
{
    public struct ChildTableViewStruct
    {
        public ChildTableViewStruct(ChildDO _child)
        {
            Country = _child.Location_Code_ID != null ? PangeaCollections.CountryCollection.Find(PangeaCollections.LocationCollection.Find(_child.Location_Code_ID)?.CountryCodeID)?.Name : String.Empty;
            Location = _child.Location_Code_ID != null ? PangeaCollections.LocationCollection.Find(_child.Location_Code_ID)?.Name : String.Empty;
            ChildID = _child.Child_ID.ToString();
            FName = _child.First_Name;
            MName = _child.Middle_Name;
            LName = _child.Last_Name;
            NName = _child.Nickname;
            DOB = _child.Date_of_Birth;
            Grade = _child.Grade_Level_Code_ID != null ? PangeaCollections.GradeLevelCollection.Find(_child.Grade_Level_Code_ID)?.Name : String.Empty;
            Health = _child.Health_Status_Code_ID != null ? PangeaCollections.HealthStatusCollection.Find(_child.Health_Status_Code_ID)?.Name : String.Empty;
            LivesWith = _child.Lives_With_Code_ID != null ? PangeaCollections.LivesWithCollection.Find(_child.Lives_With_Code_ID)?.Description : String.Empty;
            FavLearn = _child.Favorite_Learning_Code_ID != null ? PangeaCollections.FavoriteLearningCollection.Find(_child.Favorite_Learning_Code_ID)?.Name : String.Empty;
            Gender = _child.Gender_Code_ID != null ? PangeaCollections.GenderCollection.Find(_child.Gender_Code_ID)?.Description : String.Empty;
            NumBro = Convert.ToString(_child.Number_Brothers);
            NumSis = Convert.ToString(_child.Number_Sisters);
            Disability = Convert.ToBoolean(_child.Disability_Status_Code_ID) ? "Yes" : "No";
            EnrollmentReady = Convert.ToBoolean(_child.Enrollment_Ready) ? "Yes" : "No";
        }

        public String Country { get; set; }
        public String Location { get; set; }
        public String ChildID { get; set; }
        public String FName { get; set; }
        public String MName { get; set; }
        public String LName { get; set; }
        public String NName { get; set; }
        public DateTime? DOB { get; set; }
        public String Grade { get; set; }
        public String Health { get; set; }
        public String LivesWith { get; set; }
        public String FavLearn { get; set; }
        public String Gender { get; set; }
        public String NumBro { get; set; }
        public String NumSis { get; set; }
        public String Disability { get; set; }
        public String EnrollmentReady { get; set; }
    };

    public struct ChoreTableViewStruct
    {
        public ChoreTableViewStruct(Guid _chidID, GetEnrollmentChoresResult _chore)
        {
            ChildID = _chidID.ToString();
            ChoreDesc = PangeaCollections.ChoresCollection.Find(_chore.Chore_Code_ID)?.Name;
        }

        public String ChildID { get; set; }
        public String ChoreDesc { get; set; }
    }

    public struct FavActivityTableViewStruct
    {
        public FavActivityTableViewStruct(Guid _childID, GetEnrollmentFavoriteActivitiesResult _favAct)
        {
            ChildID = _childID.ToString();
            FavAct = PangeaCollections.FavoriteActivityCollection.Find(_favAct.Favorite_Activity_Code_ID)?.Name;
        }

        public String ChildID { get; set; }
        public String FavAct { get; set; }
    }

    public struct PersonalityTypeTableViewStruct
    {
        public PersonalityTypeTableViewStruct(Guid _childID, GetEnrollmentPersonalityTypeResult _pType)
        {
            ChildID = _childID.ToString();
            pType = PangeaCollections.PersonalityTypeCollection.Find(_pType.Personality_Type_Code_ID)?.Name;
        }

        public String ChildID { get; set; }
        public String pType { get; set; }
    }

    public class ViewChildTableViewModel : PangeaBaseViewModel
    {
        private Guid? _selectedGuid;

        public ViewChildTableViewModel()
        {
            Mouse.OverrideCursor = Cursors.Wait;

            _selectedGuid = null;
        }

        public void ViewChildTableWindow_Loaded(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Arrow;
        }

        public ObservableCollection<ChildTableViewStruct> ChildTableRows
        {
            get
            {
                ObservableCollection<ChildTableViewStruct> _rows = new ObservableCollection<ChildTableViewStruct>();

                ISingleResult<ChildDO> _results = new EnrollmentTablesDataContext(PangeaInfo.DBCon).Get_Saved_Enrollments(PangeaInfo.User.UserID);

                foreach (ChildDO ch in _results)
                {
                    _rows.Add(new ChildTableViewStruct(ch));
                }

                return _rows;
            }
        }


        public ObservableCollection<ChoreTableViewStruct> ChoreTableRows
        {
            get
            {
                ObservableCollection<ChoreTableViewStruct> _rows = new ObservableCollection<ChoreTableViewStruct>();


                if (_selectedGuid != null)
                {
                    EnrollmentTablesDataContext _instance = new EnrollmentTablesDataContext(PangeaInfo.DBCon);

                    foreach (GetEnrollmentChoresResult _chore in _instance.Get_Enrollment_Chores(PangeaInfo.User.UserID, _selectedGuid))
                    {
                        _rows.Add(new ChoreTableViewStruct(_selectedGuid.Value, _chore));
                    }
                }

                return _rows;
            }
        }


        public ObservableCollection<FavActivityTableViewStruct> FavActivityRows
        {
            get
            {
                ObservableCollection<FavActivityTableViewStruct> _rows = new ObservableCollection<FavActivityTableViewStruct>();

                if (_selectedGuid != null)
                {
                    EnrollmentTablesDataContext _instance = new EnrollmentTablesDataContext(PangeaInfo.DBCon);

                    foreach (GetEnrollmentFavoriteActivitiesResult _favAct in _instance.Get_Enrollment_Favorite_Activities(PangeaInfo.User.UserID, _selectedGuid))
                    {
                        _rows.Add(new FavActivityTableViewStruct(_selectedGuid.Value, _favAct));
                    }

                }

                return _rows;
            }
        }


        public ObservableCollection<PersonalityTypeTableViewStruct> PersonalityTypeRows
        {
            get
            {
                ObservableCollection<PersonalityTypeTableViewStruct> _rows = new ObservableCollection<PersonalityTypeTableViewStruct>();

                if (_selectedGuid != null)
                {
                    EnrollmentTablesDataContext _instance = new EnrollmentTablesDataContext(PangeaInfo.DBCon);

                    foreach (GetEnrollmentPersonalityTypeResult _pType in _instance.Get_Enrollment_Personality_Type(PangeaInfo.User.UserID, _selectedGuid))
                    {
                        _rows.Add(new PersonalityTypeTableViewStruct(_selectedGuid.Value, _pType));
                    }
                }

                return _rows;
            }
        }

        public void ChildTableDataGrid_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                ChildTableViewStruct _child = (ChildTableViewStruct)e.AddedItems[0];
                _selectedGuid = Guid.Parse(_child.ChildID);

                SendPropertyChanged("ChoreTableRows");
                SendPropertyChanged("FavActivityRows");
                SendPropertyChanged("PersonalityTypeRows");
            }
        }

        private ICommand _refreshTableInfo;
        public ICommand RefreshTableInfo
        {
            get
            {
                if (_refreshTableInfo == null)
                    _refreshTableInfo = new RelayCommand(param => Refresh());

                return _refreshTableInfo;
            }
        }

        private void Refresh()
        {
            Mouse.OverrideCursor = Cursors.Wait;

            SendPropertyChanged("ChildTableRows");
            SendPropertyChanged("ChoreTableRows");
            SendPropertyChanged("FavActivityRows");
            SendPropertyChanged("PersonalityTypeRows");

            Mouse.OverrideCursor = Cursors.Arrow;
        }
    }
}
