﻿using Pangea.Data.DataObjects;
using Pangea.Data.Model;
using Pangea.Utilities.Base.Commands;
using System;
using System.Collections.ObjectModel;
using System.Data.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Pangea.FieldApplication.ViewModels
{
    public class TransmitChildRecordsViewModel : PangeaBaseViewModel
    {
        private bool? _enrollment;

        public TransmitChildRecordsViewModel(bool? enrollment)
        {
            SelectedChildsForTransmit = new ObservableCollection<Child>();

            _enrollment = enrollment;
        }

        /// <summary>
        /// Allows the user to choose how many enrollments to send at a time.
        /// </summary>
        public ObservableCollection<string> AmountToTransferCollection
        {
            get
            {
                // TODO : This needs to be driven from the DataBase.
                ObservableCollection<string> _amountToTransferCol = new ObservableCollection<string>();

                _amountToTransferCol.Add("All");

                _amountToTransferCol.Add("5");

                _amountToTransferCol.Add("10");

                _amountToTransferCol.Add("15");

                _amountToTransferCol.Add("50");

                _amountToTransferCol.Add("Manual");

                return _amountToTransferCol;
            }
        }

        /// <summary>
        /// The Selected Amount to Transfer at a time.
        /// </summary>
        private string _amountToTransfer;
        public string AmountToTransfer
        {
            get
            {
                if (
                    _amountToTransfer == null
                    )
                    _amountToTransfer = 
                        PangeaInfo.DefaultTransferAmnt != String.Empty 
                        ? 
                        PangeaInfo.DefaultTransferAmnt 
                        :
                        null;

                return _amountToTransfer;
            }
            set
            {
                if (_amountToTransfer != value)
                {
                    SendPropertyChanging();

                    _amountToTransfer = value;

                    if (_amountToTransfer != "Manual")
                    {
                        SendPropertyChanged("ResetSelected");

                        SelectedChildsForTransmit.Clear();
                    }

                    SendPropertyChanged("AmountToTransfer");
                }
            }
        }

        /// <summary>
        /// The Items type they want to transmit.
        /// </summary>
        public ObservableCollection<string> TransmitTypes
        {
            get
            {
                ObservableCollection<string> _transmitTypes = new ObservableCollection<string>();

                _transmitTypes.Add("Enrollment");

                _transmitTypes.Add("Update");

                return _transmitTypes;
            }
        }

        /// <summary>
        /// Type they want to transmit
        /// </summary>
        private string _transmitType;

        public string TransmitType
        {
            get
            {
                if (String.IsNullOrEmpty(_transmitType) && _enrollment != null)
                {
                    if (_enrollment == true)
                        _transmitType = "Enrollment";
                    else
                        _transmitType = "Update";

                    SendPropertyChanged("ChildrenToTransmit");
                }

                return _transmitType;
            }
            set
            {
                if (_transmitType != value)
                {
                    SendPropertyChanging();

                    _transmitType = value;
                }

                SendPropertyChanged("TransmitType");

                SendPropertyChanged("ChildrenToTransmit");
            }
        }

        /// <summary>
        /// This is the list of children that are ready to be transmitted.
        /// </summary>
        public ObservableCollection<Child> ChildrenToTransmit
        {
            get
            {
                ObservableCollection<Child> _childrenToTransmit = new ObservableCollection<Child>();

                if (TransmitType == "Enrollment")
                {
                    EnrollmentTablesDataContext enrollTableDC = new EnrollmentTablesDataContext(PangeaInfo.DBCon);

                    ISingleResult<ChildDO> enrollmentChilds = 
                        enrollTableDC
                        .Get_Enrollment_Ready_To_Transmit(PangeaInfo.User.UserID);

                    foreach (ChildDO _child in enrollmentChilds)
                    {
                        _childrenToTransmit
                            .Add
                            (
                            PangeaInfo.GetEnrollmentChildInfo(_child.Child_ID)
                            );
                    }
                }
                else if (TransmitType == "Update")
                {
                    PendingTablesDataContext pendingTableDC = new PendingTablesDataContext(PangeaInfo.DBCon);

                    ISingleResult<ChildDO> pendingChilds = 
                        pendingTableDC
                        .Get_Pending_Ready_To_Transmit(PangeaInfo.User.UserID);

                    foreach (ChildDO _child in pendingChilds)
                    {
                        _childrenToTransmit
                            .Add
                            (
                            PangeaInfo
                            .GetPendingChildInfo(_child.Child_ID)
                            );
                    }
                }

                return _childrenToTransmit;
            }
        }

        /// <summary>
        /// This is the list of Children that we want to Transmit
        /// </summary>
        public ObservableCollection<Child> SelectedChildsForTransmit;

        /// <summary>
        /// This is used to select the Children that we want to 
        /// transfer manualy
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ChildDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                foreach (Child ch in e.AddedItems)
                {
                    SelectedChildsForTransmit.Add(ch);
                }
            }

            if (e.RemovedItems.Count > 0)
            {
                foreach (Child ch in e.RemovedItems)
                {
                    SelectedChildsForTransmit.Remove(ch);
                }
            }

            if (SelectedChildsForTransmit.Count > 0)
                AmountToTransfer = "Manual";
        }

        /// <summary>
        /// This is a custom close command. This allows us to
        /// call the close command from the view model instead
        /// of the code behind the View.
        /// </summary>
        private ICommand _closeTransmitCommand;

        public ICommand CloseTransmitCommand
        {
            get
            {
                return _closeTransmitCommand ?? 
                    (
                    _closeTransmitCommand = new RelayCommand<Window>(Transmit)
                    );
            }
        }

        /// <summary>
        /// This is a custom close command. This allows us to
        /// call the close command from the view model instead
        /// of the code behind the View.
        /// </summary> 
        private ICommand _closeCancelCommand;

        public ICommand CloseCancelCommand
        {
            get
            {
                return _closeCancelCommand ?? 
                    (
                    _closeCancelCommand = new RelayCommand<Window>(Cancel)
                    );
            }
        }

        /// <summary>
        /// This closes the Dialog and tells the app the user wants
        /// to start the transfer of child data, and which children 
        /// we want to transfer.
        /// </summary>
        /// <param name="win">Current Transfer Dialog</param>
        private void Transmit(Window win)
        {
            if (win != null)
            {
                if (string.IsNullOrEmpty(AmountToTransfer))
                {
                    MessageBox.Show("Please select the amount of children you wish to transfer.");

                    return;
                }

                if (string.IsNullOrEmpty(TransmitType))
                {
                    MessageBox.Show("Please select the transfer type.");

                    return;
                }

                if (AmountToTransfer == "All")
                {
                    SelectedChildsForTransmit.Clear();

                    SelectedChildsForTransmit = ChildrenToTransmit;
                }
                else if (AmountToTransfer != "Manual")
                {
                    int transAmount = Convert.ToInt32(AmountToTransfer);

                    SelectedChildsForTransmit.Clear();

                    for (int i = 0; i < transAmount; i++)
                    {
                        if (i >= ChildrenToTransmit.Count)
                            break;

                        SelectedChildsForTransmit.Add(ChildrenToTransmit[i]);
                    }
                }

                win.DialogResult = true;

                win.Close();
            }
        }

        /// <summary>
        /// This Closes the Dialog and tells the App the user doesn't want to
        /// start the transfer of child data.
        /// </summary>
        /// <param name="win">Current Transfer Dialog</param>
        public void Cancel(Window win)
        {
            if (win != null)
            {
                win.DialogResult = false;

                win.Close();
            }
        }
    }
}
