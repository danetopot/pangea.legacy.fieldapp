﻿/////////////////////////////////////////////////////////////////////////
/// View Model for the Location Select Window.
/// The data seen on the screen is populated by this View Model and 
/// the data changed on the screen is saved to the Buisness Objects
/// though the different properties and functions.
////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

using Pangea.Data.Collections;
using Pangea.Data.Model.CodeTables;
using Pangea.Utilities.Base.Commands;
using Pangea.Utilities.Tools;

namespace Pangea.FieldApplication.ViewModels
{
    public partial class LocationSelectViewModel : PangeaBaseViewModel
    {
        private ICommand _closeUpdateCmd;

        private ICommand _closeResetCmd;

        public LocationSelectViewModel(List<int> Approved_Country_IDs)
        {
            LogManager.DebugLogManager.MethodBegin("LocationSelectViewModel");

            _locationInformationEnabled = false;

            the_Country_IDs = Approved_Country_IDs;

            LogManager.DebugLogManager.MethodEnd("LocationSelectViewModel");
        }

        /// <summary>
        /// This is thrown after the Location Select Window has finished
        /// loading.
        /// </summary>
        /// <param name="sender">Parent Window</param>
        /// <param name="e">The Loaded event Arguments</param>
        public void LocSelectWindow_Loaded(object sender, RoutedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("LocationSelectViewModel.LocSelectWindow_Loaded");

            SelectedCountry = PangeaInfo.DefaultCountry;

            SelectedLocation = PangeaInfo.DefaultLocation;

            LocationInformationEnabled = PangeaInfo.DefaultCountry != null;

            LogManager.DebugLogManager.MethodEnd("LocationSelectViewModel.LocSelectWindow_Loaded");
        }

        /// <summary>
        /// This is a custom close command. This allows us to
        /// call the close command from the view model instead
        /// of the code behind the View.
        /// </summary>
        public ICommand CloseUpdateCmd
        {
            get
            {
                return _closeUpdateCmd 
                    ?? 
                    (_closeUpdateCmd = new RelayCommand<Window>(CloseWindowWithUpdate));
            }
        }

        /// <summary>
        /// This is a custom close command. This allows us to
        /// call the close command from the view model instead
        /// of the code behind the View.
        /// </summary>
        public ICommand CloseResetCmd
        {
            get
            {
                return _closeResetCmd 
                    ?? 
                    (_closeResetCmd = new RelayCommand<Window>(CloseWindowWithReset));
            }

        }

        /// <summary>
        /// This is used to determine if the Location Combo Boxes are 
        /// enabled or not.  They are only disabled if a Country has not been
        /// Selected since we populate the Location information based on the 
        /// Country Code ID.
        /// </summary>
        private bool _locationInformationEnabled;

        public bool LocationInformationEnabled
        {
            get { return _locationInformationEnabled; }
            set
            {
                if (_locationInformationEnabled != value)
                {
                    SendPropertyChanging();

                    _locationInformationEnabled = value;

                    SendPropertyChanged("LocationInformationEnabled");
                }
            }
        }


        private List<int> the_Country_IDs = null;


        /// <summary>
        /// This is the Property used to fill in the Country ComboBox.
        /// </summary>
        public CountryCollection Countries
        {
            // TODO : Add Where clause to check for the Countries that the User has permission too.
            get 
            {

                CountryCollection a_CountryCollection = 
                    new CountryCollection(PangeaCollections.CountryCollection.Where(xx => the_Country_IDs.Contains(xx.CodeID)));
                                
                return a_CountryCollection;

            }
            
        }

        /// <summary>
        /// This is the Property used to fill in the Location ComboBoxes.
        /// The Location Collection is based off of the Country Code ID
        /// </summary>
        public LocationCollection Locations
        {
            get
            {
                if (SelectedCountry == null)
                    return null;
                
                ;

                var the_data = 
                    new LocationCollection(PangeaCollections.LocationCollection.Where(loc => loc.CountryCodeID == SelectedCountry.CodeID));

                return the_data;
            }
        }

        /// <summary>
        /// This is used to Display and Store the Default Selected Country.
        /// </summary>
        private Country _country;

        public Country SelectedCountry
        {
            get { return _country; }
            set
            {
                if (_country != value)
                {
                    SendPropertyChanging();

                    _country = value;

                    LocationInformationEnabled = value != null;

                    SendPropertyChanged("SelectedCountry");

                    SendPropertyChanged("Locations");
                }
            }
        }

        /// <summary>
        /// This is used to Display and Store the Default Selected Location.
        /// </summary>
        private Location _location;

        public Location SelectedLocation
        {
            get { return _location; }
            set
            {
                if (_location != value)
                {
                    SendPropertyChanging();

                    _location = value;

                    SendPropertyChanged("SelectedLocation");
                }
            }
        }

        /// <summary>
        /// This takes and updates the Default Country and Location
        /// with the selected values, then closes the Select Default
        /// Location Window.
        /// </summary>
        /// <param name="window">Window to be closed.</param>
        private void CloseWindowWithUpdate(Window window)
        {
            LogManager.DebugLogManager.MethodBegin("LocationSelectViewModel.CloseWindowWithUpdate");

            if (window != null)
            {
                PangeaInfo.DefaultCountry = SelectedCountry;

                PangeaInfo.DefaultLocation = SelectedLocation;

                window.DialogResult = true;

                window.Close();
            }

            LogManager.DebugLogManager.MethodEnd("LocationSelectViewModel.CloseWindowWithUpdate");
        }

        /// <summary>
        /// This Closes the Select Default Location Window 
        /// without updating the Default Location and Country
        /// information.
        /// </summary>
        /// <param name="window">Window to be closed.</param>
        private void CloseWindowWithReset(Window window)
        {
            LogManager.DebugLogManager.MethodEnd("LocationSelectViewModel.CloseWindowWithUpdate");

            if (window != null)
            {
                window.DialogResult = false;

                window.Close();
            }

            LogManager.DebugLogManager.MethodEnd("LocationSelectViewModel.CloseWindowWithUpdate");
        }

    }

}
