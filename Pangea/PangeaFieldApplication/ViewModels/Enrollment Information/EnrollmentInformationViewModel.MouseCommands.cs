﻿using Pangea.Data.Model.CodeTables;
using System;
using System.Windows.Controls;

namespace Pangea.FieldApplication.ViewModels
{
    public partial class EnrollmentInformationViewModel
    {
        private ToolTip _toolTip;

        private bool _tooltipViewable = false;

        public void FavActivityHistoryLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void FavActivityHistoryLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                String toolTipString = String.Empty;

                if (PangeaInfo.ChildHistory.FavActivity.Count > 0)
                {
                    foreach (FavoriteActivity _favAct in PangeaInfo.ChildHistory.FavActivity)
                    {
                        if (toolTipString != String.Empty)
                            toolTipString = String.Format("{0}\n", toolTipString);

                        toolTipString = String.Format("{0}{1}", toolTipString, _favAct.Name);
                    }
                }
                else
                    toolTipString = "No History";

                _toolTip = new ToolTip { Content = toolTipString };
                _toolTip.IsOpen = true;
            }
        }

        public void FavActivityQuestionLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void FavActivityQuestionLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                int _minAllowed = Convert.ToInt32(PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetMinAllowed("Favorite Activity"));
                int _maxAllowed = Convert.ToInt32(PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetMaxAllowed("Favorite Activity"));
                _toolTip = new ToolTip { Content = String.Format("Favorite Activity requires a minimum of {0} selected. \nFavorite Activity has a maximum of {1} selected.", _minAllowed, _maxAllowed) };
                _toolTip.IsOpen = true;
            }
        }

        public void LivesWithHistoryLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void LivesWithHistoryLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                String toolTipString = String.Empty;
                if (PangeaInfo.ChildHistory.Lives_With != null)
                    toolTipString = PangeaInfo.ChildHistory.Lives_With.Description.Equals(String.Empty) ? "No History" : PangeaInfo.ChildHistory.Lives_With.Description;
                else
                    toolTipString = "No History";

                _toolTip = new ToolTip { Content = toolTipString };
                _toolTip.IsOpen = true;
            }
        }

        public void LivesWithQuestionLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void LivesWithQuestionLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                _toolTip = new ToolTip { Content = "Lives with is a Required Field." };
                _toolTip.IsOpen = true;
            }
        }

        public void FavLearnHistoryLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void FavLearnHistoryLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                String toolTipString = String.Empty;
                if (PangeaInfo.ChildHistory.FavLearning != null)
                    toolTipString = PangeaInfo.ChildHistory.FavLearning.Name.Equals(String.Empty) ? "No History" : PangeaInfo.ChildHistory.FavLearning.Name;
                else
                    toolTipString = "No History";

                _toolTip = new ToolTip { Content = toolTipString };
                _toolTip.IsOpen = true;
            }
        }

        public void FavLearnQuestionLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void FavLearnQuestionLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                _toolTip = new ToolTip { Content = "Favorite Learning is a Required Field." };
                _toolTip.IsOpen = true;
            }
        }

        public void HealthHistoryLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void HealthHistoryLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                String toolTipString = String.Empty;
                if (PangeaInfo.ChildHistory.Health.Name != null)
                    toolTipString = PangeaInfo.ChildHistory.Health.Name.Equals(String.Empty) ? "No History" : PangeaInfo.ChildHistory.Health.Name;
                else
                    toolTipString = "No History";

                _toolTip = new ToolTip { Content = toolTipString };
                _toolTip.IsOpen = true;
            }
        }

        public void HealthQuestionLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void HealthQuestionLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                _toolTip = new ToolTip { Content = "Child Health is a Required Field." };
                _toolTip.IsOpen = true;
            }
        }

        public void PersonalityHistoryLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void PersonalityHistoryLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                String toolTipString = String.Empty;
                if (PangeaInfo.ChildHistory.Personality_type != null)
                    toolTipString = PangeaInfo.ChildHistory.Personality_type.Name.Equals(String.Empty) ? "No History" : PangeaInfo.ChildHistory.Personality_type.Name;
                else
                    toolTipString = "No History";

                _toolTip = new ToolTip { Content = toolTipString };
                _toolTip.IsOpen = true;
            }
        }

        public void PersonalityQuestionLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void PersonalityQuestionLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                _toolTip = new ToolTip { Content = "Child Personality is a Required Field." };
                _toolTip.IsOpen = true;
            }
        }

        public void NumSiblingsHistoryLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void NumSiblingsHistoryLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                _toolTip = new ToolTip { Content = String.Format("Number of Brothers : {0}\nNumber of Sisters : {1}", PangeaInfo.ChildHistory.NumberBrothers, PangeaInfo.ChildHistory.NumberSisters) };
                _toolTip.IsOpen = true;
            }
        }

        public void NumSiblingsQuestionLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void NumSiblingsQuestionLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                int _minAllowedB = Convert.ToInt32(PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetMinAllowed("Number of Brothers"));
                int _minAllowedS = Convert.ToInt32(PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetMinAllowed("Number of Sisters"));
                int _maxAllowedB = Convert.ToInt32(PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetMaxAllowed("Number of Brothers"));
                int _maxAllowedS = Convert.ToInt32(PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetMaxAllowed("Number of Sisters"));
                _toolTip = new ToolTip { Content = String.Format("Minimum Number of Brothers required is {0} \nMaximum Number of Brothers is {1} \nMinimum Number of Sisters required is {2} \nMaximum Number of Brothers is {3}", _minAllowedB, _maxAllowedB, _minAllowedS, _maxAllowedS) };
                _toolTip.IsOpen = true;
            }
        }

        public void ChoresHistoryLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void ChoresHistoryLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                String toolTipString = String.Empty;

                if (PangeaInfo.ChildHistory.Chores.Count > 0)
                {
                    foreach (Chore _chore in PangeaInfo.ChildHistory.Chores)
                    {
                        if (toolTipString != String.Empty)
                            toolTipString = String.Format("{0}\n", toolTipString);

                        toolTipString = String.Format("{0}{1}", toolTipString, _chore.Name);
                    }
                }
                else
                    toolTipString = "No History";

                _toolTip = new ToolTip { Content = toolTipString };
                _toolTip.IsOpen = true;
            }
        }

        public void ChoresQuestionLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void ChoresQuestionLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                int _minAllowed = Convert.ToInt32(PangeaCollections.ColumnRulesCollection.GetMinAllowed("Enrollment Chore"));
                int _maxAllowed = Convert.ToInt32(PangeaCollections.ColumnRulesCollection.GetMaxAllowed("Enrollment Chore"));
                _toolTip = new ToolTip { Content = String.Format("Chores require a minimum of {0} selected. \nChores has a maximum of {1} selected.", _minAllowed, _maxAllowed) };
                _toolTip.IsOpen = true;
            }
        }

        public void GradeHistoryLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void GradeHistoryLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                String toolTipString = String.Empty;
                if (PangeaInfo.ChildHistory.GradeLvl != null)
                    toolTipString = PangeaInfo.ChildHistory.GradeLvl.Name.Equals(String.Empty) ? "No History" : PangeaInfo.ChildHistory.GradeLvl.Name;
                else
                    toolTipString = "No History";

                _toolTip = new ToolTip { Content = toolTipString };
                _toolTip.IsOpen = true;
            }
        }

        public void GradeQuestionLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void GradeQuestionLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                _toolTip = new ToolTip { Content = "Grade Level is a Required Field." };
                _toolTip.IsOpen = true;
            }
        }

        public void DisabilityHistoryLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void DisabilityHistoryLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                String toolTipString = String.Empty;
                if (PangeaInfo.ChildHistory.HasDisability != null)
                    toolTipString = (PangeaInfo.ChildHistory.HasDisability.Value ? "Has Disability : Yes" : "Has Disability No");
                else
                    toolTipString = "No History";

                _toolTip = new ToolTip { Content = toolTipString };
                _toolTip.IsOpen = true;
            }
        }

        public void DisabilityQuestionLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void DisabilityQuestionLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                _toolTip = new ToolTip { Content = "Disability is a Required Field." };
                _toolTip.IsOpen = true;
            }
        }

        public void MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            _tooltipViewable = true;
        }

        public void MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            _tooltipViewable = false;
            if (_toolTip != null)
            {
                if (_toolTip.IsOpen)
                    _toolTip.IsOpen = false;
            }
        }
    }
}
