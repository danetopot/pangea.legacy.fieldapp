﻿using System.Windows;

namespace Pangea.FieldApplication.ViewModels
{
    /// <summary>
    /// This Class is a Partial class to pull out the Visibilty
    /// Parts of the Fields.
    /// I did this to try and keep the ChildINformationViewModel class
    /// wouldn't get to large and We could seperate out some of the 
    /// functionality.
    /// </summary>
    public partial class ChildInformationViewModel
    {
        /// <summary>
        /// If we should show the Child ID under the Profile Picture on the Child Information Screen.
        /// </summary>
        public Visibility ChildIDVis
        {
            get
            {
                if (PangeaInfo.UpdatingChild)
                    return Visibility.Collapsed;

                return Visibility.Visible;
            }
        }

        /// <summary>
        /// If we should show the Child Number under the Profile Picture on the Child Information Screen.
        /// </summary>
        public Visibility ChildNumberVis
        {
            get
            {
                if (PangeaInfo.EnrollingChild)
                    return Visibility.Collapsed;

                return Visibility.Visible;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility DOBAstrikVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Age Range in days") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Age Range in days"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// This is used to determine if the Date of Birth
        /// textbox is to be visable or hidden 
        /// depending on if Year only is selected or not.
        /// </summary>
        private Visibility _dobStrVis;
        public Visibility DobStrVisibility
        {
            get { return _dobStrVis; }
            set
            {
                if (_dobStrVis != value)
                {
                    SendPropertyChanging();
                    _dobStrVis = value;
                    SendPropertyChanged("DobStrVisibility");
                }
            }
        }

        /// <summary>
        /// This is used to determine if the Date of Birth
        /// Selection Calendar is to be visable or hidden 
        /// depending on if Year only is selected or not.
        /// </summary>
        private Visibility _dobVis;
        public Visibility DobVisibility
        {
            get { return _dobVis; }
            set
            {
                if (_dobVis != value)
                {
                    SendPropertyChanging();
                    _dobVis = value;
                    SendPropertyChanged("DobVisibility");
                }
            }
        }

        /// <summary>
        /// Determine if the Question mark should be Displayed next to a field.
        /// </summary>
        public Visibility DOBQuestionVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Age Range in days") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Age Range in days"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility CoutryAstrikVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Location") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Location"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determine if the Question mark should be Displayed next to a field.
        /// </summary>
        public Visibility CountryQuestionVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Location") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Location"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility LocAstrikVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Location") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Location"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determine if the Bookmark should be Displayed next to a field.
        /// </summary>
        public Visibility LocNameQuestionVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Location") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Location"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determine if the Question mark should be Displayed next to a field.
        /// </summary>
        public Visibility LocCodeQuestionVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Location") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Location"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility FNameAstrikVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("First Name") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("First Name"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determine if the Question mark should be Displayed next to a field.
        /// </summary>
        public Visibility FNameQuestionVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("First Name") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("First Name"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility LNameAstrikVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Last Name") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Last Name"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determine if the Question mark should be Displayed next to a field.
        /// </summary>
        public Visibility LNameQuestionVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Last Name") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Last Name"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility GenderAstrikVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Gender") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Gender"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determine if the Question mark should be Displayed next to a field.
        /// </summary>
        public Visibility GenderQuestionVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Gender") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Gender"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility MNameAstrikVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Middle Name") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Middle Name"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determine if the Question mark should be Displayed next to a field.
        /// </summary>
        public Visibility MNameQuestionVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Middle Name") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Middle Name"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility NNameAstrikVis
        {
            get
            {
                if 
                    (
                    PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Other Name") && 
                    PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Other Name")
                    )
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determine if the Question mark should be Displayed next to a field.
        /// </summary>
        public Visibility NNameQuestionVis
        {
            get
            {
                if 
                    (
                    PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Other Name") && 
                    PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Other Name")
                    )
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }
    }
}
