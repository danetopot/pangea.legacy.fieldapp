﻿/////////////////////////////////////////////////////////////////////////
/// View Model for the Enrollment Information Control.
/// The data seen on the screen is populated by this View Model and 
/// the data changed on the screen is saved to the Buisness Objects
/// though the different properties and functions.
////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

using Pangea.Data.Collections;
using Pangea.Data.Model.CodeTables;
using Pangea.FieldApplication.Views;
using Pangea.Utilities.Base.Commands;
using Pangea.Utilities.Tools;

using Xceed.Wpf.Toolkit;

namespace Pangea.FieldApplication.ViewModels
{
    public partial class EnrollmentInformationViewModel : PangeaBaseViewModel
    {
        public EnrollmentInformationViewModel()
        {
            PangeaInfo.Child.PropertyChanged += Child_PropertyChanged;
        }

        /// <summary>
        /// The Image used for the History Icon
        /// </summary>
        public ImageSource HistoryImage
        {
            get { return DefaultImages.OpenBook; }
        }

        public String InformationLbl
        {
            get
            {
                if (PangeaInfo.EnrollingChild)
                    return PangeaInfo.UILanguageDefMapping.GetTextValue("EnrollmentInfoLbl");

                return PangeaInfo.UILanguageDefMapping.GetTextValue("UpdateInfoLbl");
            }
        }

        /// <summary>
        /// This is Used to Display the Child First Name
        /// as a read only value on the Enrollment Screen.
        /// </summary>
        public string FirstName
        {
            get { return PangeaInfo.Child.FirstName; }
        }

        /// <summary>
        /// This is Used to Display the Child Last Name
        /// as a read only value on the Enrollment Screen.
        /// </summary>
        public string LastName
        {
            get { return PangeaInfo.Child.LastName; }
        }

        /// <summary>
        /// This is Used to Display the User who is creating/reated
        /// as a read only value on the Enrollment Screen.
        /// </summary>
        public string CreatedByUserName
        {
            get { return PangeaInfo.User.UserName; }
        }

        /// <summary>
        /// This is Used to Display the Creating/Created By Date
        /// as a read only value on the Enrollment Screen.
        /// </summary>
        public string CreatedByDate
        {
            // TODO : Need to move this to the child info class.
            get { return DateTime.Today.Date.ToString("d"); }
        }

        /// <summary>
        /// This is used to display the child number as a read only value on the Enrollment Screen
        /// </summary>
        public string ChildNumber
        {
            get { return PangeaInfo.Child.ChildNumber != null ? PangeaInfo.Child.ChildNumber.ToString() : String.Empty; }
        }

        /// <summary>
        /// Used to populate the Favorite Learning Combo Box Values.
        /// </summary>
        public FavoriteLearningCollection FavoriteLearnings
        {
            get { return PangeaCollections.FavoriteLearningCollection.RestrictByAge(PangeaInfo.Child.ChildAge); }
        }

        /// <summary>
        /// Used to Display and Update the Favorite Learning Value
        /// </summary>
        public FavoriteLearning FavLearning
        {
            get { return PangeaInfo.Child.FavLearning; }
            set { PangeaInfo.Child.FavLearning = value; }
        }

        /// <summary>
        /// Used to populate the Grade Level Combo Box Values.
        /// </summary>
        public GradeLevelCollection GradeLevels
        {
            get { return (PangeaInfo.Child.ChildCountry != null && PangeaInfo.Child.ChildAge >= PangeaInfo.Child.ChildCountry.MinSchoolAge) ? PangeaCollections.GradeLevelCollection.RestrictByAge(PangeaInfo.Child.ChildAge).Without(PangeaCollections.GradeLevelCollection.Find("Too Young for School")) : PangeaCollections.GradeLevelCollection.RestrictByAge(PangeaInfo.Child.ChildAge); }
        }

        /// <summary>
        /// Used to Display and Update the Grade Level Value
        /// </summary>
        /// 
        public GradeLevel GradeLvl
        {
            get
            {
                // Bug 266 - Made it so that the Grade Level of the Child is always in the current list of Grades
                if (PangeaInfo.Child.ChildCountry != null)
                {
                    if (PangeaInfo.Child.ChildAge <= PangeaInfo.Child.ChildCountry.MinSchoolAge)
                    {
                        if (PangeaInfo.Child.GradeLvl != GradeLevels.Find("Too Young for School"))
                            PangeaInfo.Child.GradeLvl = GradeLevels.Find("Too Young for School");
                    }
                    else if (PangeaInfo.Child.ChildAge > PangeaInfo.Child.ChildCountry.MinSchoolAge)
                    {
                        if (PangeaInfo.Child.GradeLvl == GradeLevels.Find("Too Young for School"))
                            PangeaInfo.Child.GradeLvl = null;
                    }
                }

                return PangeaInfo.Child.GradeLvl;
            }
            set { PangeaInfo.Child.GradeLvl = value; }
        }

        /// <summary>
        /// Used to populate the Personality Types Combo Box Values.
        /// </summary>
        public PersonalityTypeCollection PersonalityTypes
        {
            get { return PangeaCollections.PersonalityTypeCollection; }
        }

        /// <summary>
        /// Used to Display and Update the Personality Type Value
        /// </summary>
        /// 
        public PersonalityType Personality_Type
        {
            get { return PangeaInfo.Child.Personality_type; }
            set { PangeaInfo.Child.Personality_type = value; }
        }

        /// <summary>
        /// Used to populate the Lives With Combo Box Values.
        /// </summary>
        public LivesWithCollection LivesWithCol
        {
            get { return PangeaCollections.LivesWithCollection; }
        }

        /// <summary>
        /// Used to Display and Update the Lives With Value
        /// </summary>
        /// 
        public LivesWith Lives_With
        {
            get { return PangeaInfo.Child.Lives_With; }
            set { PangeaInfo.Child.Lives_With = value; }
        }

        /// <summary>
        /// Used to populate the Health Status Combo Box Values.
        /// </summary>
        public HealthStatusCollection HealthStatusCol
        {
            get { return PangeaCollections.HealthStatusCollection; }
        }

        /// <summary>
        /// Used to Display and Update the Health Status Value
        /// </summary>
        public HealthStatus Health
        {
            get { return PangeaInfo.Child.Health; }
            set { PangeaInfo.Child.Health = value; }
        }

        /// <summary>
        /// Used to Enable and Disable the Chores Combo Box 
        /// determined by the number of Chores selected.
        /// </summary>
        public bool ChoresEnabled 
            => ChoresCol.Count > 0 
            ? 
            PangeaInfo.Child.Chores.Count < Convert.ToInt32
            (
                PangeaCollections
                .ColumnRulesCollection
                .GetRulesByModule(PangeaInfo.EnrollingChild)
                .GetMaxAllowed("Chore Range")) 
            : 
            false;


        /// <summary>
        /// Set if the child is too young for chores and its removed from there selection.
        /// This is so that the Too Young for Chores doesnt keep getting added back to the list
        /// if it is removed by the user.
        /// </summary>
        private bool _tooYoungForChoresRemoved = false;

        /// <summary>
        /// Used to populate the Chores Combo Box Values.
        /// </summary>
        public ChoresCollection ChoresCol
        {
            get
            {
                ;

                ChoresCollection the_return_data = null;

                ;

                /*
                bool Is_Required = PangeaCollections
                    .ColumnRulesCollection
                    .GetRulesByModule(PangeaInfo.EnrollingChild)
                    .GetRequired("Too Young for Chores");

                ;

                if (Is_Required)
                {

                    bool Is_Too_Young_For_Chores_Not_Shown = PangeaCollections
                        .ColumnRulesCollection
                        .GetRulesByModule(PangeaInfo.EnrollingChild)
                        .GetMaxAllowed("Too Young for Chores") < PangeaInfo.Child.ChildAge;

                    if (Is_Too_Young_For_Chores_Not_Shown)
                    {

                        the_return_data = PangeaCollections
                            .ChoresCollection
                            .RestrictByAge(PangeaInfo.Child.ChildAge)
                            .Without(PangeaInfo.Child.Chores)
                            .Without(PangeaCollections.ChoresCollection.Find("Too Young for Chores"));

                        return the_return_data;

                    }
                }
                */

                the_return_data = PangeaCollections
                    .ChoresCollection
                    .RestrictByAge(PangeaInfo.Child.ChildAge)
                    .Without(PangeaInfo.Child.Chores);

                return the_return_data;

            }

        }


        /// <summary>
        /// Used to Display and Update the Chores Value
        /// </summary>
        public ObservableCollection<StackPanel> Chores
        {
            get
            {
                LogManager.DebugLogManager.MethodBegin("EnrollmentInformationViewModel.Chores");

                bool Get_Required_TYFC = 
                    PangeaCollections
                    .ColumnRulesCollection
                    .GetRulesByModule(PangeaInfo.EnrollingChild)
                    .GetRequired("Too Young for Chores");

                // Bug 266 - reorganized the If statement to make the Too young for chores only the Default Value and 
                // not the only value.
                if (Get_Required_TYFC)
                {

                    int maxAgeTooYoung = 
                        Convert.ToInt32
                        (
                            PangeaCollections
                            .ColumnRulesCollection
                            .GetRulesByModule
                            (PangeaInfo.EnrollingChild)
                            .GetMaxAllowed("Too Young for Chores")
                            );

                    ;

                    /*
                    if (PangeaInfo.Child.ChildAge <= maxAgeTooYoung)
                    {
                        if (!(PangeaInfo.Child.Chores.Count > 0) && !_tooYoungForChoresRemoved)
                        {

                            PangeaInfo.Child.Chores.Add(ChoresCol.Find("Too Young for Chores"));

                            SendPropertyChanged("ChoresCol");

                            SendPropertyChanged("ChoresEnabled");

                        }
                    }
                    else if (PangeaInfo.Child.ChildAge > maxAgeTooYoung)
                    {

                        bool Can_Find_Too_Young_for_Chores = PangeaInfo
                            .Child
                            .Chores
                            .Contains
                            (PangeaCollections.ChoresCollection.Find("Too Young for Chores"));

                        if (Can_Find_Too_Young_for_Chores)
                        {

                            PangeaInfo
                                .Child
                                .Chores
                                .Remove(PangeaCollections.ChoresCollection
                                .Find("Too Young for Chores"));

                            SendPropertyChanged("ChoresCol");

                            SendPropertyChanged("ChoresEnabled");

                        }
                    }
                    */
                }

                ObservableCollection<StackPanel> _chores = new ObservableCollection<StackPanel>();

                foreach (Chore _c in PangeaInfo.Child.Chores)
                {
                    if (_c == null)
                        continue;

                    StackPanel tempSP = new StackPanel();
                    tempSP.Orientation = Orientation.Horizontal;

                    TextBlock tempT = new TextBlock();
                    tempT.Text = _c.Name;
                    tempT.FontSize = 10;
                    tempT.Height = 30;

                    Image tempButtonImage = new Image();
                    tempButtonImage.Source = DefaultImages.TrashImageSrc;

                    Button tempB = new Button();
                    tempB.Content = tempButtonImage;
                    tempB.BorderBrush = Brushes.Transparent;
                    tempB.Background = Brushes.Transparent;
                    tempB.Command = ChoreTrashButtonClicked;
                    tempB.CommandParameter = _c;
                    tempB.Width = 30;
                    tempB.Height = 30;

                    tempSP.Children.Add(tempT);
                    tempSP.Children.Add(tempB);

                    _chores.Add(tempSP);
                }

                _chores = new ObservableCollection<StackPanel>
                    (
                    _chores
                    .OrderBy
                    (ch => ((TextBlock)ch.Children[0]).Text.Length)
                    );

                LogManager.DebugLogManager.MethodEnd("EnrollmentInformationViewModel.Chores");

                return _chores;
            }
        }

        /// <summary>
        /// This is a custom Button Click command. This allows us to
        /// call the button click command from the view model instead
        /// of the code behind the View.
        /// </summary>
        private ICommand _choreTrashButtonClicked;
        public ICommand ChoreTrashButtonClicked
        {
            get
            {
                return _choreTrashButtonClicked ?? (_choreTrashButtonClicked = new RelayCommand<Chore>(RemoveChoreItem));
            }
        }

        /// <summary>
        /// When the Trash Can is clicked in the list, this is called to remove
        /// the item associated with the button press.
        /// We then tell the view to update the Control
        /// </summary>
        /// <param name="item"></param>
        private void RemoveChoreItem(Chore item)
        {
            LogManager.DebugLogManager.MethodBegin("EnrollmentInformationViewModel.RemoveChoreItem");

            if (item.Name.Equals("Too Young for Chores"))
                _tooYoungForChoresRemoved = true;

            PangeaInfo.Child.Chores.Remove(item);
            SendPropertyChanged("Chores");
            SendPropertyChanged("ChoresCol");
            SendPropertyChanged("ChoresEnabled");
            PangeaInfo.SaveRequired = true; // Bug 334

            LogManager.DebugLogManager.MethodEnd("EnrollmentInformationViewModel.RemoveChoreItem");
        }

        /// <summary>
        /// This is called when the Selection of the Combo Box changes. 
        /// Since we are not using the combobox to show what was selected
        /// we are resetting it to -1 in order to clear the combo box.
        /// </summary>
        /// <param name="sender">The ComboBox</param>
        /// <param name="e">The Selection Changed Arguments</param>
        public void ChoresCBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("EnrollmentInformationViewModel.ChoresCBox_SelectionChanged");

            foreach (Chore _c in e.AddedItems)
            {
                PangeaInfo.Child.Chores.Add(_c);
            }

            ;

            int num_chores =
                PangeaInfo
                .Child
                .Chores
                .Count;

            bool Chores_Include_Too_Young_for_Chores = 
                PangeaInfo
                .Child
                .Chores
                .Any
                (
                    xx => xx.Description.ToLower() == "too young for chores"
                    );

            ;

            //
            // HANDLE TOO YOUNG FOR CHORES AS ONLY ENTRY WITHIN COMBOBOX
            //
            if 
                (
                num_chores > 1 &&
                Chores_Include_Too_Young_for_Chores 
                )
            {

                string smsg = "Sorry. You can only have \"Too Young For Chores\" selected as the only chore. " +
                    "So we are deleting \"Too Young For Chores\". If you'd like \"Too Young For Chores\" as the designated chore, " +
                    "please delete all chores and readd \"Too Young For Chores\".";

                Xceed.Wpf.Toolkit.MessageBox.Show(smsg);

                Chore tyfc =
                    PangeaInfo
                    .Child
                    .Chores
                    .Where
                    (
                        xx => xx.Description.ToLower() == "too young for chores"
                        )
                    .First();

                PangeaInfo.Child.Chores.Remove(tyfc);

            }

            ((WatermarkComboBox)sender).SelectedIndex = -1;
            ((WatermarkComboBox)sender).MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
            SendPropertyChanged("Chores");
            SendPropertyChanged("ChoresCol");
            SendPropertyChanged("ChoresEnabled");
            PangeaInfo.SaveRequired = true;// Bug 334

            LogManager.DebugLogManager.MethodEnd("EnrollmentInformationViewModel.ChoresCBox_SelectionChanged");
        }


        /// <summary>
        /// Used to Enable and Disable the Favorite Activities Combo Box 
        /// determined by the number of Favorite Activities selected.
        /// </summary>
        public bool FavActEnabled => Convert.ToInt32(PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetMaxAllowed("Favorite Activity")) != int.MinValue ? PangeaInfo.Child.FavActivity.Count < Convert.ToInt32(PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetMaxAllowed("Favorite Activity")) : true;

        /// <summary>
        /// Used to populate the Favorite Activities Combo Box Values.
        /// </summary>
        public FavoriteActivityCollection FavoriteActivityCol
        {
            // Bug 266 - Added Restrict By Age to Favorite Activity
            get { return PangeaCollections.FavoriteActivityCollection.RestrictByAge(PangeaInfo.Child.ChildAge).Without(PangeaInfo.Child.FavActivity); }
        }

        /// <summary>
        /// Used to Display and Update the Favorite Activities Value
        /// </summary>
        public ObservableCollection<StackPanel> FavoriteActivities
        {
            get
            {
                LogManager.DebugLogManager.MethodBegin("EnrollmentInformationViewModel.FavoriteActivities");

                ObservableCollection<StackPanel> _fActivities = new ObservableCollection<StackPanel>();

                foreach (FavoriteActivity _fa in PangeaInfo.Child.FavActivity)
                {
                    StackPanel tempSP = new StackPanel();
                    tempSP.Orientation = Orientation.Horizontal;

                    TextBlock tempT = new TextBlock();
                    tempT.Text = _fa.Name;
                    tempT.FontSize = 10;
                    tempT.Height = 30;

                    Image tempButtonImage = new Image();
                    tempButtonImage.Source = DefaultImages.TrashImageSrc;

                    Button tempB = new Button();
                    tempB.Content = tempButtonImage;
                    tempB.BorderBrush = Brushes.Transparent;
                    tempB.Background = Brushes.Transparent;
                    tempB.Command = FavoriteActivityTrashButtonClicked;
                    tempB.CommandParameter = _fa;
                    tempB.Width = 30;
                    tempB.Height = 30;

                    tempSP.Children.Add(tempT);
                    tempSP.Children.Add(tempB);

                    _fActivities.Add(tempSP);
                }

                _fActivities = new ObservableCollection<StackPanel>(_fActivities.OrderBy(fa => ((TextBlock)fa.Children[0]).Text.Length));
                LogManager.DebugLogManager.MethodEnd("EnrollmentInformationViewModel.FavoriteActivities");

                return _fActivities;
            }
        }

        /// <summary>
        /// This is a custom Button Click command. This allows us to
        /// call the button click command from the view model instead
        /// of the code behind the View.
        /// </summary>
        private ICommand _favoriteActivityTrashButtonClicked;
        public ICommand FavoriteActivityTrashButtonClicked
        {
            get
            {
                return _favoriteActivityTrashButtonClicked ?? (_favoriteActivityTrashButtonClicked = new RelayCommand<FavoriteActivity>(RemoveFavoriteActivityItem));
            }
        }

        /// <summary>
        /// When the Trash Can is clicked in the list, this is called to remove
        /// the item associated with the button press.
        /// We then tell the view to update the Control
        /// </summary>
        /// <param name="item"></param>
        private void RemoveFavoriteActivityItem(FavoriteActivity item)
        {
            LogManager.DebugLogManager.MethodBegin("EnrollmentInformationViewModel.RemoveFavoriteActivityItem");

            PangeaInfo.Child.FavActivity.Remove(item);
            SendPropertyChanged("FavoriteActivities");
            SendPropertyChanged("FavoriteActivityCol");
            SendPropertyChanged("FavActEnabled");
            PangeaInfo.SaveRequired = true;// Bug 334

            LogManager.DebugLogManager.MethodEnd("EnrollmentInformationViewModel.RemoveFavoriteActivityItem");
        }

        /// <summary>
        /// This is called when the Selection of the Combo Box changes. 
        /// Since we are not using the combobox to show what was selected
        /// we are resetting it to -1 in order to clear the combo box.
        /// </summary>
        /// <param name="sender">The ComboBox</param>
        /// <param name="e">The Selection Changed Arguments</param>
        public void FavActivCBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("EnrollmentInformationViewModel.FavActivCBox_SelectionChanged");

            foreach (FavoriteActivity _fa in e.AddedItems)
            {
                PangeaInfo.Child.FavActivity.Add(_fa);
            }

            ((ComboBox)sender).SelectedIndex = -1;
            ((ComboBox)sender).MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
            SendPropertyChanged("FavoriteActivities");
            SendPropertyChanged("FavoriteActivityCol");
            SendPropertyChanged("FavActEnabled");
            PangeaInfo.SaveRequired = true;// Bug 334

            LogManager.DebugLogManager.MethodEnd("EnrollmentInformationViewModel.FavActivCBox_SelectionChanged");
        }

        /// <summary>
        /// This is used to populate the ComboBox for the 
        /// disabilities information.
        /// </summary>
        private ObservableCollection<string> _disabilities;
        public IQueryable<string> Disabilities
        {
            get
            {
                if (_disabilities == null)
                {
                    _disabilities = new ObservableCollection<string>();
                    _disabilities.Add("Yes");
                    _disabilities.Add("No");
                }
                return _disabilities.AsQueryable();
            }
        }
        /// <summary>
        /// This is the Value used by the View to display and update
        /// whether the child is disabled or not.
        /// This Property converts the text values of yes and no to the
        /// corresponding bool value.  Yes = True / No = False
        /// </summary>
        public string Disability
        {
            get
            {
                string retStr = null;

                if (PangeaInfo.Child.HasDisability.HasValue)
                    retStr = PangeaInfo.Child.HasDisability.Value ? "Yes" : "No";

                return retStr;
            }
            set
            {
                if (PangeaInfo.Child.HasDisability != String.Equals(value, "Yes"))
                    PangeaInfo.Child.HasDisability = String.Equals(value, "Yes");
            }
        }
        /// <summary>
        /// This is the Max Number of Siblings allowed.
        /// The value will be gotten from the config for now 
        /// so it can be changed easily in the future.
        /// </summary>
        public int MaxNumberBrothers
        {
            get { return Convert.ToInt32(PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetMaxAllowed("Number of Brothers")); }
        }
        /// <summary>
        /// This is the Min Number of Siblings allowed.
        /// The value will be gotten from the config for now 
        /// so it can be changed easily in the future.
        /// </summary>
        public int MinNumberBrothers
        {
            get { return Convert.ToInt32(PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetMinAllowed("Number of Brothers")); }
        }
        /// <summary>
        /// This is the Max Number of Siblings allowed.
        /// The value will be gotten from the config for now 
        /// so it can be changed easily in the future.
        /// </summary>
        public int MaxNumberSisters
        {
            get { return Convert.ToInt32(PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetMaxAllowed("Number of Sisters")); }
        }
        /// <summary>
        /// This is the Min Number of Siblings allowed.
        /// The value will be gotten from the config for now 
        /// so it can be changed easily in the future.
        /// </summary>
        public int MinNumberSisters
        {
            get { return Convert.ToInt32(PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetMinAllowed("Number of Sisters")); }
        }
        /// <summary>
        /// This is the Value used by the View to display and update
        /// the number of brothers the Child has.
        /// </summary>
        public int NumberOfBrothers
        {
            
            get 
            { 
                return PangeaInfo.Child.NumberBrothers; 
            }

            set 
            { 
                PangeaInfo.Child.NumberBrothers = value; 
            }
        }
        /// <summary>
        /// This is the Value used by the view to display and Update
        /// the number of sisters the Child has.
        /// </summary>
        public int NumberOfSisters
        {
            get { return PangeaInfo.Child.NumberSisters; }
            set { PangeaInfo.Child.NumberSisters = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void MajorLifeBtn_Click(object sender, RoutedEventArgs e)
        {
            MajorLifeEventsWindow _mlew = new MajorLifeEventsWindow();
            _mlew.ShowDialog();
        }


        public string MajorLifeEvent
        {

            get
            {
                ;

                return PangeaInfo.Child.MajorLifeEvent;
            }

            set
            {
                ;

                PangeaInfo.Child.MajorLifeEvent = value;
            }
        }



        /// <summary>
        /// This listens to the ChildInfo Class for Changes in its Properties.  It then throws a Property
        /// Changed event for the Corresponding View Model Property so that the changes will be reflectted
        /// to the user on the screen.
        /// </summary>
        /// <param name="sender">Child Info Class</param>
        /// <param name="e">The Property Changed Event Information.  Includes the Name of the Property that
        /// Threw the Event. </param>
        private void Child_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("EnrollmentInformationViewModel.Child_PropertyChanged");

            switch (e.PropertyName)
            {
                case "FirstName":
                    SendPropertyChanged("FirstName");
                    break;
                case "LastName":
                    SendPropertyChanged("LastName");
                    break;
                case "DateOfBirth":
                    SendPropertyChanged("Chores");
                    SendPropertyChanged("ChoresCol");
                    SendPropertyChanged("ChoresEnabled");
                    SendPropertyChanged("ChoresAstrikVis");
                    break;
                case "FavLearning":
                    SendPropertyChanged("FavLearning");
                    SendPropertyChanged("FavLearnAstrikVis");
                    break;
                case "GradeLvl":
                    SendPropertyChanged("GradeLvl");
                    SendPropertyChanged("SchoolGradeAstrikVis");
                    break;
                case "Personality_Type":
                    SendPropertyChanged("Personality_Type");
                    SendPropertyChanged("PersonalityAstrikVis");
                    break;
                case "Lives_With":
                    SendPropertyChanged("Lives_With");
                    SendPropertyChanged("FamSitAstrikVis");
                    break;
                case "Health":
                    SendPropertyChanged("Health");
                    SendPropertyChanged("HealthAstrikVis");
                    break;
                case "Chores":
                    SendPropertyChanged("Chores");
                    SendPropertyChanged("ChoresCol");
                    SendPropertyChanged("ChoresEnabled");
                    SendPropertyChanged("ChoresAstrikVis");
                    _tooYoungForChoresRemoved = false;
                    break;
                case "FavActivity":
                    SendPropertyChanged("FavoriteActivities");
                    SendPropertyChanged("FavoriteActivityCol");
                    SendPropertyChanged("FavActEnabled");
                    SendPropertyChanged("FavActivAstrikVis");
                    break;
                case "HasDisability":
                    SendPropertyChanged("Disablility");
                    SendPropertyChanged("DisabilityAstrikVis");
                    break;
                case "NumberBrothers":
                    SendPropertyChanged("NumberOfBrothers");
                    break;
                case "NumberSisters":
                    SendPropertyChanged("NumberOfSisters");
                    break;
            }

            LogManager.DebugLogManager.MethodEnd("EnrollmentInformationViewModel.Child_PropertyChanged");
        }
    }
}
