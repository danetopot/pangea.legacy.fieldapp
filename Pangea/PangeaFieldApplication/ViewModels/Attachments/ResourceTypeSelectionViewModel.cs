﻿
using Microsoft.Win32;

using Pangea.Data.Collections;
using Pangea.Data.Model;
using Pangea.Data.Model.CodeTables;
using Pangea.FieldApplication.Views;
using Pangea.Utilities.Base.Commands;
using Pangea.Utilities.Tools;

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Pangea.FieldApplication.ViewModels
{
    public class ResourceTypeSelectionViewModel : PangeaBaseViewModel
    {
        private OpenFileDialog _resourceOFD;

        private String _resourceFileLoc;

        private String _resourceFileName;

        private bool _optionalFiles;

        private FileType _selFileType;

        public ResourceTypeSelectionViewModel(bool optionalFiles)
        {
            _resourceFileLoc = String.Empty;

            _resourceFileName = String.Empty;

            _resourceOFD = new OpenFileDialog();

            _resourceOFD.Filter = "PDF Files (*.PDF)|*.PDF|JPG Files (*.JPG)|*.JPG";

            _resourceOFD.RestoreDirectory = true;

            _resourceOFD.Multiselect = false;

            _optionalFiles = optionalFiles;

            _allowCropping = true;
        }

        private Visibility _imageVisible;
        public Visibility ImageVisible
        {
            get { return _imageVisible; }
            set
            {
                if (_imageVisible != value)
                {
                    SendPropertyChanging();

                    _imageVisible = value;

                    SendPropertyChanged("ImageVisible");
                }
            }
        }

        private Visibility _cropImageVisible;
        public Visibility CropImageVisible
        {
            get { return _cropImageVisible; }
            set
            {
                if (_cropImageVisible != value)
                {
                    SendPropertyChanging();

                    _cropImageVisible = value;

                    SendPropertyChanged("CropImageVisible");
                }
            }
        }

        private bool _allowCropping;
        public bool AllowCropping
        {
            get { return _allowCropping; }
            set
            {
                if (_allowCropping != value)
                {
                    SendPropertyChanging();

                    _allowCropping = value;

                    SendPropertyChanged("AllowCropping");
                }
            }
        }

        /// <summary>
        /// Location of the Image File.
        /// </summary>
        public String ImageSource
        {
            get
            {
                String _imgSrc = null;

                try
                {
                    if (ImageSelected)
                        _imgSrc = _resourceFileLoc;
                }
                catch (Exception ex)
                {
                    LogManager.ErrorLogManager.WriteError("ResourceTypeSelectionViewModel.ImageSrc", ex);
#if DEBUG
                    MessageBox.Show(ex.Message, "ResourceTypeSelectionViewModel.ImageSrc");
#endif
                }

                return _imgSrc;
            }
        }

        /// <summary>
        /// This is the memory location of the Updated Image.
        /// </summary>
        private MemoryStream _imageSaveSrc;
        public MemoryStream ImageSaveSrc
        {
            get { return _imageSaveSrc; }
            set
            {
                if (_imageSaveSrc != value)
                {
                    SendPropertyChanging();

                    _imageSaveSrc = value;

                    SendPropertyChanged("ImageSaveSrc");

                    SendPropertyChanged("CroppedImage");
                }
            }
        }

        /// <summary>
        /// This is the Cropped Image.
        /// This is used to display what they are going to
        /// save as the image.
        /// </summary>
        public ImageSource CroppedImage
        {
            get
            {
                BitmapImage _croppedImage = null;
                try
                {
                    if (ImageSelected)
                    {
                        if (ImageSaveSrc != null)
                        {
                            _croppedImage = new BitmapImage();

                            ImageSaveSrc.Seek(0, SeekOrigin.Begin);

                            _croppedImage.BeginInit();

                            _croppedImage.StreamSource = ImageSaveSrc;

                            _croppedImage.EndInit();
                        }
                    }
                    else if (DocumentSelected)
                        return DefaultImages.DocumentImageSrc;
                }
                catch (Exception ex)
                {
                    LogManager.ErrorLogManager.WriteError("ResourceTypeSelectionViewModel.CroppedImage", ex);

                    _croppedImage = null;
#if DEBUG
                    MessageBox.Show(ex.Message, "ResourceTypeSelectionViewModel.CroppedImage");
#endif
                }

                return _croppedImage;
            }
        }

        /// <summary>
        /// This is the File Name for the Current Resource.
        /// Is Dispalyed on the screen so thay can make sure
        /// its the correct file.
        /// </summary>
        public String ResourceFileLocation
        {
            get 
            { 
                return _resourceFileLoc; 
            }
        }

        /// <summary>
        /// The File Types that can be selected for import
        /// </summary>
        private ContentTypeCollection _fileTypeCol;
        public ContentTypeCollection FileTypeCol
        {
            get
            {
                if (_fileTypeCol == null)
                    _fileTypeCol = _optionalFiles 
                        ?
                        new ContentTypeCollection(PangeaCollections.ContentTypeCollection.Where(ct => ct.Required == false)) 
                        : 
                        new ContentTypeCollection(PangeaCollections.ContentTypeCollection.Where(ct => ct.Required == true));

                // Bug 371 - Removing the Consent form if we are updating a child.
                if (PangeaInfo.UpdatingChild)
                {
                    if (_fileTypeCol.Contains(PangeaCollections.ContentTypeCollection.Find("Consent")))
                        _fileTypeCol.Remove(PangeaCollections.ContentTypeCollection.Find("Consent"));
                }

                // Bug 371 - Removing the Files already added from the list.
                foreach (PangeaFileStruct pfs in PangeaInfo.Child.AdditionalResources.GetOptionalFiles(_optionalFiles))
                {
                    if (_fileTypeCol.Contains(pfs.ContentTypeInfo))
                        _fileTypeCol.Remove(pfs.ContentTypeInfo);
                }

                return _fileTypeCol;
            }
        }

        /// <summary>
        /// The Selected File Type.
        /// </summary>
        private ContentType _selContentType;
        public ContentType SelContentType
        {
            get 
            { 
                return _selContentType; 
            }
            set
            {
                if (_selContentType != value)
                {
                    SendPropertyChanging();

                    _selContentType = value;

                    if (_selContentType.Name.ToLower().Equals("consent"))
                        _selFileType = PangeaCollections.FileTypeCollection.Find("pdf");
                    else
                        _selFileType = PangeaCollections.FileTypeCollection.Find("jpg");

                    SendPropertyChanged("SelFileType");

                    SendPropertyChanged("ImageSaveSrc");

                    SendPropertyChanged("CroppedImage");

                    SendPropertyChanged("FileLoadBtnEnabled");
                }
            }
        }

        /// <summary>
        /// The Resource file that is being loaded is the Consent Form.
        /// </summary>
        public bool DocumentSelected
        {
            get { 

                return _selFileType != null 
                    ? 
                    PangeaCollections.FileTypeCollection.Find(_selFileType).Name.ToLower().Equals("pdf") 
                    : 
                    false; 
            }
        }

        /// <summary>
        /// The Resource file that is being loaded is the Profile Picture.
        /// </summary>
        public bool ImageSelected
        {
            get 
            { 
                return _selFileType != null 
                    ? 
                    PangeaCollections.FileTypeCollection.Find(_selFileType).Name.ToLower().Equals("jpg") 
                    : 
                    false; 
            }
        }

        /// <summary>
        /// Enables the Files Load Button to be Clicked.
        /// </summary>
        public bool FileLoadBtnEnabled
        {
            get 
            { 
                return SelContentType != null; 
            }
        }

        /// <summary>
        /// This is a custom close command. This allows us to
        /// call the close command from the view model instead
        /// of the code behind the View.
        /// </summary>
        private ICommand _closeAddAdditionalResource;
        public ICommand CloseAddAdditionalResource
        {
            get
            {
                return 
                    _closeAddAdditionalResource 
                    ?? 
                    (_closeAddAdditionalResource = new RelayCommand<Window>(CloseWindowAddAdditionalResource));
            }
        }

        /// <summary>
        /// This is a custom close command. This allows us to
        /// call the close command from the view model instead
        /// of the code behind the View.
        /// </summary> 
        private ICommand _closeCancelAdditionalResourceAdd;
        public ICommand CloseCancelAdditionalResourceAdd
        {
            get
            {
                return 
                    _closeCancelAdditionalResourceAdd 
                    ?? (_closeCancelAdditionalResourceAdd = new RelayCommand<Window>(CloseWindowCancelAdditionalResourceAdd));
            }

        }

        /// <summary>
        /// Closes the Selected Window while updating what needs to be updated.
        /// </summary>
        /// <param name="window">Window to be closed.</param>
        private void CloseWindowAddAdditionalResource(Window window)
        {
            LogManager.DebugLogManager.MethodBegin("ResourceTypeSelectionViewModel.CloseWindowWithUpdate");

            if (window != null)
            {
                if 
                    (
                    !_selContentType.Name.ToLower().Equals("art") && 
                    !_selContentType.Name.ToLower().Equals("letter")
                    ) // Bug 360 - Art and Letters can be either pdf or jpg.
                {
                    if 
                        (
                        DocumentSelected && 
                        !_resourceFileName.ToLower().Contains(".pdf")
                        )
                    {
                        LogManager
                            .DebugLogManager
                            .LogSection
                            (
                            "ResourceTypeSelectionViewModel.CloseWindowWithUpdate", 
                            "Consent Forms must be in a pdf format. Please attach a PDF version of the Consent Form."
                            );

                        MessageBox.Show("Documents must be in a pdf format. Please attach a PDF version of the Consent Form.");

                        return;
                    }

                    if 
                        (
                        ImageSelected && 
                        !_resourceFileName.ToLower().Contains(".jpg")
                        )
                    {
                        LogManager
                            .DebugLogManager
                            .LogSection
                            (
                            "ResourceTypeSelectionViewModel.CloseWindowWithUpdate", 
                            "Profile and Action Photos must be in a JPG formate. Please attach a JPG version of the Photo."
                            );

                        
                        MessageBox.Show("Photos must be in a JPG formate. Please attach a JPG version of the Photo.");

                        return;
                    }
                }

                String _saveLocation = String.Empty;

                if (PangeaInfo.Child.ChildID != null)
                {
                    if (PangeaInfo.Child.ChildNumber != null)
                        _saveLocation = 
                            String.Format
                            (
                                "{0}{1}\\", 

                                PangeaInfo.EnrollingChild 
                                ? 
                                PangeaInfo.CommonAppPangeaEnrollmentLoc 
                                : 
                                PangeaInfo.CommonAppPangeaUpdateLoc, 
                                
                                PangeaInfo.Child.ChildNumber
                                );
                    else
                        _saveLocation = 
                            String.Format
                            (

                                "{0}{1}\\", 

                                PangeaInfo.EnrollingChild 
                                ? 
                                PangeaInfo.CommonAppPangeaEnrollmentLoc 
                                : 
                                PangeaInfo.CommonAppPangeaUpdateLoc, 
                                
                                PangeaInfo.Child.ChildID
                                );
                }
                else
                    _saveLocation = 
                        String.Format
                        (

                            "{0}temp\\", 
                            
                            PangeaInfo.EnrollingChild 
                            ? 
                            PangeaInfo.CommonAppPangeaEnrollmentLoc 
                            : 
                            PangeaInfo.CommonAppPangeaUpdateLoc
                            
                            );

                if (!Directory.Exists(_saveLocation))
                    Directory.CreateDirectory(_saveLocation);

                // Cant have more than one of each type.. Can be removed later if we do allow more than one of each type.
                if (PangeaInfo.Child.AdditionalResources.Contains(SelContentType))
                {
                    MessageBox.Show
                        (
                        String.Format
                        (
                            "You already have a {0} {1}. Please Remove it before adding a new one.", 
                            SelContentType.Name, 
                            
                            DocumentSelected 
                            ? 
                            "Form" 
                            : 
                            "Picture"
                            ), 
                        
                        "Replace", 
                        MessageBoxButton.OK
                        );
                    return;
                }

                FileData _fileData = new FileData();

                if (ImageSelected)
                {
                    try
                    {
                        string _metadataDateTimeFormat = "yyyy:MM:dd HH:mm:ss";

                        IReadOnlyList<MetadataExtractor.Directory> dirs = MetadataExtractor.ImageMetadataReader.ReadMetadata(_resourceFileLoc);

                        foreach (MetadataExtractor.Directory directory in dirs)
                        {
                            foreach (MetadataExtractor.Tag tag in directory.Tags)
                            {
                                if (tag.DirectoryName.ToUpper().Equals("GPS"))
                                {
                                    switch (tag.Type)
                                    {
                                        case 1: // GPS Latitude Ref
                                            _fileData.GPSLatitudeRef = tag.Description;
                                            break;
                                        case 2: // GPS Latitude
                                            _fileData.GPSLatitude = tag.Description;
                                            break;
                                        case 3: // GPS Longitude Ref
                                            _fileData.GPSLongitudeRef = tag.Description;
                                            break;
                                        case 4: // GPS Longitude
                                            _fileData.GPSLongitude = tag.Description;
                                            break;
                                        case 5: // GPS Altitude Ref
                                            _fileData.GPSAltitudeRef = tag.Description;
                                            break;
                                        case 6: // GPS Altitude
                                            _fileData.GPSAltitude = tag.Description;
                                            break;
                                        case 7: // GPS Time-Stamp
                                            _fileData.GPSTimeStamp = tag.Description;
                                            break;
                                        case 16: // GPS Img Direction Ref
                                            _fileData.GPSImgDirectionRef = tag.Description;
                                            break;
                                        case 17: // GPS Img Direction
                                            _fileData.GPSImgDirection = Convert.ToDecimal(tag.Description.Replace("degrees", ""));
                                            break;
                                    }
                                }
                                else if (tag.DirectoryName.ToUpper().Equals("EXIF IFD0"))
                                {
                                    switch (tag.Type)
                                    {
                                        case 271: // Make
                                            _fileData.Make = tag.Description;
                                            break;
                                        case 272: // Model
                                            _fileData.Model = tag.Description;
                                            break;
                                        case 205: // Software
                                            _fileData.Software = tag.Description;
                                            break;
                                        case 306: // Date/Time
                                            _fileData.DateTime = DateTime.ParseExact(tag.Description, _metadataDateTimeFormat, CultureInfo.InvariantCulture).ToUniversalTime();
                                            break;
                                    }
                                }
                                else if (tag.DirectoryName.ToUpper().Equals("EXIF SUBIFD"))
                                {
                                    switch (tag.Type)
                                    {
                                        case 36864: // Exif Version
                                            _fileData.GPSVersionID = tag.Description;
                                            break;
                                        case 36867: // Date/Time Original
                                            _fileData.DateTimeOriginal = DateTime.ParseExact(tag.Description, _metadataDateTimeFormat, CultureInfo.InvariantCulture).ToUniversalTime();
                                            break;
                                        case 36868: // Date/Time Digitized
                                            _fileData.DateTimeDigitized = DateTime.ParseExact(tag.Description, _metadataDateTimeFormat, CultureInfo.InvariantCulture).ToUniversalTime();
                                            break;
                                    }
                                }
                            }
                        }

                        _resourceFileName = String.Format("{0}.JPG", SelContentType.Name);

                        _resourceFileLoc = String.Format("{0}\\{1}", _saveLocation, _resourceFileName);

                        ImageSaveSrc.Seek(0, SeekOrigin.Begin);  // Reset to the beging of the memory stream incase its not there.

                        System.Drawing.Bitmap _bMap = new System.Drawing.Bitmap(ImageSaveSrc);

                        _bMap.Save(_resourceFileLoc);

                        _bMap.Dispose();

                    }
                    catch (Exception ex)
                    {
                        LogManager.ErrorLogManager.WriteError("ResourceTypeSelectionViewModel.CloseWindowWithUpdate", ex);
#if DEBUG
                        MessageBox.Show(ex.Message, "ResourceTypeSelectionViewModel.CloseWindowWithUpdate");
#else
                        return;
#endif
                    }
                }
                else
                {
                    _resourceFileName = String.Format("{0}.pdf", SelContentType.Name);

                    File.Copy(_resourceFileLoc, String.Format("{0}\\{1}", _saveLocation, _resourceFileName), true);

                    _resourceFileLoc = String.Format("{0}{1}", _saveLocation, _resourceFileName);
                }

                // Bug 371 - Adding way to determine file is Optional File.
                PangeaFileStruct _pangeaFileStruct = 
                    new PangeaFileStruct
                    (
                        Guid.Empty, 
                        File.ReadAllBytes(_resourceFileLoc), 
                        _optionalFiles, 
                        _resourceFileName, 
                        _resourceFileLoc, 
                        SelContentType, 
                        _selFileType, 
                        _fileData
                        );

                if (PangeaInfo.UpdatingChild && SelContentType.CodeID == PangeaCollections.ContentTypeCollection.Find("Profile").CodeID)
                {
                    ProfileHistoryWindow _profileHistoryWin = new ProfileHistoryWindow(_pangeaFileStruct);

                    if (_profileHistoryWin.ShowDialog() == true)
                    {

                        window.DialogResult = true;

                        window.Close();

                        PangeaInfo.Child.AdditionalResources.Add(_pangeaFileStruct);
                    }
                }
                else
                {
                    window.DialogResult = true;

                    window.Close();

                    PangeaInfo.Child.AdditionalResources.Add(_pangeaFileStruct);
                }
            }

            LogManager.DebugLogManager.MethodEnd("ResourceTypeSelectionViewModel.CloseWindowWithUpdate");
        }

        /// <summary>
        /// This Closes the Select Window without updating anything.
        /// information.
        /// </summary>
        /// <param name="window">Window to be closed.</param>
        private void CloseWindowCancelAdditionalResourceAdd(Window window)
        {
            LogManager.DebugLogManager.MethodEnd("ResourceTypeSelectionViewModel.CloseWindowWithUpdate");

            if (window != null)
            {
                window.DialogResult = false;

                window.Close();
            }

            LogManager.DebugLogManager.MethodEnd("ResourceTypeSelectionViewModel.CloseWindowWithUpdate");
        }

        /// <summary>
        /// Opens a File Browser so that if they selected the wrong picture or file they don't have to 
        /// cancel to get the correct one.
        /// </summary>
        /// <param name="sender">Button that was clicked</param>
        /// <param name="e">Args that may have value</param>
        public void BrowseBtn_Click(object sender, RoutedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("ResourceTypeSelectionViewModel.BrowseBtn_Click");

            try
            {
                if (_optionalFiles)
                {
                    if (!String.IsNullOrEmpty(PangeaInfo.LastDirectoryOptionalFiles))
                        _resourceOFD.InitialDirectory = PangeaInfo.LastDirectoryOptionalFiles;
                    else
                        _resourceOFD.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

                    _resourceOFD.FilterIndex = 1;
                }
                else
                {
                    if 
                        (
                        DocumentSelected && 
                        !String.IsNullOrEmpty(PangeaInfo.LastDirectoryDocuments)
                        )
                        _resourceOFD.InitialDirectory = PangeaInfo.LastDirectoryDocuments;

                    else if 
                        (
                        ImageSelected && 
                        !String.IsNullOrEmpty(PangeaInfo.LastDirectoryImages)
                        )
                        _resourceOFD.InitialDirectory = PangeaInfo.LastDirectoryImages;
                    else
                    {
                        if (DocumentSelected)
                            _resourceOFD.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                        else
                            _resourceOFD.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
                    }

                    _resourceOFD.FilterIndex = DocumentSelected ? 1 : 2;
                }

                if (_resourceOFD.ShowDialog() == true)
                {
                    _resourceFileLoc = _resourceOFD.FileName;

                    _resourceFileName = _resourceOFD.SafeFileName;

                    _selFileType = PangeaCollections.FileTypeCollection.Find_Extension(Path.GetExtension(_resourceFileName));

                    if (ImageSelected)
                    {
                        using (System.Drawing.Bitmap source = new System.Drawing.Bitmap(_resourceOFD.FileName))
                        {
                            // BUG 316 - Added check for the Width and Height to make sure the image is a minimum size and dpi.
                            if (
                                (

                                (source.HorizontalResolution < 72.0 || source.VerticalResolution < 72.0) 
                                
                                ||

                                (source.Width < 1200 || source.Height < 1600)
                                ) && 
                                
                                (
                                !_selContentType.Name.ToLower().Equals("art") && 
                                !_selContentType.Name.ToLower().Equals("letter")
                                )

                                ) // BUG 369 - Don't want to check the Dimensions or DPI when its art or letter
                            {
                                MessageBox.Show("Photo does not meet minimum standards. Photo must be at least 1200 x 1600 with a minimum 96 dpi. Please choose a different picture.", "Invalid Picture");

                                _resourceFileLoc = String.Empty;

                                _resourceFileName = String.Empty;

                                // BUG 316 - Removed the setting _selFileType to null because it was not allowing the user to
                                // load a second picture without reselecting a file type.
                            }
                            else
                            {
                                if (_optionalFiles)
                                {

                                    PangeaInfo.LastDirectoryOptionalFiles = _resourceFileLoc.Replace(_resourceFileName, "");

                                    _selFileType = PangeaCollections.FileTypeCollection.Find(Path.GetExtension(_resourceFileLoc));

                                }
                                else
                                    PangeaInfo.LastDirectoryImages = _resourceFileLoc.Replace(_resourceFileName, "");

                                if 
                                    (
                                    _selContentType.Name.ToLower().Equals("art") || 
                                    _selContentType.Name.ToLower().Equals("letter")
                                    )
                                {
                                    _allowCropping = false;

                                    ImageVisible = Visibility.Visible;

                                    CropImageVisible = Visibility.Collapsed;
                                }
                                else
                                {
                                    _allowCropping = true;

                                    ImageVisible = Visibility.Visible;

                                    CropImageVisible = Visibility.Visible;
                                }

                                ;

                            }
                        }

                        ;

                        if (Show_Image_Content_Item != null)
                        {
                            Show_Image_Content_Item();
                        }

                    }
                    else
                    {
                        if (_optionalFiles)
                        {
                            PangeaInfo.LastDirectoryOptionalFiles = _resourceFileLoc.Replace(_resourceFileName, "");

                            _selFileType = PangeaCollections.FileTypeCollection.Find(Path.GetExtension(_resourceFileLoc));
                        }
                        else
                            PangeaInfo.LastDirectoryDocuments = _resourceFileLoc.Replace(_resourceFileName, "");

                        _allowCropping = false;

                        ImageVisible = Visibility.Collapsed;

                        CropImageVisible = Visibility.Visible;

                        /*
                         * ^^^^
                         */

                        ;

                        
                        if(_selFileType.Name == "pdf")
                        {
                            if(Show_Web_Browser_Content_Item != null)
                            {
                                Show_Web_Browser_Content_Item(_resourceOFD.FileName);
                            }
                        }
                        else
                        {
                            if (Show_Image_Content_Item != null)
                            {
                                Show_Image_Content_Item();
                            }
                        }


                    }

                    // Cause the View to reset all its properties
                    SendPropertyChanged(null);
                }
            }
            catch (Exception ex)
            {
                LogManager.DebugLogManager.LogSection("ResourceTypeSelectionViewModel.BrowseBtn_Click", ex.Message);
#if DEBUG
                MessageBox.Show(ex.Message, "ResourceTypeSelectionViewModel.BrowseBtn_Click");
#endif
            }

            LogManager.DebugLogManager.MethodEnd("ResourceTypeSelectionViewModel.BrowseBtn_Click");
        }



        
        public delegate void dlg_Show_Web_Browser_Content_Item(string file_location);

        public event dlg_Show_Web_Browser_Content_Item Show_Web_Browser_Content_Item;


        public delegate void dlg_Show_Image_Content_Item();

        public event dlg_Show_Image_Content_Item Show_Image_Content_Item;


    }
}
