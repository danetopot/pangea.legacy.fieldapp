﻿using Pangea.Data.Model;
using System.Windows;

namespace Pangea.FieldApplication.ViewModels
{
    public partial class ActionBarViewModel : PangeaBaseViewModel
    {
        private bool _newEnrollLMD;

        private bool _editEnrollLMD;

        private bool _deleteEnrollLMD;

        private bool _enrollmentDataListMD;

        private bool _updateDataListMD;

        private bool _updateCodeTablesMD;

        private Child _selectedChild;


        public ActionBarViewModel()
        {
            _newEnrollLMD = false;

            _editEnrollLMD = false;

            _deleteEnrollLMD = false;

            _enrollmentDataListMD = false;

            _updateDataListMD = false;

            _homeSelected = true;

            _enrollmentSelected = false;

            _updateSelected = false;

            _enrollmentDataListSelected = false;

            _updateDataListSelected = false;

            _updateCodeTablesMD = false;

            _enrollmentDataListItemSelected = false;

            _updateDataListItemSelected = false;

            PangeaInfo.User.PropertyChanged += User_PropertyChanged;
        }

        /// <summary>
        /// This tells us if the Enrollment Data List text
        /// should be visible of not.
        /// </summary>
        public Visibility EnrollmentDataListTextboxVisibility
        {
            get
            {
                if (
                    (HomeSelected || EnrollmentSelected) && 
                    PangeaInfo.User.CanEnroll
                    )
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// This tells us if the Update Data List text
        /// should be visible of not.
        /// </summary>
        public Visibility UpdateDataListTextboxVisibility
        {
            get
            {
                if (
                    (HomeSelected || UpdateSelected) && 
                    PangeaInfo.User.CanUpdate
                    )
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// This tells us if the Enrollment Action controls
        /// should be visible or not.
        /// </summary>
        public Visibility NewEnrollmentTextBlockVisibility
        {
            get
            {
                if (
                    (EnrollmentSelected || EnrollmentDataListSelected) && 
                    PangeaInfo.User.CanEnroll
                    )
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// This tells us if the Enrollment Data List
        /// Action controls should be visible or not.
        /// </summary>
        public Visibility EditVisibility
        {
            get
            {
                if (
                    (EnrollmentDataListSelected && PangeaInfo.User.CanEnroll) 
                    || 
                    (UpdateDataListSelected && PangeaInfo.User.CanUpdate)
                    )
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// This tells us if the Delte Button
        /// Action Controls should be visible or not.
        /// </summary>
        public Visibility DeleteVisibility
        {
            get
            {
                if (EnrollmentDataListSelected && PangeaInfo.User.CanEnroll)
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }


        /// <summary>
        /// This tells us if the Update Data List
        /// Action Controls should be visible or not.
        /// </summary>
        public Visibility RemoveVisibility
        {
            get
            {
                if (
                    (UpdateDataListSelected || UpdateSelected) && 
                    PangeaInfo.User.CanUpdate
                    )
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Color of the Text for the Action Bar Controls when the 
        /// Enrollment Data List is Active.
        /// </summary>
        public System.Windows.Media.SolidColorBrush ControlsTextColor
        {
            get
            {
                if (EnrollmentDataListItemSelected || UpdateDataListItemSelected)
                    return System.Windows.Media.Brushes.Black;

                return System.Windows.Media.Brushes.Gray;
            }
        }

        /// <summary>
        /// Determines if the Edit Text Control is Enabled and allows
        /// mouse clicks.
        /// </summary>
        public bool EnableEditControl
        {
            get
            {
                if (_selectedChild != null)
                {
                    if (
                        (EnrollmentDataListItemSelected || UpdateDataListItemSelected) 
                        && 
                        _selectedChild.Action != PangeaCollections.ActionCollection.Find("Transfer Complete")
                        )
                        return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Determines if the Remove Text Control is Enabled and allows mouse clicks
        /// </summary>
        public bool EnableRemoveControl
        {
            get
            {
                if (_selectedChild != null)
                {
                    if (
                        (UpdateDataListItemSelected || UpdateSelected) && 
                        _selectedChild.Action != PangeaCollections.ActionCollection.Find("Transfer Complete")
                        )
                        return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Used to tell if the Home Action Bar tab should be selected.
        /// Set by Selecting the Hoome tab on the Action bar or by selecting
        /// the Home tab on the Main Form.
        /// </summary>
        private bool _homeSelected;
        public bool HomeSelected
        {
            get { return _homeSelected; }
            set
            {
                if (_homeSelected != value)
                {
                    SendPropertyChanging();

                    _homeSelected = value;

                    EnrollmentDataListSelected = false;

                    UpdateDataListSelected = false;

                    EnrollmentDataListItemSelected = false;

                    UpdateDataListItemSelected = false;

                    SendPropertyChanged(null);
                }
            }
        }

        /// <summary>
        /// Used to tell if the Enrollment Action Bar tab should be selected
        /// at this time.
        /// Set by selecting the Tab on the Action Bar or if the Enroolment
        /// Tab of the Main Form is Selected.
        /// </summary>
        private bool _enrollmentSelected;
        public bool EnrollmentSelected
        {
            get { return _enrollmentSelected; }
            set
            {
                if (_enrollmentSelected != value)
                {
                    SendPropertyChanging();

                    _enrollmentSelected = value;

                    UpdateDataListSelected = false;

                    EnrollmentDataListItemSelected = false;

                    UpdateDataListItemSelected = false;

                    SendPropertyChanged(null);
                }
            }
        }

        /// <summary>
        /// Used to tell if the Update Action Bar tab shold be Selected
        /// at this time.
        /// </summary>
        private bool _updateSelected;

        public bool UpdateSelected
        {
            get { return _updateSelected; }
            set
            {
                if (_updateSelected != value)
                {
                    SendPropertyChanging();

                    _updateSelected = value;

                    EnrollmentDataListSelected = false;

                    EnrollmentDataListItemSelected = false;

                    UpdateDataListItemSelected = false;

                    SendPropertyChanged(null);
                }
            }
        }

        /// <summary>
        /// Used to tell if the Enrollment Data List Action Bar tab should be 
        /// Selected at this time.
        /// Set by pressing Enrollment Data List TextBlock.
        /// </summary>
        private bool _enrollmentDataListSelected;

        public bool EnrollmentDataListSelected
        {
            get { return _enrollmentDataListSelected; }
            set
            {
                if (_enrollmentDataListSelected != value)
                {
                    SendPropertyChanging();

                    _enrollmentDataListSelected = value;

                    EnrollmentDataListItemSelected = false;

                    UpdateDataListItemSelected = false;

                    SendPropertyChanged(null);
                }
            }
        }

        /// <summary>
        /// Used to tell if the Update Data List Action Bar tab should
        /// be Selected at this time.
        /// Set by press Update Data list TextBlock
        /// </summary>
        private bool _updateDataListSelected;

        public bool UpdateDataListSelected
        {
            get { return _updateDataListSelected; }
            set
            {
                if (_updateDataListSelected != value)
                {
                    SendPropertyChanging();

                    _updateDataListSelected = value;

                    EnrollmentDataListItemSelected = false;

                    UpdateDataListItemSelected = false;

                    SendPropertyChanged(null);
                }
            }
        }

        /// <summary>
        /// This is used when the Enrollment Data List Action
        /// Items are Selected.
        /// </summary>
        private bool _enrollmentDataListItemSelected;

        public bool EnrollmentDataListItemSelected
        {
            get { return _enrollmentDataListItemSelected; }
            set
            {
                if (_enrollmentDataListItemSelected != value)
                {
                    SendPropertyChanging();

                    _enrollmentDataListItemSelected = value;

                    UpdateDataListSelected = false;

                    UpdateDataListItemSelected = false;

                    SendPropertyChanged(null);
                }
            }
        }

        /// <summary>
        /// This is used when the Updated Data List Action
        /// Items are Selected.
        /// </summary>
        private bool _updateDataListItemSelected;

        public bool UpdateDataListItemSelected
        {
            get { return _updateDataListItemSelected; }
            set
            {
                if (_updateDataListItemSelected != value)
                {
                    SendPropertyChanging();

                    _updateDataListItemSelected = value;

                    // Null Causes the Property Changing event to say all controls need to be refreshed
                    SendPropertyChanged(null); 
                }
            }
        }

        /// <summary>
        /// This Catches the Property Changed Events for the Child Data List Control if
        /// there is a Child Data list control active.
        /// </summary>
        /// <param name="sender">Child Data List Control View Model</param>
        /// <param name="e">the arguments that contain the property changed.</param>
        public void ChildDataListControl_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "SelectedChild":
                    _selectedChild = ((ChildDataListControlViewModel)sender).SelectedChild;
                    if (EnrollmentDataListSelected && _selectedChild != null)
                    {
                        EnrollmentDataListItemSelected = true;

                        UpdateDataListItemSelected = false;
                    }

                    if (UpdateDataListSelected && _selectedChild != null)
                    {
                        EnrollmentDataListItemSelected = false;

                        UpdateDataListItemSelected = true;
                    }

                    if (_selectedChild == null)
                    {
                        EnrollmentDataListItemSelected = false;

                        UpdateDataListItemSelected = false;
                    }

                    SendPropertyChanged(null);

                    break;

                default:
                    break;
            }
        }

        private void User_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "CanEnroll":
                    SendPropertyChanged("EnrollmentDataListTextboxVisibility");

                    SendPropertyChanged("EditVisibility");

                    SendPropertyChanged("DeleteVisibility");
                    break;

                case "CanUpdate":
                    SendPropertyChanged("UpdateDataListTextboxVisibility");

                    SendPropertyChanged("EditVisibility");

                    SendPropertyChanged("RemoveVisibility");
                    break;
            }
        }
    }
}
