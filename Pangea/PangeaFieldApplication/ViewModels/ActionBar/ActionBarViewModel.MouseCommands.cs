﻿using Pangea.Data.DataObjects;
using Pangea.FieldApplication.Views;
using System;
using System.Data.Linq;
using System.Windows;
using System.Windows.Input;

namespace Pangea.FieldApplication.ViewModels
{
    public partial class ActionBarViewModel
    {
        /// <summary>
        /// Is used to determine if the New Enroll Text Has had Mouse Click On it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void NewEnrollmentTextBlock_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (_newEnrollLMD && !PangeaInfo.UpdatingChild)
            {
                // Check to see if either they are currently enrolling or Updating a Child.
                if (PangeaInfo.EnrollingChild && PangeaInfo.SaveRequired) // Bug 334
                {
                    if (MessageBox.Show("Do you wish to Save Current Child?", "Save Child", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    {
                        if (!PangeaInfo.SaveProgress())
                            return;
                    }
                }

                PangeaInfo.StartChildEnrollment();

                SendPropertyChanged("StartNewEnrollment");
                _newEnrollLMD = false;
                HomeSelected = false;
                EnrollmentSelected = true;
                EnrollmentDataListSelected = false;
                UpdateDataListSelected = false;
            }
        }

        /// <summary>
        /// Is used to determine if the New Enroll Text Has had Mouse Click On it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void NewEnrollmentTextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _newEnrollLMD = true;
        }

        /// <summary>
        /// Is used to determine if the Edit Enroll Text Has had Mouse Click On it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void EditEnrollmentTextBlock_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

            if (
                _editEnrollLMD && 
                _selectedChild != null
                )
            {
                // Bug 333 - Added Check for the Action reason to make sure the Child has not alread been
                // transfered
                if (_selectedChild.Action != PangeaCollections.ActionCollection.Find("Transfer Complete"))
                {
                    if (EnrollmentDataListSelected)
                    {
                        PangeaInfo.ContinueChildEnrollment(_selectedChild.ChildID);

                        SendPropertyChanged("EditEnrollment");

                        EnrollmentSelected = true;

                        UpdateSelected = false;
                    }
                    else if (UpdateDataListItemSelected)
                    {
                        PangeaInfo.StartChildUpdating(_selectedChild.ChildID);

                        SendPropertyChanged("EditUpdate");

                        EnrollmentSelected = false;

                        UpdateSelected = true;
                    }

                    _editEnrollLMD = false;

                    HomeSelected = false;

                    EnrollmentDataListSelected = false;

                    UpdateDataListSelected = false;
                }
            }
        }

        /// <summary>
        /// Is used to determine if the Edit Enroll Text Has had Mouse Click On it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void EditEnrollmentTextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _editEnrollLMD = true;
        }

        /// <summary>
        /// Is used to determine if the Delete Enroll Text Has had Mouse Click On it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void DeleteEnrollmentTextBlock_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (_deleteEnrollLMD && EnrollmentDataListSelected && _selectedChild != null)
            {
                if (_selectedChild.Action != PangeaCollections.ActionCollection.Find("Transfer Complete"))
                {

                    Delete_Child_Instance                        
                        (
                        _selectedChild.ChildID,
                        PangeaInfo.User.UserID
                        );

                    _deleteEnrollLMD = false;
                    _selectedChild = null;
                    SendPropertyChanged("DeleteEnrollment");
                }
            }
        }

        public static void Delete_Child_Instance
            (
            Guid? ChildID,
            Guid UserID
            )
        {

            EnrollmentTablesDataContext _instance = new EnrollmentTablesDataContext(PangeaInfo.DBCon);

            int _results = 
                _instance
                .Delete_Enrollment
                (
                    ChildID, 
                    PangeaInfo.User.UserID
                    );

            if (Convert.ToBoolean(_results))
            {

                ISingleResult<GetFileDataResult> _fileDataRes = 
                    _instance
                    .Get_File_Data
                    (
                        UserID, 
                        null, 
                        ChildID, 
                        null, 
                        null, 
                        true
                        );

                foreach (GetFileDataResult _fileData in _fileDataRes)
                {

                    _results = _instance.Delete_File
                        (
                        UserID, 
                        _fileData.stream_id, 
                        ChildID, 
                        null, 
                        null, 
                        true
                        );

                }
            }

        }


        /// <summary>
        /// Is used to determine if the Delete Enroll Text Has had Mouse Click On it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void DeleteEnrollmentTextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _deleteEnrollLMD = true;
        }

        /// <summary>
        /// Is used to determine if the Enrollment Data List Text has had a Mouse Click on it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void EnrollmentDataListTextBlock_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (_enrollmentDataListMD)
            {
                SendPropertyChanged("EnrollmentDataList");
                _enrollmentDataListMD = false;
                HomeSelected = false;
                EnrollmentSelected = false;
                EnrollmentDataListSelected = true;
                UpdateDataListSelected = false;
            }
        }

        /// <summary>
        /// Is used to determine if the Enrollment Data List Text has had a Mouse Click on it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void EnrollmentDataListTextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _enrollmentDataListMD = true;
        }

        /// <summary>
        /// Is used to determine if the Enrollment Data List Text has had a Mouse Click on it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void UpdateDataListTextBlock_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (_updateDataListMD)
            {
                SendPropertyChanged("UpdateDataList");

                _updateDataListMD = false;

                HomeSelected = false;

                EnrollmentSelected = false;

                EnrollmentDataListSelected = false;

                UpdateDataListSelected = true;
            }
        }

        /// <summary>
        /// Is used to determine if the Enrollment Data List Text has had a Mouse Click on it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void UpdateDataListTextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _updateDataListMD = true;
        }

        /// <summary>
        /// Is used to determine if the Remove Update Child Text has had a Mouse Click on it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void RemoveUpdateChildTextBlock_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (_deleteEnrollLMD)
            {
                if (UpdateDataListSelected)
                    PangeaInfo.Child.Copy(_selectedChild);

                RemoveUpdateChildWindow _removeUpdateChildWin = new RemoveUpdateChildWindow();
                if (_removeUpdateChildWin.ShowDialog() == true)
                {
                    PangeaInfo.Child.RemoveUpdateChild();
                    SendPropertyChanged("RemoveUpdateChild");
                }

                _deleteEnrollLMD = false;
            }
        }

        /// <summary>
        /// Is used to determine if the Remove Update Child Text has had a Mouse Click on it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void RemoveUpdateChildTextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _deleteEnrollLMD = true;
        }

        /// <summary>
        /// Is used to determine if the Update Code Tables Action has been clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void UpdateCodeTablesTextBlock_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_updateCodeTablesMD)
            {
                SendPropertyChanged("UpdateCodeTables");
                _updateCodeTablesMD = false;
            }
        }

        /// <summary>
        /// Is used to determine if the Update Code Tables Action has been clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void UpdateCodeTablesTextBlock_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            _updateCodeTablesMD = true;
        }
    }
}
