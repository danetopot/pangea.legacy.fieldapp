﻿
using System;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Pangea.Utilities.Tools;

namespace Pangea.FieldApplication
{
    public class DefaultImages
    {
        /// <summary>
        /// The Trash Icon Source. 
        /// </summary>
        public static ImageSource TrashImageSrc
        {
            get { return ImageMethods.DoGetImageSourceFromResource("PangeaFieldApplication", "Resources/TrashBin.ico"); }
        }

        /// <summary>
        /// Document Image to show when there is a document, not a picture.
        /// </summary>
        public static ImageSource DocumentImageSrc
        {
            get { return ImageMethods.DoGetImageSourceFromResource("PangeaFieldApplication", "Resources/Document.ico"); }
        }

        /// <summary>
        /// Video Image to show when there is a Video, not a picture or document.
        /// </summary>
        public static ImageSource VideoImageSrc
        {
            get { return ImageMethods.DoGetImageSourceFromResource("PangeaFieldApplication", "Resources/Video.ico"); }
        }

        /// <summary>
        /// Default Profile Image Source
        /// </summary>
        public static ImageSource DefaultProfileImageSrc
        {
            get { return ImageMethods.DoGetImageSourceFromResource("PangeaFieldApplication", "Resources/default-user.png"); }
        }

        /// <summary>
        /// this is the icon used to show that we are retrieving data.
        /// </summary>
        public static ImageSource RetrieveArrow
        {
            get { return ImageMethods.DoGetImageSourceFromResource("PangeaFieldApplication", "Resources/Coud_Arrow_Down.png"); }
        }

        /// <summary>
        /// This is the Icon used to show we are transfering data.
        /// </summary>
        public static ImageSource TransferArrow
        {
            get { return ImageMethods.DoGetImageSourceFromResource("PangeaFieldApplication", "Resources/Cloud_Arrow_Up.png"); }
        }

        /// <summary>
        /// This is the Icon used to show the transfer failed.
        /// </summary>
        public static ImageSource TransferFailed
        {
            get { return ImageMethods.DoGetImageSourceFromResource("PangeaFieldApplication", "Resources/Button-cross_red.png"); }
        }

        /// <summary>
        /// This is the icon shown when the Transfer has been completed.
        /// </summary>
        public static ImageSource TransferComplete
        {
            get { return ImageMethods.DoGetImageSourceFromResource("PangeaFieldApplication", "Resources/Green_Check.png"); }
        }

        /// <summary>
        /// This is the Icon that is used for the Stop Transfer Icon
        /// </summary>
        public static ImageSource StopTransfer
        {
            get { return ImageMethods.DoGetImageSourceFromResource("PangeaFieldApplication", "Resources/Stop.png"); }
        }

        /// <summary>
        /// This is the icon that is used for the retry transfer option
        /// </summary>
        public static ImageSource RetryTransfer
        {
            get { return ImageMethods.DoGetImageSourceFromResource("PangeaFieldApplication", "Resources/Retry_Refresh.png"); }
        }

        /// <summary>
        /// Search Magnifing Glass for use on Search Buttons.
        /// </summary>
        public static ImageSource SearchImageSrc
        {
            get { return ImageMethods.DoGetImageSourceFromResource("PangeaFieldApplication", "Resources/Search.ico"); }
        }

        /// <summary>
        /// This is the Item used to send when testing the connection speed. 
        /// </summary>
        public static ImageSource OpenBook
        {
            get { return ImageMethods.DoGetImageSourceFromResource("PangeaFieldApplication", "Resources/Open_Book.png"); }
        }
    }
}
