﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Pangea.Utilities.Tools;

namespace Pangea.Utilities.CustomControls.ImageControl
{
    /// <summary>
    /// This is the control used for Displaying the Image and
    /// also allows the user to Select Parts of the Image to
    /// be cropped.
    /// </summary>
    public class ImageDisplay : Canvas
    {
        // BUG 316 - Changed to Bitmap from BitmapImage because the bitmap image was not opening
        // the images with there full width and height, they were being opened already scaled
        // and that was working for some images but not all.
        private System.Drawing.Bitmap _bitmapSource;
        private Image _pangeaImage;

        private Rectangle _cropBorder;

        private Point _origCursorLocation;
        private Point? _lastLocation;

        private double _origHorizOffset, _origVertOffset;
        public double _scale;
        public double _imageLeft, _imageTop, _imageRight, _imageBottom;

        private bool _widthSmaller, _heightSmaller;
        private bool _modifyLeftOffset, _modifyTopOffset;
        private bool _isDragInProgress;
        private bool _leftBorderSelected, _topBorderSelected, _bottomBorderSelected, _rightBorderSelected;

        private Size _origSize;

        public ImageDisplay()
        {
            _isDragInProgress = false;

            _leftBorderSelected = false;
            _topBorderSelected = false;
            _bottomBorderSelected = false;
            _rightBorderSelected = false;

            _origSize = Size.Empty;
        }

        #region Dependency Properties
        /// <summary>
        /// Location of the Image Source.
        /// </summary>
        public String ImageURL
        {
            get { return (String)GetValue(ImageURLProperty); }
            set { SetValue(ImageURLProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ImageURLProperty =
            DependencyProperty.Register("ImageURL", typeof(String), typeof(ImageDisplay), new PropertyMetadata(null, ImageDisplay_ImageURLPropertyChanged));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private static void ImageDisplay_ImageURLPropertyChanged(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("ImageDisplay.ImageDisplay_ImageURLPropertyChanged");

            try
            {
                ((ImageDisplay)source).CreateImageSource();
            }
            catch (Exception ex)
            {
                LogManager.ErrorLogManager.WriteError("ImageDisplay.ImageDisplay_ImageURLPropertyChanged", ex);
#if DEBUG
                MessageBox.Show(ex.Message, "ImageDisplay.ImageDisplay_ImageURLPropertyChanged");
#endif
            }

            LogManager.DebugLogManager.MethodEnd("ImageDisplay.ImageDisplay_ImageURLPropertyChanged");
        }


        public MemoryStream CroppedImage
        {
            get { return (MemoryStream)GetValue(CroppedImageProperty); }
            set { SetValue(CroppedImageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CroppedImage.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CroppedImageProperty =
            DependencyProperty.Register("CroppedImage", typeof(MemoryStream), typeof(ImageDisplay));

        // Bug 369 - Begin
        public bool AllowCropping
        {
            get { return (bool)GetValue(AllowCroppingProperty); }
            set { SetValue(AllowCroppingProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AllowCroppingProperty =
            DependencyProperty.Register("AllowCropping", typeof(bool), typeof(ImageDisplay), new PropertyMetadata(true, ImageDisplay_AllowCroppingPropertyChanged));

        private static void ImageDisplay_AllowCroppingPropertyChanged(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("ImageDisplay.ImageDisplay_ImageURLPropertyChanged");

            try
            {
                ((ImageDisplay)source).CreateImageSource();
            }
            catch (Exception ex)
            {
                LogManager.ErrorLogManager.WriteError("ImageDisplay.ImageDisplay_ImageURLPropertyChanged", ex);
#if DEBUG
                MessageBox.Show(ex.Message);
#endif
            }

            LogManager.DebugLogManager.MethodEnd("ImageDisplay.ImageDisplay_ImageURLPropertyChanged");
        }
        // Bug 369 - End
        #endregion

        #region Crop Box Manipulation

        /// <summary>
        /// This resizes the cropped border by adjusting the width and location
        /// of the left side of the cropped border.
        /// </summary>
        /// <param name="diff">The amount to add or remove from the width</param>
        public void LeftBorderMove(double diff)
        {
            if (_cropBorder == null)
                return;

            bool setPos = true;
            double left = Double.IsNaN(GetLeft(_cropBorder)) ? GetRight(_cropBorder) - _cropBorder.Width : GetLeft(_cropBorder);

            if (_cropBorder.Width < _cropBorder.MinWidth)
            {
                _cropBorder.Width = _cropBorder.MinWidth;
                setPos = false;
                diff = 0;
            }

            if (_cropBorder.Width > _cropBorder.MaxWidth)
            {
                _cropBorder.Width = _cropBorder.MaxWidth;
                setPos = false;
                diff = 0;
            }

            _cropBorder.Width -= diff;

            if (setPos)
                SetLeft(_cropBorder, left + diff);
        }

        /// <summary>
        /// This resizes the cropped border by adjusting the width and location
        /// of the right side of the cropped border.
        /// </summary>
        /// <param name="diff">The amount to add or remove from the width</param>
        public void RightBorderMove(double diff)
        {
            if (_cropBorder == null)
                return;

            double right = Double.IsNaN(GetRight(_cropBorder)) ? GetLeft(_cropBorder) + _cropBorder.Width : GetRight(_cropBorder);

            if (right + diff >= _imageRight)
                diff = 0;

            if (_cropBorder.Width + diff < _cropBorder.MinWidth)
            {
                _cropBorder.Width = _cropBorder.MinWidth;
                diff = 0;
            }

            if (_cropBorder.Width + diff > _cropBorder.MaxWidth)
            {
                _cropBorder.Width = _cropBorder.MaxWidth;
                diff = 0;
            }

            _cropBorder.Width += diff;
        }

        /// <summary>
        /// This resizes the cropped border by adjusting the height and location
        /// of the top side of the cropped border.
        /// </summary>
        /// <param name="diff">The amount to add or remove from the Height</param>
        public void TopBorderMove(double diff)
        {
            if (_cropBorder == null)
                return;

            bool setPos = true;
            double top = Double.IsNaN(GetTop(_cropBorder)) ? GetBottom(_cropBorder) - _cropBorder.Height : GetTop(_cropBorder);

            if (top + diff <= _imageTop)
                diff = 0;

            if (_cropBorder.Height - diff < _cropBorder.MinHeight)
            {
                _cropBorder.Height = _cropBorder.MinHeight;
                setPos = false;
                diff = 0;
            }

            if (_cropBorder.Height - diff > _cropBorder.MaxHeight)
            {
                _cropBorder.Height = _cropBorder.MaxHeight;
                setPos = false;
                diff = 0;
            }

            _cropBorder.Height -= diff;

            if (setPos)
                SetTop(_cropBorder, top + diff);
        }

        /// <summary>
        /// This resizes the cropped border by adjusting the height and location
        /// of the bottom side of the cropped border.
        /// </summary>
        /// <param name="diff">The amount to add or remove from the Height</param>
        public void BottomBorderMove(double diff)
        {
            if (_cropBorder == null)
                return;

            double bottom = Double.IsNaN(GetBottom(_cropBorder)) ? GetTop(_cropBorder) + _cropBorder.Height : GetBottom(_cropBorder);

            if (bottom + diff >= _imageBottom)
                diff = 0;

            if (_cropBorder.Height + diff < _cropBorder.MinHeight)
            {
                _cropBorder.Height = _cropBorder.MinHeight;
                diff = 0;
            }

            if (_cropBorder.Height + diff > _cropBorder.MaxHeight)
            {
                _cropBorder.Height = _cropBorder.MaxHeight;
                diff = 0;
            }

            _cropBorder.Height += diff;
        }

        /// <summary>
        /// Moves the Cropped Border in the Bounds of the Image.
        /// </summary>
        /// <param name="newHorizontalOffset">Amount to move the Cropped Border on the horizontal axis</param>
        /// <param name="newVerticalOffset">Amount to move the Cropped Border on the Vertical axis</param>
        public void MoveCroppedBorder(double newHorizontalOffset, double newVerticalOffset)
        {
            if (_cropBorder == null)
                return;

            #region Verify Drag Element Location
            bool leftAlign = (_origHorizOffset + newHorizontalOffset) <= _imageLeft;
            bool rightAlign = ((_origHorizOffset + newHorizontalOffset) + _cropBorder.Width) >= _imageRight;

            bool topAlign = (_origVertOffset + newVerticalOffset) <= _imageTop;
            bool bottomAlign = ((_origVertOffset + newVerticalOffset) + _cropBorder.Height) >= _imageBottom;
            #endregion // Verify Drag Element Location

            #region Move Drag Element
            if (_modifyLeftOffset)
                SetLeft(_cropBorder, (leftAlign ? _imageLeft : (rightAlign ? (_imageRight - _cropBorder.Width) : (_origHorizOffset + newHorizontalOffset))));
            else
                SetRight(_cropBorder, (leftAlign ? _imageLeft : (rightAlign ? (_imageRight - _cropBorder.Width) : (_origHorizOffset + newHorizontalOffset))));

            if (_modifyTopOffset)
                SetTop(_cropBorder, (topAlign ? _imageTop : (bottomAlign ? (_imageBottom - _cropBorder.Height) : (_origVertOffset + newVerticalOffset))));
            else
                SetBottom(_cropBorder, (topAlign ? _imageTop : (bottomAlign ? (_imageBottom - _cropBorder.Height) : (_origVertOffset + newVerticalOffset))));
            #endregion
        }

        /// <summary>
        /// This gets the current Origin Point for the Cropped Border.
        /// </summary>
        public void DetermineCroppedBoarderOrigOffsets()
        {
            if (_cropBorder == null)
                return;

            // Get the element's offsets from the four sides of the Canvas.
            double left = GetLeft(_cropBorder);
            double right = GetRight(_cropBorder);
            double top = GetTop(_cropBorder);
            double bottom = GetBottom(_cropBorder);

            // Calculate the offset deltas and determine for which sides
            // of the Canvas to adjust the offsets.
            _origHorizOffset = ResolveOffset(left, right, out _modifyLeftOffset);
            _origVertOffset = ResolveOffset(top, bottom, out _modifyTopOffset);
        }

        /// <summary>
        /// Updates the Cropped Border Drawing Information
        /// </summary>
        public void UpdateCroppedBorder()
        {
            if (_cropBorder == null)
                return;

            CreateCroppedImage();
        }
        #endregion

        #region overrides

        /// <summary>
        /// This is called when a left mouse button is pressed down on the Image Control
        /// </summary>
        /// <param name="e">the Mouse Click event</param>
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            _isDragInProgress = false;
            _lastLocation = null;

            Shape _elementBeingDragged = e.Source as Shape;
            if (_cropBorder == null || _elementBeingDragged != _cropBorder)
                return;

            // Cache the mouse cursor location.
            _origCursorLocation = Mouse.GetPosition(this);

            DetermineCroppedBoarderOrigOffsets();

            _isDragInProgress = true;
        }

        /// <summary>
        /// This is called when the mouse is moved over the Image control
        /// </summary>
        /// <param name="e">the Mouse Move Events</param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (e.LeftButton == MouseButtonState.Pressed)
            {
                // We only want to do this if we are over the Crop Border Element.
                Shape _elementBeingDragged = e.Source as Shape;
                if ((_cropBorder == null || _elementBeingDragged != _cropBorder) && !_isDragInProgress)
                    return;

                // These values will store the new offsets of the drag element.
                double newHorizontalOffset, newVerticalOffset;

                // Get the position of the mouse cursor, relative to the Canvas.
                Point cursorLocation = Mouse.GetPosition(this);

                if (!_isDragInProgress)
                    return;

                if (_leftBorderSelected || _rightBorderSelected || _topBorderSelected || _bottomBorderSelected)
                {
                    if (_leftBorderSelected)
                        LeftBorderMove((cursorLocation.X - (_lastLocation.HasValue ? _lastLocation.Value.X : _origCursorLocation.X)));

                    if (_rightBorderSelected)
                        RightBorderMove((cursorLocation.X - (_lastLocation.HasValue ? _lastLocation.Value.X : _origCursorLocation.X)));

                    if (_topBorderSelected)
                        TopBorderMove((cursorLocation.Y - (_lastLocation.HasValue ? _lastLocation.Value.Y : _origCursorLocation.Y)));

                    if (_bottomBorderSelected)
                        BottomBorderMove((cursorLocation.Y - (_lastLocation.HasValue ? _lastLocation.Value.Y : _origCursorLocation.Y)));

                    _lastLocation = cursorLocation;
                }
                else
                {
                    #region Calculate Offsets
                    // Determine the horizontal offset.
                    if (_modifyLeftOffset)
                        newHorizontalOffset = (cursorLocation.X - _origCursorLocation.X);
                    else
                        newHorizontalOffset = -(cursorLocation.X - _origCursorLocation.X);

                    if (_modifyTopOffset)
                        newVerticalOffset = (cursorLocation.Y - _origCursorLocation.Y);
                    else
                        newVerticalOffset = -(cursorLocation.Y - _origCursorLocation.Y);
                    #endregion // Calculate Offsets

                    MoveCroppedBorder(newHorizontalOffset, newVerticalOffset);
                }

                UpdateCroppedBorder();
            }
        }

        /// <summary>
        /// This is a Preview of the moue move event.
        /// This is thrown before the actual Mouse Move Event, allows us
        /// to prepare for the mouse move event.
        /// </summary>
        /// <param name="e">The mouse move events.</param>
        protected override void OnPreviewMouseMove(MouseEventArgs e)
        {
            base.OnPreviewMouseMove(e);

            if (_isDragInProgress)
            {
                if (e.LeftButton == MouseButtonState.Released)
                {
                    if (Mouse.OverrideCursor != Cursors.Arrow)
                        Mouse.OverrideCursor = Cursors.Arrow;

                    _isDragInProgress = false;
                    _lastLocation = null;
                }
                return;
            }

            // We only want to do this if we are over the Crop Border Element.
            Shape _elementBeingDragged = e.Source as Shape;
            if (_cropBorder == null || _elementBeingDragged != _cropBorder)
            {
                if (Mouse.OverrideCursor != Cursors.Arrow)
                    Mouse.OverrideCursor = Cursors.Arrow;
                return;
            }

            // This is Figuring out if we are even over the Cropped Border element or not
            // and if we are over the Cropped Border Element we want to know if its on 
            // one of the sides or in the center.
            Point cursorLocation = e.GetPosition(this);

            _leftBorderSelected = false;
            _topBorderSelected = false;
            _bottomBorderSelected = false;
            _rightBorderSelected = false;

            double insideVariance = 5.0;
            double outSideVariance = 10.0;
            double left, right, top, bottom;
            left = Double.IsNaN(GetLeft(_cropBorder)) ? GetRight(_cropBorder) - _cropBorder.Width : GetLeft(_cropBorder);
            right = Double.IsNaN(GetRight(_cropBorder)) ? GetLeft(_cropBorder) + _cropBorder.Width : GetRight(_cropBorder);
            top = Double.IsNaN(GetTop(_cropBorder)) ? GetBottom(_cropBorder) - _cropBorder.Height : GetTop(_cropBorder);
            bottom = Double.IsNaN(GetBottom(_cropBorder)) ? GetTop(_cropBorder) + _cropBorder.Height : GetBottom(_cropBorder);

            //_tempTextBlock.Text = String.Format("orgPosX: {0} orgPosY: {1} \n x: {2} Y: {3} \n", _origCursorLocation.X, _origCursorLocation.Y, cursorLocation.X, cursorLocation.Y);
            //_tempTextBlock.Text = String.Format("x: {0} Y: {1} \n Crop Border\n Left: {2} Right: {3} Top: {4} Bottom: {5} \n Width: {6} Height: {6} \n ActualWidth: {7} ActualHeight: {8}", cursorLocation.X, cursorLocation.Y, left, right, top, bottom, _cropBorder.Width, _cropBorder.Height, _cropBorder.ActualWidth, _cropBorder.ActualHeight);

            if (cursorLocation.X >= (left - outSideVariance) && cursorLocation.X <= (left + insideVariance))
                _leftBorderSelected = true;
            else if (cursorLocation.X >= (right - insideVariance) && cursorLocation.X <= (right + outSideVariance))
                _rightBorderSelected = true;

            if (cursorLocation.Y >= (top - outSideVariance) && cursorLocation.Y <= (top + insideVariance))
                _topBorderSelected = true;
            else if (cursorLocation.Y >= (bottom - insideVariance) && cursorLocation.Y <= (bottom + outSideVariance))
                _bottomBorderSelected = true;

            if (_leftBorderSelected || _rightBorderSelected)
            {
                if (_topBorderSelected)
                    Mouse.OverrideCursor = _leftBorderSelected ? Cursors.SizeNWSE : Cursors.SizeNESW;
                else if (_bottomBorderSelected)
                    Mouse.OverrideCursor = _leftBorderSelected ? Cursors.SizeNESW : Cursors.SizeNWSE;
                else
                    Mouse.OverrideCursor = Cursors.SizeWE;
            }
            else if (_topBorderSelected || _bottomBorderSelected)
                Mouse.OverrideCursor = Cursors.SizeNS;
            else
                Mouse.OverrideCursor = Cursors.ScrollAll;
        }

        /// <summary>
        /// This is called when the left mouse button is released on the Image Control
        /// </summary>
        /// <param name="e">the Mouse Release event</param>
        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonUp(e);

            if (!(_leftBorderSelected || _rightBorderSelected || _topBorderSelected || _bottomBorderSelected))
            {
                if (Mouse.OverrideCursor != Cursors.Arrow)
                    Mouse.OverrideCursor = Cursors.Arrow;
            }

            _isDragInProgress = false;
            _lastLocation = null;
        }

        #endregion

        #region ResolveOffset

        /// <summary>
        /// Determines one component of a UIElement's location 
        /// within a Canvas (either the horizontal or vertical offset).
        /// </summary>
        /// <param name="side1">
        /// The value of an offset relative to a default side of the 
        /// Canvas (i.e. top or left).
        /// </param>
        /// <param name="side2">
        /// The value of the offset relative to the other side of the 
        /// Canvas (i.e. bottom or right).
        /// </param>
        /// <param name="useSide1">
        /// Will be set to true if the returned value should be used 
        /// for the offset from the side represented by the 'side1' 
        /// parameter.  Otherwise, it will be set to false.
        /// </param>
        private static double ResolveOffset(double side1, double side2, out bool useSide1)
        {
            // If the Canvas.Left and Canvas.Right attached properties 
            // are specified for an element, the 'Left' value is honored.
            // The 'Top' value is honored if both Canvas.Top and 
            // Canvas.Bottom are set on the same element.  If one 
            // of those attached properties is not set on an element, 
            // the default value is Double.NaN.
            useSide1 = true;
            double result;
            if (Double.IsNaN(side1))
            {
                if (Double.IsNaN(side2))
                {
                    // Both sides have no value, so set the
                    // first side to a value of zero.
                    result = 0;
                }
                else
                {
                    result = side2;
                    useSide1 = false;
                }
            }
            else
            {
                result = side1;
            }
            return result;
        }

        #endregion // ResolveOffset

        /// <summary>
        /// This actually takes the Image Location and creates
        /// what is used to display the image on the Form.
        /// </summary>
        private void CreateImageSource()
        {
            if (_pangeaImage != null)
            {
                _pangeaImage = null;
                _cropBorder = null;

                Children.Clear();
            }

            // BUG 316 - Open the Image and set the Image display to the loaded image.
            // BUG 320 - Added check to make sure ImageURL is not empty or null.
            if (!String.IsNullOrEmpty(ImageURL))
                _bitmapSource = new System.Drawing.Bitmap(ImageURL);
            else
            {
                _bitmapSource = null;
                CroppedImage = null;

                if (_origSize != Size.Empty)
                {
                    Width = _origSize.Width;
                    Height = _origSize.Height;
                }

                if (_cropBorder != null)
                    _cropBorder = null;
                if (CroppedImage != null)
                    CroppedImage = null;

                return;
            }

            _pangeaImage = new Image();

            _pangeaImage.Source = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap
                (
                _bitmapSource.GetHbitmap(), 
                IntPtr.Zero, 
                Int32Rect.Empty, 
                BitmapSizeOptions.FromEmptyOptions()
                );

            Children.Add(_pangeaImage);

            if (_origSize == Size.Empty)
                _origSize = new Size(ActualWidth, ActualHeight);

            double _scaleX = Math.Round(_origSize.Width / _bitmapSource.Width, 2);

            double _scaleY = Math.Round(_origSize.Height / _bitmapSource.Height, 2);

            _scale = Math.Min(_scaleX, _scaleY);

            _widthSmaller = (_bitmapSource.Width < _bitmapSource.Height);

            _heightSmaller = (_bitmapSource.Height < _bitmapSource.Width);

            Width = _bitmapSource.Width * _scale;

            Height = _bitmapSource.Height * _scale;

            _imageLeft = 0.0;

            _imageTop = 0.0;

            _imageRight = Math.Round(_imageLeft + (_bitmapSource.Width * _scale), 2);

            _imageBottom = Math.Round(_imageTop + (_bitmapSource.Height * _scale), 2);

            _pangeaImage.RenderTransform = new ScaleTransform(_scale, _scale, _imageLeft, _imageTop);

            InitializeCropBorder();
        }

        /// <summary>
        /// Initialize the Crop Rectangle.
        /// </summary>
        private void InitializeCropBorder()
        {
            if (_cropBorder == null)
            {
                _cropBorder = new Rectangle();
                _cropBorder.MinWidth = 50.0;
                _cropBorder.MinHeight = 50.0;
                _cropBorder.MaxWidth = _bitmapSource.Width * _scale;
                _cropBorder.MaxHeight = _bitmapSource.Height * _scale;
                _cropBorder.Stroke = Brushes.Blue;
                _cropBorder.StrokeEndLineCap = PenLineCap.Round;
                _cropBorder.StrokeThickness = 2.0;
                _cropBorder.Fill = Brushes.Transparent;

                if (AllowCropping)
                    Children.Add(_cropBorder);
            }

            // BUG 316 - the Crop border needs to start off as the same size as the 
            // Canvas.
            _cropBorder.Width = Width;
            _cropBorder.Height = Height;

            double startX = _imageLeft;
            double startY = _imageTop;

            SetLeft(_cropBorder, startX);
            SetTop(_cropBorder, startY);

            CreateCroppedImage();
        }

        /// <summary>
        /// This creates the cropped image from the
        /// crop border.  It saves it to a memory stream
        /// </summary>
        private void CreateCroppedImage()
        {
            LogManager.DebugLogManager.MethodBegin("ImageDisplay.CreateCroppedImage");

            MemoryStream _croppedImageStream = new MemoryStream();
            try
            {
                //create a new bitmap (which allowing saving) based on the bound bitmap url
                using (System.Drawing.Bitmap source = new System.Drawing.Bitmap(ImageURL))
                {
                    //create a new bitmap (which allowing saving) to store cropped image in, should be 
                    //same size as rubberBand element which is the size of the area of the original image we want to keep
                    using (System.Drawing.Bitmap target = new System.Drawing.Bitmap(1200, 1600))
                    {

                        target.SetResolution(source.HorizontalResolution, source.VerticalResolution);

                        //create a new destination rectange
                        System.Drawing.RectangleF recDest = new System.Drawing.RectangleF
                            (
                            0.0f, 
                            0.0f, 
                            (float)target.Width, 
                            (float)target.Height
                            );

                        //different resolution fix prior to cropping image
                        // BUG 316 - Added Float to the two so that it keeps the possible decimal values.
                        float hd = 1.0f / 
                            (
                            (float)target.HorizontalResolution 
                            / 
                            (float)source.HorizontalResolution
                            );

                        float vd = 1.0f 
                            / 
                            (
                            (float)target.VerticalResolution 
                            / 
                            (float)source.VerticalResolution
                            );

                        float hScale = 1.0f / (float)_scale;

                        float vScale = 1.0f / (float)_scale;

                        float xPos = (source.Width < source.Height) 
                            ? 
                            ((GetLeft(_cropBorder) - _imageLeft) < 0.0 ? 0.0f : (float)((GetLeft(_cropBorder)) - _imageLeft)) 
                            :
                            (float)(GetLeft(_cropBorder) - _imageLeft);

                        float yPos = (source.Height < source.Width) 
                            ? 
                            ((GetTop(_cropBorder) - _imageTop) < 0.0 ? 0.0f : (float)((GetTop(_cropBorder)) - _imageTop)) 
                            : 
                            (float)(GetTop(_cropBorder) - _imageTop);

                        System.Drawing.RectangleF recSrc = new System.Drawing.RectangleF
                            (
                            (float)((hd * xPos) * hScale), 
                            (float)((hd * yPos) * vScale), 
                            (hd * (float)_cropBorder.Width) * hScale, 
                            (vd * (float)_cropBorder.Height) * vScale
                            );

                        using (System.Drawing.Graphics gfx = System.Drawing.Graphics.FromImage(target))
                        {
                            gfx.DrawImage
                                (
                                source, 
                                recDest, 
                                recSrc, 
                                System.Drawing.GraphicsUnit.Pixel
                                );
                        }

                        target.Save
                            (
                            _croppedImageStream, 
                            System.Drawing.Imaging.ImageFormat.Jpeg
                            );
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorLogManager.WriteError("ImageDisplay.CreateCroppedImage", ex);

                _croppedImageStream = null;
#if DEBUG
                MessageBox.Show(ex.Message, "ImageDisplay.CreateCroppedImage");
#endif
            }

            CroppedImage = _croppedImageStream;

            LogManager.DebugLogManager.MethodEnd("ImageDisplay.CreateCroppedImage");
        }
    }
}
