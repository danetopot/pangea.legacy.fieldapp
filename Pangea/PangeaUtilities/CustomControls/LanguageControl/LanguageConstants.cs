﻿/*
 * RePaver
 * 
 * Copyright (c)2009, Daniel McGaughran
 * 
 * Licence:	CodeProject Open Licence (CPOL) 1.2
 *			Please refer to 'Licence.txt' for further details of this licence.
 *			Alternatively, the licence may be viewed at:
 *			http://www.codeproject.com/info/cpol10.aspx
 * 
 */

namespace Pangea.Utilities.CustomControls.LanguageControl
{
    class LanguageConstants
    {
        //Language definition XML names/XPaths
        internal const string LanguageCodeAttr = "code";
        internal const string TextEntryXPath = "//UIText/Entry";
        internal const string TextEntryKeyAttr = "key";

        internal const string IsRtlXPath = "//IsRtl";
        internal const string IsNotRtlValue = "0";

        internal const string MinFontSizeXPath = "//MinFontSize";
        internal const string HeadingFontSizeXPath = "//HeadingFontSize";
    }
}