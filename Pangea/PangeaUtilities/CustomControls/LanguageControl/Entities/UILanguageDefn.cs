﻿/*
 * RePaver
 * 
 * Copyright (c)2009, Daniel McGaughran
 * 
 * Licence:	CodeProject Open Licence (CPOL) 1.2
 *			Please refer to 'Licence.txt' for further details of this licence.
 *			Alternatively, the licence may be viewed at:
 *			http://www.codeproject.com/info/cpol10.aspx
 * 
 */

using System;
using System.Collections.Generic;
using System.Xml;
using System.Linq;
using Pangea.Utilities.Tools;
using System.Windows;

namespace Pangea.Utilities.CustomControls.LanguageControl.Entities
{
    public class UILanguageDefn
    {
        public const int DefaultMinFontSize = 12;
        public const int DefaultHeadingFontSize = 16;

        private Dictionary<string, string> _uiText;
        private bool _isRightToLeft;
        private int _minFontSize;
        private int _headingFontSize;
        private bool _isLoaded;

        private static Dictionary<string, string> _languageDisplayNames;



        static UILanguageDefn()
        {
            _languageDisplayNames = new Dictionary<string, string>();
        }



        public UILanguageDefn()
        {
            _uiText = new Dictionary<string, string>();
            _isRightToLeft = false;
            _minFontSize = DefaultMinFontSize;
            _headingFontSize = DefaultHeadingFontSize;

            _isLoaded = false;
        }



        /// <summary>
        /// Gets a list of all languages that are supported in the application.
        /// </summary>
        /// <value>The list of all languages.</value>
        public static List<string> AllSupportedLanguageCodes
        {
            get { return _languageDisplayNames.Select(ldr => ldr.Value).ToList<string>(); }
        }


        /// <summary>
        /// Gets the minimum font size to render text in this language.
        /// </summary>
        /// <value>The minimum font size.</value>
        public int MinFontSize
        {
            get { return _minFontSize; }
        }


        /// <summary>
        /// Gets the font size to render heading text in this language.
        /// </summary>
        /// <value>The minimum font size.</value>
        public int HeadingFontSize
        {
            get { return _headingFontSize; }
        }


        /// <summary>
        /// Gets a value indicating whether a language has been loaded.
        /// </summary>
        /// <value>True if this instance is loaded, otherwise false.</value>
        internal bool IsLoaded
        {
            get { return _isLoaded; }
        }


        /// <summary>
        /// Gets a value indicating whether the language is right-to-left.
        /// </summary>
        /// <value>true if the language is right-to-left, otherwise false.</value>
        public bool IsRightToLeft
        {
            get { return _isRightToLeft; }
        }



        /// <summary>
        /// Gets the localised name of the given language.
        /// </summary>
        /// <param name="queriedLanguage">The language to look up.</param>
        /// <returns></returns>
        public static String GetLanguageName(String queriedLanguageCode)
        {
            LogManager.DebugLogManager.MethodBegin("UILanguageDefn.GetLanguageName");
            String retValu = String.Empty;

            if (_languageDisplayNames.ContainsKey(queriedLanguageCode))
                retValu = _languageDisplayNames[queriedLanguageCode];

            LogManager.DebugLogManager.MethodEnd("UILanguageDefn.GetLanguageName");

            return retValu;
        }



        /// <summary>
        /// Gets the language code of the given language.
        /// </summary>
        /// <param name="queriedLanguage">The language to look up.</param>
        /// <returns></returns>
        public static String GetLanguageCode(String languageName)
        {
            LogManager.DebugLogManager.MethodBegin("UILanguageDefn.GetLanguageCode");

            String retValu = String.Empty;

            if (_languageDisplayNames.ContainsValue(languageName))
            {
                foreach (KeyValuePair<string, string> langName in _languageDisplayNames)
                {
                    if (langName.Value == languageName)
                    {
                        retValu = langName.Key;
                        break;
                    }
                }
            }

            LogManager.DebugLogManager.MethodEnd("UILanguageDefn.GetLanguageCode");

            return retValu;
        }



        /// <summary>
        /// Gets the localised text value for the given key.
        /// </summary>
        /// <param name="key">The key of the localised text to retrieve.</param>
        /// <returns>The localised text if found, otherwise an empty string.</returns>
        public String GetTextValue(String key)
        {
            LogManager.DebugLogManager.MethodBegin("UILanguageDefn.GetTextValue");

            String retVal = String.Empty;

            if (_uiText.ContainsKey(key))
                retVal = _uiText[key];

            LogManager.DebugLogManager.MethodEnd("UILanguageDefn.GetTextValue");

            return retVal;
        }



        /// <summary>
        /// Loads the language names for display and selection.
        /// </summary>
        /// <param name="titles">The language names localised to that particular language.</param>
        /// <remarks>All languages are to be registered by this stage.</remarks>
        public static void LoadLanguageNames(XmlElement titles)
        {
            LogManager.DebugLogManager.MethodBegin("UILanguageDefn.LoadLanguageNames");

            try
            {
                _languageDisplayNames.Clear();

                //Dictionary<string, string> langNames = new Dictionary<string, string>();
                XmlNodeList childNodes = titles.ChildNodes;

                foreach (XmlNode currentLangNode in childNodes)
                {
                    if (currentLangNode.LocalName.Contains("#comment"))
                        continue;

                    XmlAttribute languageCode = currentLangNode.Attributes[LanguageConstants.LanguageCodeAttr];

                    if (languageCode != null && !String.IsNullOrEmpty(languageCode.Value))
                    {
                        _languageDisplayNames.Add(currentLangNode.InnerText, languageCode.Value);
                    }
                }
            }
            catch(Exception ex)
            {
                // TODO: Need to Write the Exception out to be stored.
                LogManager.DebugLogManager.LogSection("UILanguageDefn.LoadLanguageNames", ex.Message);
                LogManager.ErrorLogManager.WriteError("UILanguageDefn.LoadLanguageNames", ex);
            }

            LogManager.DebugLogManager.MethodEnd("UILanguageDefn.LoadLanguageNames");
        }



        /// <summary>
        /// Loads the language data for a single language.
        /// </summary>
        /// <param name="languageData">The language data.</param>
        public void LoadLanguageData(XmlElement languageData)
        {
            LogManager.DebugLogManager.MethodBegin("UILanguageDefn.LoadLanguageData");

            try
            {
                XmlNodeList mappings = languageData.SelectNodes(LanguageConstants.TextEntryXPath);

                //Add key-value pairs for each localised text entry.
                _uiText.Clear();
                foreach (XmlNode currentMapping in mappings)
                {
                    XmlAttribute key = currentMapping.Attributes[LanguageConstants.TextEntryKeyAttr];

                    if (key != null && !String.IsNullOrEmpty(key.Value))
                        _uiText.Add(key.Value, currentMapping.InnerText);
                }

                XmlNode rtlNode = languageData.SelectSingleNode(LanguageConstants.IsRtlXPath);
                if (rtlNode != null && rtlNode.InnerText != LanguageConstants.IsNotRtlValue)
                    _isRightToLeft = true;

                XmlNode minFontSizeNode = languageData.SelectSingleNode(LanguageConstants.MinFontSizeXPath);
                if (minFontSizeNode != null && !Int32.TryParse(minFontSizeNode.InnerText, out _minFontSize))
                    _minFontSize = DefaultMinFontSize;

                XmlNode headingFontSizeNode = languageData.SelectSingleNode(LanguageConstants.HeadingFontSizeXPath);
                if (headingFontSizeNode != null && !Int32.TryParse(headingFontSizeNode.InnerText, out _headingFontSize))
                    _headingFontSize = DefaultHeadingFontSize;

                _isLoaded = true;
            }
            catch (Exception ex)
            {
                // TODO: Need to Write the Exception out to be stored.
                LogManager.DebugLogManager.LogSection("UILanguageDefn.LoadLanguageData", ex.Message);
                LogManager.ErrorLogManager.WriteError("UILanguageDefn.LoadLanguageData", ex);
#if DEBUG
                MessageBox.Show(ex.Message, "UILanguageDefn.LoadLanguageData");
#endif
            }

            LogManager.DebugLogManager.MethodEnd("UILanguageDefn.LoadLanguageData");
        }
    }
}