﻿/////////////////////////////////////////////////////////////////////
/// This is a base class to add the Notify functionality to classes
/// without having to write the notify properties for every class
/// that needs them.
/////////////////////////////////////////////////////////////////////

using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Pangea.Utilities.Base.Classes
{
    public class PangeaBaseNotifyProperties : INotifyPropertyChanging, INotifyPropertyChanged
    {
        #region INotifyPropertyChanged Properties

        /// Needed to implement INotifyPropertyChanged.
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Used to Raise the Property has changed event.
        /// </summary>
        /// <param name="prop">Property that has Changed.</param>
        public void SendPropertyChanged([CallerMemberName] string propertyName = "")
        {
            // Will not be invoked if PropertyChanged is NULL.
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        # region INotifyPropertyChanging Properties
        public event PropertyChangingEventHandler PropertyChanging;

        /// <summary>
        /// Used to Raise the Property is Changing event.
        /// </summary>
        public void SendPropertyChanging()
        {
            // Will not be invoked if PropertyChanged is NULL.
            PropertyChanging?.Invoke(this, new PropertyChangingEventArgs(String.Empty));
        }
        #endregion
    }
}
