﻿////////////////////////////////////////////////////////////////////////////////
/// This is a custom interface of the ICommand properties.  The ICommands are 
/// used to allow Binding of different types of commands to different Actions
/// of Controls.  These Generic Command Classes reduce the rewriting of different
/// values in the classes that use custom ICommands.
////////////////////////////////////////////////////////////////////////////////

using System;
using System.Windows.Input;

namespace Pangea.Utilities.Base.Commands
{
    public class RelayCommand : ICommand
    {

        private readonly Predicate<object> _canExecute;

        private readonly Action<object> _execute;

        public RelayCommand(Action<object> execute)
            : this(execute, null)
        { }

        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            _execute = execute;

            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute?.Invoke(parameter) ?? true;
        }

        public void Execute(object parameter)
        {
            _execute?.Invoke(parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;

                CanExecuteChangedInternal += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;

                CanExecuteChangedInternal += value;
            }
        }

        private event EventHandler CanExecuteChangedInternal;

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChangedInternal.Raise(this);
        }
    }
}
