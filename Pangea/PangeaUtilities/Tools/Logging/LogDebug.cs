﻿///////////////////////////////////////////////////////////////////
/// The Class used to log a Debug Message. The Message will be 
/// logged to the Debug Messages file whenever if the Debug flag is
/// set in the config file.
///////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;

namespace Pangea.Utilities.Tools.Logging
{
    public class LogDebug : LogBasic
    {
        //private static DebugLog _instance;

        /// <summary>
        /// Set in the config file.
        /// Whether we should be logging the debug information or not.
        /// </summary>
        private bool _debug;

        private static bool _testVer
        {
            get
            {
#if TESTVER || DEBUG
                return true;
#else
                return false;
#endif
            }
        }

        /// <summary>
        /// This is the Amount of time a Method has run, the Last Logged Time MS, the Last Logged Time Ticks information
        /// all associated to a Method Name which is the key.
        /// </summary>
        private Dictionary<String, LogStruct> _methodInfo;

        public Dictionary<String, LogStruct> MethodInfo
        {
            get
            {
                if (_methodInfo == null)
                    _methodInfo = new Dictionary<string, LogStruct>();

                return _methodInfo;
            }
        }

        /// <summary>
        /// Path to the Debug Logs
        /// </summary>
        private readonly String _debugPath = String.Format("{0}\\{1}\\Logs\\DebugFiles", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), _testVer ? Centraler.Database_Settingz.DB_Staging : Centraler.Database_Settingz.DB_Production );

        public LogDebug()
        {
            _debug = Convert.ToBoolean(ConfigurationManager.AppSettings["Debug"]);

            int daysToKeepFiles = Convert.ToInt32(ConfigurationManager.AppSettings["DaysToKeepDebugLogs"]);

            /// If DaysToKeepDebugLogs is equal to 0 then we will not ever delete the Debug Logs.
            if (Directory.Exists(_debugPath) && daysToKeepFiles > 0)
            {
                string[] files = Directory.GetFiles(_debugPath, "*_debug.txt");

                foreach (string file in files)
                {
                    DateTime fCreated = File.GetCreationTimeUtc(file);

                    if ((DateTime.UtcNow - fCreated).Days >= daysToKeepFiles)
                        File.Delete(file);
                }
            }

            if (!Directory.Exists(_debugPath))
                Directory.CreateDirectory(_debugPath);

            FileName = String.Format("{0}\\{1}{2}{3}_debug.txt", _debugPath, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Year);
        }

        ~LogDebug()
        {
            if (LogStream != null)
                LogStream.Close();
        }

        /// <summary>
        /// Sets the Method Name, Starts the Timer, and internal lastLoggedTimes
        /// </summary>
        /// <param name="methodName">The Method being invoked.</param>
        public void MethodBegin(string methodName)
        {
            if (!_debug)
                return;

            if (MethodInfo.ContainsKey(methodName))
            {
                MethodInfo[methodName].Initialize();

                MethodInfo[methodName].Timer.Start();
            }
            else
            {
                LogStruct dls = new LogStruct();

                dls.Initialize();

                MethodInfo.Add(methodName, dls);

                MethodInfo[methodName].Timer.Start();
            }

            WriteLine(methodName + " Begin");
        }

        /// <summary>
        /// Creates an intermediate log statment in the middle of a method, logs elapsed time since last logSection and current total running time (in ms)
        /// </summary>
        /// <param name="methodName">The Method being invoked.</param>
        /// <param name="section">A name to denote the subsection in the logs</param>
        public void LogSection(string methodName, string section)
        {
            if (!_debug || !MethodInfo.ContainsKey(methodName))
                return;

            WriteLine(String.Format("{0}, {1} - Elapsed Time (ms): {2} - Current Total Time (ms): {3}", methodName, section, (MethodInfo[methodName].Timer.ElapsedMilliseconds - MethodInfo[methodName].LastLoggedTimeMs), MethodInfo[methodName].Timer.ElapsedMilliseconds));

            MethodInfo[methodName].Update();
        }

        /// <summary>
        /// Creates an intermediate log statment in the middle of a method, logs elapsed time since last logSection and current total running time (in ticks)
        /// </summary>
        /// <param name="methodName">The Method being invoked.</param>
        /// <param name="section">A name to denote the subsection in the logs</param>
        public void LogSectionTick(string methodName, string section)
        {
            if (!_debug || !MethodInfo.ContainsKey(methodName))
                return;

            WriteLine(String.Format("{0}, {1} - Elapsed Time (ticks): {2} - Current Total Time (ticks): {3}", methodName, section, (MethodInfo[methodName].Timer.ElapsedTicks - MethodInfo[methodName].LastLoggedTimeTicks), MethodInfo[methodName].Timer.ElapsedTicks));

            MethodInfo[methodName].Update();
        }

        /// <summary>
        /// Creates an end log statment, logs elapsed time since begin (or last logSection) as well as current total running time (in ms)
        /// </summary>
        /// <param name="methodName">The Method being invoked.</param>
        public void MethodEnd(string methodName)
        {
            if (!_debug || !MethodInfo.ContainsKey(methodName))
                return;

            MethodInfo[methodName].Timer.Stop();

            WriteLine(String.Format("{0}, End - Elapsed Time (ms): {1} - Total Time (ms): {2}", methodName, (MethodInfo[methodName].Timer.ElapsedMilliseconds - MethodInfo[methodName].LastLoggedTimeMs), MethodInfo[methodName].Timer.ElapsedMilliseconds));

            MethodInfo.Remove(methodName);
        }

        /// <summary>
        /// Creates an end log statment, logs elapsed time since begin (or last logSection) as well as current total running time (in ticks)
        /// </summary>
        /// <param name="methodName">The Method being invoked.</param>
        public void MethodEndTick(string methodName)
        {
            if (!_debug || !MethodInfo.ContainsKey(methodName))
                return;

            MethodInfo[methodName].Timer.Stop();

            WriteLine(String.Format("{0}, End - Elapsed Time (ticks): {1} - Total Time (ticks): {2}", methodName, (MethodInfo[methodName].Timer.ElapsedTicks - MethodInfo[methodName].LastLoggedTimeTicks), MethodInfo[methodName].Timer.ElapsedMilliseconds));

            MethodInfo.Remove(methodName);
        }
    }
}
