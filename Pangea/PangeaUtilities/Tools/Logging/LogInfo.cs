﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pangea.Utilities.Tools.Logging
{
    public class LogInfo : LogBasic
    {
        // <summary>
        /// Path to the Error Log
        /// </summary>
        private readonly String _LogPath = String.Format("{0}\\Logs", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));

        public LogInfo()
        {
            int daysToKeepLogFiles = 0;

            if (Directory.Exists(_LogPath) && daysToKeepLogFiles > 0)
            {
                string[] files = Directory.GetFiles(_LogPath, "*.txt");
                foreach (String file in files)
                {
                    DateTime fCreated = File.GetCreationTimeUtc(file);
                    if ((DateTime.UtcNow - fCreated).Days > daysToKeepLogFiles)
                        File.Delete(file);
                }
            }

            if (!Directory.Exists(_LogPath))
                Directory.CreateDirectory(_LogPath);

            FileName = String.Format("{0}\\{1}{2}{3}.txt", _LogPath, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Year);
            FileName2 = String.Format("{0}\\LogFile2ndFile.txt", _LogPath);
        }

        ~LogInfo()
        {
            if (LogStream != null)
                LogStream.Close();
        }

        /// <summary>
        /// This will write out the Message to be logged
        /// </summary>
        /// <param name="msg">Message to be written</param>
        public void WriteLog(String msg, bool secondFile = false)
        {
            WriteLine(msg, false, secondFile);
        }
    }
}
