﻿///////////////////////////////////////////////////////////////////
/// The Class used to log an Error Message. The Message will be 
/// logged to the Error Message file whenever it is called.
///////////////////////////////////////////////////////////////////

using System;
using System.Configuration;
using System.IO;

namespace Pangea.Utilities.Tools.Logging
{
    public class LogError : LogBasic
    {
        private static bool _testVer
        {
            get
            {
#if TESTVER || DEBUG
                return true;
#else
                return false;
#endif
            }
        }

        // <summary>
        /// Path to the Error Log
        /// </summary>
        private readonly String _ErrorLogPath = String.Format
            (
            "{0}\\{1}\\Logs\\ErrorLogs", 
            Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), 
            
            _testVer 
            
            ? 
            Centraler.Database_Settingz.DB_Staging : 
            Centraler.Database_Settingz.DB_Production
            );

        public LogError()
        {
            int daysToKeepErrorLogFiles = Convert.ToInt32(ConfigurationManager.AppSettings["DaysToKeepErrorLogs"]);

            if (Directory.Exists(_ErrorLogPath) && daysToKeepErrorLogFiles > 0)
            {
                string[] files = Directory.GetFiles(_ErrorLogPath, "*_error.txt");
                foreach (String file in files)
                {
                    DateTime fCreated = File.GetCreationTimeUtc(file);
                    if ((DateTime.UtcNow - fCreated).Days > daysToKeepErrorLogFiles)
                        File.Delete(file);
                }
            }

            if (!Directory.Exists(_ErrorLogPath))
                Directory.CreateDirectory(_ErrorLogPath);

            FileName = String.Format("{0}\\{1}{2}{3}_error.txt", _ErrorLogPath, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Year);
        }

        ~LogError()
        {
            if (LogStream != null)
                LogStream.Close();
        }

        /// <summary>
        /// This will write out the Date and Time the Error was logged, the 
        /// Method that captured the Error, and the exception message.
        /// </summary>
        /// <param name="method">The Method that called the LogError</param>
        /// <param name="ex">The Exception that was captured</param>
        public void WriteError(String method, Exception ex)
        {
            WriteLine(String.Format("{0}  -  {1}", method, ex.Message));
        }
    }
}
