﻿using System;

namespace Pangea.Utilities.Tools
{
    public class ImageMethods
    {
        /// <summary>
        /// Retrieves an Embedded Source Image.
        /// </summary>
        /// <param name="psAssemblyName">PangeaFieldApplication</param>
        /// <param name="psResourceName">Name of the Resouce we want to retreive.</param>
        /// <returns></returns>
        public static System.Windows.Media.ImageSource DoGetImageSourceFromResource(string psAssemblyName, string psResourceName)
        {
            LogManager.DebugLogManager.MethodBegin("DefaultImages.DoGetImageSourceFromResource");

            Uri oUri = new Uri("pack://application:,,,/" + psAssemblyName + ";component/" + psResourceName, UriKind.RelativeOrAbsolute);

            LogManager.DebugLogManager.MethodEnd("DefaultImages.DoGetImageSourceFromResource");

            if (oUri != null)
                return System.Windows.Media.Imaging.BitmapFrame.Create(oUri);

            return null;
        }
    }
}
